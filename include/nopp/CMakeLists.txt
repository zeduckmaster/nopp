cmake_minimum_required(VERSION 3.22)

project("nopp" LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

include("${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/CPM.cmake")

if(NOT MINGW)
    set(CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH}
        "${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/"
    )
endif()

option(BUILD_WITH_LUA "Build with lua support")
option(BUILD_WITH_JSON "Build with json support")
option(BUILD_WITH_SDL "Build with sdl support")

# files
set(SRC_FILES
    "${CMAKE_CURRENT_SOURCE_DIR}/any.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/base64.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/bound.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/camera.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/container.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/dice.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/enum.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/evaluate.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/event.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/fast_rtti.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/float16.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/frustum.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/frustum_string.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/gpu.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/gpu_gles2.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/hash_string.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/id.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/io.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/log.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/math.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/math_string.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/mesh.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/mesh_obj.hpp"    
    "${CMAKE_CURRENT_SOURCE_DIR}/mesh_primitives.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/packed_normal.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/quantity.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/ref_object.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/size.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/string.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/support_sdl.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/task.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/texture.hpp"
    #"${CMAKE_CURRENT_SOURCE_DIR}/texture_data_resize.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/time.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/transform.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/unused.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/viewport.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/viewport_string.hpp"
)
set(INC_DIRS
    "${CMAKE_CURRENT_SOURCE_DIR}/.."
    "${CMAKE_CURRENT_SOURCE_DIR}/../../external/eigen"
)
set(DEFS "")
set(LIBS "")

if(BUILD_WITH_LUA)
    # add lua
    add_subdirectory(
        "${CMAKE_CURRENT_SOURCE_DIR}/../../external/lua"
        "${CMAKE_CURRENT_BINARY_DIR}/lua"
    )

    # add sol2
    CPMAddPackage(
        NAME sol2
        SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../external/sol2"
        OPTIONS
        "SOL2_BUILD_LUA OFF"
        "SOL2_SINGLE ON"
    )

    # add lib_core lua related files
    # list(APPEND SRC_FILES
    # "lua/support_lua.hpp"
    # "lua/support_lua.cpp"
    # "lua/support_lua_math.cpp"
    # )
    # if(MINGW)
    # set_source_files_properties("lua/support_lua_math.cpp"
    # PROPERTIES COMPILE_FLAGS "-Wa,-mbig-obj"
    # )
    # endif()
    list(APPEND DEFS "-DWITH_LUA")
    list(APPEND LIBS "sol2" "lua")
endif()

if(BUILD_WITH_JSON)
    CPMAddPackage(
        NAME nlohmann_json
        SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../external/json"
        OPTIONS "JSON_BuildTests OFF"
    )
    list(APPEND LIBS "nlohmann_json")

    # list(APPEND SRC_FILE
    # "json/support_json.hpp"
    # "json/support_json.cpp"
    # )
    list(APPEND DEFS "-DWITH_JSON")
endif()

if(BUILD_WITH_SDL)
    # list(APPEND SRC_FILES
    # "sdl/support_sdl.hpp"
    # "sdl/support_sdl.cpp"
    # )

    CPMAddPackage(
        NAME SDL
        SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../external/SDL"
        OPTIONS
        "SDL_SHARED OFF"
        "SDL_TEST OFF"
        "SDL2_DISABLE_INSTALL ON"
        "SDL2_DISABLE_SDL2MAIN ON"
        "SDL2_DISABLE_UNINSTALL ON"
        "SDL_DIRECTX OFF"
        "SDL_RENDER_D3D OFF"
        "SDL_VULKAN OFF"
    )
    list(APPEND LIBS "SDL2-static")

    CPMAddPackage(
        NAME SDL_image
        SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../external/SDL_image"
        OPTIONS
        "BUILD_SHARED_LIBS OFF"
        "SDL2IMAGE_BUILD_SHARED_LIBS OFF"
        "SDL2IMAGE_INSTALL OFF"
        "SDL2IMAGE_DEPS_SHARED OFF"
        "SDL2IMAGE_VENDORED ON"
        "SDL2IMAGE_SAMPLES OFF"
        "SDL2IMAGE_BACKEND_STB OFF"
        "SDL2IMAGE_BACKEND_WIC OFF"
        "SDL2IMAGE_BACKEND_IMAGEIO OFF"
        "SDL2IMAGE_AVIF OFF"
        "SDL2IMAGE_BMP OFF"
        "SDL2IMAGE_BMP OFF"
        "SDL2IMAGE_GIF OFF"
        "SDL2IMAGE_JPG ON"
        "SDL2IMAGE_JXL OFF"
        "SDL2IMAGE_LBM OFF"
        "SDL2IMAGE_PCX OFF"
        "SDL2IMAGE_PNG ON"
        "SDL2IMAGE_PNM OFF"
        "SDL2IMAGE_QOI OFF"
        "SDL2IMAGE_SVG OFF"
        "SDL2IMAGE_TGA OFF"
        "SDL2IMAGE_TIF OFF"
        "SDL2IMAGE_WEBP OFF"
        "SDL2IMAGE_XCF OFF"
        "SDL2IMAGE_XPM OFF"
        "SDL2IMAGE_XV OFF"
        # png & zlib related
        "PNG_SHARED OFF"
        "PNG_TESTS OFF"
        "SKIP_INSTALL_ALL ON"
    )
    list(APPEND LIBS "SDL2_image")

    list(APPEND DEFS "-DWITH_SDL")

endif()

# project
add_library(${PROJECT_NAME} INTERFACE)
target_sources(${PROJECT_NAME} INTERFACE ${SRC_FILES})
target_link_libraries(${PROJECT_NAME} INTERFACE ${LIBS})
target_include_directories(${PROJECT_NAME} INTERFACE ${INC_DIRS})
target_compile_definitions(${PROJECT_NAME} INTERFACE ${DEFS})
