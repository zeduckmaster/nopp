#include "camera_viewer.hpp"
#include "defaults.hpp"
#include "frame_data.hpp"

using namespace nopp;

////////////////////////////////////////////////////////////////////////////////
// globals

FrameData frameData0;
FrameData frameData1;
FrameData* appFrame = &frameData0;
FrameData* renderFrame = &frameData1;

Path dataPath;
PrecisionTimer timer;

void freezeFrame()
{
    auto curTime = timer.timeMilliSec();
    appFrame->timeDelta = curTime - appFrame->time;
    appFrame->time = curTime;
    std::swap(appFrame, renderFrame);
}

void prepareNextFrame() { appFrame->prepareNext(*renderFrame); }

struct Rendering
{
    gles2::GLCmd glInitialize = glInitializeDefault;
    gles2::GLCmd glFrame = glFrameDefault;
    gles2::GLCmd glTerminate = glTerminateDefault;
    gles2::GLCmd glImGuiDraw = glImGuiDrawDefault;
    // used for hot reload
    gles2::GLCmd glTerminatePrev;
};
Rendering rendering;

struct World
{
    using Callback = std::function<void()>;
    Callback initialize = worldInitializeDefault;
    Callback update = worldUpdateDefault;
    Callback terminate = worldTerminateDefault;
    // used for hot reload
    Callback terminatePrev;
};
World world;

struct EventHandler
{
    using Callback = std::function<void()>;
    Callback mousePress = eventMousePressDefault;
    Callback mouseRelease = eventMouseReleaseDefault;
    Callback mouseMove = eventMouseMoveDefault;
    Callback keyPress = eventKeyPressDefault;
    Callback keyRelease = eventKeyReleaseDefault;
    Callback windowResize = eventWindowResize;

    void handle(SDL_Event const& sdlEvent)
    {
        switch (sdlEvent.type)
        {
        case SDL_MOUSEBUTTONDOWN: mousePress(); break;
        case SDL_MOUSEBUTTONUP: mouseRelease(); break;
        case SDL_MOUSEMOTION: mouseMove(); break;
        case SDL_KEYDOWN: keyPress(); break;
        case SDL_KEYUP: keyRelease(); break;
        case SDL_WINDOWEVENT:
        {
            switch (sdlEvent.window.event)
            {
            case SDL_WINDOWEVENT_RESIZED: windowResize(); break;
            default: break;
            }
        }
        default: break;
        }
    }
};
EventHandler eventHandler;

////////////////////////////////////////////////////////////////////////////////
// gl functions

void glInitialize(gles2::GL const& gl)
{
    if (rendering.glTerminatePrev != nullptr)
    {
        rendering.glTerminatePrev(gl);
        rendering.glTerminatePrev = nullptr;
    }
    rendering.glInitialize(gl);
}

void glFrame(gles2::GL const& gl) { rendering.glFrame(gl); }

void glTerminate(gles2::GL const& gl) { rendering.glTerminate(gl); }

void setImGuiStyle()
{
    auto& style = ImGui::GetStyle();
    style.WindowRounding = 0.0f;
    style.WindowBorderSize = 0.0f;
    style.ChildRounding = 0.0f;
    style.ChildBorderSize = 0.0f;
    style.PopupRounding = 0.0f;
    style.PopupBorderSize = 0.0f;
    style.FrameRounding = 0.0f;
    style.FrameBorderSize = 0.0f;

    style.AntiAliasedLines = false;
    style.AntiAliasedFill = false;
}

void glImGuiDraw(gles2::GL const& gl)
{
    auto& io = ImGui::GetIO();

    static auto isImGuiInitialized = false;
    if (isImGuiInitialized == false)
    {
        gles2::glImGuiInit(gl);
        setImGuiStyle();
        io.KeyMap[ImGuiKey_Tab] = SDL_SCANCODE_TAB;
        io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
        io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
        io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
        io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
        io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
        io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
        io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
        io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
        io.KeyMap[ImGuiKey_Insert] = SDL_SCANCODE_INSERT;
        io.KeyMap[ImGuiKey_Delete] = SDL_SCANCODE_DELETE;
        io.KeyMap[ImGuiKey_Backspace] = SDL_SCANCODE_BACKSPACE;
        io.KeyMap[ImGuiKey_Space] = SDL_SCANCODE_SPACE;
        io.KeyMap[ImGuiKey_Enter] = SDL_SCANCODE_RETURN;
        io.KeyMap[ImGuiKey_Escape] = SDL_SCANCODE_ESCAPE;
        io.KeyMap[ImGuiKey_KeyPadEnter] = SDL_SCANCODE_KP_ENTER;
        // io.MouseDrawCursor = true;
        io.IniFilename = nullptr;
        isImGuiInitialized = true;
    }

    // update io
    renderFrame->copyTo(io);

    // draw
    ImGui::NewFrame();
    rendering.glImGuiDraw(gl);
    ImGui::Render();
    gles2::glImGuiRenderFrame(gl);
}

////////////////////////////////////////////////////////////////////////////////
// lua functions

void luaSetRendering(sol::function const& glInitialize,
                     sol::function const& glFrame,
                     sol::function const& glTerminate)
{
    rendering.glInitialize = glInitialize;
    rendering.glFrame = glFrame;
    rendering.glTerminatePrev = rendering.glTerminate;
    rendering.glTerminate = glTerminate;
}

void luaSetImGuiDraw(sol::function const& glImGuiDraw)
{
    rendering.glImGuiDraw = glImGuiDraw;
}

void luaSetWorld(sol::function const& initialize,
                 sol::function const& update,
                 sol::function const& terminate)
{
    world.initialize = initialize;
    world.update = update;
    world.terminatePrev = world.terminate;
    world.terminate = terminate;
}

void luaSetMousePress(sol::function const& callback)
{
    eventHandler.mousePress = callback;
}

void luaSetMouseRelease(sol::function const& callback)
{
    eventHandler.mouseRelease = callback;
}

void luaSetMouseMove(sol::function const& callback)
{
    eventHandler.mouseMove = callback;
}

void luaSetKeyPress(sol::function const& callback)
{
    eventHandler.keyPress = callback;
}

void luaSetKeyRelease(sol::function const& callback)
{
    eventHandler.keyRelease = callback;
}

void luaSetWindowResize(sol::function const& callback)
{
    eventHandler.windowResize = callback;
}

void loadLuaCommon(sol::state& luaState)
{
    luaState.open_libraries(
        sol::lib::base, sol::lib::string, sol::lib::table, sol::lib::math);
    loadLuaCore(luaState);
    loadLuaDisplay(luaState);
    loadLuaFrameData(luaState);
    loadLuaCamera(luaState);
    luaState["print"] = nopp::luaPrint;
    luaState["dataPath"] = [&]() { return dataPath.string(); };
}

void loadLuaApp(sol::state& luaState)
{
    loadLuaCommon(luaState);
    luaState["frameData"] = [&]() { return appFrame; };
    luaState["setWorld"] = luaSetWorld;
    luaState["setMousePress"] = luaSetMousePress;
    luaState["setMouseRelease"] = luaSetMouseRelease;
    luaState["setMouseMove"] = luaSetMouseMove;
    luaState["setKeyPress"] = luaSetKeyPress;
    luaState["setKeyRelease"] = luaSetKeyRelease;
    luaState["setWindowResize"] = luaSetWindowResize;
}

void loadLuaRender(sol::state& luaState)
{
    loadLuaCommon(luaState);
    loadLuaDisplayImGui(luaState);
    gles2::loadLuaDisplay(luaState);
    luaState["setRendering"] = luaSetRendering;
    luaState["setImGuiDraw"] = luaSetImGuiDraw;
    luaState["frameData"] = [&]() { return renderFrame; };
}

////////////////////////////////////////////////////////////////////////////////

extern "C" int main(int argc, char* argv[])
{
    // init sdl
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        SDL_Log("Failed to initialize SDL (\"%s\").\n", SDL_GetError());
        return -1;
    }
    auto window = SDL_CreateWindow("app_gl",
                                   SDL_WINDOWPOS_CENTERED,
                                   SDL_WINDOWPOS_CENTERED,
                                   640,
                                   480,
                                   SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    appFrame->windowSize = Vec2{640.0f, 480.0f};

    // init lua
    auto appLuaState = sol::state{};
    loadLuaApp(appLuaState);
    auto renderLuaState = sol::state{};
    loadLuaRender(renderLuaState);

    if (argc > 1)
        dataPath = argv[1];

    ImGui::CreateContext();
    auto glThread = new gles2::GLThread{};
    glThread->setOnTerminate({glTerminate});
    auto glMakeCurrent = [&](gles2::GL const& gl) {
        gl.wglMakeCurrent(window);
    };
    auto glSwapWindow = [&](gles2::GL const& gl) { gl.wglSwapWindow(window); };
    gles2::GLFence nextFrame;
    auto sdlQuit = false;
    auto reload = true;
    gles2::GLCmd glTerminatePrev;
    while (sdlQuit == false)
    {
        ////////////////////////////////////////////////////////////////////////
        // handle sdl events

        auto sdlEvent = SDL_Event{0};
        while (SDL_PollEvent(&sdlEvent))
        {
            sdlQuit = (sdlEvent.type == SDL_QUIT) ||
                      ((sdlEvent.type == SDL_KEYDOWN) &&
                       (sdlEvent.key.keysym.scancode == SDL_SCANCODE_ESCAPE));
            if (sdlQuit == true)
                continue;

            switch (sdlEvent.type)
            {
            case SDL_WINDOWEVENT:
            {
                auto& windowEvent = sdlEvent.window;
                switch (windowEvent.event)
                {
                case SDL_WINDOWEVENT_RESIZED:
                    appFrame->windowSize =
                        Vec2{static_cast<float>(windowEvent.data1),
                             static_cast<float>(windowEvent.data2)};
                    break;
                default: break;
                }
            }
            break;
            case SDL_MOUSEMOTION:
            {
                auto& motionEvent = sdlEvent.motion;
                appFrame->mousePos = Vec2{static_cast<float>(motionEvent.x),
                                          static_cast<float>(motionEvent.y)};
            }
            break;
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            {
                auto& buttonEvent = sdlEvent.button;
                auto isDown = (buttonEvent.state == SDL_PRESSED);
                if (buttonEvent.button == SDL_BUTTON_LEFT)
                    appFrame->mouseLeft = isDown;
                else if (buttonEvent.button == SDL_BUTTON_RIGHT)
                    appFrame->mouseRight = isDown;
            }
            break;
            case SDL_KEYDOWN:
            case SDL_KEYUP:
            {
                auto& keyEvent = sdlEvent.key;
                auto isDown = (keyEvent.state == SDL_PRESSED);
                appFrame->keysDown[keyEvent.keysym.scancode] = isDown;
                auto mod = keyEvent.keysym.mod;
                appFrame->keyShift = (mod & KMOD_LSHIFT) || (mod & KMOD_RSHIFT);
                appFrame->keyCtrl = (mod & KMOD_LCTRL) || (mod & KMOD_RCTRL);
                appFrame->keyAlt = (mod & KMOD_LALT) || (mod & KMOD_RALT);
                reload = (isDown == false) &&
                         (keyEvent.keysym.scancode == SDL_SCANCODE_F5) &&
                         (appFrame->keyCtrl == true);
            }
            break;
            case SDL_TEXTINPUT:
            {
                appFrame->textInput = sdlEvent.text.text;
            }
            break;
            default: break;
            }
            eventHandler.handle(sdlEvent);
        }

        ////////////////////////////////////////////////////////////////////////
        // update world

        if (reload == true)
        {
            timer.start();
            if (dataPath.isEmpty() == false)
            {
                // load world
                runLuaScript(appLuaState,
                             readFileContentAsString(
                                 Path{dataPath}.append("world.lua")));
            }
        }

        if (world.terminatePrev != nullptr)
        {
            world.terminatePrev();
            world.terminatePrev = nullptr;
            world.initialize();
        }

        // update world
        world.update();

        if (sdlQuit == true)
            world.terminate();

        // sync frame between app and rendering
        nextFrame.wait();

        ////////////////////////////////////////////////////////////////////////
        // update rendering

        // prepare frame data
        freezeFrame();

        // prepare commands
        auto cmds = std::vector<gles2::GLCmd>{};
        cmds.push_back(glMakeCurrent);
        // if a new rendering needs to be loaded
        if (reload == true)
        {
            if (dataPath.isEmpty() == false)
            {
                // load rendering
                runLuaScript(renderLuaState,
                             readFileContentAsString(
                                 Path{dataPath}.append("rendering.lua")));
            }
            cmds.push_back(glInitialize);
        }
        append(cmds, {glFrame, glImGuiDraw, glSwapWindow});

        // send commands to renderings
        glThread->push(cmds, &nextFrame);

        // prepare next frame
        prepareNextFrame();

        reload = false;
    }

    delete glThread;
    eventHandler = EventHandler{};
    rendering = Rendering{};
    world = World{};
    ImGui::DestroyContext();
    SDL_DestroyWindow(window);
    return 0;
}
