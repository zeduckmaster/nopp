#pragma once

#include <cstdint>
#include <limits>
#include <string>

namespace nopp
{

// magic numbers from http://www.isthe.com/chongo/tech/comp/fnv/
using HashValue = uint32_t;
constexpr auto const HashOffset = HashValue(2166136261u);
constexpr auto const HashPrime = HashValue(16777619u);
constexpr auto const HashInvalid = HashValue(0xffffffff);

// Fowler / Noll / Vo (FNV) Hash
// computes a hash value from a given string.
// the hash generation is based on : http://www.isthe.com/chongo/tech/comp/fnv/.
// This function is intended to convert runtime or dynamic strings into hash
// values. if you want the hash value of static strings, please use
// replaceByHash instead for much more improved performance.
inline HashValue computeHash(std::string const& s)
{
    auto hash = HashValue(HashOffset);
    auto l = s.size();
    for (auto i = decltype(l)(0); i < l; ++i)
    {
        hash = hash ^ (s[i]);    // xor  the low 8 bits
        hash = hash * HashPrime; // multiply by the magic number
    }
    return hash;
}

// template<int N>
// constexpr HashValue replaceByHash(char const (&str)[N])
//{
//     auto hash = HashValue{HashOffset};
//     for(auto i = 0; i < N; ++i)
//     {
//         hash = hash ^ (str[i]); // xor  the low 8 bits
//         hash = hash * HashPrime; // multiply by the magic number
//     }
//     return hash;
// }

#define NOPP_HASH_STRING_1 (HashOffset)
#define NOPP_HASH_STRING_2 ((NOPP_HASH_STRING_1 ^ (str[0])) * HashPrime)
#define NOPP_HASH_STRING_3 ((NOPP_HASH_STRING_2 ^ (str[1])) * HashPrime)
#define NOPP_HASH_STRING_4 ((NOPP_HASH_STRING_3 ^ (str[2])) * HashPrime)
#define NOPP_HASH_STRING_5 ((NOPP_HASH_STRING_4 ^ (str[3])) * HashPrime)
#define NOPP_HASH_STRING_6 ((NOPP_HASH_STRING_5 ^ (str[4])) * HashPrime)
#define NOPP_HASH_STRING_7 ((NOPP_HASH_STRING_6 ^ (str[5])) * HashPrime)
#define NOPP_HASH_STRING_8 ((NOPP_HASH_STRING_7 ^ (str[6])) * HashPrime)
#define NOPP_HASH_STRING_9 ((NOPP_HASH_STRING_8 ^ (str[7])) * HashPrime)
#define NOPP_HASH_STRING_10 ((NOPP_HASH_STRING_9 ^ (str[8])) * HashPrime)
#define NOPP_HASH_STRING_11 ((NOPP_HASH_STRING_10 ^ (str[9])) * HashPrime)
#define NOPP_HASH_STRING_12 ((NOPP_HASH_STRING_11 ^ (str[10])) * HashPrime)
#define NOPP_HASH_STRING_13 ((NOPP_HASH_STRING_12 ^ (str[11])) * HashPrime)
#define NOPP_HASH_STRING_14 ((NOPP_HASH_STRING_13 ^ (str[12])) * HashPrime)
#define NOPP_HASH_STRING_15 ((NOPP_HASH_STRING_14 ^ (str[13])) * HashPrime)
#define NOPP_HASH_STRING_16 ((NOPP_HASH_STRING_15 ^ (str[14])) * HashPrime)
#define NOPP_HASH_STRING_17 ((NOPP_HASH_STRING_16 ^ (str[15])) * HashPrime)
#define NOPP_HASH_STRING_18 ((NOPP_HASH_STRING_17 ^ (str[16])) * HashPrime)
#define NOPP_HASH_STRING_19 ((NOPP_HASH_STRING_18 ^ (str[17])) * HashPrime)
#define NOPP_HASH_STRING_20 ((NOPP_HASH_STRING_19 ^ (str[18])) * HashPrime)
#define NOPP_HASH_STRING_21 ((NOPP_HASH_STRING_20 ^ (str[19])) * HashPrime)
#define NOPP_HASH_STRING_22 ((NOPP_HASH_STRING_21 ^ (str[20])) * HashPrime)
#define NOPP_HASH_STRING_23 ((NOPP_HASH_STRING_22 ^ (str[21])) * HashPrime)
#define NOPP_HASH_STRING_24 ((NOPP_HASH_STRING_23 ^ (str[22])) * HashPrime)
#define NOPP_HASH_STRING_25 ((NOPP_HASH_STRING_24 ^ (str[23])) * HashPrime)
#define NOPP_HASH_STRING_26 ((NOPP_HASH_STRING_25 ^ (str[24])) * HashPrime)
#define NOPP_HASH_STRING_27 ((NOPP_HASH_STRING_26 ^ (str[25])) * HashPrime)
#define NOPP_HASH_STRING_28 ((NOPP_HASH_STRING_27 ^ (str[26])) * HashPrime)
#define NOPP_HASH_STRING_29 ((NOPP_HASH_STRING_28 ^ (str[27])) * HashPrime)
#define NOPP_HASH_STRING_30 ((NOPP_HASH_STRING_29 ^ (str[28])) * HashPrime)
#define NOPP_HASH_STRING_31 ((NOPP_HASH_STRING_30 ^ (str[29])) * HashPrime)
#define NOPP_HASH_STRING_32 ((NOPP_HASH_STRING_31 ^ (str[30])) * HashPrime)
#define NOPP_HASH_STRING_33 ((NOPP_HASH_STRING_32 ^ (str[31])) * HashPrime)

#define NOPP_HASH_STRING_SPEC(n)                                  \
    inline constexpr HashValue replaceByHash(char const(&str)[n]) \
    {                                                             \
        (void)str;                                                \
        return NOPP_HASH_STRING_##n;                              \
    }

NOPP_HASH_STRING_SPEC(1)
NOPP_HASH_STRING_SPEC(2)
NOPP_HASH_STRING_SPEC(3)
NOPP_HASH_STRING_SPEC(4)
NOPP_HASH_STRING_SPEC(5)
NOPP_HASH_STRING_SPEC(6)
NOPP_HASH_STRING_SPEC(7)
NOPP_HASH_STRING_SPEC(8)
NOPP_HASH_STRING_SPEC(9)
NOPP_HASH_STRING_SPEC(10)
NOPP_HASH_STRING_SPEC(11)
NOPP_HASH_STRING_SPEC(12)
NOPP_HASH_STRING_SPEC(13)
NOPP_HASH_STRING_SPEC(14)
NOPP_HASH_STRING_SPEC(15)
NOPP_HASH_STRING_SPEC(16)
NOPP_HASH_STRING_SPEC(17)
NOPP_HASH_STRING_SPEC(18)
NOPP_HASH_STRING_SPEC(19)
NOPP_HASH_STRING_SPEC(20)
NOPP_HASH_STRING_SPEC(21)
NOPP_HASH_STRING_SPEC(22)
NOPP_HASH_STRING_SPEC(23)
NOPP_HASH_STRING_SPEC(24)
NOPP_HASH_STRING_SPEC(25)
NOPP_HASH_STRING_SPEC(26)
NOPP_HASH_STRING_SPEC(27)
NOPP_HASH_STRING_SPEC(28)
NOPP_HASH_STRING_SPEC(29)
NOPP_HASH_STRING_SPEC(30)
NOPP_HASH_STRING_SPEC(31)
NOPP_HASH_STRING_SPEC(32)
NOPP_HASH_STRING_SPEC(33)

} // namespace nopp
