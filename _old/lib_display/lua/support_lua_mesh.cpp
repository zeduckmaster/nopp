#include "../mesh.hpp"
#include "support_lua.hpp"

using namespace nopp;

namespace sol
{
template <typename T> struct unique_usertype_traits<nopp::Ptr<T>>
{
    typedef T type;
    typedef nopp::Ptr<T> actual_type;
    static bool const value = true;

    static bool is_null(actual_type const& ptr) { return ptr == nullptr; }

    static type* get(actual_type const& ptr) { return ptr.get(); }
};
} // namespace sol

namespace nopp
{
void loadLuaDisplayMesh(sol::state& luaState)
{
    // eVertexDeclaration
    {
        auto t = luaState.new_enum("eVertexDeclaration",
                                   "Position",
                                   eVertexDeclaration::Position,
                                   "Normal",
                                   eVertexDeclaration::Normal,
                                   "Tangent",
                                   eVertexDeclaration::Tangent,
                                   "Color",
                                   eVertexDeclaration::Color,
                                   "TexCoord0",
                                   eVertexDeclaration::TexCoord0,
                                   "TexCoord1",
                                   eVertexDeclaration::TexCoord1,
                                   "Custom0",
                                   eVertexDeclaration::Custom0,
                                   "All",
                                   eVertexDeclaration::All);
    }

    // eMeshTopology
    {
        auto t = luaState.new_enum("eMeshTopology",
                                   "Points",
                                   eMeshTopology::Points,
                                   "Lines",
                                   eMeshTopology::Lines,
                                   "Triangles",
                                   eMeshTopology::Triangles);
    }

    // MeshDesc
    {
        auto t = luaState.new_usertype<MeshDesc>(
            "MeshDesc", sol::call_constructor, sol::constructors<MeshDesc()>());
        t["vertexDeclaration"] = &MeshDesc::vertexDeclaration;
        t["vertexCount"] = &MeshDesc::vertexCount;
        t["indexCount"] = &MeshDesc::indexCount;
        t["primitiveType"] = &MeshDesc::primitiveType;
        t["primitiveCount"] = &MeshDesc::primitiveCount;
        t[sol::meta_function::equal_to] = &MeshDesc::operator==;
    }

    // PackedNormal
    {
        luaState.new_usertype<PackedNormal>(
            "PackedNormal",
            sol::call_constructor,
            sol::constructors<PackedNormal()>());
    }

    // MeshData
    {
        auto t =
            luaState.new_usertype<MeshData>("MeshData_", sol::no_constructor);
        luaState["MeshData"] = sol::overload(
            []() -> nopp::Ptr<MeshData> { return makePtr<MeshData>(); },
            [](MeshDesc const& desc) -> Ptr<MeshData> {
                return makePtr<MeshData>(desc);
            },
            [](char const* filepath) -> Ptr<MeshData> {
                return makePtr<MeshData>(Path{filepath});
            });
        t["id"] = sol::property(&MeshData::id);
        t["commit"] = &MeshData::commit;
        t["create"] = sol::overload(
            [](MeshData& value, MeshDesc const& desc) { value.create(desc); },
            [](MeshData& value, char const* path) {
                value.create(Path{path});
            });
        t["computeTangents"] = &MeshData::computeTangents;
        t["computeWireframe"] = &MeshData::computeWireframe;
        t["positions"] = &MeshData::positions;
        t["colors"] = &MeshData::colors;
        t["customs"] = &MeshData::customs;
        t["indexes"] = &MeshData::indexes;
        t["wireframeIndexes"] = &MeshData::wireframeIndexes;
        t["desc"] = &MeshData::desc;

        sol::table tt = luaState["MeshData_"];
        tt.new_usertype<MeshData::VtxData>(
            "VtxData",
            sol::call_constructor,
            sol::constructors<MeshData::VtxData()>());
        tt["texCoord0"] = &MeshData::VtxData::texCoord0;
        tt["texCoord1"] = &MeshData::VtxData::texCoord1;
        tt["normal"] = &MeshData::VtxData::normal;
        tt["tangent"] = &MeshData::VtxData::tangent;
        t["datas"] = &MeshData::datas;
    }

    // MeshGridDesc
    {
        auto t = luaState.new_usertype<MeshGridDesc>(
            "MeshGridDesc",
            sol::call_constructor,
            sol::constructors<MeshGridDesc(), MeshGridDesc(int32_t, float)>());
        t["lineCount"] = &MeshGridDesc::lineCount;
        t["scale"] = &MeshGridDesc::scale;
        t["colorLine"] = &MeshGridDesc::colorLine;
        t["colorAxisX"] = &MeshGridDesc::colorAxisX;
        t["colorAxisY"] = &MeshGridDesc::colorAxisY;
        t["colorAxisZ"] = &MeshGridDesc::colorAxisZ;
        t["create"] = &MeshGridDesc::create;
    }

    // MeshCubeDesc
    {
        auto t = luaState.new_usertype<MeshCubeDesc>(
            "MeshCubeDesc",
            sol::call_constructor,
            sol::constructors<MeshCubeDesc(),
                              MeshCubeDesc(Vec3 const&, float)>());
        t["center"] = &MeshCubeDesc::center;
        t["size"] = &MeshCubeDesc::size;
        t["color"] = &MeshCubeDesc::color;
        t["vertexDeclaration"] = &MeshCubeDesc::vertexDeclaration;
        t["create"] = &MeshCubeDesc::create;
    }

    // MeshQuadDesc
    {
        auto t = luaState.new_usertype<MeshQuadDesc>(
            "MeshQuadDesc",
            sol::call_constructor,
            sol::constructors<MeshQuadDesc(),
                              MeshQuadDesc(Vec3 const&, float, float)>());
        t["center"] = &MeshQuadDesc::center;
        t["width"] = &MeshQuadDesc::width;
        t["height"] = &MeshQuadDesc::height;
        t["color"] = &MeshQuadDesc::color;
        t["vertexDeclaration"] = &MeshQuadDesc::vertexDeclaration;
        t["isTwoSided"] = &MeshQuadDesc::isTwoSided;
        t["create"] = &MeshQuadDesc::create;
    }
}
} // namespace nopp
