#include "mud.hpp"
#include "dice.hpp"
#include "helpers.hpp"
#include "log.hpp"
#include <chrono>

using namespace nopp;
using namespace std::chrono_literals;

World::World(Path const& dataDirpath, std::string const& lang)
    : _dataDirpath{dataDirpath}
{
    // init
    _luaState.open_libraries(sol::lib::base,sol::lib::string,sol::lib::table,sol::lib::math);
    _luaState["world"] = this;
    _luaState["lang"] = lang;
    _luaState["dataDirpath"] = _dataDirpath.string();
    _luaState["print"] = luaPrint;
    _luaState["roll"] = [&](uint32_t sessionId, std::string const& expr) -> int32_t
    {
        return roll(expr);
    };
    _luaState["send"] = [&](uint32_t sessionId, std::string const& msg)
    {
        send(Id{sessionId}, msg);
    };
    _luaState["quit"] = [&](uint32_t sessionId)
    {
        quit(Id{sessionId});
    };
    _luaState["readFile"] = [&](std::string const& filepath)
    {
        return readFileContentAsString(filepath);
    };
    _sessions.reserve(1024);
    _clock.start();

    // boot
    auto bootFilepath = Path{_dataDirpath}.append("boot.lua");
    auto script = readFileContentAsString(bootFilepath);
    runLuaScript(_luaState, script);
}

Id World::addSession(SessionIO const& sessionIO)
{
    auto index = 0;
    if(_sessionFreeIds.size() >= 1024)
    {
        index = _sessionFreeIds.front();
        _sessionFreeIds.pop_front();
    }
    else
    {
        _sessions.emplace_back(_Session{});
        index = _sessions.size() - 1;
    }
    auto& s = _sessions[index];
    s.io = sessionIO;
    auto sessionId = Id::make(index, s.gen);
    sol::function funcNewSession = _luaState["newSession"];
    if(funcNewSession.valid() == true)
    {
        auto ret = funcNewSession(sessionId.value());
        if(ret.valid() == true)
            s.luaTable = ret;
        else
        {
            sol::error err = ret;
            logError("[LUA]%s", err.what());
        }
    }
    return sessionId;
}

void World::removeSession(Id sessionId)
{
    auto index = sessionId.index();
    auto& s = _sessions[index];
    s.io = SessionIO{};
    s.lastInput = _SessionInput{};
    s.luaTable = sol::table{};
    ++s.gen;
    _sessionFreeIds.push_back(index);
}

void World::processCommand(Id sessionId, std::string const& msg)
{
    if(_isSessionIdValid(sessionId) == false)
    {
        logError("invalid session id: %d", sessionId);
        return;
    }
    auto& s = _sessions[sessionId.index()];
    auto now = _clock.time();
    if((now - s.lastInput.time) >= 1s)
    {
        s.lastInput.time = now;
        s.lastInput.input = msg;
        sol::function f = _luaState["processCommand"];
        auto ret = f(s.luaTable, msg);
        if(ret.valid() == false)
        {
            sol::error err = ret;
            logError("[LUA]%s", err.what());
        }
    }
}

void World::send(Id sessionId, std::string const& msg)
{
    if(_isSessionIdValid(sessionId) == false)
    {
        logError("invalid session id: %d", sessionId);
        return;
    }
    auto& s = _sessions[sessionId.index()];
    s.io.output(msg);
}

void World::quit(Id sessionId)
{
    if(_isSessionIdValid(sessionId) == false)
    {
        logError("invalid session id: %d", sessionId);
        return;
    }
    auto& s = _sessions[sessionId.index()];
    s.io.quit();
}

bool World::_isSessionIdValid(Id sessionId) const
{
    auto index = sessionId.index();
    return (index < static_cast<int32_t>(_sessions.size()))
        && (sessionId.gen() == _sessions[index].gen)
        && (_sessions[index].io.isValid() == true);
}

