local api = {
  ------------------------------------------------------------------------------
  -- core
  Id = {
    type = "class",
  },
  Vec2 = {
    type = "class",
    childs = {
      x = { type = "value" },
      y = { type = "value" },
      norm = {
        type = "method",
        args = "()",
        returns = "(number)"
      },
      normalize = {
        type = "method",
        args = "()",
        returns = ""
      },
    }
  },
  Vec3 = {
    type = "class",
    childs = {
      x = { type = "value" },
      y = { type = "value" },
      z = { type = "value" },
      norm = {
        type = "method",
        args = "()",
        returns = "(number)"
      },
      normalize = {
        type = "method",
        args = "()",
        returns = ""
      },
      dot = {
        type = "method",
        args = "()",
        returns = "(number)",
      },
      cross = {
        type = "method",
        args = "()",
        returns = "(Vec3)",
      }
    }
  },
  Vec4 = {
    type = "class",
    childs = {
      x = { type = "value" },
      y = { type = "value" },
      z = { type = "value" },
      w = { type = "value" },      
      norm = {
        type = "method",
        args = "()",
        returns = "(number)"
      },
      normalize = {
        type = "method",
        args = "()",
        returns = ""
      }
    }
  },
  Mat3 = {
    type = "class",
  },
  Mat4 = {
    type = "class",
  },
  Log2 = { type = "value" },
  Pi = { type = "value" },
  HalfPi = { type = "value" },
  TwoPi = { type = "value" },
  InvPi = { type = "value" },
  ToRadian = { type = "value" },
  ToDegree = { type = "value" },
  Epsilon = { type = "value" },
  lerp = {
    type = "function",
    args = "(a, b, w: number)",
    returns = "(number|Vec2|Vec3|Vec4)"
  },
  clamp = {
    type = "function",
    args = "(value: number)",
    returns = "(number)"
  },
  Transform = {
    type = "class",
    childs = {
      translation = { type = "value", valuetype = "Vec3" },
      rotation = { type = "value", valuetype = "Mat3" },
      scale = { type = "value", valuetype = "Vec3" },
      matrix = {
        type = "method",
        args = "()",
        returns = "(Mat4)"
      }
    }
  },
  Frustum = {
    type = "class",
    childs = {
      left = { type = "value" },
      right = { type = "value" },
      top = { type = "value" },
      bottom = { type = "value" },
      znear = { type = "value" },
      zfar = { type = "value" },
    }
  },
  Viewport = {
    type = "class",
    childs = {
      x = { type = "value" },
      y = { type = "value" },
      width = { type = "value" },
      height = { type = "value" },
      minDepth = { type = "value" },
      maxDepth = { type = "value" },
    }
  },
  computeMatView = {
    type = "function",
    args = "(matrix: Mat4)",
    returns = "(Mat4)"
  },
  computeViewLookDir = {
    type = "function",
    args = "(pos: Vec3, dir: Vec3, up: Vec3)",
    returns = "(Transform)"
  },
  computeViewLookAt = {
    type = "function",
    args = "(pos: Vec3, at: Vec3, up: Vec3)",
    returns = "(Transform)"
  },
  computeMatProj = {
    type = "function",
    args = "(frustum: Frustum)",
    returns = "(Mat4)"
  },
  computeMatProjAlt = {
    type = "function",
    args = "(frustum: Frustum)",
    returns = "(Mat4)"
  },
  computeProjPerspFOVH = {
    type = "function",
    args = "(fovhRad: number, aspectRatio: number, znear: number, zfar: number)",
    returns = "(Frustum)"
  },
  computeProjPerspFOVV = {
    type = "function",
    args = "(fovvRad: number, aspectRatio: number, znear: number, zfar: number)",
    returns = "(Frustum)"
  },
  computeWorldToViewport = {
    type = "function",
    args = "(worldPoint: Vec3, matView: Mat4, matProj: Mat4, viewport: Viewport)",
    returns = "(Vec2)"
  },
  cameraWorldRight = {
    type = "function",
    args = "(matView: Mat4)",
    returns = "(Vec3)"
  },
  cameraWorldUp = {
    type = "function",
    args = "(matView: Mat4)",
    returns = "(Vec3)"
  },
  cameraWorldDir = {
    type = "function",
    args = "(matView: Mat4)",
    returns = "(Vec3)"
  },
  Ray = {
    type = "class",
    childs = {
      start = { type = "value", valuetype = "Vec3" },
      --"end" = { type = "value", valuetype = "Vec3" }
    }
  },
  computeViewportToRay = {
    type = "function",
    args = "(viewportPoint: Vec2, matView: Mat4, matProj: Mat4, viewport: Viewport)",
    returns = "(Ray)"
  },
  ------------------------------------------------------------------------------
  -- display
  CameraEditor = {
    type = "class",
    childs = {
      fitView = {
        type = "method",
        args = "(min: Vec3, max: Vec3)",
        returns = ""
      }
    }
  },
  eVertexDeclaration = {
    type = "lib",
    childs = {
      Position = { type = "value" },
      Normal = { type = "value" },
      Tangent = { type = "value" },
      Color = { type = "value" },
      TexCoord0 = { type = "value" },
      TexCoord1 = { type = "value" },
      Custom0 = { type = "value" },
      All = { type = "value" }
    }
  },
  eMeshTopology = {
    type = "lib",
    childs = {
      Points = { type = "value" },
      Lines = { type = "value" },
      Triangles = { type = "value" },
      NbMeshTopology = { type = "value" }
    }
  },
  MeshDesc = {
    type = "class",
    childs = {
      vertexDeclaration = { type = "value" },
      vertexCount = { type = "value" },
      indexCount = { type = "value" },
      primitiveType = { type = "value", valuetype = "eMeshTopology" },
      primitiveCount = { type = "value" }
    }
  },
  PackedNormal = {
    type = "class"
  },
  MeshData = {
    type = "class",
    childs = {
      id = { type = "value", valuetype = "Id" },
      desc = { type = "value", valuetype = "MeshDesc" },
      positions = { type = "value" },
      colors = { type = "value" },
      customs = { type = "value" },
      indexes = { type = "value" },
      wireframeIndexes = { type = "value" },
      datas = { type = "value" },
      commit = {
        type = "method",
        args = "()",
        returns = ""
      },
      create = {
        type = "method",
        args = "(meshDesc: MeshDesc)",
        returns = ""
      }
    }
  },
  MeshGridDesc = {
    type = "class",
    childs = {
      lineCount = { type = "value" },
      scale = { type = "value" },
      colorLine = { type = "value", valuetype = "Vec4" },
      colorAxisX = { type = "value", valuetype = "Vec4" },
      colorAxisY = { type = "value", valuetype = "Vec4" },
      colorAxisZ = { type = "value", valuetype = "Vec4" },
      create = {
        type = "method",
        args = "(meshData: MeshData)",
        returns = ""
      }
    }
  },
  MeshCubeDesc = {
    type = "class",
    childs = {
      center = { type = "value", valuetype = "Vec3" },
      size = { type = "value" },
      color = { type = "value", valuetype = "Vec4" },
      vertexDeclaration = { type = "value" },
      create = {
        type = "method",
        args = "(meshData: MeshData)",
        returns = ""
      }
    }
  },
  MeshQuadDesc = {
    type = "class",
    childs = {
      center = { type = "value", valuetype = "Vec3" },
      width = { type = "value" },
      height = { type = "value" },
      color = { type = "value", valuetype = "Vec4" },
      vertexDeclaration = { type = "value" },
      isTwoSided = { type = "value" },
      create = {
        type = "method",
        args = "(meshData: MeshData)",
        returns = ""
      }
    }
  },
  Size3i = {
    type = "class",
    childs = {
      width = { type = "value" },
      height = { type = "value" },
      depth = { type = "value" }
    }
  },
  ePixelFormat = {
    type = "lib",
    childs = {
      RGBA8_UNorm = { type = "value" },
      RGBx8_UNorm = { type = "value" },
      R8_UNorm = { type = "value" },
      RGBA16_Float = { type = "value" },
      RGBA32_Float = { type = "value" },
      RGB_ETC1 = { type = "value" },
      NbPixelFormat = { type = "value" }
    }
  },
  TextureDesc = {
    type = "class",
    childs = {
      size = { type = "value", valuetype = "Size3i" },
      format = { type = "value", valuetype = "ePixelFormat" },
      mipLevelCount = { type = "value" },
      arraySize = { type = "value" },
      isCubemap = { type = "value" }
    }
  },
  TextureData = {
    type = "class",
    childs = {
      id = { type = "value", valuetype = "Id" },
      commit = {
        type = "method",
        args = "()",
        returns = ""
      },
      create = {
        type = "method",
        args = "(textureDesc: TextureDesc|filepath: string)",
        returns = ""
      }
    }
  },
  TextureDefaultDesc = {
    type = "class",
    childs = {
      createColor = {
        type = "method",
        args = "(textureData: TextureData)",
        returns = ""
      },
      createSpecular = {
        type = "method",
        args = "(textureData: TextureData)",
        returns = ""
      },
      createNormal = {
        type = "method",
        args = "(textureData: TextureData)",
        returns = ""
      }
    }
  },
  ------------------------------------------------------------------------------
  -- gles2
  GLES2 = {
    type = "lib",
    childs = {
      GL_NONE = { type = "value" },
      GL_NO_ERROR = { type = "value" },
      GL_ZERO = { type = "value" },
      GL_FALSE = { type = "value" },
      GL_TRUE = { type = "value" },
      GL_ONE = { type = "value" },
      
      GL_POINTS = { type = "value" },
      GL_LINES = { type = "value" },
      GL_LINE_LOOP = { type = "value" },
      GL_LINE_STRIP = { type = "value" },
      GL_TRIANGLES = { type = "value" },
      GL_TRIANGLE_STRIP = { type = "value" },
      GL_TRIANGLE_FAN = { type = "value" },
      
      GL_NEVER = { type = "value" },
      GL_LESS = { type = "value" },
      GL_EQUAL = { type = "value" },
      GL_LEQUAL = { type = "value" },
      GL_GREATER = { type = "value" },
      GL_NOTEQUAL = { type = "value" },
      GL_GEQUAL = { type = "value" },
      GL_ALWAYS = { type = "value" },
      
      GL_SRC_COLOR = { type = "value" },
      GL_ONE_MINUS_SRC_COLOR = { type = "value" },
      GL_SRC_ALPHA = { type = "value" },
      GL_ONE_MINUS_SRC_ALPHA = { type = "value" },
      GL_DST_ALPHA = { type = "value" },
      GL_ONE_MINUS_DST_ALPHA = { type = "value" },
      GL_DST_COLOR = { type = "value" },
      GL_ONE_MINUS_DST_COLOR = { type = "value" },
      GL_SRC_ALPHA_SATURATE = { type = "value" },
      
      GL_FRONT = { type = "value" },
      GL_BACK = { type = "value" },
      
      GL_INVALID_ENUM = { type = "value" },
      GL_INVALID_VALUE = { type = "value" },
      GL_INVALID_OPERATION = { type = "value" },
      GL_OUT_OF_MEMORY = { type = "value" },
      
      GL_CW = { type = "value" },
      GL_CCW = { type = "value" },
      
      GL_CULL_FACE = { type = "value" },
      GL_DEPTH_TEST = { type = "value" },
      GL_STENCIL_TEST = { type = "value" },
      GL_VIEWPORT = { type = "value" },
      GL_BLEND = { type = "value" },
      
      GL_SCISSOR_BOX = { type = "value" },
      GL_SCISSOR_TEST = { type = "value" },
      GL_UNPACK_ALIGNMENT = { type = "value" },
      
      GL_MAX_TEXTURE_SIZE = { type = "value" },
      GL_TEXTURE_2D = { type = "value" },
      
      GL_DONT_CARE = { type = "value" },
      GL_FASTEST = { type = "value" },
      GL_NICEST = { type = "value" },
      
      GL_BYTE = { type = "value" },
      GL_UNSIGNED_BYTE = { type = "value" },
      GL_SHORT = { type = "value" },
      GL_UNSIGNED_SHORT = { type = "value" },
      GL_INT = { type = "value" },
      GL_UNSIGNED_INT = { type = "value" },
      GL_FLOAT = { type = "value" },
      
      GL_DEPTH_COMPONENT = { type = "value" },
      GL_RGB = { type = "value" },
      GL_RGBA = { type = "value" },
      
      GL_KEEP = { type = "value" },
      GL_REPLACE = { type = "value" },
      GL_INCR = { type = "value" },
      GL_DECR = { type = "value" },
      
      GL_VENDOR = { type = "value" },
      GL_RENDERER = { type = "value" },
      GL_VERSION = { type = "value" },
      GL_EXTENSIONS = { type = "value" },
      
      GL_NEAREST = { type = "value" },
      GL_LINEAR = { type = "value" },
      GL_NEAREST_MIPMAP_NEAREST = { type = "value" },
      GL_LINEAR_MIPMAP_NEAREST = { type = "value" },
      GL_NEAREST_MIPMAP_LINEAR = { type = "value" },
      GL_LINEAR_MIPMAP_LINEAR = { type = "value" },
      GL_TEXTURE_MAG_FILTER = { type = "value" },
      GL_TEXTURE_MIN_FILTER = { type = "value" },
      GL_TEXTURE_WRAP_S = { type = "value" },
      GL_TEXTURE_WRAP_T = { type = "value" },
      GL_REPEAT = { type = "value" },
      
      GL_CONSTANT_COLOR = { type = "value" },
      GL_ONE_MINUS_CONSTANT_COLOR = { type = "value" },
      GL_CONSTANT_ALPHA = { type = "value" },
      GL_ONE_MINUS_CONSTANT_ALPHA = { type = "value" },
      GL_BLEND_COLOR = { type = "value" },
      GL_FUNC_ADD = { type = "value" },
      GL_BLEND_EQUATION = { type = "value" },
      GL_BLEND_EQUATION_RGB = { type = "value" },
      GL_FUNC_SUBTRACT = { type = "value" },
      GL_FUNC_REVERSE_SUBTRACT = { type = "value" },
      
      GL_UNSIGNED_SHORT_5_5_5_1 = { type = "value" },
      GL_POLYGON_OFFSET_FILL = { type = "value" },
      GL_POLYGON_OFFSET_FACTOR = { type = "value" },
      GL_RGB5_A1 = { type = "value" },
      GL_TEXTURE_BINDING_2D = { type = "value" },
      GL_BLEND_DST_RGB = { type = "value" },
      GL_BLEND_SRC_RGB = { type = "value" },
      GL_BLEND_DST_ALPHA = { type = "value" },
      GL_BLEND_SRC_ALPHA = { type = "value" },
      
      GL_CLAMP_TO_EDGE = { type = "value" },
      GL_GENERATE_MIPMAP_HINT = { type = "value" },
      GL_DEPTH_COMPONENT16 = { type = "value" },
      GL_UNSIGNED_SHORT_5_6_5 = { type = "value" },
      GL_MIRRORED_REPEAT = { type = "value" },
      
      GL_TEXTURE0 = { type = "value" },
      GL_ACTIVE_TEXTURE = { type = "value" },
      GL_TEXTURE_CUBE_MAP = { type = "value" },
      GL_TEXTURE_CUBE_MAP_POSITIVE_X = { type = "value" },
      GL_TEXTURE_CUBE_MAP_NEGATIVE_X = { type = "value" },
      GL_TEXTURE_CUBE_MAP_POSITIVE_Y = { type = "value" },
      GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = { type = "value" },
      GL_TEXTURE_CUBE_MAP_POSITIVE_Z = { type = "value" },
      GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = { type = "value" },
      GL_MAX_CUBE_MAP_TEXTURE_SIZE = { type = "value" },
      GL_NUM_COMPRESSED_TEXTURE_FORMATS = { type = "value" },
      GL_COMPRESSED_TEXTURE_FORMATS = { type = "value" },
      
      GL_BLEND_EQUATION_ALPHA = { type = "value" },
      
      GL_ARRAY_BUFFER = { type = "value" },
      GL_ELEMENT_ARRAY_BUFFER = { type = "value" },
      GL_ARRAY_BUFFER_BINDING = { type = "value" },
      GL_ELEMENT_ARRAY_BUFFER_BINDING = { type = "value" },
      GL_STREAM_DRAW = { type = "value" },
      GL_STATIC_DRAW = { type = "value" },
      GL_DYNAMIC_DRAW = { type = "value" },
      
      GL_FRAGMENT_SHADER = { type = "value" },
      GL_VERTEX_SHADER = { type = "value" },
      GL_FLOAT_VEC2 = { type = "value" },
      GL_FLOAT_VEC3 = { type = "value" },
      GL_FLOAT_VEC4 = { type = "value" },
      GL_INT_VEC2 = { type = "value" },
      GL_INT_VEC3 = { type = "value" },
      GL_INT_VEC4 = { type = "value" },
      GL_FLOAT_MAT2 = { type = "value" },
      GL_FLOAT_MAT3 = { type = "value" },
      GL_FLOAT_MAT4 = { type = "value" },
      GL_SAMPLER_2D = { type = "value" },
      GL_SAMPLER_CUBE = { type = "value" },
      
      GL_COMPILE_STATUS = { type = "value" },
      GL_LINK_STATUS = { type = "value" },
      GL_INFO_LOG_LENGTH = { type = "value" },
      GL_ACTIVE_UNIFORMS = { type = "value" },
      GL_ACTIVE_UNIFORM_MAX_LENGTH = { type = "value" },
      GL_ACTIVE_ATTRIBUTES = { type = "value" },
      GL_ACTIVE_ATTRIBUTE_MAX_LENGTH = { type = "value" },
      GL_SHADING_LANGUAGE_VERSION = { type = "value" },
      GL_CURRENT_PROGRAM = { type = "value" },
      
      GL_FRAMEBUFFER_BINDING = { type = "value" },
      GL_FRAMEBUFFER_COMPLETE = { type = "value" },
      GL_COLOR_ATTACHMENT0 = { type = "value" },
      GL_DEPTH_ATTACHMENT = { type = "value" },
      GL_STENCIL_ATTACHMENT = { type = "value" },
      GL_FRAMEBUFFER = { type = "value" },
      GL_RENDERBUFFER = { type = "value" },
      GL_STENCIL_INDEX8 = { type = "value" },
      
      GL_DEPTH_BUFFER_BIT = { type = "value" },
      GL_STENCIL_BUFFER_BIT = { type = "value" },
      GL_COLOR_BUFFER_BIT = { type = "value" },
      
      glActiveTexture = {
        type = "method",
        args = "(texture: number)",
        returns = ""
      },
      glAttachShader = {
        type = "method",
        args = "(program: number, shader: number)",
        returns = ""
      },
      glBindAttribLocation = {
        type = "method",
        args = "(program: number, index: number, name: string)",
        returns = ""
      },
      glBindBuffer = {
        type = "method",
        args = "(target: enum, buffer: number)",
        returns = ""
      },
      glBindFramebuffer = {
        type = "method",
        args = "(target: enum, framebuffer: number)",
        returns = ""
      },
      glBindRenderbuffer = {
        type = "method",
        args = "(target: enum, renderbuffer: number)",
        returns = ""
      },
      glBindTexture = {
        type = "method",
        args = "(target: enum, texture: number)",
        returns = ""
      },
      glBlendColor = {
        type = "method",
        args = "(red: number, green: number, blue: number, alpha: number)",
        returns = ""
      },
      glBlendEquation = {
        type = "method",
        args = "(mode: enum)",
        returns = ""
      },
      glBlendEquationSeparate = {
        type = "method",
        args = "(modeRGB: enum, modeAlpha: enum)",
        returns = ""
      },
      glBlendFunc = {
        type = "method",
        args = "(sfactor: enum, dfactor: enum)",
        returns = ""
      },
      glBlendFuncSeparate = {
        type = "method",
        args = "(srcRGB: enum, dstRGB: enum, srcAlpha: enum, dstAlpha: enum)",
        returns = ""
      }
    }
  },
  ------------------------------------------------------------------------------
  -- ImGui
  ImGui = {
    type = "lib",
    childs = {
      ImGuiWindowFlags_None = { type = "value" },
      ImGuiWindowFlags_NoTitleBar = { type = "value" },
      ImGuiWindowFlags_NoResize = { type = "value" },
      ImGuiWindowFlags_NoMove = { type = "value" },
      ImGuiWindowFlags_NoScrollbar = { type = "value" },
      ImGuiWindowFlags_NoScrollWithMouse = { type = "value" },
      ImGuiWindowFlags_NoCollapse = { type = "value" },
      ImGuiWindowFlags_AlwaysAutoResize = { type = "value" },
      ImGuiWindowFlags_NoBackground = { type = "value" },
      ImGuiWindowFlags_NoSavedSettings = { type = "value" },
      ImGuiWindowFlags_NoMouseInputs = { type = "value" },
      ImGuiWindowFlags_MenuBar = { type = "value" },
      ImGuiWindowFlags_HorizontalScrollbar = { type = "value" },
      ImGuiWindowFlags_NoFocusOnAppearing = { type = "value" },
      ImGuiWindowFlags_NoBringToFrontOnFocus = { type = "value" },
      ImGuiWindowFlags_AlwaysVerticalScrollbar = { type = "value" },
      ImGuiWindowFlags_AlwaysHorizontalScrollbar = { type = "value" },
      ImGuiWindowFlags_AlwaysUseWindowPadding = { type = "value" },
      ImGuiWindowFlags_NoNavInputs = { type = "value" },
      ImGuiWindowFlags_NoNavFocus = { type = "value" },
      ImGuiWindowFlags_UnsavedDocument = { type = "value" },
      ImGuiWindowFlags_NoNav = { type = "value" },
      ImGuiWindowFlags_NoDecoration = { type = "value" },
      ImGuiWindowFlags_NoInputs = { type = "value" },
      
      ImGuiColorEditFlags_None = { type = "value" },
      ImGuiColorEditFlags_NoAlpha = { type = "value" },
      ImGuiColorEditFlags_NoPicker = { type = "value" },
      ImGuiColorEditFlags_NoOptions = { type = "value" },
      ImGuiColorEditFlags_NoSmallPreview = { type = "value" },
      ImGuiColorEditFlags_NoInputs = { type = "value" },
      ImGuiColorEditFlags_NoTooltip = { type = "value" },
      ImGuiColorEditFlags_NoLabel = { type = "value" },
      ImGuiColorEditFlags_NoSidePreview = { type = "value" },
      ImGuiColorEditFlags_NoDragDrop = { type = "value" },
      ImGuiColorEditFlags_AlphaBar = { type = "value" },
      ImGuiColorEditFlags_AlphaPreview = { type = "value" },
      ImGuiColorEditFlags_AlphaPreviewHalf = { type = "value" },
      ImGuiColorEditFlags_HDR = { type = "value" },
      ImGuiColorEditFlags_DisplayRGB = { type = "value" },
      ImGuiColorEditFlags_DisplayHSV = { type = "value" },
      ImGuiColorEditFlags_DisplayHex = { type = "value" },
      ImGuiColorEditFlags_Uint8 = { type = "value" },
      ImGuiColorEditFlags_Float = { type = "value" },
      ImGuiColorEditFlags_PickerHueBar = { type = "value" },
      ImGuiColorEditFlags_PickerHueWheel = { type = "value" },
      ImGuiColorEditFlags_InputRGB = { type = "value" },
      ImGuiColorEditFlags_InputHSV = { type = "value" },
      ImGuiColorEditFlags__OptionsDefault = { type = "value" },
      Begin = {
        type = "function",
        args = "(name: string [, flags: number])",
        returns = ""
      },
      End = {
        type = "function",
        args = "()",
        returns = ""
      },
      IsWindowAppearing = {
        type = "function",
        args = "()",
        returns = "(boolean)"
      },
      IsWindowCollapsed = {
        type = "function",
        args = "()",
        returns = "(boolean)"
      },
      GetWindowPos = {
        type = "function",
        args = "()",
        returns = "(Vec2)"
      },
      GetWindowSize = {
        type = "function",
        args = "()",
        returns = "(Vec2)"
      },
      GetWindowWidth = {
        type = "function",
        args = "()",
        returns = "(number)"
      },
      GetWindowHeight = {
        type = "function",
        args = "()",
        returns = "(number)"
      },
      SetNextWindowSize = {
        type = "function",
        args = "(size: Vec2)",
        returns = ""
      },
      SetNextWindowSizeConstraints = {
        type = "function",
        args = "(sizeMin: Vec2, sizeMax: Vec2)",
        returns = ""
      },
      SetNextWindowContentSize = {
        type = "function",
        args = "(size: Vec2)",
        returns = ""
      },
      SetNextWindowCollapsed = {
        type = "function",
        args = "(collapsed: boolean, cond: number)",
        returns = ""
      },
      SetNextWindowFocus = {
        type = "function",
        args = "(collapsed: boolean, cond: number)",
        returns = ""
      },
      Text = {
        type = "function",
        args = "(text: string)",
        returns = ""
      },
      TextColored = {
        type = "function",
        args = "(text: string, color: Vec4)",
        returns = ""
      },
      ColorEdit3 = {
        type = "function",
        args = "(label: string, color: Vec3, flags: enum)",
        returns = "(Vec3)"
      },
      ColorEdit4 = {
        type = "function",
        args = "(label: string, color: Vec4, flags: enum)",
        returns = "(Vec4)"
      },
      SliderFloat = {
        type = "function",
        args = "(label: string, value: number, valueMin: number, valueMax: number)",
        returns = "(number)"
      },
      Combo = {
        type = "function",
        args = "(label: string, current: number, data: string)",
        returns = "(number)"
      },
      Checkbox = {
        type = "function",
        args = "(label: string, value: boolean)",
        returns = "(boolean)"
      },
      Separator = {
        type = "function",
        args = "()",
        returns = ""
      },
      SetItemDefaultFocus = {
        type = "function",
        args = "()",
        returns = ""
      }
    }
  },
  frameData = {
    type = "function",
    args = "()",
    returns = ""
  }
}

local spec_gles100 = {
  exts = { "gles100" },
  lexer = wxstc.wxSTC_LEX_CPP,
  apitype = "glsl",
  linecomment = "//",
  stylingbits = 5,
  lexerstyleconvert = {
    text = {wxstc.wxSTC_C_IDENTIFIER,},
    lexerdef = {wxstc.wxSTC_C_DEFAULT,},
    comment = {wxstc.wxSTC_C_COMMENT,
      wxstc.wxSTC_C_COMMENTLINE,
      wxstc.wxSTC_C_COMMENTDOC,},
    stringtxt = {wxstc.wxSTC_C_STRING,
      wxstc.wxSTC_C_CHARACTER,
      wxstc.wxSTC_C_VERBATIM,},
    stringeol = {wxstc.wxSTC_C_STRINGEOL,},
    preprocessor= {wxstc.wxSTC_C_PREPROCESSOR,},
    operator = {wxstc.wxSTC_C_OPERATOR,},
    number = {wxstc.wxSTC_C_NUMBER,},

    keywords0 = {wxstc.wxSTC_C_WORD,},
    keywords1 = {wxstc.wxSTC_C_WORD2,},
  },
  keywords = {
    [[attribute const uniform varying break continue do for while
    if else in out inout float int void bool true false lowp mediump highp
    precision invariant discard return mat2 mat3 mat4 vec2 vec3 vec4 ivec2
    ivec3 ivec4 bvec2 bvec3 bvec4 sampler2D samplerCube struct]],
    
    [[gl_Position gl_PointSize gl_FragColor gl_FragData gl_FragCoord 
    gl_FrontFacing gl_PointCoord gl_MaxVertexAttribs gl_MaxVertexUniform
    gl_MaxVaryingVectors gl_MaxVertexTextureImageUnits
    gl_MaxCombinedTextureImageUnits gl_MaxTextureImageUnits
    gl_MaxFragmentUniformVectors gl_MaxDrawBuffers
    
    radians degrees sin cos tan asin acos atan
    pow exp log exp2 log2 sqrt inversesqrt
    abs sign floor ceil fract mod min max clamp mix
    step smoothstep
    length distance dot cross normalize faceforward reflect refract
    matrixCompMult
    lessThan lessThanEqual greaterThan greaterThanEqual equal notEqual any all
    not
    texture2D texture2DProj texture2DLod texture2DProjLod textureCube
    textureCubeLod]]
  }
}

local api_gles100 = {
  gl_Position = { type = "value", valuetype = "vec4" },
  gl_PointSize = { type = "value" },
  gl_FragColor = { type = "value", valuetype = "vec4" },
  gl_FragData = { type = "value" },
  gl_FragCoord = { type = "value", valuetype = "vec4" },
  gl_FrontFacing = { type = "value" },
  gl_PointCoord = { type = "value", valuetype = "vec2" },
  gl_MaxVertexAttribs = { type = "value" },
  gl_MaxVertexUniform = { type = "value" },
  gl_MaxVaryingVectors = { type = "value" },
  gl_MaxVertexTextureImageUnits = { type = "value" },
  gl_MaxCombinedTextureImageUnits = { type = "value" },
  gl_MaxTextureImageUnits = { type = "value" },
  gl_MaxFragmentUniformVectors = { type = "value" },
  gl_MaxDrawBuffers = { type = "value" },
  
  radians = {
    type = "function",
    args = "(degrees: type)",
    returns = "(type)"
  },
  degrees = {
    type = "function",
    args = "(radians: type)",
    returns = "(type)"
  },
  sin = {
    type = "function",
    args = "(angle: type)",
    returns = "(type)"
  },
  cos = {
    type = "function",
    args = "(angle: type)",
    returns = "(type)"
  },
  tan = {
    type = "function",
    args = "(angle: type)",
    returns = "(type)"
  },
  asin = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  acos = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  atan = {
    type = "function",
    args = "(x: type, y: type | y_over_x: type)",
    returns = "(type)"
  },
  pow = {
    type = "function",
    args = "(x: type, y: type)",
    returns = "(type)"
  },
  exp = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  log = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  exp2 = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  log2 = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  sqrt = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  inversesqrt = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  abs = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  sign = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  floor = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  ceil = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  fract = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  mod = {
    type = "function",
    args = "(x: type, y: any)",
    returns = "(type)"
  },
  min = {
    type = "function",
    args = "(x: type, y: any)",
    returns = "(type)"
  },
  max = {
    type = "function",
    args = "(x: type, y: any)",
    returns = "(type)"
  },
  clamp = {
    type = "function",
    args = "(x: type, minVal: any, maxVal: any)",
    returns = "(type)"
  },
  mix = {
    type = "function",
    args = "(x: type, y: type, a: any)",
    returns = "(type)"
  },
  step = {
    type = "function",
    args = "(edge: any, y: type)",
    returns = "(type)"
  },
  smoothstep = {
    type = "function",
    args = "(edge0: any, edge1: any, x: type)",
    returns = "(type)"
  },
  length = {
    type = "function",
    args = "(x: type)",
    returns = "(float)"
  },
  distance = {
    type = "function",
    args = "(p0: type, p1: type)",
    returns = "(float)"
  },
  dot = {
    type = "function",
    args = "(x: type, y: type)",
    returns = "(float)"
  },
  cross = {
    type = "function",
    args = "(x: vec3, y: vec3)",
    returns = "(vec3)"
  },
  normalize = {
    type = "function",
    args = "(x: type)",
    returns = "(type)"
  },
  faceforward = {
    type = "function",
    args = "(N: type, I: type, Nref: type)",
    returns = "(type)"
  },
  reflect = {
    type = "function",
    args = "(I: type, N: type)",
    returns = "(type)"
  },
  refract = {
    type = "function",
    args = "(I: type, N: type, eta: float)",
    returns = "(type)"
  },
  matrixCompMult = {
    type = "function",
    args = "(x: mat, y: mat)",
    returns = "(mat)"
  },
  lessThan = {
    type = "function",
    args = "(x: vec, y: vec)",
    returns = "(bvec)"
  },
  lessThanEqual = {
    type = "function",
    args = "(x: vec, y: vec)",
    returns = "(bvec)"
  },
  greaterThan = {
    type = "function",
    args = "(x: vec, y: vec)",
    returns = "(bvec)"
  },
  greaterThanEqual = {
    type = "function",
    args = "(x: vec, y: vec)",
    returns = "(bvec)"
  },
  equal = {
    type = "function",
    args = "(x: vec, y: vec)",
    returns = "(bvec)"
  },
  notEqual = {
    type = "function",
    args = "(x: vec, y: vec)",
    returns = "(bvec)"
  },
  any = {
    type = "function",
    args = "(x: bvec)",
    returns = "(bool)"
  },
  all = {
    type = "function",
    args = "(x: bvec)",
    returns = "(bool)"
  },
  --not = {
  --  type = "function",
  --  args = "(x: bvec)",
  --  returns = "(bvec)"
  --},
  texture2D = {
    type = "function",
    args = "(sampler: sampler2D, coord: vec2 [, bias: float])",
    returns = "(vec4)"
  },
  texture2DProj = {
    type = "function",
    args = "(sampler: sampler2D, coord: vec3 [, bias: float])",
    returns = "(vec4)"
  },
  texture2DLod = {
    type = "function",
    args = "(sampler: sampler2D, coord: vec2, lod: float)",
    returns = "(vec4)"
  },
  texture2DProjLod = {
    type = "function",
    args = "(sampler: sampler2D, coord: vec3, lod: float)",
    returns = "(vec4)"
  },
  textureCube = {
    type = "function",
    args = "(sampler: samplerCube, coord: vec3 [, bias: float])",
    returns = "(vec4)"
  },
  textureCubeLod = {
    type = "function",
    args = "(sampler: samplerCube, coord: vec3, lod: float)",
    returns = "(vec4)"
  }
}

local interpreter_gles100 = {
  name = "GLSL ES 100",
  description = "GLSL ES 1.00",
  api = {"gles100"}
}

return {
  name = "nopp",
  description = "",
  author = "",
  version = 0.1,
  
  onRegister = function(self)
    ide:AddAPI("lua", "nopp", api)
    ide:AddAPI("glsl", "gles100", api_gles100)
    ide:AddSpec("gles100", spec_gles100)
    ide:AddInterpreter("gles100", interpreter_gles100)
  end,

  onUnRegister = function(self)
    ide:RemoveAPI("lua", "nopp")
    ide:RemoveAPI("glsl", "gles100")
    ide:RemoveSpec("gles100")
    ide:RemoveInterpreter("gles100")
  end
}