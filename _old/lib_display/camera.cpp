#include "camera.hpp"

using namespace nopp;

namespace
{
void updateOrbital(Vec3& pos,
                   Vec3& up,
                   Vec3& right,
                   Vec3 const& at,
                   float targetOffset,
                   float lateral,
                   float longitudinal)
{
    auto q = Quat{Eigen::AngleAxisf(lateral, Vec3::UnitZ()) *
                  Eigen::AngleAxisf(longitudinal, Vec3::UnitX())};
    auto rot = q.toRotationMatrix();
    up = rot * Vec3::UnitZ();
    up.normalize();
    right = rot * Vec3::UnitX();
    right.normalize();
    auto dir = Vec3{rot * Vec3{0.0f, 1.0, 0.0f}};
    dir.normalize();
    pos = at - dir * targetOffset;
}
} // namespace

CameraEditor::CameraEditor()
{
    // default camera
    _frustum = Frustum{
        -1.0f, 1.0f, -1.0f, 1.0f, 0.1f, 100.0f, Frustum::eType::Orthographic};
    setPerspFOVV(_fovv, 1.33f, _frustum.znear, _frustum.zfar);
    lookAt(_pos, _at, _up);
    _update();
}

void CameraEditor::setViewPreset(eViewPreset viewPreset)
{
    switch (viewPreset)
    {
    case eViewPreset::Front:
        _longitudinal = 0.0f;
        _lateral = 0.0f;
        break;
    case eViewPreset::Top:
        _longitudinal = -HalfPi;
        _lateral = 0;
        break;
    case eViewPreset::Right:
        _longitudinal = 0.0f;
        _lateral = HalfPi;
        break;
    }
    _update();
}

void CameraEditor::fitView(Vec3 const& min, Vec3 const& max)
{
    auto radius = (max - min).norm() * 0.5f;
    if (radius == 0.0f)
        return;
    auto center = Vec3{(max + min) * 0.5f};
    // logDebug("center(%.2f, %.2f, %.2f) radius(%.2f)", center.x(), center.y(),
    // center.z(), radius);
    auto matView = computeMatView(_transform.matrix());
    auto vcenter =
        Vec4{matView * Vec4{center.x(), center.y(), center.z(), 1.0f}};
    _at += (_right * vcenter.x()) + (_up * vcenter.y());
    auto d = radius / std::tan(_fovv * 0.5f);
    _targetOffset = d;
    if (_frustum.type == Frustum::eType::Orthographic)
    {
        auto aspectRatio = _frustum.aspectRatio();
        setOrtho(-radius * aspectRatio,
                 radius * aspectRatio,
                 -radius,
                 radius,
                 _frustum.znear,
                 _frustum.zfar);
    }
    _update();
}

void CameraEditor::setProjType(Frustum::eType type)
{
    if (_frustum.type == type)
        return;
    auto aspectRatio = _frustum.aspectRatio();
    switch (type)
    {
    case Frustum::eType::Perspective:
        setPerspFOVV(_fovv, aspectRatio, _frustum.znear, _frustum.zfar);
        break;
    case Frustum::eType::Orthographic:
    {
        auto d = (_at - _pos).norm() * tanf(_fovv * 0.5f);
        setOrtho(-d * aspectRatio,
                 d * aspectRatio,
                 -d,
                 d,
                 _frustum.znear,
                 _frustum.zfar);
    }
    break;
    }
}

void CameraEditor::setPerspFOVV(float fovvRad,
                                float aspectRatio,
                                float znear,
                                float zfar)
{
    _fovv = fovvRad;
    _frustum = computeProjPerspFOVV(_fovv, aspectRatio, znear, zfar);
    _matProj = computeMatProj(_frustum);
}

void CameraEditor::setOrtho(
    float left, float right, float bottom, float top, float znear, float zfar)
{
    _frustum.left = left;
    _frustum.right = right;
    _frustum.bottom = bottom;
    _frustum.top = top;
    _frustum.znear = znear;
    _frustum.zfar = zfar;
    _frustum.type = Frustum::eType::Orthographic;
    _matProj = computeMatProj(_frustum);
}

void CameraEditor::setAspectRatio(float aspectRatio)
{
    if (_frustum.type == Frustum::eType::Orthographic)
    {
        setOrtho(-_frustum.top * aspectRatio,
                 _frustum.top * aspectRatio,
                 -_frustum.top,
                 _frustum.top,
                 _frustum.znear,
                 _frustum.zfar);
    }
    else
    {
        _frustum = computeProjPerspFOVV(
            _fovv, aspectRatio, _frustum.znear, _frustum.zfar);
    }
    _matProj = computeMatProj(_frustum);
}

void CameraEditor::setViewport(Viewport const& viewport)
{
    _viewport = viewport;
    setAspectRatio(_viewport.width / _viewport.height);
}

void CameraEditor::lookAt(Vec3 const& pos, Vec3 const& at, Vec3 const& up)
{
    _at = at;
    _pos = pos;
    _up = up;
    _targetOffset = (_pos - _at).norm();
    _transform = computeViewLookAt(_pos, _at, _up);
    _matView = computeMatView(_transform.matrix());
}

void CameraEditor::zoom(float step)
{
    auto dir = Vec3{_pos - _at};
    auto dn = dir.norm();
    auto l = dn - (step * dn);
    if (l < _frustum.znear)
        l = _frustum.znear;
    else if (l > _frustum.zfar)
        l = _frustum.zfar;
    dir.normalize();
    _pos = (dir * l) + _at;
    lookAt(_pos, _at, _up);
    if (_frustum.type == Frustum::eType::Orthographic)
    {
        auto aspectratio = _frustum.aspectRatio();
        auto d = (_at - _pos).norm() * std::tan(_fovv * 0.5f);
        setOrtho(-d * aspectratio,
                 d * aspectratio,
                 -d,
                 d,
                 _frustum.znear,
                 _frustum.zfar);
    }
}

void CameraEditor::pan(Vec2 const& step)
{
    _at += (_right * step.x()) - (_up * step.y());
    _update();
}

void CameraEditor::rotate(float lateral, float longitudinal)
{
    _lateral = lateral;
    _longitudinal = longitudinal;
    _update();
}

void CameraEditor::_update()
{
    updateOrbital(
        _pos, _up, _right, _at, _targetOffset, _lateral, _longitudinal);
    lookAt(_pos, _at, _up);
}
