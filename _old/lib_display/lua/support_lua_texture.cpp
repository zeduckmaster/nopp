#include "../texture.hpp"
#include "support_lua.hpp"

using namespace nopp;

namespace sol
{
template <typename T> struct unique_usertype_traits<nopp::Ptr<T>>
{
    typedef T type;
    typedef nopp::Ptr<T> actual_type;
    static bool const value = true;

    static bool is_null(actual_type const& ptr) { return ptr == nullptr; }

    static type* get(actual_type const& ptr) { return ptr.get(); }
};
} // namespace sol

namespace nopp
{
void loadLuaDisplayTexture(sol::state& luaState)
{
    // ePixelFormat
    {
        auto t = luaState.new_enum("ePixelFormat",
                                   "Unknown",
                                   ePixelFormat::Unknown,
                                   "RGBA8_UNorm",
                                   ePixelFormat::RGBA8_UNorm,
                                   "RGBx8_UNorm",
                                   ePixelFormat::RGBx8_UNorm,
                                   "R8_UNorm",
                                   ePixelFormat::R8_UNorm,
                                   "RGBA16_Float",
                                   ePixelFormat::RGBA16_Float,
                                   "RGBA32_Float",
                                   ePixelFormat::RGBA32_Float,
                                   "RGB_ETC1",
                                   ePixelFormat::RGB_ETC1);
    }

    // eInterpolation
    {
        auto t = luaState.new_enum("eInterpolation",
                                   "Nearest",
                                   eInterpolation::Nearest,
                                   "Linear",
                                   eInterpolation::Linear,
                                   "Cubic",
                                   eInterpolation::Cubic);
    }

    // TextureDesc
    {
        auto t = luaState.new_usertype<TextureDesc>(
            "TextureDesc",
            sol::call_constructor,
            sol::constructors<TextureDesc()>());
        t["size"] = &TextureDesc::size;
        t["format"] = &TextureDesc::format;
        t["mipLevelCount"] = &TextureDesc::mipLevelCount;
        t["arraySize"] = &TextureDesc::arraySize;
        t["isCubemap"] = &TextureDesc::isCubemap;
    }

    // Texture
    {
        auto t = luaState.new_usertype<TextureData>("TextureData_",
                                                    sol::no_constructor);
        luaState["TextureData"] = sol::overload(
            []() -> Ptr<TextureData> { return makePtr<TextureData>(); },
            [](TextureDesc const& desc) -> Ptr<TextureData> {
                return makePtr<TextureData>(desc);
            },
            [](char const* filepath) -> Ptr<TextureData> {
                return makePtr<TextureData>(Path{filepath});
            });
        t["id"] = sol::property(&TextureData::id);
        t["create"] =
            sol::overload([](TextureData& value,
                             TextureDesc const& desc) { value.create(desc); },
                          [](TextureData& value, char const* filepath) {
                              value.create(Path{filepath});
                          });
        t["setData"] = [](TextureData& value,
                          int32_t level,
                          int32_t pos,
                          char const* filepath) {
            value.setData(level, pos, filepath);
        };
        t["resize"] = &TextureData::resize;
        t["save"] = [](TextureData& value, char const* filepath) -> eIOResult {
            return value.save(filepath);
        };
        t["commit"] = &TextureData::commit;
    }

    // TextureDefaultDesc
    {
        auto t = luaState.new_usertype<TextureDefaultDesc>(
            "TextureDefaultDesc",
            sol::call_constructor,
            sol::constructors<TextureDefaultDesc()>());
        t["createColor"] = &TextureDefaultDesc::createColor;
        t["createSpecular"] = &TextureDefaultDesc::createSpecular;
        t["createNormal"] = &TextureDefaultDesc::createNormal;
    }
}
} // namespace nopp
