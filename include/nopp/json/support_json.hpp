#pragma once

#include "math.h"
#include <json.hpp>

namespace nopp
{
using Json = nlohmann::json;

Json toJson(Vec2 const& value);
Json toJson(Vec3 const& value);
Json toJson(Vec4 const& value);
Json toJson(Mat3 const& value);
Json toJson(Mat4 const& value);

Vec2 toVec2(Json const& value);
Vec3 toVec3(Json const& value);
Vec4 toVec4(Json const& value);
Mat3 toMat3(Json const& value);
Mat4 toMat4(Json const& value);

} // namespace nopp
