#include <catch2/catch.hpp>
#include "nopp/math_string.hpp"

using namespace nopp;

TEST_CASE("toString", "[math_string]")
{
    REQUIRE(toString(Vec2{1.1f, 2.2f}) == "1.1 2.2");
    REQUIRE(toString(Vec3{1.2f, 3.4f, 5.6f}) == "1.2 3.4 5.6");
    REQUIRE(toString(Vec4{-0.1f, 2.3f, -4.5f, 6.789f}) == "-0.1 2.3 -4.5 6.789");
    REQUIRE(toString(Mat3{Mat3::Identity()}) == "1 0 0 0 1 0 0 0 1");
    auto mat = Mat4{Mat4::Zero()};
    mat(0, 0) = 1.1f;
    mat(1, 0) = -2.2f;
    mat(2, 0) = 3.3f;
    mat(3, 0) = -4.4f;
    REQUIRE(toString(mat) == "1.1 0 0 0 -2.2 0 0 0 3.3 0 0 0 -4.4 0 0 0");
}

TEST_CASE("toValue", "[math_string]") 
{
    REQUIRE(toVec2("1.1 -2.2") == Vec2{1.1f, -2.2f});
    REQUIRE(toVec3("1.1 -2.2 3.3") == Vec3{1.1f, -2.2f, 3.3f});
    REQUIRE(toVec4("1.1 -2.2 3.3 -4.4") == Vec4{1.1f, -2.2f, 3.3f, -4.4f});
    REQUIRE(toMat3("1 0 0 0 1 0 0 0 1") == Mat3{Mat3::Identity()});
}
