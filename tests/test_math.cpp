#include <catch2/catch.hpp>
#include "nopp/math.hpp"

using namespace nopp;

TEST_CASE("lerp", "[math]")
{
    // with floats
    REQUIRE(lerp(-0.7f, 2.1f, 0.0f) == -0.7f);
    REQUIRE(almostEqual(lerp(-3.8f, 3.0f, 0.5f), -0.4f) == true);
    REQUIRE(lerp(-12.0f, 12.0f, 0.5f) == 0.0f);
    REQUIRE(almostEqual(lerp(-10.0f, 1.9f, 1.0f), 1.9f));
    // with int
    REQUIRE(lerp(-7, 10, 0.0f) == -7);
    REQUIRE(lerp(2, 10, 0.5f) == 6);
    REQUIRE(lerp(12, 22, 0.7f) == 19);
    REQUIRE(lerp(-3, 10, 1.0f) == 10);
    // with Vec2
    REQUIRE(lerp(Vec2{0.0f, 0.0f}, Vec2{1.0f, 1.0f}, 0.0f) == Vec2{0.0f, 0.0f});
    REQUIRE(lerp(Vec2{0.0f, 0.0f}, Vec2{1.0f, 1.0f}, 0.5f) == Vec2{0.5f, 0.5f});
    REQUIRE(lerp(Vec2{0.0f, 0.0f}, Vec2{1.0f, 1.0f}, 1.0f) == Vec2{1.0f, 1.0f});
}

TEST_CASE("clamp", "[math]")
{
    REQUIRE(clamp(0.3f, 0.1f, 0.9f) == 0.3f);
    REQUIRE(clamp(1.1f, 0.0f, 1.0f) == 1.0f);
    REQUIRE(clamp(0.9f, 1.0f, 2.0f) == 1.0f);
    REQUIRE(clamp(0.7f, 0.0f, 1.0f) == 0.7f);
}

TEST_CASE("saturate", "[math]")
{
    REQUIRE(saturate(0.35f) == 0.35f);
    REQUIRE(saturate(-0.12f) == 0.0f);
    REQUIRE(saturate(1.01f) == 1.0f);
    REQUIRE(saturate(Vec3{1.1f, 0.5f, -0.1f}) == Vec3{1.0f, 0.5f, 0.0f});
}

TEST_CASE("frac", "[math]")
{
    REQUIRE(almostEqual(frac(12.37f), 0.37f));
    REQUIRE(almostEqual(frac(Vec2{0.123f, 4.567f}), Vec2{0.123f, 0.567f}));
    REQUIRE(almostEqual(frac(Vec3{0.123f, 4.567f, 8.901f}), Vec3{0.123f, 0.567f, 0.901f}));
    REQUIRE(almostEqual(frac(Vec4{0.123f, 4.567f, 8.901f, 2.345f}),
                        Vec4{0.123f, 0.567f, 0.901f, 0.345f}));
}

TEST_CASE("log2", "[math]")
{
    REQUIRE(log2(1.0f) == 0.0f);
    REQUIRE(log2(2.0f) == 1.0f);
    REQUIRE(log2(4.0f) == 2.0f);
    REQUIRE(log2(8.0f) == 3.0f);
    REQUIRE(log2(0.0f) == -std::numeric_limits<float>::infinity());
}

TEST_CASE("reverseBit", "[math]")
{
    // 110101-0x0000006A-53 -> 101011..-0xAC000000-2885681152
    REQUIRE(reverseBits(53) == 2885681152);
}

TEST_CASE("fastexp", "[math]")
{
    for (auto f = -1.0f; f < 1.1f; f += 0.05f)
    {
        REQUIRE(almostEqual(fastexp(f), std::exp(f)));
    }
}

TEST_CASE("computeMatAxisTransform", "[math]")
{
    auto vx = Vec3{-0.151014f, -0.034263f, 0.987938f};
    auto vy = Vec3{-0.326436f, 0.945064f, -0.017122f};
    auto vz = Vec3{-0.933077f, -0.325084f, -0.153903f};
    auto mat = computeMatAxisTransform(vx, vy, vz);
    REQUIRE(almostEqual(Vec3{mat * vx}, Vec3::UnitX()));
    REQUIRE(almostEqual(Vec3{mat * vy}, Vec3::UnitY()));
    REQUIRE(almostEqual(Vec3{mat * vz}, Vec3::UnitZ()));
}

TEST_CASE("matYUpToZUp", "[math]")
{
    auto mat = matYUpToZUp();
    REQUIRE(Vec3{mat * Vec3{0.123f, 4.567f, 8.901f}} == Vec3{0.123f, -8.901f, 4.567f});
}
