#include <catch2/catch.hpp>
#include "nopp/mesh.hpp"
#include "nopp/mesh_primitives.hpp"

using namespace nopp;

TEST_CASE("Mesh", "[mesh]")
{
    auto mesh = Mesh{};
    REQUIRE(mesh.isEmpty() == true);

    auto meshDesc = MeshDesc{};
    REQUIRE(((meshDesc.hasTexCoord0() == false) && (meshDesc.hasNormal() == false) &&
             (meshDesc.hasTangent() == false) && (meshDesc.hasColor() == false) &&
             (meshDesc.primitiveCount() == 0)));
    meshDesc = MeshDesc{
        eVertexDeclaration::Position | eVertexDeclaration::Normal | eVertexDeclaration::Color, 33};
    REQUIRE(((meshDesc.hasTexCoord0() == false) && (meshDesc.hasNormal() == true) &&
             (meshDesc.hasTangent() == false) && (meshDesc.hasColor() == true) &&
             (meshDesc.primitiveCount() == 11)));
    mesh = Mesh{meshDesc};
    REQUIRE(mesh.positions.size() == meshDesc.vertexCount);
    REQUIRE(mesh.datas.size() == meshDesc.vertexCount);
    REQUIRE(mesh.colors.size() == meshDesc.vertexCount);
    mesh.setVertexColor(Vec4::UnitX());
}
