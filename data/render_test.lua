local mesh = MeshData()
local quadDesc = MeshQuadDesc()
quadDesc:create(mesh)

--local tex = TextureData("C:/Users/dcremoux/Documents/nopp/app_data/image_rgb.png")

local glProg

local glInit = function(gl)
    print("glInit")
    glProg = gl:Program("C:/Users/dcremoux/Documents/nopp/app_data/prg_obj_vcolor.gles100")
    print(glProg)
end

local glFrame = function(gl)
    --print(gl)
    gl:glClearColor(1, 0, 0, 1)
    gl:glClear(gl.GL_COLOR_BUFFER_BIT)
    glProg:glBegin()
    glProg:glDrawMesh(mesh.id)
    glProg:glEnd()
end

local glTerminate = function(gl)

end

setRenderConfig(glInit, glFrame, glTerminate)
