#include <catch2/catch.hpp>
#include "nopp/log.hpp"

using namespace nopp;

namespace
{

auto infoLog = std::string{};
auto debugLog = std::string{};
auto warningLog = std::string{};
auto errorLog = std::string{};

void testLogFunction(eLogLevel logLevel, char const* fmt, va_list argList)
{
    constexpr auto BufSize = 128;
    char buf[BufSize];
    vsnprintf(buf, BufSize - 1, fmt, argList);
    buf[BufSize - 1] = '\0';
    switch (logLevel)
    {
    case eLogLevel::Info: infoLog = buf; break;
    case eLogLevel::Debug: debugLog = buf; break;
    case eLogLevel::Warning: warningLog = buf; break;
    case eLogLevel::Error: errorLog = buf; break;
    default: break;
    }
}

void fileLogFunction(eLogLevel logLevel, char const* fmt, va_list argList)
{
    
}

} // namespace

TEST_CASE("log", "[log]")
{
    nopp::logFunction = testLogFunction;
    logInfo("info %d", 3);
    REQUIRE(infoLog == "info 3");
    logDebug("debug %d%d", 3, 4);
    REQUIRE(debugLog == "debug 34");
    logWarning("warning %d", 4);
    REQUIRE(warningLog == "warning 4");
    logError("error %s", "foo");
    REQUIRE(errorLog == "error foo");
}

TEST_CASE("log to file", "[log]")
{




}

#if defined(WITH_SDL)

TEST_CASE("SDL_Log to log", "[log]")
{
    REQUIRE(SDL_Init(SDL_INIT_EVENTS) == 0);
    nopp::logFunction = testLogFunction;
    SDL_LogSetOutputFunction(logSDLLogOutputFunction, nullptr);

    SDL_Log("log %d", 5);
    REQUIRE(infoLog == "[SDL][APPLICATION]log 5");
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "debug %d%d", 4, 3);
    REQUIRE(errorLog == "[SDL][APPLICATION]debug 43");
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "info %d", 6);
    REQUIRE(infoLog == "[SDL][APPLICATION]info 6");
    SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "warning %s", "foo");
    REQUIRE(warningLog == "[SDL][APPLICATION]warning foo");
}

namespace
{

void* sdlUserData = nullptr;
auto sdlInfoLog = std::string{};
auto sdlDebugLog = std::string{};
auto sdlWarningLog = std::string{};
auto sdlErrorLog = std::string{};

void testOutputFunction(void* userdata, int category, SDL_LogPriority priority, const char* message)
{
    sdlUserData = userdata;
    switch (priority)
    {
    case SDL_LOG_PRIORITY_INFO: sdlInfoLog = message; break;
    case SDL_LOG_PRIORITY_DEBUG: sdlDebugLog = message; break;
    case SDL_LOG_PRIORITY_WARN: sdlWarningLog = message; break;
    case SDL_LOG_PRIORITY_ERROR: sdlErrorLog = message; break;
    default: break;
    }
}

} // namespace

TEST_CASE("log to SDL_Log", "[log]")
{
    REQUIRE(SDL_Init(SDL_INIT_EVENTS) == 0);
    nopp::logFunction = logFunctionToSDLLog;
    uint8_t userData = 3;
    SDL_LogSetOutputFunction(testOutputFunction, &userData);

    logInfo("info %d", 3);
    REQUIRE(sdlUserData == &userData);
    REQUIRE(*static_cast<uint8_t*>(sdlUserData) == userData);
    REQUIRE(sdlInfoLog == "info 3");

    logDebug("debug %d%d", 3, 4);
    REQUIRE(sdlDebugLog == "debug 34");

    logWarning("warning %d", 4);
    REQUIRE(sdlWarningLog == "warning 4");

    logError("error %s", "foo");
    REQUIRE(sdlErrorLog == "error foo");
}

#endif
