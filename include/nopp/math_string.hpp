#pragma once

#include "enum.hpp"
#include "math.hpp"
#include "string.hpp"

namespace nopp
{

namespace impl
{
auto const IoFmt = Eigen::IOFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, " ", " ");
auto const VectorDelims = std::string(" \t\n\r");
} // namespace impl

inline std::string toString(Vec2 const& value)
{
    std::ostringstream sstream;
    sstream << value.format(impl::IoFmt);
    return sstream.str();
}

inline std::string toString(Vec3 const& value)
{
    std::ostringstream sstream;
    sstream << value.format(impl::IoFmt);
    return sstream.str();
}

inline std::string toString(Vec4 const& value)
{
    std::ostringstream sstream;
    sstream << value.format(impl::IoFmt);
    return sstream.str();
}

inline std::string toString(Mat3 const& value)
{
    std::ostringstream sstream;
    sstream << value.format(impl::IoFmt);
    return sstream.str();
}

inline std::string toString(Mat4 const& value)
{
    std::ostringstream sstream;
    sstream << value.format(impl::IoFmt);
    return sstream.str();
}

inline Vec2 toVec2(std::string const& value)
{
    auto floats = splitString(value, impl::VectorDelims);
    assert(floats.size() == 2);
    return Vec2(toFloat(floats[0]), toFloat(floats[1]));
}

inline Vec3 toVec3(std::string const& value)
{
    auto floats = splitString(value, impl::VectorDelims);
    assert(floats.size() == 3);
    return Vec3(toFloat(floats[0]), toFloat(floats[1]), toFloat(floats[2]));
}

inline Vec4 toVec4(std::string const& value)
{
    auto floats = splitString(value, impl::VectorDelims);
    assert(floats.size() == 4);
    return Vec4(toFloat(floats[0]), toFloat(floats[1]), toFloat(floats[2]), toFloat(floats[3]));
}

inline Mat3 toMat3(std::string const& value)
{
    auto floats = splitString(value, impl::VectorDelims);
    assert(floats.size() == 9);
    auto mat = Mat3();
    for (auto n = size_t{0}; n < 9; ++n)
    {
        mat(n) = toFloat(floats[n]);
    }
    return mat.transpose();
}

inline Mat4 toMat4(std::string const& value)
{
    auto floats = splitString(value, impl::VectorDelims);
    assert(floats.size() == 16);
    auto mat = Mat4();
    for (auto n = 0; n < 16; ++n)
    {
        mat(n) = toFloat(floats[n]);
    }
    return mat.transpose();
}

} // namespace nopp
