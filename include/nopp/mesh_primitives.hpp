#pragma once

#include "mesh.hpp"

namespace nopp
{

// grid as a collection of colored lines and z-up aligned
// each vertex contains:
// - position
// - color
struct MeshGridDesc final
{
    int32_t lineCount = 10;
    float scale = 1.0f;
    Vec4 colorLine = Vec4(0.5f, 0.5f, 0.5f, 1.0f);
    Vec4 colorAxisX = Vec4::UnitX();
    Vec4 colorAxisY = Vec4::UnitY();
    Vec4 colorAxisZ = Vec4::UnitZ();

    constexpr MeshGridDesc() = default;

    MeshGridDesc(int32_t lineCount, float scale) : lineCount(lineCount), scale(scale) {}

    void create(Mesh& mesh) const
    {
        struct GridLine
        {
            Vec3 from;
            Vec3 to;
            Vec4 color;
        };
        auto gridLines = std::vector<GridLine>();
        auto l = GridLine();
        auto size = static_cast<float>(lineCount / 2) * scale;
        auto nb = static_cast<int>(lineCount / 2);
        for (auto n = -nb; n <= nb; ++n)
        {
            auto f = static_cast<float>(n) * scale;
            l.from = Vec3(f, -size, 0.0f);
            l.to = Vec3(f, size, 0.0f);
            l.color = colorLine;
            if (n == 0)
            {
                l.color = colorAxisY;
            }
            gridLines.emplace_back(l);
            l.from = Vec3(-size, f, 0.0f);
            l.to = Vec3(size, f, 0.0f);
            l.color = colorLine;
            if (n == 0)
            {
                l.color = colorAxisX;
            }
            gridLines.push_back(l);
        }
        // z axis
        // l.from = Vec3f{0.0f, 0.0f, -size};
        // l.to = Vec3f{0.0f, 0.0f, size};
        // l.color = params.colorAxisZ;
        // gridLines.push_back(l);

        auto totalLineCount = static_cast<int32_t>(gridLines.size());
        auto meshDesc = MeshDesc();
        meshDesc.vertexDeclaration |= eVertexDeclaration::Position | eVertexDeclaration::Color;
        meshDesc.vertexCount = totalLineCount * 2;
        meshDesc.primitiveType = eMeshTopology::Lines;
        mesh.create(meshDesc);
        for (auto n = 0; n < totalLineCount; ++n)
        {
            auto& l = gridLines[n];
            mesh.positions[n * 2] = l.from;
            mesh.colors[n * 2] = l.color;
            mesh.positions[n * 2 + 1] = l.to;
            mesh.colors[n * 2 + 1] = l.color;
        }
    }
};

// a cube
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshCubeDesc final
{
    Vec3 center = Vec3::Zero();
    float size = 1.0f;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration = eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;

    constexpr MeshCubeDesc() = default;

    MeshCubeDesc(Vec3 const& center, float size) : center(center), size(size) {}

    MeshCubeDesc(Vec3 const& center, float size, uint32_t vertexDeclaration)
        : center(center), size(size), vertexDeclaration(vertexDeclaration)
    {
    }

    void create(Mesh& mesh) const
    {
        assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) == true);
        auto meshDesc = MeshDesc();
        meshDesc.vertexDeclaration = vertexDeclaration;
        meshDesc.vertexCount = _cubePositions.size();
        meshDesc.indexCount = _cubeIndexes.size();
        meshDesc.primitiveType = eMeshTopology::Triangles;
        auto nb = static_cast<int32_t>(_cubePositions.size());
        mesh.create(meshDesc);
        auto hasTexCoord = hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::TexCoord0);
        auto hasNormal = hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Normal);
        auto hasColor = hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Color);
        for (auto n = 0; n < nb; ++n)
        {
            mesh.positions[n] = (_cubePositions[n] * size) + center;
            if (hasTexCoord == true)
            {
                mesh.datas[n].texCoord0 = _cubeTexCoord0[n];
            }
            if (hasNormal == true)
            {
                mesh.datas[n].normal = _cubeNormal[n];
            }
            if (hasColor == true)
            {
                mesh.colors[n] = color;
            }
        }
        mesh.indexes = _cubeIndexes;
    }

  private:
    inline static auto const _cubePositions = std::array<Vec3, 24>{
        Vec3(-0.5f, -0.5f, -0.5f), Vec3(0.5f, -0.5f, -0.5f),
        Vec3(0.5f, -0.5f, 0.5f),   Vec3(0.5f, -0.5f, 0.5f), // front
        Vec3(0.5f, -0.5f, -0.5f),  Vec3(0.5f, 0.5f, -0.5f),
        Vec3(0.5f, -0.5f, 0.5f),   Vec3(0.5f, 0.5f, 0.5f), // right
        Vec3(0.5f, 0.5f, -0.5f),   Vec3(-0.5f, 0.5f, -0.5f),
        Vec3(0.5f, 0.5f, 0.5f),    Vec3(-0.5f, 0.5f, 0.5f), // back
        Vec3(-0.5f, 0.5f, -0.5f),  Vec3(-0.5f, -0.5f, -0.5f),
        Vec3(-0.5f, 0.5f, 0.5f),   Vec3(-0.5f, -0.5f, 0.5f), // left
        Vec3(-0.5f, -0.5f, 0.5f),  Vec3(0.5f, -0.5f, 0.5f),
        Vec3(-0.5f, 0.5f, 0.5f),   Vec3(0.5f, 0.5f, 0.5f), // top
        Vec3(-0.5f, 0.5f, -0.5f),  Vec3(0.5f, 0.5f, -0.5f),
        Vec3(-0.5f, -0.5f, -0.5f), Vec3(0.5f, -0.5f, -0.5f) // bottom
    };

    inline static auto const _cubeTexCoord0 = std::array<Vec2, 24>{
        Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), // front
        Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), // right
        Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), // back
        Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), // left
        Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), // top
        Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), // bottom
    };

    inline static auto const _cubeNormal = std::array<PackedNormal, 24>{
        Vec3(0.0f, -1.0f, 0.0f), Vec3(0.0f, -1.0f, 0.0f),
        Vec3(0.0f, -1.0f, 0.0f), Vec3(0.0f, -1.0f, 0.0f), // front
        Vec3(1.0f, 0.0f, 0.0f),  Vec3(1.0f, 0.0f, 0.0f),
        Vec3(1.0f, 0.0f, 0.0f),  Vec3(1.0f, 0.0f, 0.0f), // right
        Vec3(0.0f, 1.0f, 0.0f),  Vec3(0.0f, 1.0f, 0.0f),
        Vec3(0.0f, 1.0f, 0.0f),  Vec3(0.0f, 1.0f, 0.0f), // back
        Vec3(-1.0f, 0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f),
        Vec3(-1.0f, 0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f), // left
        Vec3(0.0f, 0.0f, 1.0f),  Vec3(0.0f, 0.0f, 1.0f),
        Vec3(0.0f, 0.0f, 1.0f),  Vec3(0.0f, 0.0f, 1.0f), // top
        Vec3(0.0f, 0.0f, -1.0f), Vec3(0.0f, 0.0f, -1.0f),
        Vec3(0.0f, 0.0f, -1.0f), Vec3(0.0f, 0.0f, -1.0f) // bottom
    };

    inline static auto const _cubeIndexes = std::vector<uint16_t>{
        0,  1,  2,  2,  1,  3,  // front
        4,  5,  6,  6,  5,  7,  // right
        8,  9,  10, 10, 9,  11, // back
        12, 13, 14, 14, 13, 15, // left
        16, 17, 18, 18, 17, 19, // top
        20, 21, 22, 22, 21, 23  // bottom
    };

    static_assert((_cubePositions.size() == _cubeTexCoord0.size()) &&
                  (_cubePositions.size() == _cubeNormal.size()));
};

// a quad z-up aligned
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshQuadDesc final
{
    Vec3 center = Vec3::Zero();
    float width = 1.0f;
    float height = 1.0f;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration = eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;
    bool isTwoSided = false; // if true, quad will have also a back face

    constexpr MeshQuadDesc() = default;

    MeshQuadDesc(Vec3 const& center, float width, float height)
        : center(center), width(width), height(height)
    {
    }

    MeshQuadDesc(Vec3 const& center, float width, float height, uint32_t vertexDeclaration)
        : center(center), width(width), height(height), vertexDeclaration(vertexDeclaration)
    {
    }

    MeshQuadDesc(
        Vec3 const& center, float width, float height, uint32_t vertexDeclaration, bool isTwoSided)
        : center(center), width(width), height(height), vertexDeclaration(vertexDeclaration),
          isTwoSided(isTwoSided)
    {
    }

    void create(Mesh& mesh) const
    {
        assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) == true);
        auto meshDesc = MeshDesc();
        meshDesc.vertexDeclaration = vertexDeclaration;
        meshDesc.vertexCount = _quadPositions.size();
        meshDesc.indexCount =
            (isTwoSided == true) ? _quadTwoSidedIndexes.size() : _quadIndexes.size();
        meshDesc.primitiveType = eMeshTopology::Triangles;
        auto nb = static_cast<int32_t>(_quadPositions.size());
        mesh.create(meshDesc);
        auto hasTexCoord = hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::TexCoord0);
        auto hasNormal = hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Normal);
        auto hasColor = hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Color);
        auto scale = Vec3(width, height, 1.0f);
        for (auto n = 0; n < nb; ++n)
        {
            mesh.positions[n] = _quadPositions[n].cwiseProduct(scale) + center;
            if (hasTexCoord == true)
            {
                mesh.datas[n].texCoord0 = _quadTexCoord0[n];
            }
            if (hasNormal == true)
            {
                mesh.datas[n].normal = _quadNormal[n];
            }
            if (hasColor == true)
            {
                mesh.colors[n] = color;
            }
        }
        mesh.indexes = (isTwoSided == true) ? _quadTwoSidedIndexes : _quadIndexes;
    }

  private:
    inline static auto const _quadPositions = std::array<Vec3, 4>{Vec3(-0.5f, -0.5f, 0.0f),
                                                                  Vec3(0.5f, -0.5f, 0.0f),
                                                                  Vec3(-0.5f, 0.5f, 0.0f),
                                                                  Vec3(0.5f, 0.5f, 0.0f)};

    inline static auto const _quadTexCoord0 =
        std::array<Vec2, 4>{Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f)};

    inline static auto const _quadNormal =
        std::array<PackedNormal, 4>{PackedNormal(0.0f, 0.0f, 1.0f),
                                    PackedNormal(0.0f, 0.0f, 1.0f),
                                    PackedNormal(0.0f, 0.0f, 1.0f),
                                    PackedNormal(0.0f, 0.0f, 1.0f)};

    inline static auto const _quadIndexes = std::vector<uint16_t>{0, 1, 2, 2, 1, 3};

    inline static auto const _quadTwoSidedIndexes =
        std::vector<uint16_t>{0, 1, 2, 2, 1, 3, 0, 2, 1, 1, 2, 3};

    static_assert((_quadPositions.size() == _quadTexCoord0.size()) &&
                  (_quadPositions.size() == _quadNormal.size()));
};

// a cylinder aligned along z axis
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshCylinderDesc final
{
    enum class eCapType : uint32_t
    {
        // no cap at the end
        None = 0,
        // normal triangulation
        Normal,
        // flipped triangulation, useful for light shape
        Flipped,
    };

    Vec3 center = Vec3::Zero();
    float radiusTop = 1.0f;
    float radiusBottom = 1.0f;
    float height = 1.0f;
    uint32_t radialSegments = 8;
    uint32_t heightSegments = 1;
    float thetaStart = 0.0f;
    float thetaLength = TwoPi;
    eCapType capTop = eCapType::Normal;
    eCapType capBottom = eCapType::Normal;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration = eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;

    constexpr MeshCylinderDesc() = default;

    MeshCylinderDesc(float radiusTop, float radiusBottom, float height)
        : radiusTop(radiusTop), radiusBottom(radiusBottom), height(height)
    {
    }

    MeshCylinderDesc(float radiusTop, float radiusBottom, float height, uint32_t vertexDeclaration)
        : radiusTop(radiusTop), radiusBottom(radiusBottom), height(height),
          vertexDeclaration(vertexDeclaration)
    {
    }

    void createMesh(Mesh& mesh) const
    {
        assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) == true);
        // create the mesh
        auto meshDesc = MeshDesc();
        meshDesc.vertexCount = (radialSegments + 1) * (heightSegments + 1);
        meshDesc.indexCount = radialSegments * heightSegments * 2 * 3;
        if (capTop != MeshCylinderDesc::eCapType::None)
        {
            meshDesc.vertexCount += radialSegments * 2 + 1;
            meshDesc.indexCount += radialSegments * 3;
        }
        if (capBottom != MeshCylinderDesc::eCapType::None)
        {
            meshDesc.vertexCount += radialSegments * 2 + 1;
            meshDesc.indexCount += radialSegments * 3;
        }
        meshDesc.primitiveType = eMeshTopology::Triangles;
        meshDesc.vertexDeclaration = vertexDeclaration;
        mesh.create(meshDesc);
        auto hasTexCoord = meshDesc.hasTexCoord0();
        auto hasNormal = meshDesc.hasNormal();
        auto hasColor = meshDesc.hasColor();

        // generate vertices, normals and uvs
        auto tanTheta = (radiusBottom - radiusTop) / height;
        auto index = size_t(0);
        auto halfHeight = height * 0.5f;
        auto indexArray = std::vector<std::vector<int32_t>>();
        for (auto y = 0; y <= heightSegments; ++y)
        {
            auto indexRow = std::vector<int32_t>();
            auto v = static_cast<float>(y) / static_cast<float>(heightSegments);
            // calculate the radius of the current row
            auto radius = v * (radiusBottom - radiusTop) + radiusTop;
            for (auto x = 0; x <= radialSegments; ++x, ++index)
            {
                auto u = static_cast<float>(x) / static_cast<float>(radialSegments);
                // vertex
                auto pos = Vec3(radius * std::sin(u * thetaLength + thetaStart),
                                -v * height + halfHeight,
                                radius * std::cos(u * thetaLength + thetaStart));
                mesh.positions[index] =
                    Vec3(pos.x() + center.x(), -pos.z() + center.y(), pos.y() + center.z());
                // normal
                if (hasNormal == true)
                {
                    auto nm = pos;
                    // handle special case if radiusTop/radiusBottom is zero
                    if (((radiusTop == 0.0f) && (y == 0)) ||
                        ((radiusBottom == 0.0f) && (y == heightSegments)))
                    {
                        nm.x() = std::sin(u * thetaLength + thetaStart);
                        nm.z() = std::cos(u * thetaLength + thetaStart);
                    }
                    nm.y() = std::sqrt(nm.x() * nm.x() + nm.z() * nm.z()) * tanTheta;
                    nm.normalize();
                    mesh.datas[index].normal = Vec3(nm.x(), -nm.z(), nm.y());
                }
                // uv
                if (hasTexCoord == true)
                {
                    mesh.datas[index].texCoord0 = Vec2(u, 1.0f - v);
                }
                // color
                if (hasColor == true)
                {
                    mesh.colors[index] = color;
                }
                // save index of vertex in respective row
                indexRow.push_back(index);
            }
            // now save vertices of the row in our index array
            indexArray.emplace_back(indexRow);
        }
        auto indexOffset = 0;
        for (auto x = 0; x < radialSegments; ++x)
        {
            for (auto y = 0; y < heightSegments; ++y)
            {
                // we use the index array to access the correct indices
                auto i1 = static_cast<uint16_t>(indexArray[y][x]);
                auto i2 = static_cast<uint16_t>(indexArray[y + 1][x]);
                auto i3 = static_cast<uint16_t>(indexArray[y + 1][x + 1]);
                auto i4 = static_cast<uint16_t>(indexArray[y][x + 1]);
                // face one
                mesh.indexes[indexOffset++] = i1;
                mesh.indexes[indexOffset++] = i2;
                mesh.indexes[indexOffset++] = i4;
                // face two
                mesh.indexes[indexOffset++] = i2;
                mesh.indexes[indexOffset++] = i3;
                mesh.indexes[indexOffset++] = i4;
            }
        }

        auto funcCap = [&](bool top, MeshCylinderDesc::eCapType capType)
        {
            auto radius = (top == true) ? radiusTop : radiusBottom;
            auto sign = (top == true) ? 1.0f : -1.0f;
            // save the index of the first center vertex
            auto centerIndexStart = index;
            // first we generate the center vertex data of the cap.
            // because the geometry needs one set of uvs per face,
            // we must generate a center vertex per face/segment
            for (auto x = 1; x <= radialSegments; ++x, ++index)
            {
                mesh.positions[index] =
                    Vec3(center.x(), center.y(), halfHeight * sign + center.z());
                if (hasColor == true)
                {
                    mesh.colors[index] = color;
                }
                if (hasNormal == true)
                {
                    mesh.datas[index].normal = Vec3{
                        0.0f,
                        0.0f,
                        sign * ((capType == MeshCylinderDesc::eCapType::Flipped) ? -1.0f : 1.0f)};
                }
                if (hasTexCoord == true)
                {
                    if (top == true)
                    {
                        mesh.datas[index].texCoord0 =
                            Vec2(static_cast<float>(x) / static_cast<float>(radialSegments), 0.0f);
                    }
                    else
                    {
                        mesh.datas[index].texCoord0 = Vec2(
                            static_cast<float>(x - 1) / static_cast<float>(radialSegments), 0.0f);
                    }
                }
            }
            // save the index of the last center vertex
            auto centerIndexEnd = index;
            // now we generate the surrounding vertices, normals and uvs
            for (auto x = 0; x <= radialSegments; ++x, ++index)
            {
                auto u = static_cast<float>(x) / static_cast<float>(radialSegments);
                auto pos = Vec3();
                pos.x() = radius * std::sin(u * thetaLength + thetaStart);
                pos.y() = halfHeight * sign;
                pos.z() = radius * std::cos(u * thetaLength + thetaStart);
                mesh.positions[index] =
                    Vec3(pos.x() + center.x(), -pos.z() + center.y(), pos.y() + center.z());
                if (hasColor == true)
                {
                    mesh.colors[index] = color;
                }
                if (hasNormal == true)
                {
                    mesh.datas[index].normal = Vec3(
                        0.0f,
                        0.0f,
                        sign * ((capType == MeshCylinderDesc::eCapType::Flipped) ? -1.0f : 1.0f));
                }

                if (hasTexCoord == true)
                {
                    mesh.datas[index].texCoord0 = Vec2(u, (top == true) ? 1.0f : 0.0f);
                }
            }
            // generate indices
            for (auto x = 0; x < radialSegments; ++x)
            {
                auto c = static_cast<uint16_t>(centerIndexStart + x);
                auto i = static_cast<uint16_t>(centerIndexEnd + x);
                if (top == true)
                {
                    mesh.indexes[indexOffset++] = i;
                    if (capType == MeshCylinderDesc::eCapType::Normal)
                    {
                        mesh.indexes[indexOffset++] = i + 1;
                        mesh.indexes[indexOffset++] = c;
                    }
                    else if (capType == MeshCylinderDesc::eCapType::Flipped)
                    {
                        mesh.indexes[indexOffset++] = c;
                        mesh.indexes[indexOffset++] = i + 1;
                    }
                }
                else
                {
                    mesh.indexes[indexOffset++] = i + 1;
                    if (capType == MeshCylinderDesc::eCapType::Normal)
                    {
                        mesh.indexes[indexOffset++] = i;
                        mesh.indexes[indexOffset++] = c;
                    }
                    else if (capType == MeshCylinderDesc::eCapType::Flipped)
                    {
                        mesh.indexes[indexOffset++] = c;
                        mesh.indexes[indexOffset++] = i;
                    }
                }
            }
        };

        if ((capTop != MeshCylinderDesc::eCapType::None) && (radiusTop > 0.0f))
        {
            funcCap(true, capTop);
        }
        if ((capBottom != MeshCylinderDesc::eCapType::None) && (radiusBottom > 0.0f))
        {
            funcCap(false, capBottom);
        }
    }
};

// an icosphere
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshIcosphereDesc final
{
    Vec3 center = Vec3::Zero();
    uint32_t subdivisions = 1;
    float size = 1.0f;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration = eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;

    constexpr MeshIcosphereDesc() = default;

    MeshIcosphereDesc(Vec3 const& center, uint32_t subdivisions, float size)
        : center(center), subdivisions(subdivisions), size(size)
    {
    }

    MeshIcosphereDesc(Vec3 const& center,
                      uint32_t subdivisions,
                      float size,
                      uint32_t vertexDeclaration)
        : center(center), subdivisions(subdivisions), size(size),
          vertexDeclaration(vertexDeclaration)
    {
    }

    void create(Mesh& mesh) const
    {
        assert(subdivisions < 7);
        assert(subdivisions > 0);
        assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) == true);
        auto meshDesc = MeshDesc();
        meshDesc.vertexDeclaration = vertexDeclaration;
        meshDesc.vertexCount = static_cast<int32_t>(std::pow(4, subdivisions - 1)) * 60;
        // meshInfo.indexCount = static_cast<int32_t>(std::pow(4, meshIcosphereInfo.subdivisions -
        // 1)) * 60;
        meshDesc.primitiveType = eMeshTopology::Triangles;
        mesh.create(meshDesc);
        auto hasTexCoord = meshDesc.hasTexCoord0();
        auto hasNormal = meshDesc.hasNormal();
        auto hasColor = meshDesc.hasColor();
        // create base Icosahedron
        for (auto n = 0; n < 60; ++n)
        {
            mesh.positions[n] = (_icospherePositions[n] * size) + center;
            if (hasTexCoord == true)
            {
                mesh.datas[n].texCoord0 = _icosphereTexCoord0[n];
            }
            if (hasNormal == true)
            {
                mesh.datas[n].normal = _icosphereNormal[n];
            }
            if (hasColor == true)
            {
                mesh.colors[n] = color;
            }
        }
        // subdivide
        auto faces = 60;
        auto nextPositions = 59;
        for (auto i = 1; i < subdivisions; ++i)
        {
            for (auto j = 2; j < faces; j += 3)
            {
                auto x = mesh.positions[j - 2];
                auto y = mesh.positions[j - 1];
                auto z = mesh.positions[j - 0];
                auto a = Vec3(((y - x) * 0.5f) + x - center);
                auto b = Vec3(((z - y) * 0.5f) + y - center);
                auto c = Vec3(((x - z) * 0.5f) + z - center);
                a = a.normalized() * size + center;
                b = b.normalized() * size + center;
                c = c.normalized() * size + center;
                mesh.positions[j - 2] = a;
                mesh.positions[j - 1] = b;
                mesh.positions[j - 0] = c;
                if (hasNormal == true)
                {
                    auto normal = ((b - a).cross(c - a)).normalized();
                    mesh.datas[j - 2].normal = normal;
                    mesh.datas[j - 1].normal = normal;
                    mesh.datas[j - 0].normal = normal;
                }
                mesh.positions[++nextPositions] = x;
                mesh.positions[++nextPositions] = a;
                mesh.positions[++nextPositions] = c;
                if (hasNormal == true)
                {
                    auto normal = ((a - x).cross(c - x)).normalized();
                    mesh.datas[nextPositions - 2].normal = normal;
                    mesh.datas[nextPositions - 1].normal = normal;
                    mesh.datas[nextPositions - 0].normal = normal;
                }
                mesh.positions[++nextPositions] = y;
                mesh.positions[++nextPositions] = b;
                mesh.positions[++nextPositions] = a;
                if (hasNormal == true)
                {
                    auto normal = ((b - y).cross(a - y)).normalized();
                    mesh.datas[nextPositions - 2].normal = normal;
                    mesh.datas[nextPositions - 1].normal = normal;
                    mesh.datas[nextPositions - 0].normal = normal;
                }
                mesh.positions[++nextPositions] = z;
                mesh.positions[++nextPositions] = c;
                mesh.positions[++nextPositions] = b;
                if (hasNormal == true)
                {
                    auto normal = ((c - z).cross(b - z)).normalized();
                    mesh.datas[nextPositions - 2].normal = normal;
                    mesh.datas[nextPositions - 1].normal = normal;
                    mesh.datas[nextPositions - 0].normal = normal;
                }
            }
            faces *= 4;
        }

        if (hasTexCoord == true)
        {
            for (auto i = 0; i < mesh.datas.size(); ++i)
            {
                // meshData.indexes[i] = static_cast<uint16_t>(i);
                mesh.datas[i].texCoord0 = _icosphereTexCoord0[i % 60];
            }
        }
        if (hasColor == true)
        {
            mesh.setVertexColor(color);
        }
    }

  private:
    // {  0.000000f,  0.000000f, -1.000000f } 0
    // {  0.723600f, -0.525720f, -0.447215f } 1
    // { -0.276385f, -0.850640f, -0.447215f } 2
    // { -0.894425f,  0.000000f, -0.447215f } 3
    // { -0.276385f,  0.850640f, -0.447215f } 4
    // {  0.723600f,  0.525720f, -0.447215f } 5
    // {  0.276385f, -0.850640f,  0.447215f } 6
    // { -0.723600f, -0.525720f,  0.447215f } 7
    // { -0.723600f,  0.525720f,  0.447215f } 8
    // {  0.276385f,  0.850640f,  0.447215f } 9
    // {  0.894425f,  0.000000f,  0.447215f } 10
    // {  0.000000f,  0.000000f,  1.000000f } 11

    inline static auto const _icospherePositions = std::vector<Vec3>{
        Vec3(0.000000f, 0.000000f, -1.000000f),   Vec3(0.723600f, -0.525720f, -0.447215f),
        Vec3(-0.276385f, -0.850640f, -0.447215f), // 0, 1, 2,
        Vec3(0.000000f, 0.000000f, -1.000000f),   Vec3(-0.276385f, -0.850640f, -0.447215f),
        Vec3(-0.894425f, 0.000000f, -0.447215f), // 0, 2, 3,
        Vec3(0.000000f, 0.000000f, -1.000000f),   Vec3(-0.894425f, 0.000000f, -0.447215f),
        Vec3(-0.276385f, 0.850640f, -0.447215f), // 0, 3, 4,
        Vec3(0.000000f, 0.000000f, -1.000000f),   Vec3(-0.276385f, 0.850640f, -0.447215f),
        Vec3(0.723600f, 0.525720f, -0.447215f), // 0, 4, 5,
        Vec3(0.000000f, 0.000000f, -1.000000f),   Vec3(0.723600f, 0.525720f, -0.447215f),
        Vec3(0.723600f, -0.525720f, -0.447215f), // 0, 5, 1,

        Vec3(0.723600f, -0.525720f, -0.447215f),  Vec3(0.894425f, 0.000000f, 0.447215f),
        Vec3(0.276385f, -0.850640f, 0.447215f), // 1, 10, 6,
        Vec3(0.276385f, -0.850640f, 0.447215f),   Vec3(-0.276385f, -0.850640f, -0.447215f),
        Vec3(0.723600f, -0.525720f, -0.447215f), // 6, 2, 1,
        Vec3(-0.276385f, -0.850640f, -0.447215f), Vec3(0.276385f, -0.850640f, 0.447215f),
        Vec3(-0.723600f, -0.525720f, 0.447215f), // 2, 6, 7,
        Vec3(-0.723600f, -0.525720f, 0.447215f),  Vec3(-0.894425f, 0.000000f, -0.447215f),
        Vec3(-0.276385f, -0.850640f, -0.447215f), // 7, 3, 2,
        Vec3(-0.894425f, 0.000000f, -0.447215f),  Vec3(-0.723600f, -0.525720f, 0.447215f),
        Vec3(-0.723600f, 0.525720f, 0.447215f), // 3, 7, 8,
        Vec3(-0.723600f, 0.525720f, 0.447215f),   Vec3(-0.276385f, 0.850640f, -0.447215f),
        Vec3(-0.894425f, 0.000000f, -0.447215f), // 8, 4, 3,
        Vec3(-0.276385f, 0.850640f, -0.447215f),  Vec3(-0.723600f, 0.525720f, 0.447215f),
        Vec3(0.276385f, 0.850640f, 0.447215f), // 4, 8, 9,
        Vec3(0.276385f, 0.850640f, 0.447215f),    Vec3(0.723600f, 0.525720f, -0.447215f),
        Vec3(-0.276385f, 0.850640f, -0.447215f), // 9, 5, 4,
        Vec3(0.723600f, 0.525720f, -0.447215f),   Vec3(0.276385f, 0.850640f, 0.447215f),
        Vec3(0.894425f, 0.000000f, 0.447215f), // 5, 9, 10,
        Vec3(0.894425f, 0.000000f, 0.447215f),    Vec3(0.723600f, -0.525720f, -0.447215f),
        Vec3(0.723600f, 0.525720f, -0.447215f), // 10, 1, 5,

        Vec3(0.000000f, 0.000000f, 1.000000f),    Vec3(0.894425f, 0.000000f, 0.447215f),
        Vec3(0.276385f, 0.850640f, 0.447215f), // 11, 10, 9,
        Vec3(0.000000f, 0.000000f, 1.000000f),    Vec3(0.276385f, 0.850640f, 0.447215f),
        Vec3(-0.723600f, 0.525720f, 0.447215f), // 11, 9, 8,
        Vec3(0.000000f, 0.000000f, 1.000000f),    Vec3(-0.723600f, 0.525720f, 0.447215f),
        Vec3(-0.723600f, -0.525720f, 0.447215f), // 11, 8, 7,
        Vec3(0.000000f, 0.000000f, 1.000000f),    Vec3(-0.723600f, -0.525720f, 0.447215f),
        Vec3(0.276385f, -0.850640f, 0.447215f), // 11, 7, 6,
        Vec3(0.000000f, 0.000000f, 1.000000f),    Vec3(0.276385f, -0.850640f, 0.447215f),
        Vec3(0.894425f, 0.000000f, 0.447215f) // 11, 6, 10
    };

    inline static auto const _icosphereTexCoord0 = std::vector<Vec2>{
        Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(0.0f, 0.0f), Vec2(0.0f, 1.0f),
        Vec2(1.0f, 1.0f), Vec2(0.0f, 0.0f), Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 0.0f),
        Vec2(1.0f, 0.0f), Vec2(1.0f, 1.0f), Vec2(0.0f, 0.0f), Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f),

        Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f), Vec2(0.0f, 1.0f),
        Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(0.0f, 0.0f), Vec2(1.0f, 0.0f), Vec2(1.0f, 0.0f),
        Vec2(1.0f, 1.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f),
        Vec2(0.0f, 1.0f), Vec2(1.0f, 0.0f), Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f), Vec2(0.0f, 1.0f),
        Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f), Vec2(1.0f, 1.0f), Vec2(1.0f, 0.0f), Vec2(1.0f, 1.0f),
        Vec2(0.0f, 0.0f), Vec2(0.0f, 1.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 0.0f), Vec2(1.0f, 1.0f),

        Vec2(1.0f, 1.0f), Vec2(0.0f, 1.0f), Vec2(0.0f, 0.0f), Vec2(1.0f, 1.0f), Vec2(0.0f, 0.0f),
        Vec2(0.0f, 1.0f), Vec2(1.0f, 1.0f), Vec2(0.0f, 1.0f), Vec2(1.0f, 0.0f), Vec2(1.0f, 1.0f),
        Vec2(1.0f, 0.0f), Vec2(0.0f, 0.0f), Vec2(1.0f, 1.0f), Vec2(0.0f, 0.0f), Vec2(0.0f, 1.0f),
    };

    inline static auto const _icosphereNormal = std::vector<PackedNormal>{
        PackedNormal(0.187600f, -0.577350f, -0.794650f),
        PackedNormal(0.187600f, -0.577350f, -0.794650f),
        PackedNormal(0.187600f, -0.577350f, -0.794650f),
        PackedNormal(-0.491120f, -0.356830f, -0.794650f),
        PackedNormal(-0.491120f, -0.356830f, -0.794650f),
        PackedNormal(-0.491120f, -0.356830f, -0.794650f),
        PackedNormal(-0.491120f, 0.356830f, -0.794650f),
        PackedNormal(-0.491120f, 0.356830f, -0.794650f),
        PackedNormal(-0.491120f, 0.356830f, -0.794650f),
        PackedNormal(0.187660f, 0.577350f, -0.794650f),
        PackedNormal(0.187660f, 0.577350f, -0.794650f),
        PackedNormal(0.187660f, 0.577350f, -0.794650f),
        PackedNormal(0.607060f, 0.000000f, -0.794650f),
        PackedNormal(0.607060f, 0.000000f, -0.794650f),
        PackedNormal(0.607060f, 0.000000f, -0.794650f),

        PackedNormal(0.794645f, -0.577360f, 0.187585f),
        PackedNormal(0.794645f, -0.577360f, 0.187585f),
        PackedNormal(0.794645f, -0.577360f, 0.187585f),
        PackedNormal(0.303535f, -0.934170f, -0.187585f),
        PackedNormal(0.303535f, -0.934170f, -0.187585f),
        PackedNormal(0.303535f, -0.934170f, -0.187585f),
        PackedNormal(-0.303535f, -0.934170f, 0.187585f),
        PackedNormal(-0.303535f, -0.934170f, 0.187585f),
        PackedNormal(-0.303535f, -0.934170f, 0.187585f),
        PackedNormal(-0.794645f, -0.577360f, -0.187585f),
        PackedNormal(-0.794645f, -0.577360f, -0.187585f),
        PackedNormal(-0.794645f, -0.577360f, -0.187585f),
        PackedNormal(-0.982245f, 0.000000f, 0.187585f),
        PackedNormal(-0.982245f, 0.000000f, 0.187585f),
        PackedNormal(-0.982245f, 0.000000f, 0.187585f),
        PackedNormal(-0.794645f, 0.577360f, -0.187585f),
        PackedNormal(-0.794645f, 0.577360f, -0.187585f),
        PackedNormal(-0.794645f, 0.577360f, -0.187585f),
        PackedNormal(-0.303535f, 0.934170f, 0.187585f),
        PackedNormal(-0.303535f, 0.934170f, 0.187585f),
        PackedNormal(-0.303535f, 0.934170f, 0.187585f),
        PackedNormal(0.303535f, 0.934170f, -0.187585f),
        PackedNormal(0.303535f, 0.934170f, -0.187585f),
        PackedNormal(0.303535f, 0.934170f, -0.187585f),
        PackedNormal(0.794645f, 0.577360f, 0.187585f),
        PackedNormal(0.794645f, 0.577360f, 0.187585f),
        PackedNormal(0.794645f, 0.577360f, 0.187585f),
        PackedNormal(0.982245f, -0.000000f, -0.187585f),
        PackedNormal(0.982245f, -0.000000f, -0.187585f),
        PackedNormal(0.982245f, -0.000000f, -0.187585f),

        PackedNormal(0.491120f, 0.356830f, 0.794650f),
        PackedNormal(0.491120f, 0.356830f, 0.794650f),
        PackedNormal(0.491120f, 0.356830f, 0.794650f),
        PackedNormal(-0.187600f, 0.577350f, 0.794650f),
        PackedNormal(-0.187600f, 0.577350f, 0.794650f),
        PackedNormal(-0.187600f, 0.577350f, 0.794650f),
        PackedNormal(-0.607060f, 0.000000f, 0.794650f),
        PackedNormal(-0.607060f, 0.000000f, 0.794650f),
        PackedNormal(-0.607060f, 0.000000f, 0.794650f),
        PackedNormal(-0.187600f, -0.577350f, 0.794650f),
        PackedNormal(-0.187600f, -0.577350f, 0.794650f),
        PackedNormal(-0.187600f, -0.577350f, 0.794650f),
        PackedNormal(0.491120f, -0.356830f, 0.794650f),
        PackedNormal(0.491120f, -0.356830f, 0.794650f),
        PackedNormal(0.491120f, -0.356830f, 0.794650f),
    };
};

} // namespace nopp
