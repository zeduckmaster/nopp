#include "texture.hpp"
#include <fstream>
#include <lib_core/log.hpp>

using namespace nopp;

namespace
{
constexpr uint32_t
CORE_MAKEFOURCC(uint8_t ch0, uint8_t ch1, uint8_t ch2, uint8_t ch3)
{
    return ((static_cast<uint32_t>(ch3) << 24) & 0xFF000000) |
           ((static_cast<uint32_t>(ch2) << 16) & 0x00FF0000) |
           ((static_cast<uint32_t>(ch1) << 8) & 0x0000FF00) |
           (static_cast<uint32_t>(ch0) & 0x000000FF);
}

enum D3DFORMAT
{
    D3DFMT_UNKNOWN = 0,

    D3DFMT_R8G8B8 = 20,
    D3DFMT_A8R8G8B8 = 21,
    D3DFMT_X8R8G8B8 = 22,
    D3DFMT_R5G6B5 = 23,
    D3DFMT_X1R5G5B5 = 24,
    D3DFMT_A1R5G5B5 = 25,
    D3DFMT_A4R4G4B4 = 26,
    D3DFMT_R3G3B2 = 27,
    D3DFMT_A8 = 28,
    D3DFMT_A8R3G3B2 = 29,
    D3DFMT_X4R4G4B4 = 30,
    D3DFMT_A2B10G10R10 = 31,
    D3DFMT_A8B8G8R8 = 32,
    D3DFMT_X8B8G8R8 = 33,
    D3DFMT_G16R16 = 34,
    D3DFMT_A2R10G10B10 = 35,
    D3DFMT_A16B16G16R16 = 36,

    D3DFMT_A8P8 = 40,
    D3DFMT_P8 = 41,

    D3DFMT_L8 = 50,
    D3DFMT_A8L8 = 51,
    D3DFMT_A4L4 = 52,

    D3DFMT_V8U8 = 60,
    D3DFMT_L6V5U5 = 61,
    D3DFMT_X8L8V8U8 = 62,
    D3DFMT_Q8W8V8U8 = 63,
    D3DFMT_V16U16 = 64,
    D3DFMT_A2W10V10U10 = 67,

    D3DFMT_UYVY = CORE_MAKEFOURCC('U', 'Y', 'V', 'Y'),
    D3DFMT_R8G8_B8G8 = CORE_MAKEFOURCC('R', 'G', 'B', 'G'),
    D3DFMT_YUY2 = CORE_MAKEFOURCC('Y', 'U', 'Y', '2'),
    D3DFMT_G8R8_G8B8 = CORE_MAKEFOURCC('G', 'R', 'G', 'B'),
    D3DFMT_DXT1 = CORE_MAKEFOURCC('D', 'X', 'T', '1'),
    D3DFMT_DXT2 = CORE_MAKEFOURCC('D', 'X', 'T', '2'),
    D3DFMT_DXT3 = CORE_MAKEFOURCC('D', 'X', 'T', '3'),
    D3DFMT_DXT4 = CORE_MAKEFOURCC('D', 'X', 'T', '4'),
    D3DFMT_DXT5 = CORE_MAKEFOURCC('D', 'X', 'T', '5'),

    D3DFMT_ATI1 = CORE_MAKEFOURCC('A', 'T', 'I', '1'),
    D3DFMT_AT1N = CORE_MAKEFOURCC('A', 'T', '1', 'N'),
    D3DFMT_ATI2 = CORE_MAKEFOURCC('A', 'T', 'I', '2'),
    D3DFMT_AT2N = CORE_MAKEFOURCC('A', 'T', '2', 'N'),

    D3DFMT_ETC1 = CORE_MAKEFOURCC('E', 'T', 'C', '1'),
    D3DFMT_ETC = CORE_MAKEFOURCC('E', 'T', 'C', ' '),

    D3DFMT_POWERVR_2BPP = CORE_MAKEFOURCC('P', 'T', 'C', '2'),
    D3DFMT_POWERVR_4BPP = CORE_MAKEFOURCC('P', 'T', 'C', '4'),

    D3DFMT_D16_LOCKABLE = 70,
    D3DFMT_D32 = 71,
    D3DFMT_D15S1 = 73,
    D3DFMT_D24S8 = 75,
    D3DFMT_D24X8 = 77,
    D3DFMT_D24X4S4 = 79,
    D3DFMT_D16 = 80,

    D3DFMT_D32F_LOCKABLE = 82,
    D3DFMT_D24FS8 = 83,

    D3DFMT_L16 = 81,

    D3DFMT_VERTEXDATA = 100,
    D3DFMT_INDEX16 = 101,
    D3DFMT_INDEX32 = 102,

    D3DFMT_Q16W16V16U16 = 110,

    D3DFMT_MULTI2_ARGB8 = CORE_MAKEFOURCC('M', 'E', 'T', '1'),

    D3DFMT_R16F = 111,
    D3DFMT_G16R16F = 112,
    D3DFMT_A16B16G16R16F = 113,

    D3DFMT_R32F = 114,
    D3DFMT_G32R32F = 115,
    D3DFMT_A32B32G32R32F = 116,

    D3DFMT_CxV8U8 = 117,

    D3DFMT_DX10 = CORE_MAKEFOURCC('D', 'X', '1', '0'),

    D3DFMT_FORCE_DWORD = 0x7fffffff
};

enum dxgiFormat
{
    DXGI_FORMAT_UNKNOWN = 0,
    DXGI_FORMAT_R32G32B32A32_TYPELESS = 1,
    DXGI_FORMAT_R32G32B32A32_FLOAT = 2,
    DXGI_FORMAT_R32G32B32A32_UINT = 3,
    DXGI_FORMAT_R32G32B32A32_SINT = 4,
    DXGI_FORMAT_R32G32B32_TYPELESS = 5,
    DXGI_FORMAT_R32G32B32_FLOAT = 6,
    DXGI_FORMAT_R32G32B32_UINT = 7,
    DXGI_FORMAT_R32G32B32_SINT = 8,
    DXGI_FORMAT_R16G16B16A16_TYPELESS = 9,
    DXGI_FORMAT_R16G16B16A16_FLOAT = 10,
    DXGI_FORMAT_R16G16B16A16_UNORM = 11,
    DXGI_FORMAT_R16G16B16A16_UINT = 12,
    DXGI_FORMAT_R16G16B16A16_SNORM = 13,
    DXGI_FORMAT_R16G16B16A16_SINT = 14,
    DXGI_FORMAT_R32G32_TYPELESS = 15,
    DXGI_FORMAT_R32G32_FLOAT = 16,
    DXGI_FORMAT_R32G32_UINT = 17,
    DXGI_FORMAT_R32G32_SINT = 18,
    DXGI_FORMAT_R32G8X24_TYPELESS = 19,
    DXGI_FORMAT_D32_FLOAT_S8X24_UINT = 20,
    DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS = 21,
    DXGI_FORMAT_X32_TYPELESS_G8X24_UINT = 22,
    DXGI_FORMAT_R10G10B10A2_TYPELESS = 23,
    DXGI_FORMAT_R10G10B10A2_UNORM = 24,
    DXGI_FORMAT_R10G10B10A2_UINT = 25,
    DXGI_FORMAT_R11G11B10_FLOAT = 26,
    DXGI_FORMAT_R8G8B8A8_TYPELESS = 27,
    DXGI_FORMAT_R8G8B8A8_UNORM = 28,
    DXGI_FORMAT_R8G8B8A8_UNORM_SRGB = 29,
    DXGI_FORMAT_R8G8B8A8_UINT = 30,
    DXGI_FORMAT_R8G8B8A8_SNORM = 31,
    DXGI_FORMAT_R8G8B8A8_SINT = 32,
    DXGI_FORMAT_R16G16_TYPELESS = 33,
    DXGI_FORMAT_R16G16_FLOAT = 34,
    DXGI_FORMAT_R16G16_UNORM = 35,
    DXGI_FORMAT_R16G16_UINT = 36,
    DXGI_FORMAT_R16G16_SNORM = 37,
    DXGI_FORMAT_R16G16_SINT = 38,
    DXGI_FORMAT_R32_TYPELESS = 39,
    DXGI_FORMAT_D32_FLOAT = 40,
    DXGI_FORMAT_R32_FLOAT = 41,
    DXGI_FORMAT_R32_UINT = 42,
    DXGI_FORMAT_R32_SINT = 43,
    DXGI_FORMAT_R24G8_TYPELESS = 44,
    DXGI_FORMAT_D24_UNORM_S8_UINT = 45,
    DXGI_FORMAT_R24_UNORM_X8_TYPELESS = 46,
    DXGI_FORMAT_X24_TYPELESS_G8_UINT = 47,
    DXGI_FORMAT_R8G8_TYPELESS = 48,
    DXGI_FORMAT_R8G8_UNORM = 49,
    DXGI_FORMAT_R8G8_UINT = 50,
    DXGI_FORMAT_R8G8_SNORM = 51,
    DXGI_FORMAT_R8G8_SINT = 52,
    DXGI_FORMAT_R16_TYPELESS = 53,
    DXGI_FORMAT_R16_FLOAT = 54,
    DXGI_FORMAT_D16_UNORM = 55,
    DXGI_FORMAT_R16_UNORM = 56,
    DXGI_FORMAT_R16_UINT = 57,
    DXGI_FORMAT_R16_SNORM = 58,
    DXGI_FORMAT_R16_SINT = 59,
    DXGI_FORMAT_R8_TYPELESS = 60,
    DXGI_FORMAT_R8_UNORM = 61,
    DXGI_FORMAT_R8_UINT = 62,
    DXGI_FORMAT_R8_SNORM = 63,
    DXGI_FORMAT_R8_SINT = 64,
    DXGI_FORMAT_A8_UNORM = 65,
    DXGI_FORMAT_R1_UNORM = 66,
    DXGI_FORMAT_R9G9B9E5_SHAREDEXP = 67,
    DXGI_FORMAT_R8G8_B8G8_UNORM = 68,
    DXGI_FORMAT_G8R8_G8B8_UNORM = 69,
    DXGI_FORMAT_BC1_TYPELESS = 70,
    DXGI_FORMAT_BC1_UNORM = 71,
    DXGI_FORMAT_BC1_UNORM_SRGB = 72,
    DXGI_FORMAT_BC2_TYPELESS = 73,
    DXGI_FORMAT_BC2_UNORM = 74,
    DXGI_FORMAT_BC2_UNORM_SRGB = 75,
    DXGI_FORMAT_BC3_TYPELESS = 76,
    DXGI_FORMAT_BC3_UNORM = 77,
    DXGI_FORMAT_BC3_UNORM_SRGB = 78,
    DXGI_FORMAT_BC4_TYPELESS = 79,
    DXGI_FORMAT_BC4_UNORM = 80,
    DXGI_FORMAT_BC4_SNORM = 81,
    DXGI_FORMAT_BC5_TYPELESS = 82,
    DXGI_FORMAT_BC5_UNORM = 83,
    DXGI_FORMAT_BC5_SNORM = 84,
    DXGI_FORMAT_B5G6R5_UNORM = 85,
    DXGI_FORMAT_B5G5R5A1_UNORM = 86,
    DXGI_FORMAT_B8G8R8A8_UNORM = 87,
    DXGI_FORMAT_B8G8R8X8_UNORM = 88,
    DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM = 89,
    DXGI_FORMAT_B8G8R8A8_TYPELESS = 90,
    DXGI_FORMAT_B8G8R8A8_UNORM_SRGB = 91,
    DXGI_FORMAT_B8G8R8X8_TYPELESS = 92,
    DXGI_FORMAT_B8G8R8X8_UNORM_SRGB = 93,
    DXGI_FORMAT_BC6H_TYPELESS = 94,
    DXGI_FORMAT_BC6H_UF16 = 95,
    DXGI_FORMAT_BC6H_SF16 = 96,
    DXGI_FORMAT_BC7_TYPELESS = 97,
    DXGI_FORMAT_BC7_UNORM = 98,
    DXGI_FORMAT_BC7_UNORM_SRGB = 99,
    DXGI_FORMAT_AYUV = 100,
    DXGI_FORMAT_Y410 = 101,
    DXGI_FORMAT_Y416 = 102,
    DXGI_FORMAT_NV12 = 103,
    DXGI_FORMAT_P010 = 104,
    DXGI_FORMAT_P016 = 105,
    DXGI_FORMAT_420_OPAQUE = 106,
    DXGI_FORMAT_YUY2 = 107,
    DXGI_FORMAT_Y210 = 108,
    DXGI_FORMAT_Y216 = 109,
    DXGI_FORMAT_NV11 = 110,
    DXGI_FORMAT_AI44 = 111,
    DXGI_FORMAT_IA44 = 112,
    DXGI_FORMAT_P8 = 113,
    DXGI_FORMAT_A8P8 = 114,
    DXGI_FORMAT_B4G4R4A4_UNORM = 115,
    DXGI_FORMAT_FORCE_UINT = 0xffffffffUL
};

enum DDPF
{
    DDPF_ALPHAPIXELS = 0x1,
    DDPF_ALPHA = 0x2,
    DDPF_FOURCC = 0x4,
    DDPF_RGB = 0x40,
    DDPF_YUV = 0x200,
    DDPF_LUMINANCE = 0x20000
};

enum DDSCubemapflag
{
    DDSCAPS2_CUBEMAP = 0x00000200,
    DDSCAPS2_CUBEMAP_POSITIVEX = 0x00000400,
    DDSCAPS2_CUBEMAP_NEGATIVEX = 0x00000800,
    DDSCAPS2_CUBEMAP_POSITIVEY = 0x00001000,
    DDSCAPS2_CUBEMAP_NEGATIVEY = 0x00002000,
    DDSCAPS2_CUBEMAP_POSITIVEZ = 0x00004000,
    DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x00008000,
    DDSCAPS2_VOLUME = 0x00200000
};

uint32_t const DDSCAPS2_CUBEMAP_ALLFACES =
    (DDSCAPS2_CUBEMAP_POSITIVEX | DDSCAPS2_CUBEMAP_NEGATIVEX |
     DDSCAPS2_CUBEMAP_POSITIVEY | DDSCAPS2_CUBEMAP_NEGATIVEY |
     DDSCAPS2_CUBEMAP_POSITIVEZ | DDSCAPS2_CUBEMAP_NEGATIVEZ);

enum DDSFlag
{
    DDSD_CAPS = 0x00000001,
    DDSD_HEIGHT = 0x00000002,
    DDSD_WIDTH = 0x00000004,
    DDSD_PITCH = 0x00000008,
    DDSD_PIXELFORMAT = 0x00001000,
    DDSD_MIPMAPCOUNT = 0x00020000,
    DDSD_LINEARSIZE = 0x00080000,
    DDSD_DEPTH = 0x00800000
};

enum DDSSurfaceflag
{
    DDSCAPS_COMPLEX = 0x00000008,
    DDSCAPS_MIPMAP = 0x00400000,
    DDSCAPS_TEXTURE = 0x00001000
};

struct DDSPixelFormat
{
    uint32_t dwSize;
    uint32_t dwFlags;
    uint32_t dwFourCC;
    uint32_t dwRGBBitCount;
    uint32_t dwRBitMask;
    uint32_t dwGBitMask;
    uint32_t dwBBitMask;
    uint32_t dwABitMask;
};

struct DDSHeader
{
    uint32_t dwSize;
    uint32_t dwFlags;
    uint32_t dwHeight;
    uint32_t dwWidth;
    uint32_t dwPitchOrLinearSize;
    uint32_t dwDepth;
    uint32_t dwMipMapCount;
    uint32_t dwReserved1[11];
    DDSPixelFormat ddspf;
    uint32_t dwCaps;
    uint32_t dwCaps2;
    uint32_t dwCaps3;
    uint32_t dwCaps4;
    uint32_t dwReserved2;
};

enum D3D10_RESOURCE_DIMENSION
{
    D3D10_RESOURCE_DIMENSION_UNKNOWN = 0,
    D3D10_RESOURCE_DIMENSION_BUFFER = 1,
    D3D10_RESOURCE_DIMENSION_TEXTURE1D = 2,
    D3D10_RESOURCE_DIMENSION_TEXTURE2D = 3,
    D3D10_RESOURCE_DIMENSION_TEXTURE3D = 4
};

enum D3D10_RESOURCE_MISC_FLAG
{
    D3D10_RESOURCE_MISC_GENERATE_MIPS = 0x1L,
    D3D10_RESOURCE_MISC_SHARED = 0x2L,
    D3D10_RESOURCE_MISC_TEXTURECUBE = 0x4L,
    D3D10_RESOURCE_MISC_SHARED_KEYEDMUTEX = 0x10L,
    D3D10_RESOURCE_MISC_GDI_COMPATIBLE = 0x20L
};

struct DDSHeader10
{
    dxgiFormat format = DXGI_FORMAT_UNKNOWN;
    D3D10_RESOURCE_DIMENSION resourceDimension =
        D3D10_RESOURCE_DIMENSION_UNKNOWN;
    uint32_t miscFlag = 0;
    uint32_t arraySize = 1;
    uint32_t miscFlags2 = 0;
};

using FuncConvertPixel = std::function<void(uint8_t* dst, uint8_t* src)>;
void CP_ARGB8_RGBA8(uint8_t* src, uint8_t* dst)
{
    struct _ARGB8
    {
        uint8_t b, g, r, a;
    }; // little endian
    auto s = reinterpret_cast<_ARGB8*>(src);
    auto d = reinterpret_cast<RGBA8*>(dst);
    // auto i = reinterpret_cast<uint32_t*>(src);
    // auto li = reinterpret_cast<uint32_t*>(dst);
    d->r = s->r;
    d->g = s->g;
    d->b = s->b;
    d->a = s->a;
}

struct TextureInfo
{
    TextureDesc desc;
    FuncConvertPixel convertPixel = nullptr;
    int32_t srcPixelByteSize = 0;
};
TextureInfo toTextureInfo(DDSHeader const& ddsHeader,
                          DDSHeader10 const& ddsHeader10)
{
    auto desc = TextureDesc{};
    desc.size.width = ddsHeader.dwWidth;
    desc.size.height = ddsHeader.dwHeight;
    desc.size.depth =
        (ddsHeader.dwCaps2 & DDSCAPS2_VOLUME) ? ddsHeader.dwDepth : 1;
    desc.mipLevelCount =
        (ddsHeader.dwFlags & DDSD_MIPMAPCOUNT) ? ddsHeader.dwMipMapCount : 1;
    desc.arraySize = 1;

    if (ddsHeader.dwCaps2 & DDSCAPS2_CUBEMAP)
    {
        assert(ddsHeader.dwCaps & DDSCAPS2_CUBEMAP_ALLFACES);
        desc.arraySize = 6;
        desc.isCubemap = true;
    }

    auto info = TextureInfo{};
    info.desc = desc;

    if (ddsHeader.ddspf.dwFourCC == D3DFMT_DX10)
    {
        switch (ddsHeader10.format)
        {
        case DXGI_FORMAT_R8G8B8A8_UNORM:
            info.desc.format = ePixelFormat::RGBA8_UNorm;
            break;
        default: break;
        }
    }
    else if (ddsHeader.ddspf.dwFlags & DDPF_FOURCC)
    {
        auto fourCC = ddsHeader.ddspf.dwFourCC;
        switch (fourCC)
        {
        case D3DFMT_ETC:
        case D3DFMT_ETC1: info.desc.format = ePixelFormat::RGB_ETC1; break;
        default: break;
        }
    }
    else if (ddsHeader.ddspf.dwFlags & DDPF_RGB)
    {
        auto flag = ddsHeader.ddspf.dwFlags;
        auto rmask = ddsHeader.ddspf.dwRBitMask;
        auto gmask = ddsHeader.ddspf.dwGBitMask;
        auto bmask = ddsHeader.ddspf.dwBBitMask;
        auto amask = ddsHeader.ddspf.dwABitMask;
        // not very optimized, can do better than that
        if (flag & DDPF_ALPHAPIXELS)
        {
            if ((rmask == 0x00FF0000) && (gmask == 0x0000FF00) &&
                (bmask == 0x000000FF) && (amask == 0xFF000000))
            {
                info.desc.format = ePixelFormat::RGBA8_UNorm;
                info.srcPixelByteSize = 4;
                info.convertPixel = CP_ARGB8_RGBA8;
            }
            else if ((rmask == 0x000000FF) && (gmask == 0x0000FF00) &&
                     (bmask == 0x00FF0000) && (amask == 0xFF000000))
                info.desc.format = ePixelFormat::RGBA8_UNorm;
        }
        // else
        //{
        //    if((rmask == 0xFF0000) && (gmask == 0x00FF00) && (bmask ==
        //    0x0000FF))
        //        return ePixelFormat::B8G8R8_UNorm;
        //    else if((rmask == 0x0000FF) && (gmask == 0x00FF00) && (bmask ==
        //    0xFF0000))
        //        return ePixelFormat::R8G8B8_UNorm;
        //}
    }
    if (info.desc.format == ePixelFormat::Unknown)
    {
        logError("toTextureInfo: dds format not supported");
        return TextureInfo{};
    }
    return info;
}
} // namespace

eIOResult TextureData::_loadDDS(Path const& filepath)
{
    std::ifstream in;
    in.open(filepath.string(), std::ios_base::in|std::ios_base::binary);
    if(in.is_open() == false)
        return eIOResult::InvalidFile;

    // read magic number from dds
    char DDSMagic[4];
    in.read(DDSMagic, sizeof(DDSMagic));
    if ((DDSMagic[0] == 'D') && (DDSMagic[1] == 'D') && (DDSMagic[2] == 'S') &&
        (DDSMagic[3] == ' '))
    {
        // get headers
        auto ddsHeader = DDSHeader{};
        auto ddsHeader10 = DDSHeader10{};
        in.read(reinterpret_cast<char*>(&ddsHeader), sizeof(ddsHeader));
        if ((ddsHeader.ddspf.dwFlags & DDPF_FOURCC) &&
            (ddsHeader.ddspf.dwFourCC == D3DFMT_DX10))
        {
            in.read(reinterpret_cast<char*>(&ddsHeader10), sizeof(ddsHeader10));
        }
        auto info = toTextureInfo(ddsHeader, ddsHeader10);

        // reset/recreate data
        create(info.desc);
        if(info.desc.size.depth == 1)
        {
            auto arraySize = info.desc.arraySize;
            for(auto n = 0; n < arraySize; ++n)
            {
                for (auto& level : levels)
                {
                    auto& dst = level.images[n];
                    auto imgsz = imageByteSize(level.size, info.desc.format);
                    if(info.convertPixel == nullptr)
                        in.read(reinterpret_cast<char*>(dst.data()), imgsz);
                    else
                    {
                        auto buffer = std::vector<uint8_t>{};
                        buffer.resize(imgsz);
                        in.read(reinterpret_cast<char*>(buffer.data()), imgsz);
                        auto srcData = buffer.data();
                        auto dstData = dst.data();
                        auto dstPixelByteSize = pixelByteSize(info.desc.format);
                        auto pixelCount = imgsz / dstPixelByteSize;
                        for(auto nn = 0; nn < pixelCount; ++nn)
                        {
                            info.convertPixel(srcData, dstData);
                            dstData += dstPixelByteSize;
                            srcData += info.srcPixelByteSize;
                        }
                    }
                }
            }
        }
    }
    return eIOResult::OK;
}

namespace
{
D3DFORMAT toD3DFORMAT(ePixelFormat format)
{
    switch (format)
    {
    // case ePixelFormat::RGBA8_UNorm: return D3DFMT_A8B8G8R8;
    // case ePixelFormat::RGBx8_UNorm: return D3DFMT_X8B8G8R8;
    // case ePixelFormat::TC_RGB_S3TC_DXT1:
    // case ePixelFormat::TC_RGBA_S3TC_DXT1: return D3DFMT_DXT1;
    // case ePixelFormat::TC_RGBA_S3TC_DXT3: return D3DFMT_DXT3;
    // case ePixelFormat::TC_RGBA_S3TC_DXT5: return D3DFMT_DXT5;
    default: return D3DFMT_UNKNOWN;
    }
}

dxgiFormat toDXGIFormat(ePixelFormat format)
{
    switch (format)
    {
    case ePixelFormat::RGBA8_UNorm:
    case ePixelFormat::RGBx8_UNorm: return DXGI_FORMAT_R8G8B8A8_UNORM;

    case ePixelFormat::R8_UNorm: return DXGI_FORMAT_R8_UNORM;
    case ePixelFormat::RGBA16_Float: return DXGI_FORMAT_R16G16B16A16_FLOAT;
    case ePixelFormat::RGBA32_Float: return DXGI_FORMAT_R32G32B32A32_FLOAT;

    default: return DXGI_FORMAT_UNKNOWN;
    }
}

uint32_t getDDPFFlags(ePixelFormat format)
{
    switch (format)
    {
    // case ePixelFormat::RGBA8_UNorm:
    // case ePixelFormat::RGBx8_UNorm: return DDPF_RGB | DDPF_ALPHAPIXELS;

    // case ePixelFormat::R8G8B8_UNorm: return DDPF_RGB;
    // case ePixelFormat::B8G8R8A8_UNorm:
    // case ePixelFormat::R8G8B8A8_UNorm: return DDPF_RGB | DDPF_ALPHAPIXELS;
    // case ePixelFormat::R16G16_Float:
    // case ePixelFormat::R16G16B16A16_Float:
    // case ePixelFormat::TC_RGB_S3TC_DXT1:
    // case ePixelFormat::TC_RGBA_S3TC_DXT3:
    // case ePixelFormat::TC_RGBA_S3TC_DXT5: return DDPF_FOURCC;
    // case ePixelFormat::TC_RGBA_S3TC_DXT1: return DDPF_FOURCC |
    // DDPF_ALPHAPIXELS;
    default: return DDPF_FOURCC;
    }
}

// because the microsoft way (in directx especially) is to read little endian
// way for 24 and 32bpp and as a result, all softwares that can read dds do the
// same way (or even swap their data while saving/loading)... we trick this
// behavior by switching RGBA masks. for 24 bits format, RGB -> BGR, BGR -> RGB
// for 32 bits format, RGBA -> ABGR, BGRA -> ARGB
uint32_t getMaskRed(ePixelFormat format)
{
    switch (format)
    {
    // case ePixelFormat::B8G8R8_UNorm: return 0xFF0000;
    // case ePixelFormat::B8G8R8A8_UNorm: return 0x00FF0000;
    // case ePixelFormat::R8G8B8_UNorm: return 0x0000FF;
    // case ePixelFormat::R8G8B8A8_UNorm: return 0x000000FF;
    default: return 0x00000000;
    }
}

uint32_t getMaskGreen(ePixelFormat format)
{
    switch (format)
    {
    // case ePixelFormat::R8G8B8_UNorm:
    // case ePixelFormat::B8G8R8_UNorm: return 0x00FF00;
    // case ePixelFormat::R8G8B8A8_UNorm:
    // case ePixelFormat::B8G8R8A8_UNorm: return 0x0000FF00;
    default: return 0x00000000;
    }
}

uint32_t getMaskBlue(ePixelFormat format)
{
    switch (format)
    {
    // case ePixelFormat::B8G8R8_UNorm: return 0x0000FF;
    // case ePixelFormat::B8G8R8A8_UNorm: return 0x000000FF;
    // case ePixelFormat::R8G8B8_UNorm: return 0xFF0000;
    // case ePixelFormat::R8G8B8A8_UNorm: return 0x00FF0000;
    default: return 0x00000000;
    }
}

uint32_t getMaskAlpha(ePixelFormat format)
{
    switch (format)
    {
    case ePixelFormat::RGBA8_UNorm:
    case ePixelFormat::RGBx8_UNorm: return 0xFF000000;
    default: return 0x00000000;
    }
}
} // namespace

eIOResult
TextureData::_saveDDS(int32_t level, int32_t pos, Path const& filepath) const
{
    std::ofstream out;
    out.open(filepath.string(), std::ios_base::out | std::ios_base::binary);
    if (out.is_open() == false)
        return eIOResult::InvalidFile;

    auto isCompressed = isPixelFormatCompressed(desc.format);
    auto size = desc.size;
    auto pixelFormat = desc.format;
    auto flags = uint32_t{DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT |
                          DDSD_PIXELFORMAT | DDSD_MIPMAPCOUNT};
    flags |= (size.depth > 1) ? DDSD_DEPTH : 0;
    flags |= (isCompressed) ? DDSD_LINEARSIZE : DDSD_PITCH;
    auto ddspfFlags = getDDPFFlags(pixelFormat);
    auto isDX10 = (ddspfFlags == DDPF_FOURCC) ? true : false;

    auto pixelsize = pixelByteSize(pixelFormat);
    auto pitchsize = (isCompressed == true) ? imageByteSize(size, pixelFormat)
                                            : pixelsize * size.width;

    // write file
    char const DDSMagic[]{"DDS "};
    out.write(const_cast<char*>(DDSMagic), sizeof(char) * 4);

    auto ddsHeader = DDSHeader{0};
    memset(ddsHeader.dwReserved1, 0, sizeof(ddsHeader.dwReserved1));
    ddsHeader.dwSize = sizeof(ddsHeader);
    ddsHeader.dwFlags = flags;
    ddsHeader.dwWidth = size.width;
    ddsHeader.dwHeight = size.height;
    ddsHeader.dwPitchOrLinearSize = static_cast<uint32_t>(pitchsize);
    ddsHeader.dwDepth = (size.depth > 1) ? size.depth : 0;
    ddsHeader.dwMipMapCount = desc.mipLevelCount;
    ddsHeader.ddspf.dwSize = sizeof(DDSPixelFormat);
    ddsHeader.ddspf.dwFlags = ddspfFlags;
    ddsHeader.ddspf.dwFourCC =
        (isDX10 == true)
            ? D3DFMT_DX10
            : ((isCompressed == true) ? toD3DFORMAT(pixelFormat) : 0);
    ddsHeader.ddspf.dwRGBBitCount =
        static_cast<uint32_t>((isCompressed == true) ? 0 : pixelsize * 8);
    ddsHeader.ddspf.dwRBitMask = getMaskRed(pixelFormat);
    ddsHeader.ddspf.dwGBitMask = getMaskGreen(pixelFormat);
    ddsHeader.ddspf.dwBBitMask = getMaskBlue(pixelFormat);
    ddsHeader.ddspf.dwABitMask = getMaskAlpha(pixelFormat);
    ddsHeader.dwCaps = DDSCAPS_TEXTURE | DDSCAPS_MIPMAP | DDSCAPS_COMPLEX;
    if (desc.isCubemap == true)
        ddsHeader.dwCaps2 |= DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_ALLFACES;

    if (size.depth > 1)
        ddsHeader.dwCaps2 |= DDSCAPS2_VOLUME;

    out.write(reinterpret_cast<char*>(&ddsHeader), sizeof(ddsHeader));

    if (isDX10 == true)
    {
        auto ddsHeader10 = DDSHeader10{};
        ddsHeader10.format = toDXGIFormat(pixelFormat);
        ddsHeader10.resourceDimension =
            (size.depth > 1) ? D3D10_RESOURCE_DIMENSION_TEXTURE3D
                             : D3D10_RESOURCE_DIMENSION_TEXTURE2D;
        ddsHeader10.miscFlag =
            (desc.isCubemap == true) ? D3D10_RESOURCE_MISC_TEXTURECUBE : 0;
        ddsHeader10.arraySize = desc.arraySize;
        out.write(reinterpret_cast<char*>(&ddsHeader10), sizeof(ddsHeader10));
    }

    if (size.depth == 1)
    {
        for (auto n = 0; n < desc.arraySize; ++n)
        {
            for (auto& level : levels)
            {
                auto& src = level.images[n];
                auto imgsz = imageByteSize(level.size, pixelFormat);
                out.write(reinterpret_cast<char const*>(src.data()), imgsz);
            }
        }
    }
    else
    {
        logError("not handling volume texture for the moment");
        return eIOResult::InvalidFileFormat;
    }

    return eIOResult::OK;
}
