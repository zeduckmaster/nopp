#pragma once

#include "math.hpp"

namespace nopp
{

struct Transform final
{
    Mat3 rotation = Mat3::Identity();
    Vec3 translation = Vec3::Zero();
    Vec3 scale = Vec3::Ones();

    Transform() = default;

    Transform(Mat3 const& rotation, Vec3 const& translation)
        : rotation(rotation), translation(translation), scale(Vec3::Ones())
    {
    }

    Transform(Mat3 const& rotation, Vec3 const& translation, Vec3 const& scale)
        : rotation(rotation), translation(translation), scale(scale)
    {
    }

    Transform operator*(Transform const& transform) const
    {
        auto t = Transform();
        t.scale = transform.scale;
        t.rotation = rotation * scale.asDiagonal() * transform.rotation;
        t.translation = translation + (rotation * (scale.cwiseProduct(transform.translation)));
        return t;
    }

    Vec3 operator*(Vec3 const& v) const { return translation + (rotation * scale.cwiseProduct(v)); }

    void composeAffine(Aff3& affine) const
    {
        affine.setIdentity();
        affine.translate(translation);
        affine.rotate(rotation);
        affine.scale(scale);
    }

    Mat4 matrix() const
    {
        auto affine = Aff3();
        composeAffine(affine);
        return affine.matrix();
    }
};

// view transform is computed according to OpenGL standard, i.e.: right is +x, up is +y, front is
// -z.
inline Transform computeViewLookDir(Vec3 const& pos, Vec3 const& dir, Vec3 const& up)
{
    // opengl camera view orientation, so dir = -dir (z positive in the eye/backwards and NOT
    // forward)
    auto tmpdir = Vec3(-dir);
    if (tmpdir.squaredNorm() < Epsilon)
    {
        return Transform();
    }
    tmpdir.normalize();
    auto tmpright = up.cross(tmpdir);
    if (tmpright.squaredNorm() < Epsilon)
    {
        return Transform();
    }
    tmpright.normalize();
    auto tmpup = tmpdir.cross(tmpright);
    if (tmpup.squaredNorm() < Epsilon)
    {
        return Transform();
    }
    tmpup.normalize();
    auto rot = Mat3();
    rot(0, 0) = tmpright.x();
    rot(0, 1) = tmpup.x();
    rot(0, 2) = tmpdir.x();
    rot(1, 0) = tmpright.y();
    rot(1, 1) = tmpup.y();
    rot(1, 2) = tmpdir.y();
    rot(2, 0) = tmpright.z();
    rot(2, 1) = tmpup.z();
    rot(2, 2) = tmpdir.z();
    return Transform(rot, pos);
}

inline Transform computeViewLookAt(Vec3 const& pos, Vec3 const& at, Vec3 const& up)
{
    return computeViewLookDir(pos, at - pos, up);
}

} // namespace nopp
