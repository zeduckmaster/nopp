#pragma once

#include <type_traits>

namespace nopp
{

// Convert an enum value to its base type value
template <typename T>
constexpr auto toType(T enumValue) -> typename std::underlying_type<T>::type
{
    return static_cast<typename std::underlying_type<T>::type>(enumValue);
}

// Convert a base type value to the corresponding enum type
template <typename T>
constexpr auto toEnum(typename std::underlying_type<T>::type typeValue) -> T
{
    return static_cast<T>(typeValue);
}

// Check if an enum flag type has a flag in its value
template <typename T, typename E>
constexpr bool hasEnumFlag(T value, E flag)
{
    return (value & toType(flag)) == toType(flag);
}

// Set a flag value type inside an enum flag type
template <typename T, typename E>
constexpr void setEnumFlag(T& value, E flag)
{
    value |= toType(flag);
}

// Unset a flag value type inside an enum flag type
template <typename T, typename E>
constexpr void unsetEnumFlag(T& value, E flag)
{
    value &= ~toType(flag);
}

} // namespace nopp
