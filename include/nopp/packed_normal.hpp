#pragma once

#include "math.hpp"

namespace nopp
{

// store normal in 32bits (uint32_t)
// - storage is destructive, i.e.: loose of precision
// - normal should be normalized
struct PackedNormal final
{
    union
    {
        uint32_t value = 0;
        struct
        {
            uint8_t x;
            uint8_t y;
            uint8_t z;
            uint8_t w;
        } v;
    };

    PackedNormal() = default;

    PackedNormal(Vec3 const& normal) { *this = normal; }

    PackedNormal(float nx, float ny, float nz) : PackedNormal(Vec3(nx, ny, nz)) {}

    PackedNormal& operator=(Vec3 const& normal)
    {
        v.x = clamp<uint8_t>(static_cast<uint8_t>(normal.x() * 127.5f + 127.5f), 0, 255);
        v.y = clamp<uint8_t>(static_cast<uint8_t>(normal.y() * 127.5f + 127.5f), 0, 255);
        v.z = clamp<uint8_t>(static_cast<uint8_t>(normal.z() * 127.5f + 127.5f), 0, 255);
        return *this;
    }

    operator Vec3() const
    {
        constexpr auto const f = 1.0f / 127.5f;
        return Vec3{(static_cast<float>(v.x) - 127.5f) * f,
                    (static_cast<float>(v.y) - 127.5f) * f,
                    (static_cast<float>(v.z) - 127.5f) * f};
    }
};

static_assert(sizeof(PackedNormal) == sizeof(uint32_t));

} // namespace nopp
