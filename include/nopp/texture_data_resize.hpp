#pragma once

#include "texture_data.hpp"

namespace nopp
{

enum class eInterpolation
{
    Nearest = 0,
    Linear,
    Cubic
};

}
