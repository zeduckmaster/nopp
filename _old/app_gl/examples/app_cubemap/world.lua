--------------------------------------------------------------------------------
-- globals

local gridMesh = nil
local screenQuad = nil
local tex = nil
local camera = nil

local onWindowResize = function()
  if (camera == nil) then
    return
  end
  local frame = frameData()
  local width = frame.windowSize.x
  local height = frame.windowSize.y
  camera:setViewport(Viewport(0, 0, width, height))
  frame:setRenderData("matProj", camera:matProj())
end

--------------------------------------------------------------------------------
-- world

local worldInitialize = function()
  print("worldInitialize")
 
  gridMesh = MeshData()
  MeshGridDesc():create(gridMesh)
  
  screenQuad = MeshData()
  MeshQuadDesc(Vec3(0, 0, 0), 2, 2):create(screenQuad)
  
  tex = TextureData(dataPath() .. "/../common/kueken7_RGB.png")
  tex:resize(Size3i(57, 57, 1), eInterpolation.Nearest)
  tex:save("/home/duckmaster/Documents/test.png")
 
  camera = CameraEditorSimple()
  camera:lookAt(Vec3(0, -10, 0), Vec3(0, 0, 0), Vec3(0, 0, 1))
  camera:setPerspFOVV(45, 1, 0.01, 1000.0)
  onWindowResize()
  
  local frame = frameData()
  frame:setRenderData("matView", camera:matView())
  frame:setRenderData("matProj", camera:matProj())
end

local worldUpdate = function()
  local frame = frameData()
  frame:setRenderData("matView", camera:matView())
  frame:addRenderObject("vcolor", gridMesh.id)
  frame:addRenderObject("screen", screenQuad.id)
end

local worldTerminate = function()
  print("worldTerminate")
  gridMesh = nil
end

setWorld(worldInitialize, worldUpdate, worldTerminate)

--------------------------------------------------------------------------------
-- events

setWindowResize(onWindowResize)

local onMousePress = function()
  local frame = frameData()
  if frame.mouseLeft == true then
    camera:onMousePress(frame.mousePos, frame.keyCtrl, frame.keyShift, frame.keyAlt)
  end
end
setMousePress(onMousePress)

local onMouseMove = function()
  local frame = frameData()
  camera:onMouseMove(frame.mousePos)
end
setMouseMove(onMouseMove)

local onMouseRelease = function()
  local frame = frameData()
  camera:onMouseRelease(frame.mousePos)
end
setMouseRelease(onMouseRelease)