#include <catch2/catch.hpp>
#include "nopp/float16.hpp"
#include "nopp/math.hpp"

using namespace nopp;

auto const min = 0.00006103515625f;

TEST_CASE("0 and 1", "[float16]")
{
    REQUIRE(static_cast<float>(Float16{0.0f}) == 0.0f);
    REQUIRE(static_cast<float>(Float16{1.0f}) == 1.0f);
    REQUIRE(static_cast<float>(Float16{-1.0f}) == -1.0f);
}

TEST_CASE("smallest positive normal number", "[float16]")
{
    REQUIRE(static_cast<float>(Float16{min}) == min);
    REQUIRE(static_cast<float>(Float16{min - Epsilon}) == 0.0f);
}

TEST_CASE("largest number less than one", "[float16]")
{
    auto const value = 0.99951172f;
    REQUIRE(static_cast<float>(Float16{value}) < 1.0f);
    REQUIRE(Float16{value}.value == Float16{1.0f - min}.value);
}

TEST_CASE("smallest number larger than one", "[float16]")
{
    auto const value = 1.00097656f;
    REQUIRE(static_cast<float>(Float16{value}) > 1.0f);
    REQUIRE(Float16{value}.value == Float16{1.001f}.value);
}

TEST_CASE("largest normal number", "[float16]")
{
    auto const value = 65504.0f;
    REQUIRE(static_cast<float>(Float16{value}) == value);
    REQUIRE(static_cast<float>(Float16{value + 1000.0f}) == value);
}
