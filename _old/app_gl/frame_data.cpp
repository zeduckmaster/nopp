#include "frame_data.hpp"

void FrameData::prepareNext(FrameData const& previous)
{
    id = previous.id + 1;
    time = previous.time;
    timeDelta = previous.timeDelta;
    windowSize = previous.windowSize;
    mousePos = previous.mousePos;
    mouseLeft = previous.mouseLeft;
    mouseRight = previous.mouseRight;
    keyCtrl = previous.keyCtrl;
    keyShift = previous.keyShift;
    keyAlt = previous.keyAlt;
    std::memcpy(keysDown, previous.keysDown, sizeof keysDown);
    textInput.clear();
    renderBins.clear();
    renderData = previous.renderData;
}

void FrameData::copyTo(ImGuiIO& io) const
{
    io.DisplaySize = ImVec2{windowSize.x(), windowSize.y()};
    io.MousePos = ImVec2{mousePos.x(), mousePos.y()};
    io.MouseDown[0] = mouseLeft;
    io.MouseDown[1] = mouseRight;
    io.KeyCtrl = keyCtrl;
    io.KeyShift = keyShift;
    io.KeyAlt = keyAlt;
    std::memcpy(io.KeysDown, keysDown, sizeof keysDown);
    io.AddInputCharactersUTF8(textInput.c_str());
}

namespace sol
{
template <> struct is_automagical<FrameData> : std::false_type
{
};
template <> struct is_container<FrameData> : std::false_type
{
};
template <> struct is_automagical<nopp::Vec2> : std::false_type
{
};
template <> struct is_container<nopp::Vec2> : std::false_type
{
};
template <> struct is_automagical<nopp::Vec3> : std::false_type
{
};
template <> struct is_container<nopp::Vec3> : std::false_type
{
};
template <> struct is_automagical<nopp::Vec4> : std::false_type
{
};
template <> struct is_container<nopp::Vec4> : std::false_type
{
};
template <> struct is_automagical<nopp::Mat3> : std::false_type
{
};
template <> struct is_container<nopp::Mat3> : std::false_type
{
};
template <> struct is_automagical<nopp::Mat4> : std::false_type
{
};
template <> struct is_container<nopp::Mat4> : std::false_type
{
};
} // namespace sol

void loadLuaFrameData(sol::state& luaState)
{
    // render object
    {
        auto t = luaState.new_usertype<RenderObject>("RenderObject",
                                                     sol::no_constructor);
        t["mesh"] = sol::readonly(&RenderObject::mesh);
        t["textureColor"] = sol::readonly(&RenderObject::textureColor);
    }

    // render bin
    {
        auto t =
            luaState.new_usertype<RenderBin>("RenderBin", sol::no_constructor);
        t["objects"] = sol::readonly(&RenderBin::objects);
    }

    // frame data
    {
        auto t =
            luaState.new_usertype<FrameData>("FrameData", sol::no_constructor);
        t["id"] = sol::readonly(&FrameData::id);
        t["time"] = sol::readonly(&FrameData::time);
        t["timeDelta"] = sol::readonly(&FrameData::timeDelta);
        t["windowSize"] = &FrameData::windowSize;
        t["mousePos"] = &FrameData::mousePos;
        t["mouseLeft"] = &FrameData::mouseLeft;
        t["mouseRight"] = &FrameData::mouseRight;
        t["keyCtrl"] = &FrameData::keyCtrl;
        t["keyShift"] = &FrameData::keyShift;
        t["keyAlt"] = &FrameData::keyAlt;

        t["addRenderObject"] = sol::overload(
            [](FrameData& frameData, char const* binName, nopp::Id meshId) {
                frameData.renderBins[binName].objects.push_back(
                    RenderObject{meshId, nopp::IdInvalid});
            },
            [](FrameData& frameData,
               char const* binName,
               nopp::Id meshId,
               nopp::Id textureColorId) {
                frameData.renderBins[binName].objects.push_back(
                    RenderObject{meshId, textureColorId});
            });
        t["findRenderBin"] = [](FrameData& frameData,
                                char const* binName) -> RenderBin const& {
            return frameData.renderBins[binName];
        };

        t["setRenderData"] = sol::overload(
            [](FrameData& frameData, char const* name, float value) {
                frameData.renderData[name] = value;
            },
            [](FrameData& frameData,
               char const* name,
               nopp::Vec2 const& value) { frameData.renderData[name] = value; },
            [](FrameData& frameData,
               char const* name,
               nopp::Vec3 const& value) { frameData.renderData[name] = value; },
            [](FrameData& frameData,
               char const* name,
               nopp::Vec4 const& value) { frameData.renderData[name] = value; },
            [](FrameData& frameData,
               char const* name,
               nopp::Mat3 const& value) { frameData.renderData[name] = value; },
            [](FrameData& frameData,
               char const* name,
               nopp::Mat4 const& value) { frameData.renderData[name] = value; },
            [](FrameData& frameData, char const* name, nopp::Id const& value) {
                frameData.renderData[name] = value;
            });
        t["renderDataAsFloat"] = [](FrameData& frameData, char const* name) {
            return std::any_cast<float>(frameData.renderData[name]);
        };
        t["renderDataAsVec2"] = [](FrameData& frameData, char const* name) {
            return std::any_cast<nopp::Vec2>(frameData.renderData[name]);
        };
        t["renderDataAsVec3"] = [](FrameData& frameData, char const* name) {
            return std::any_cast<nopp::Vec3>(frameData.renderData[name]);
        };
        t["renderDataAsVec4"] = [](FrameData& frameData, char const* name) {
            return std::any_cast<nopp::Vec4>(frameData.renderData[name]);
        };
        t["renderDataAsMat3"] = [](FrameData& frameData, char const* name) {
            return std::any_cast<nopp::Mat3>(frameData.renderData[name]);
        };
        t["renderDataAsMat4"] = [](FrameData& frameData, char const* name) {
            return std::any_cast<nopp::Mat4>(frameData.renderData[name]);
        };
        t["renderDataAsId"] = [](FrameData& frameData, char const* name) {
            return std::any_cast<nopp::Id>(frameData.renderData[name]);
        };
    }
}
