#pragma once

#include "math.hpp"

namespace nopp
{

// A spherical bound only defined by its center and radius
struct Bound final
{
    Vec3 center = Vec3::Zero();
    float radius = 0.0f;

    constexpr Bound() = default;

    Bound(Vec3 const& center, float radius) : center(center), radius(radius) {}

    // Create a new bound from an array of 3D positions
    Bound(std::vector<Vec3> const& positions) { create(positions); }

    // Recreate the bound from an array of 3D positions
    void create(std::vector<Vec3> const& positions)
    {
        auto citer = positions.cbegin();
        if (citer == positions.cend())
        {
            return;
        }
        auto min = *citer;
        auto max = min;
        ++citer;
        // find the min and max
        while (citer != positions.cend())
        {
            if (min.x() > citer->x())
            {
                min.x() = citer->x();
            }
            if (min.y() > citer->y())
            {
                min.y() = citer->y();
            }
            if (min.z() > citer->z())
            {
                min.z() = citer->z();
            }
            if (max.x() < citer->x())
            {
                max.x() = citer->x();
            }
            if (max.y() < citer->y())
            {
                max.y() = citer->y();
            }
            if (max.z() < citer->z())
            {
                max.z() = citer->z();
            }
            ++citer;
        }
        // compute the center
        center = (min + max) * 0.5f;
        // compute the radius
        auto radiusSqr = 0.0f;
        citer = positions.cbegin();
        while (citer != positions.cend())
        {
            auto diff = *citer - center;
            auto lengthSqr = diff.dot(diff);
            if (lengthSqr > radiusSqr)
            {
                radiusSqr = lengthSqr;
            }
            ++citer;
        }
        radius = std::sqrt(radiusSqr);
    }

    // Extend the bound with another bound
    // The bound is enlarged to contain the bound in parameter
    Bound& extend(Bound const& bound)
    {
        // if (bound.radius == 0.0f)
        //{
        //     return *this;
        // }
        auto diff = center - bound.center;
        auto lengthSqr = diff.dot(diff);
        auto deltaRad = bound.radius - radius;
        auto deltaRadSqr = deltaRad * deltaRad;
        // auto length, alpha;

        if (deltaRad >= 0.0f)
        {
            if (deltaRadSqr >= lengthSqr)
            {
                // bound's sphere encloses this's sphere
                center = bound.center;
                radius = bound.radius;
            }
            else
            {
                // this' sphere does not enclose bound's sphere
                auto length = std::sqrt(lengthSqr);
                if (length > Tolerance)
                {
                    auto alpha = (length - deltaRad) / (2.0f * length);
                    center = bound.center + diff * alpha;
                }
                radius = 0.5f * (bound.radius + length + radius);
            }
        }
        else if (deltaRadSqr < lengthSqr)
        {
            // this' sphere does not enclose bound's sphere
            auto length = std::sqrt(lengthSqr);
            if (length > Tolerance)
            {
                auto alpha = (length - deltaRad) / (2.0f * length);
                center = bound.center + diff * alpha;
            }
            radius = 0.5f * (bound.radius + length + radius);
        }
        // else this sphere encloses bound's sphere
        return *this;
    }

    // Extend the bound with an array of positions
    // The bound is enlarged to contain all the positions
    Bound& extend(std::vector<Vec3> const& positions) { return extend(Bound{positions}); }

    // Test if a position is inside the bound
    bool contains(Vec3 const& position) const
    {
        auto diff = position - center;
        return diff.norm() <= (radius + FuzzFactor);
    }

    // Test if a bound is inside the bound
    bool contains(Bound const& bound) const
    {
        // returns true if 'this' contains 'bound'
        auto diff = bound.center - center;
        return (diff.norm() + bound.radius) <= (radius + FuzzFactor);
    }

    // Test if two bounds are strictly equals
    constexpr bool operator==(Bound const& bound) const
    {
        return (radius == bound.radius) && (center == bound.center);
    }

    // Test if two bounds are approximatelt equals (based on nopp::almostEqual)
    bool almostEqual(Bound const& bound) const
    {
        return (nopp::almostEqual(radius, bound.radius) && nopp::almostEqual(center, bound.center));
    }

  private:
    static constexpr auto const FuzzFactor = 1e-04f;
    static constexpr auto const Tolerance = 1e-06f;
};
} // namespace nopp
