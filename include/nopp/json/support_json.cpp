#include "support_json.h"

using namespace nopp;

Json nopp::toJson(Vec2 const& value) { return {value.x(), value.y()}; }

Json nopp::toJson(Vec3 const& value)
{
    return {value.x(), value.y(), value.z()};
}

Json nopp::toJson(Vec4 const& value)
{
    return {value.x(), value.y(), value.z(), value.w()};
}

Json nopp::toJson(Mat3 const& value)
{
    return {value(0),
            value(1),
            value(2),
            value(3),
            value(4),
            value(5),
            value(6),
            value(7),
            value(8)};
}

Json nopp::toJson(Mat4 const& value)
{
    return {value(0),
            value(1),
            value(2),
            value(3),
            value(4),
            value(5),
            value(6),
            value(7),
            value(8),
            value(9),
            value(10),
            value(11),
            value(12),
            value(13),
            value(14),
            value(15)};
}

Vec2 nopp::toVec2(Json const& value)
{
    assert(value.is_array() == true);
    assert(value.size() == 2);
    return Vec2{value[0].get<Vec2::Scalar>(), value[1].get<Vec2::Scalar>()};
}

Vec3 nopp::toVec3(Json const& value)
{
    assert(value.is_array() == true);
    assert(value.size() == 3);
    return Vec3{value[0].get<Vec3::Scalar>(),
                value[1].get<Vec3::Scalar>(),
                value[2].get<Vec3::Scalar>()};
}

Vec4 nopp::toVec4(Json const& value)
{
    assert(value.is_array() == true);
    assert(value.size() == 4);
    return Vec4{value[0].get<Vec4::Scalar>(),
                value[1].get<Vec4::Scalar>(),
                value[2].get<Vec4::Scalar>(),
                value[3].get<Vec4::Scalar>()};
}

Mat3 nopp::toMat3(Json const& value)
{
    assert(value.is_array() == true);
    assert(value.size() == 9);
    auto mat = Mat3{};
    for (auto n = 0; n < 9; ++n)
        mat(n) = value[n];
    return mat;
}

Mat4 nopp::toMat4(Json const& value)
{
    assert(value.is_array() == true);
    assert(value.size() == 16);
    auto mat = Mat4{};
    for (auto n = 0; n < 16; ++n)
        mat(n) = value[n];
    return mat;
}
