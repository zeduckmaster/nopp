#include <catch2/catch.hpp>
#include "nopp/io.hpp"

using namespace nopp;

TEST_CASE("windows path style", "[io]")
{
    auto path = Path{"d:\\dir\\tmp\\file.some.test"};
    REQUIRE(path == "d:/dir/tmp/file.some.test");
    REQUIRE(path.filename() == "file.some.test");
    REQUIRE(path.extension() == "test");
    REQUIRE(path.stem() == "file.some");
    REQUIRE(path.parentPath() == "d:/dir/tmp");
    REQUIRE(path.rootDirectory() == "d:");
    REQUIRE(path.isAbsolute() == true);
    path.clear();
    REQUIRE(path.isEmpty() == true);
}

TEST_CASE("unix path style", "[io]")
{
    auto path = Path{"/usr/tmp/file.test"};
    REQUIRE("/usr/tmp/file.test" == path);
    REQUIRE(path.parentPath() == "/usr/tmp");
    REQUIRE(path.rootDirectory() == "/");
    REQUIRE(path.isAbsolute() == true);
}

TEST_CASE("Path::append", "[io]")
{
    REQUIRE(Path{"/usr/tmp"}.append("file.test") == "/usr/tmp/file.test");
    REQUIRE(Path{"/usr/tmp/"}.append("file.test") == "/usr/tmp/file.test");
}

TEST_CASE("getCurrentWorkingPath", "[io]")
{
    auto workingPath = getCurrentWorkingPath();
    REQUIRE(workingPath.isEmpty() == false);
    REQUIRE(workingPath.isAbsolute() == true);
}
