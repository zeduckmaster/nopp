#include "texture.hpp"
#include <fstream>
#include <lib_core/log.hpp>

using namespace nopp;

namespace
{
uint8_t const KTX2FileIdentifier[] = {
    0xAB, 0x4B, 0x54, 0x58, 0x20, 0x32, 0x30, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A};
auto const KTX2HeaderSize = 80;

struct KTXIndexEntry32
{
    uint32_t byteOffset = 0;
    uint32_t byteLength = 0;
};

struct KTXIndexEntry64
{
    uint64_t byteOffset = 0;
    uint64_t byteLength = 0;
};

struct KTX2Header
{
    uint8_t identifier[12];
    uint32_t vkFormat = 0;
    uint32_t typeSize = 0;
    uint32_t pixelWidth = 0;
    uint32_t pixelHeight = 0;
    uint32_t pixelDepth = 0;
    uint32_t layerCount = 0;
    uint32_t faceCount = 0;
    uint32_t levelCount = 0;
    uint32_t supercompressionScheme = 0;
    KTXIndexEntry32 dataFormatDescriptor;
    KTXIndexEntry32 keyValueData;
    KTXIndexEntry64 supercompressionGlobalData;

    bool isValid() const
    {
        if (std::memcmp(identifier, KTX2FileIdentifier, 12) != 0)
            return false;

        return true;
    }

    operator TextureDesc() const
    {
        if (std::memcmp(identifier, KTX2FileIdentifier, 12) != 0)
            return TextureDesc{};

        auto desc = TextureDesc{};

        return desc;
    }
};

} // namespace

eIOResult TextureData::_loadKTX2(Path const& filepath)
{
    std::ifstream in;
    in.open(filepath.string(), std::ios_base::in | std::ios_base::binary);
    if (in.is_open() == false)
        return eIOResult::InvalidFile;

    auto ktx2Header = KTX2Header{};
    in.read(reinterpret_cast<char*>(&ktx2Header), sizeof(ktx2Header));

    auto textureDesc = static_cast<TextureDesc>(ktx2Header);
    if (textureDesc.isValid() == false)
    {
        return eIOResult::InvalidFileFormat;
    }

    return eIOResult::OK;
}

eIOResult
TextureData::_saveKTX2(int32_t level, int32_t pos, Path const& filepath) const
{
    return eIOResult::OK;
}
