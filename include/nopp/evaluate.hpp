#pragma once

#include "string.hpp"

namespace nopp
{

// A simple operation evaluation function:
// - supports: + - * / ( )
// - works with positive integers
inline int32_t evaluate(std::string const& expression)
{
    auto expr = toLower(expression);
    expr.erase(std::remove(expr.begin(), expr.end(), ' '), expr.end());
    // tokenize
    auto toRPN = [](std::vector<std::string> const& tokens) -> std::vector<std::string>
    {
        auto rpn = std::vector<std::string>();
        rpn.reserve(tokens.size());
        auto ops = std::stack<std::string>();
        auto preced = [](char op)
        {
            if ((op == '*') || (op == '/'))
            {
                return 2;
            }
            if ((op == '+') || (op == '-'))
            {
                return 1;
            }
            return 0;
        };
        for (auto& token : tokens)
        {
            auto c = token[0];
            if (c == '(')
            {
                ops.push(token);
            }
            else if (c == ')')
            {
                while (ops.top()[0] != '(')
                {
                    rpn.push_back(ops.top());
                    ops.pop();
                }
                ops.pop();
            }
            else if ((c == '+') || (c == '-') || (c == '*') || (c == '/'))
            {
                while ((ops.empty() == false) && (preced(c) <= preced(ops.top()[0])) &&
                       (ops.top()[0] != '('))
                {
                    rpn.push_back(ops.top());
                    ops.pop();
                }
                ops.push(token);
            }
            else
            {
                rpn.push_back(token);
            }
        }
        while (ops.empty() == false)
        {
            rpn.push_back(ops.top());
            ops.pop();
        }
        return rpn;
    };
    auto tokens = toRPN(tokenizeString(expr, "+-*/()"));
    // evaluate
    auto values = std::stack<int32_t>();
    for (auto& token : tokens)
    {
        auto c = token[0];
        if (c == '+')
        {
            auto a = values.top();
            values.pop();
            auto b = values.top();
            values.pop();
            values.push(b + a);
        }
        else if (c == '*')
        {
            auto a = values.top();
            values.pop();
            auto b = values.top();
            values.pop();
            values.push(b * a);
        }
        else if (c == '-')
        {
            auto a = values.top();
            values.pop();
            auto b = values.top();
            values.pop();
            values.push(b - a);
        }
        else if (c == '/')
        {
            auto a = values.top();
            values.pop();
            auto b = values.top();
            values.pop();
            values.push(b / a);
        }
        else
        {
            values.push(toInt32(token));
        }
    }
    return values.top();
}

} // namespace nopp
