#pragma once

#include <atomic>
#include <cassert>

namespace nopp
{

// smart intrusive pointers and ref counted classes
// to support Ptr for a class T, you should have defined ptrAddRef(T*) and
// ptrRelease(T*) functions.

template <class T>
class Ptr final
{
  public:
    constexpr Ptr() = default;

    Ptr(T* const p) : _px(p)
    {
        if (_px != nullptr)
        {
            ptrAddRef(_px);
        }
    }

    Ptr(Ptr const& ptr) : _px(ptr._px)
    {
        if (_px != nullptr)
        {
            ptrAddRef(_px);
        }
    }

    Ptr(Ptr&& ptr) : _px(ptr._px) { ptr._px = nullptr; }

    ~Ptr()
    {
        if (_px != nullptr)
        {
            ptrRelease(_px);
        }
    }

    Ptr& operator=(Ptr const& ptr)
    {
        Ptr(ptr).swap(*this);
        return *this;
    }

    Ptr& operator=(Ptr&& ptr)
    {
        Ptr(static_cast<Ptr&&>(ptr)).swap(*this);
        return *this;
    }

    Ptr& operator=(T* const rhs)
    {
        Ptr(rhs).swap(*this);
        return *this;
    }

    constexpr T* get() const { return _px; }

    constexpr T& operator*() const
    {
        assert(_px != nullptr);
        return *_px;
    }

    constexpr T* operator->() const
    {
        assert(_px != nullptr);
        return _px;
    }

    constexpr operator T*() const { return _px; }

    void reset() { Ptr().swap(*this); }

    void reset(T* const rhs) { Ptr(rhs).swap(*this); }

    void swap(Ptr& rhs) { std::swap(_px, rhs._px); }

  private:
    T* _px = nullptr;
};

template <typename T, typename... Args>
inline Ptr<T> makePtr(Args&&... args)
{
    return Ptr<T>(new T(std::forward<Args>(args)...));
}

class RefObject
{
    friend void ptrAddRef(RefObject* obj) { obj->_addRef(); }
    friend void ptrRelease(RefObject* obj) { obj->_release(); }

  public:
    virtual ~RefObject() { assert(_refCount == 0); }

    RefObject(RefObject const& obj) = delete;

    RefObject& operator=(RefObject const& obj) = delete;

  protected:
    RefObject() = default;

  private:
    void _addRef() { ++_refCount; }

    void _release()
    {
        --_refCount;
        if (_refCount == 0)
        {
            delete this;
        }
    }

    std::atomic<int32_t> _refCount{0};
};

} // namespace nopp
