--------------------------------------------------------------------------------
-- rendering

local mesh = MeshData()
MeshQuadDesc(Vec3(0, 0, 0), 2.0, 2.0):create(mesh)

local glProg
local timeSpeed = 1
local normalStrength = 40
local distortionStrength = 0.1

local glInit = function(gl)
  --print("glInit")
  glProg = gl:Program(dataPath() .. "/prg_test.gles100")
end

local glFrame = function(gl)
  local frame = frameData()

  -- clear background
  gl:glClearColor(0, 0, 0, 1)
  gl:glClear(gl.GL_COLOR_BUFFER_BIT)
  gl:glViewport(0, 0, frame.windowSize.x, frame.windowSize.y)

  -- draw screen quad
  glProg:glBegin()
  glProg:glSetUniform("time", frame.time)
  glProg:glSetUniform("timeSpeed", timeSpeed)
  glProg:glSetUniform("normalStrength", normalStrength)
  glProg:glSetUniform("distortionStrength", distortionStrength)
  glProg:glSetUniform("resolution", frame.windowSize)
  glProg:glDrawMesh(mesh.id)
  glProg:glEnd()
end

local glTerminate = function(gl)
  --print("glTerminate")
end

setRendering(glInit, glFrame, glTerminate)

--------------------------------------------------------------------------------
-- gui

local firstRun = true

local glGuiDraw = function(gl)
  if (firstRun == true) then
    ImGui.SetNextWindowPos(Vec2(0, 0))
    firstRun = false
  end

  local frame = frameData()
  ImGui.Begin("Fire Shader", ImGui.ImGuiWindowFlags_AlwaysAutoResize)
  timeSpeed = ImGui.SliderFloat("timeSpeed", timeSpeed, 0.1, 3.0)
  normalStrength = ImGui.SliderFloat("normalStrength", normalStrength, 0.0, 50.0)
  distortionStrength = ImGui.SliderFloat("distortionStrength", distortionStrength, 0.0, 5.0)
  ImGui.End()
end

setImGuiDraw(glGuiDraw)
