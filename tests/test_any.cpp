#include <catch2/catch.hpp>
#include "nopp/any.hpp"

using namespace nopp;

struct Data
{
    int32_t i = 0;
    double d = 0.0;
    inline static auto counter = 0;

    Data() { ++counter; }
    Data(Data const& data) : i(data.i), d(data.d)
    {
        ++counter;
    }

    ~Data() { --counter; }
};
FAST_RTTI_REGISTER_TYPE(Data);

TEST_CASE("Base types", "[any]")
{
    auto any = Any{};
    REQUIRE(any.isEmpty() == true);
    any = 42;
    REQUIRE(any.is<int32_t>() == true);
    any = 4.2f;
    REQUIRE(any.is<float>() == true);
    any = 4.2;
    REQUIRE(any.is<double>() == true);
}

TEST_CASE("Custom types", "[any]")
{
    auto any = Any{};
    any = Data{};
    REQUIRE(any.is<Data>() == true);
    REQUIRE(Data::counter == 1);
    any = 42;
    REQUIRE(Data::counter == 0);
}
