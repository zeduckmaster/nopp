#include <catch2/catch.hpp>
#include <functional>
#include <future>
#include "nopp/task.hpp"

using namespace nopp;

TEST_CASE("task", "[task]")
{
    auto threadPool0 = std::make_unique<SimpleTaskThreadPool>(4);
    REQUIRE(threadPool0->threadCount() == 4);
    defaultTaskThreadPool = threadPool0.get();
    using Func = std::function<void()>;
    auto promise = std::promise<bool>{};
    auto future = promise.get_future();
    startDynamicAsyncTask<Func>([&]() { promise.set_value(true); });
    auto status = future.wait_for(std::chrono::seconds(1));
    REQUIRE(status == std::future_status::ready);
    REQUIRE(future.get() == true);
}
