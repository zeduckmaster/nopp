#pragma once

#include "gpu.hpp"
#include <lib_core/float16.hpp>
#include <lib_core/io.hpp>
#include <lib_core/math.hpp>
#include <lib_core/sdl/support_sdl.hpp>

namespace nopp
{
enum class ePixelFormat : uint32_t
{
    Unknown = 0,

    // pixel formats
    RGBA8_UNorm,
    RGBx8_UNorm,

    R8_UNorm,
    RGBA16_Float,
    RGBA32_Float,

    // compressed formats
    RGB_ETC1
};

int32_t imageByteSize(Size3i const& size, ePixelFormat format);

int32_t pixelByteSize(ePixelFormat format);

bool isPixelFormatCompressed(ePixelFormat format);

struct TextureDesc final
{
    Size3i size;
    ePixelFormat format = ePixelFormat::Unknown;
    int32_t mipLevelCount = -1; // negative value ~ automatic mip generation
    int32_t arraySize = 1;
    bool isCubemap = false;

    TextureDesc() = default;

    TextureDesc(int32_t width, int32_t height, ePixelFormat format);

    bool isValid() const;
};

// base and abstract class for all textures
class TextureBase : public GPUData
{
    FAST_RTTI_CLASS(TextureBase, GPUData)

  public:
    TextureDesc desc;
};

// window display texture represent a window
class WindowDisplayTexture final : public TextureBase
{
  public:
    SDL_Window* window = nullptr;
};

////////////////////////////////////////////////////////////////////////////
// pixel structures

struct RGBA8
{
    uint8_t r = 0;
    uint8_t g = 0;
    uint8_t b = 0;
    uint8_t a = 0;
};

using RGBx8 = RGBA8;

struct R8
{
    uint8_t r = 0;
};

struct RGBA16f
{
    Float16 r;
    Float16 g;
    Float16 b;
    Float16 a;
};

struct RGBA32f
{
    float r = 0.0f;
    float g = 0.0f;
    float b = 0.0f;
    float a = 0.0f;
};

////////////////////////////////////////////////////////////////////////////

enum class eInterpolation
{
    Nearest = 0,
    Linear,
    Cubic
};

using ImageData = std::vector<uint8_t>;

class TextureData final : public TextureBase
{
    FAST_RTTI_CLASS(TextureData, TextureBase)

  public:
    TextureData() = default;

    TextureData(TextureDesc const& textureDesc);

    TextureData(SDL_Surface* surface);

    TextureData(Path const& filepath);

    TextureData(TextureData const& textureData);

    TextureData& operator=(TextureData const& textureData)
    {
        _copy(textureData);
        return *this;
    }

    void create(TextureDesc const& textureDesc);

    void create(SDL_Surface* surface);

    void create(Path const& filepath);

    void setData(int32_t level, int32_t pos, SDL_Surface* surface);

    void setData(int32_t level, int32_t pos, Path const& filepath);

    ImageData& data(int32_t level, int32_t pos)
    {
        return levels[level].images[pos];
    }

    ImageData const& data(int32_t level, int32_t pos) const
    {
        return levels[level].images[pos];
    }

    void resize(Size3i const& newSize, eInterpolation interpolation);

    void generateMipmap(eInterpolation interpolation);

    void removeMipmap();

    eIOResult save(Path const& filepath) const
    {
        return saveData(-1, -1, filepath);
    }

    eIOResult saveData(int32_t level, int32_t pos, Path const& filepath) const;

  public:
    struct MipLevel final
    {
        std::vector<ImageData> images;
        Size3i size;
    };
    std::vector<MipLevel> levels;

  private:
    void _copy(TextureData const& textureData);

    eIOResult _loadKTX2(Path const& filepath);

    eIOResult _saveKTX2(int32_t level, int32_t pos, Path const& filepath) const;

    eIOResult _loadDDS(Path const& filepath);

    eIOResult _saveDDS(int32_t level, int32_t pos, Path const& filepath) const;
};

////////////////////////////////////////////////////////////////////////////////
// complex creation functions

struct TextureDefaultDesc final
{
    void createColor(TextureData& textureData) const;

    void createSpecular(TextureData& textureData) const;

    void createNormal(TextureData& textureData) const;
};

} // namespace nopp
