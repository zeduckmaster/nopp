#include <catch2/catch.hpp>
#include "nopp/gpu_gles2.hpp"

using namespace nopp;

namespace
{
auto infoLog = std::string{};
auto debugLog = std::string{};
auto warningLog = std::string{};
auto errorLog = std::string{};

void testLogFunction(eLogLevel logLevel, char const* fmt, va_list argList)
{
    constexpr auto BufSize = 256;
    char buf[BufSize];
    vsnprintf(buf, BufSize - 1, fmt, argList);
    buf[BufSize - 1] = '\0';
    switch (logLevel)
    {
    case eLogLevel::Info: infoLog = buf; break;
    case eLogLevel::Debug: debugLog = buf; break;
    case eLogLevel::Warning: warningLog = buf; break;
    case eLogLevel::Error: errorLog = buf; break;
    default: break;
    }
}
} // namespace

TEST_CASE("GLThread", "[gpu_gles2]")
{
    auto glThread = new gles2::GLThread{};

    delete glThread;
}
