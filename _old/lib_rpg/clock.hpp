#pragma once

#include <chrono>

namespace nopp
{
    using TimeValue = std::chrono::seconds;

    struct TimeStamp final
    {
        uint16_t years  = 1; // [1, 65535]
        uint16_t days   = 1; // [1, 65535]
        uint16_t hours  = 0; // [0, 65535]
        uint8_t minutes = 0; // [0, 255]
        uint8_t seconds = 0; // [0, 255]
    };
    TimeValue systemNow();
    TimeStamp toTimeStamp(TimeValue const& value);
    TimeValue toTimeValue(TimeStamp const& value);

    class Clock final
    {
    public:
        void start();
        void stop();
        TimeValue time();

    private:
        TimeValue _start = TimeValue::zero();
        TimeValue _stop = TimeValue::zero();
        bool _isStopped = true;
    };
}
