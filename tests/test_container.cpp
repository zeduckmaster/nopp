#include <catch2/catch.hpp>
#include <vector>
#include "nopp/container.hpp"

using namespace nopp;

TEST_CASE("sortAndRemoveDuplicates", "[container]")
{
    auto src = std::vector<int32_t>{1, 37, 1, -4, 57, -98, -3, 11, 22};
    sortAndRemoveDuplicates(src);
    REQUIRE(src.size() == 8);
    REQUIRE(src[0] == -98);
    REQUIRE(src[2] == -3);
    REQUIRE(src[4] == 11);
}

struct Data
{
    int32_t i = 0;

    Data() = default;

    Data(int32_t const value) : i(value) {}

    bool operator==(Data const& other) const { return i == other.i; }

    bool operator==(int32_t const other) const { return i == other; }
};

TEST_CASE("sortAndRemoveDuplicates with compare", "[container]")
{
    auto src = std::vector<Data>{Data{1}, Data{37}, Data{1}, Data{-4}};
    sortAndRemoveDuplicates(src, [](Data const& v1, Data const& v2) { return v1.i < v2.i; });
    REQUIRE(src.size() == 3);
    REQUIRE(src[0] == -4);
    REQUIRE(src[2] == 37);
}

TEST_CASE("contains", "[container]")
{
    auto src = std::vector<int32_t>{1, 37, 1, -4, 57, -98, -3, 11, 22};
    REQUIRE(contains(src, 37) == true);
    REQUIRE(contains(src, 38) == false);
}

TEST_CASE("swapAndPopBack", "[container]")
{
    auto src = std::vector<int32_t>{1, 37, 1, -4, 57, -98, -3, 11, 22};
    REQUIRE(swapAndPopBack(src, -98) == true);
    REQUIRE(src.size() == 8);
    REQUIRE(src[5] == 22);
    REQUIRE(swapAndPopBack(src, -98) == false);
}

TEST_CASE("swapAndPopBackIf", "[container]")
{
    auto src = std::vector<int32_t>{1, 37, 1, -4, 57, -98, -3, 11, 22};
    REQUIRE(swapAndPopBackIf(src, [](int32_t value) { return value == 57; }) == true);
    REQUIRE(src.size() == 8);
    REQUIRE(src[4] == 22);
}

TEST_CASE("append", "[container]")
{
    auto src = std::vector<int32_t>{1, 37, 1, -4, 57, -98, -3, 11, 22};
    append(src, {10, -10});
    REQUIRE(src.size() == 11);
    REQUIRE(src[9] == 10);
    REQUIRE(src[10] == -10);
}
