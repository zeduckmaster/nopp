#pragma once

#include <functional>
#include <vector>
#include <deque>
#include "clock.h"
#include "id.h"
#include "io.h"
#include "support_lua.h"

namespace nopp
{
    using FuncSessionOutput = std::function<void(std::string const&)>;
    using FuncSessionQuit = std::function<void()>;
    using FuncCommand = sol::function;

    struct SessionIO
    {
        FuncSessionOutput output;
        FuncSessionQuit quit;

        inline bool isValid() const
        {
            return (output != nullptr) && (quit != nullptr);
        }
    };

    class World
    {
    public:
        World(Path const& dataDirpath, std::string const& lang);
        Id addSession(SessionIO const& sessionIO);
        void removeSession(Id sessionId);
        void processCommand(Id sessionId, std::string const& command);
        void send(Id sessionId, std::string const& msg);
        void quit(Id sessionId);


    private:
        bool _isSessionIdValid(Id sessionId) const;

        struct _SessionInput
        {
            TimeValue time;
            std::string input;
        };
        struct _Session
        {
            SessionIO io;
            _SessionInput lastInput;
            sol::table luaTable;
            uint8_t gen = 0;
        };
        Path _dataDirpath;
        Clock _clock;
        sol::state _luaState;
        std::vector<_Session> _sessions;
        std::deque<int32_t> _sessionFreeIds;
    };
}
