TEMPLATE = app
CONFIG += c++14 link_prl
CONFIG -= qt console

SOURCES += \
main.cpp

INCLUDEPATH += \
./lib_core

LIBS += -L../lib_core/debug -llibCore
