#include <catch2/catch.hpp>
#include "nopp/size.hpp"
#include "nopp/math.hpp"

using namespace nopp;

TEST_CASE("Size2", "[size]")
{
    REQUIRE(Size2i{}.isValid() == false);
    REQUIRE(Size2i{1, 0}.isValid() == false);
    REQUIRE(Size2i{0, -10}.isValid() == false);
    REQUIRE(Size2i{-2, -1}.isValid() == false);
    REQUIRE(Size2i{1, 1}.isValid() == true);

    REQUIRE(Size2i{4, 2} == Size2i{4, 2});
    REQUIRE(Size2i{4, 2} != Size2i{4, 3});
    REQUIRE(Size2i{4, 2}.area() == 8);

    REQUIRE(Size2f{}.isValid() == false);
    REQUIRE(Size2f{1.1f, 0.0f}.isValid() == false);
    REQUIRE(Size2f{0.0f, -10.7f}.isValid() == false);
    REQUIRE(Size2f{-2.05f, -1.3f}.isValid() == false);
    REQUIRE(Size2f{1.6f, 2.91f}.isValid() == true);

    REQUIRE(Size2f{4.1f, 2.2f} == Size2f{4.1f, 2.2f});
    REQUIRE(Size2f{4.7f, 2.0f} != Size2f{4.7f, 3.1f});
    REQUIRE(almostEqual(Size2f{4.3f, 7.4f}.area(), 31.82f) == true);

    REQUIRE(Size2d{}.isValid() == false);
    REQUIRE(Size2d{3.3, 0.0}.isValid() == false);
    REQUIRE(Size2d{0.0, -8.01}.isValid() == false);
    REQUIRE(Size2d{-7.6, -1.1}.isValid() == false);
    REQUIRE(Size2d{1.1, 0.8}.isValid() == true);

    REQUIRE(Size2d{7.8, 2.1} == Size2d{7.8, 2.1});
    REQUIRE(Size2d{0.3, 2.2} != Size2d{0.3, 2.3});
    REQUIRE(almostEqual(Size2d{6.1, 7.4}.area(), 45.14) == true);
}

using CustomSize2 = Size2<uint16_t>;

TEST_CASE("custom Size2", "[size]")
{
    REQUIRE(CustomSize2{}.isValid() == false);
    REQUIRE(CustomSize2{1, 0}.isValid() == false);
    REQUIRE(CustomSize2{0, 10}.isValid() == false);
    REQUIRE(CustomSize2{1, 1}.isValid() == true);

    REQUIRE(CustomSize2{4, 2} == CustomSize2{4, 2});
    REQUIRE(CustomSize2{4, 2} != CustomSize2{4, 3});
    REQUIRE(CustomSize2{4, 2}.area() == 8);
}

TEST_CASE("Size3", "[size]")
{
    REQUIRE(Size3i{}.isValid() == false);
    REQUIRE(Size3i{1, 0, 10}.isValid() == false);
    REQUIRE(Size3i{0, -10, 7}.isValid() == false);
    REQUIRE(Size3i{-2, -1, -3}.isValid() == false);
    REQUIRE(Size3i{1, 1, 1}.isValid() == true);
    REQUIRE(Size3i{1, 0}.isValid() == false);
    REQUIRE(Size3i{0, -10}.isValid() == false);
    REQUIRE(Size3i{-2, -1}.isValid() == false);
    REQUIRE(Size3i{1, 1}.isValid() == true);

    REQUIRE(Size3i{4, 2, 7} == Size3i{4, 2, 7});
    REQUIRE(Size3i{4, 2, 7} != Size3i{4, 3, 7});
    REQUIRE(Size3i{4, 2, 6}.volume() == 48);

    REQUIRE(Size3f{}.isValid() == false);
    REQUIRE(Size3f{1.0f, 0.0f, 10.0f}.isValid() == false);
    REQUIRE(Size3f{0.0f, -10.0f, 7.0f}.isValid() == false);
    REQUIRE(Size3f{-2.0f, -1.0f, -3.0f}.isValid() == false);
    REQUIRE(Size3f{1.0f, 1.0f, 1.0f}.isValid() == true);
    REQUIRE(Size3f{1.0f, 0.0f}.isValid() == false);
    REQUIRE(Size3f{0.0f, -10.0f}.isValid() == false);
    REQUIRE(Size3f{-2.0f, -1.0f}.isValid() == false);
    REQUIRE(Size3f{1.0f, 1.0f}.isValid() == true);

    REQUIRE(Size3f{5.2f, 2.3f, 7.7f} == Size3f{5.2f, 2.3f, 7.7f});
    REQUIRE(Size3f{4.1f, 4.1f, 6.4f} != Size3f{4.1f, 4.1f, 6.3f});
    REQUIRE(almostEqual(Size3f{6.9f, 2.04f, 6.7f}.volume(), 94.3092f) == true);

    REQUIRE(Size3d{}.isValid() == false);
    REQUIRE(Size3d{1.0, 0.0, 10.0}.isValid() == false);
    REQUIRE(Size3d{0.0, -10.0, 7.0}.isValid() == false);
    REQUIRE(Size3d{-2.0, -1.0, -3.0}.isValid() == false);
    REQUIRE(Size3d{1.0, 1.0, 1.0}.isValid() == true);
    REQUIRE(Size3d{1.0, 0.0}.isValid() == false);
    REQUIRE(Size3d{0.0, -10.0}.isValid() == false);
    REQUIRE(Size3d{-2.0, -1.0}.isValid() == false);
    REQUIRE(Size3d{1.0, 1.0}.isValid() == true);

    REQUIRE(Size3d{5.63, -2.3, 0.92} == Size3d{5.63, -2.3, 0.92});
    REQUIRE(Size3d{4.4, 2.01, 1.7} != Size3d{4.41, 2.01, 1.69});
    REQUIRE(almostEqual(Size3d{6.66, 4.01, 1.203}.volume(), 32.12803) == true);
}
