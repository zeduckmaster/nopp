#include <catch2/catch.hpp>
#include "nopp/string.hpp"

using namespace nopp;

TEST_CASE("splitString", "[string]")
{
    {
        auto strs = splitString(std::string{"foo,bar"}, ",");
        REQUIRE(strs.size() == 2);
        REQUIRE(strs[0] == std::string{"foo"});
        REQUIRE(strs[1] == std::string{"bar"});
    }
    {
        auto strs = splitString(std::string{", foo , bar  , ,"}, ",");
        REQUIRE(strs.size() == 3);
        REQUIRE(strs[0] == std::string{" foo "});
        REQUIRE(strs[1] == std::string{" bar  "});
        REQUIRE(strs[2] == std::string{" "});
    }
}

TEST_CASE("joinString", "[string]") { REQUIRE(joinStrings({{"foo"}, {"bar"}}, ";") == "foo;bar"); }

TEST_CASE("toLower", "[string]") { REQUIRE(toLower("fOObAr") == "foobar"); }

TEST_CASE("toUpper", "[string]") { REQUIRE(toUpper("fOObAr") == "FOOBAR"); }

TEST_CASE("tokenizeString", "[string]")
{
    {
        auto result = tokenizeString("3+4=", "+=");
        REQUIRE(result.size() == 4);
        auto expected = {"3", "+", "4", "="};
        REQUIRE(std::equal(result.begin(), result.end(), expected.begin()) == true);
    }

    {
        auto result = tokenizeString("mov rax,1", std::vector<std::string>{"mov", ","});
        REQUIRE(result.size() == 4);
        REQUIRE(result[0] == "mov");
        REQUIRE(result[2] == ",");
    }
}

TEST_CASE("bool", "[string]")
{
    REQUIRE(toString(false) == "false");
    REQUIRE(toString(true) == "true");
    REQUIRE(toBool("true") == true);
    REQUIRE(toBool("false") == false);
}

TEST_CASE("int32_t", "[string]")
{
    REQUIRE(toString(static_cast<int32_t>(32)) == "32");
    REQUIRE(toString(static_cast<int32_t>(-452)) == "-452");
    REQUIRE(toString(std::numeric_limits<int32_t>::min()) == "-2147483648");
    REQUIRE(toString(std::numeric_limits<int32_t>::max()) == "2147483647");
    REQUIRE(toInt32("32") == 32);
    REQUIRE(toInt32("-452") == -452);
    REQUIRE(toInt32("-2147483648") == std::numeric_limits<int32_t>::min());
    REQUIRE(toInt32("2147483647") == std::numeric_limits<int32_t>::max());
}

TEST_CASE("uint32_t", "[string]")
{
    REQUIRE(toString(static_cast<uint32_t>(32)) == "32");
    REQUIRE(toString(std::numeric_limits<uint32_t>::min()) == "0");
    REQUIRE(toString(std::numeric_limits<uint32_t>::max()) == "4294967295");
    REQUIRE(toUInt32("32") == 32);
    REQUIRE(toUInt32("-452") == -452);
    REQUIRE(toUInt32("0") == std::numeric_limits<uint32_t>::min());
    REQUIRE(toUInt32("4294967295") == std::numeric_limits<uint32_t>::max());
}

TEST_CASE("int64_t", "[string]")
{
    REQUIRE(toString(static_cast<int64_t>(32)) == "32");
    REQUIRE(toString(static_cast<int64_t>(-452)) == "-452");
    REQUIRE(toString(std::numeric_limits<int64_t>::min()) == "-9223372036854775808");
    REQUIRE(toString(std::numeric_limits<int64_t>::max()) == "9223372036854775807");
    REQUIRE(toInt64("32") == 32);
    REQUIRE(toInt64("-452") == -452);
    REQUIRE(toInt64("-9223372036854775808") == std::numeric_limits<int64_t>::min());
    REQUIRE(toInt64("9223372036854775807") == std::numeric_limits<int64_t>::max());
}

TEST_CASE("float", "[string]")
{
    REQUIRE(toString(static_cast<float>(3.2f)) == "3.2");
    REQUIRE(toString(static_cast<float>(-0.452f)) == "-0.452");
    REQUIRE(toFloat("3.2") == 3.2f);
    REQUIRE(toFloat("-0.452") == -0.452f);
}

TEST_CASE("double", "[string]")
{
    REQUIRE(toString(static_cast<double>(3.2)) == "3.2");
    REQUIRE(toString(static_cast<double>(-0.452)) == "-0.452");
    REQUIRE(toDouble("3.2") == 3.2);
    REQUIRE(toDouble("-0.452") == -0.452);
}
