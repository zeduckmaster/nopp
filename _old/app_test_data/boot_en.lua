Errors =
{
   InvalidCommand = "Invalid command: ",
   InvalidArgument = "Invalid argument"
}

CommandAliases =
{
   help = "help",
   quit = "quit",
   q = "quit"
}

CommandDescriptions =
{
   help = "",
   quit = "Quit application."
}