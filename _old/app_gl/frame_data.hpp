#pragma once

#include <any>
#include <cstdint>
#include <lib_core/api.hpp>
#include <lib_display/api.hpp>

struct RenderObject
{
    nopp::Id mesh;
    nopp::Id textureColor;
};

struct RenderBin
{
    std::vector<RenderObject> objects;
};

struct FrameData
{
    int32_t id = 0;
    double time = 0.0;
    double timeDelta = 0.0;
    nopp::Vec2 windowSize;
    nopp::Vec2 mousePos;
    bool mouseLeft = false;
    bool mouseRight = false;
    bool keyCtrl = false;
    bool keyShift = false;
    bool keyAlt = false;
    bool keysDown[512];
    std::string textInput;
    std::unordered_map<std::string, RenderBin> renderBins;
    std::unordered_map<std::string, std::any> renderData;

    void prepareNext(FrameData const& previous);

    void copyTo(ImGuiIO& io) const;
};

void loadLuaFrameData(sol::state& luaState);
