#pragma once

#include "math.hpp"

namespace nopp
{

struct Frustum final
{
    enum class eType : uint32_t
    {
        Perspective = 0,
        Orthographic
    };

    float left = -0.5f;
    float right = 0.5f;
    float bottom = -0.5f;
    float top = 0.5f;
    float znear = 1.0f;
    float zfar = 1000.0f;
    eType type = eType::Perspective;

    constexpr Frustum() = default;

    constexpr Frustum(
        float left, float right, float bottom, float top, float znear, float zfar, eType type)
        : left(left), right(right), bottom(bottom), top(top), znear(znear), zfar(zfar), type(type)
    {
    }

    constexpr float fovh() const { return std::atan((right - left) * 0.5f / znear) * 2.0f; }

    constexpr float fovv() const { return std::atan((top - bottom) * 0.5f / znear) * 2.0f; }

    constexpr float aspectRatio() const { return right / top; }

    // Compute the projection matrix represented by this Frustum
    // The projection is computed according to OpenGL standard, i.e.: z is [-1; 1]
    Mat4 matProj() const
    {
        auto rml = right - left;
        auto rpl = right + left;
        auto tmb = top - bottom;
        auto tpb = top + bottom;
        auto invnmf = 1.0f / (znear - zfar);
        auto m = Mat4(Mat4::Zero());
        switch (type)
        {
        case Frustum::eType::Perspective:
            m(0) = 2.0f * znear / rml;
            m(5) = 2.0f * znear / tmb;
            m(8) = rpl / rml;
            m(9) = tpb / tmb;
            m(10) = (zfar + znear) * invnmf;
            m(11) = -1.0f;
            m(14) = 2.0f * zfar * znear * invnmf;
            break;
        case Frustum::eType::Orthographic:
            m(0) = 2.0f / rml;
            m(5) = 2.0f / tmb;
            m(10) = 2.0f * invnmf;
            m(12) = -rpl / rml;
            m(13) = -tpb / tmb;
            m(14) = (znear + zfar) * invnmf;
            m(15) = 1.0f;
            break;
        default: break;
        }
        return m;
    }

    // Compute the projection matrix represented by this Frustum
    // Do an alternative matrix projection computation, i.e.: z is [0; 1]
    Mat4 matProjAlt() const
    {
        auto rml = right - left;
        auto rpl = right + left;
        auto tmb = top - bottom;
        auto tpb = top + bottom;
        auto invnmf = 1.0f / (znear - zfar);
        auto m = Mat4(Mat4::Zero());
        switch (type)
        {
        case Frustum::eType::Orthographic:
            m(0) = 2.0f / rml;
            m(5) = 2.0f / tmb;
            m(10) = invnmf;
            m(12) = -rpl / rml;
            m(13) = -tpb / tmb;
            m(14) = (znear)*invnmf;
            m(15) = 1.0f;
            break;
        case Frustum::eType::Perspective:
            m(0) = 2.0f * znear / rml;
            m(5) = 2.0f * znear / tmb;
            m(8) = rpl / rml;
            m(9) = tpb / tmb;
            m(10) = zfar * invnmf;
            m(11) = -1.0f;
            m(14) = zfar * znear * invnmf;
            break;
        default: break;
        }
        return m;
    }
};

inline Mat4 computeMatProj(Frustum const& frustum) { return frustum.matProj(); }

inline Mat4 computeMatProjAlt(Frustum const& frustum) { return frustum.matProjAlt(); }

// A helper function to get a Frustum from a horizontal field of view parameters set
inline Frustum computeProjPerspFOVH(float fovhRad, float aspectRatio, float znear, float zfar)
{
    auto right = znear * std::tan(fovhRad * 0.5f);
    auto left = -right;
    auto top = right / aspectRatio;
    auto bottom = -top;
    return Frustum{left, right, bottom, top, znear, zfar, Frustum::eType::Perspective};
}

// A helper function to get a Frustum from a vertical field of view parameters set
inline Frustum computeProjPerspFOVV(float fovvRad, float aspectRatio, float znear, float zfar)
{
    auto top = znear * std::tan(fovvRad * 0.5f);
    auto bottom = -top;
    auto right = top * aspectRatio;
    auto left = -right;
    return Frustum(left, right, bottom, top, znear, zfar, Frustum::eType::Perspective);
}

} // namespace nopp
