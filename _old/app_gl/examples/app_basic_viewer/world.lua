--------------------------------------------------------------------------------
-- globals

local gridMesh = nil
local mesh = nil
local camera = nil
local texture = nil

function sign(x)
  return (x < 0 and -1) or 1
end

local onWindowResize = function()
  if (camera == nil) then
    return
  end
  local frame = frameData()
  local width = frame.windowSize.x
  local height = frame.windowSize.y
  camera:setViewport(Viewport(0, 0, width, height))
  print("resize: " .. width .. "x" .. height)
  frame:setRenderData("matProj", camera:matProj())
end

--------------------------------------------------------------------------------
-- world

local worldInitialize = function()
  print("worldInitialize")
 
  gridMesh = MeshData()
  MeshGridDesc():create(gridMesh)
  
  mesh = MeshData(dataPath() .. "/../common/monkey.obj")
  --MeshCubeDesc():create(mesh)
  mesh:computeWireframe()
  
  texture = TextureData(dataPath() .. "/../../../../CesiumLogoFlat.ktx2")
  
  camera = CameraEditorSimple()
  camera:lookAt(Vec3(0, -10, 0), Vec3(0, 0, 0), Vec3(0, 0, 1))
  camera:setPerspFOVV(45, 1, 0.01, 1000.0)
  onWindowResize()
  
  local frame = frameData()
  frame:setRenderData("matView", camera:matView())
  frame:setRenderData("matProj", camera:matProj())
end

local worldUpdate = function()
  local frame = frameData()
  frame:setRenderData("matView", camera:matView())
  frame:addRenderObject("vcolor", gridMesh.id)
  frame:addRenderObject("mesh", mesh.id)
end

local worldTerminate = function()
  print("worldTerminate")
  gridMesh = nil
end

setWorld(worldInitialize, worldUpdate, worldTerminate)

--------------------------------------------------------------------------------
-- events

setWindowResize(onWindowResize)

local onMousePress = function()
  local frame = frameData()
  if frame.mouseLeft == true then
    camera:onMousePress(frame.mousePos, frame.keyCtrl, frame.keyShift, frame.keyAlt)
  end
end
setMousePress(onMousePress)

local onMouseMove = function()
  local frame = frameData()
  camera:onMouseMove(frame.mousePos)
end
setMouseMove(onMouseMove)

local onMouseRelease = function()
  local frame = frameData()
  camera:onMouseRelease(frame.mousePos)
end
setMouseRelease(onMouseRelease)