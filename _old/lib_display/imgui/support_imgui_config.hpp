#pragma once

#define IMGUI_DISABLE_INCLUDE_IMCONFIG_H

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS

#define IMGUI_DISABLE_WIN32_DEFAULT_CLIPBOARD_FUNCTIONS
#define IMGUI_DISABLE_WIN32_DEFAULT_IME_FUNCTIONS
#define IMGUI_DISABLE_WIN32_FUNCTIONS
#define IMGUI_DISABLE_DEFAULT_FILE_FUNCTIONS

#define IM_VEC2_CLASS_EXTRA                                                    \
    ImVec2(nopp::Vec2 const& v) : x{v.x()}, y{v.y()} {}                        \
    operator nopp::Vec2() const { return nopp::Vec2{x, y}; }

#define IM_VEC4_CLASS_EXTRA                                                    \
    ImVec4(nopp::Vec4 const& v) : x{v.x()}, y{v.y()}, z{v.z()}, w{v.w()} {}    \
    operator nopp::Vec4() const { return nopp::Vec4{x, y, z, w}; }
