#include "support_lua.hpp"
#include "../id.hpp"
#include "../io.hpp"
#include "../log.hpp"

using namespace nopp;

namespace
{
}

namespace nopp
{
void loadLuaCoreMath(sol::state& luaState);
}

int32_t nopp::luaPrint(lua_State* L)
{
    assert(L != nullptr);
    auto nargs = lua_gettop(L);
    lua_getglobal(L, "tostring");
    auto ret = std::string{};
    // make sure you start at 1 *NOT* 0
    for (auto i = 1; i <= nargs; ++i)
    {
        lua_pushvalue(L, -1);
        lua_pushvalue(L, i);
        lua_call(L, 1, 1);
        char const* s{lua_tostring(L, -1)};
        // if(s == NULL)
        //	return luaL_error(_L, LUA_QL("tostring") " must return a string
        // to ", LUA_QL("print"));
        if (i > 1)
        {
            ret.push_back('t');
        }
        ret.append(s);
        lua_pop(L, 1);
    }
    logInfo("[LUA]%s", ret.c_str());
    return 0;
}

sol::protected_function_result nopp::runLuaScript(sol::state& luaState,
                                                  std::string const& script)
{
    auto ret = luaState.safe_script(script.c_str());
    if (ret.valid() == false)
    {
        sol::error err = ret;
        logError("[LUA]%s", err.what());
    }
    return ret;
}

void nopp::loadLuaCore(sol::state& luaState)
{
    // id
    {
        luaState.new_usertype<Id>("Id", sol::no_constructor);
    }

    // eIOResult
    {
        luaState.new_enum("eIOResult",
                          "OK",
                          eIOResult::OK,
                          "InvalidFile",
                          eIOResult::InvalidFile,
                          "InvalidFileFormat",
                          eIOResult::InvalidFileFormat);
    }

    loadLuaCoreMath(luaState);
}
