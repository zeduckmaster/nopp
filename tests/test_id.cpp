#include <catch2/catch.hpp>
#include "nopp/id.hpp"

using namespace nopp;

TEST_CASE("IdInvalid", "[id]") { REQUIRE(Id{} == IdInvalid); }

TEST_CASE("Id", "[id]")
{
    auto testId = Id::make(31, 7);
    REQUIRE(testId.index() == 31);
    REQUIRE(testId.gen() == 7);
    REQUIRE(testId.value() == 0x0700001F);
    REQUIRE(testId == Id{0x0700001F});
}

TEST_CASE("SimpleIdDataManager", "[id]")
{
    auto manager = SimpleIdDataManager<double>{};
    auto id = manager.add(0.2);
    REQUIRE(manager.isIdValid(id) == true);
    REQUIRE(manager.size() == 1);
    REQUIRE(manager.data(id) == 0.2);
    manager.data(id) = 0.7;
    REQUIRE(manager.data(id) == 0.7);
    auto value = manager.remove(id);
    REQUIRE(value == 0.7);
    REQUIRE(manager.isIdValid(id) == false);
}
