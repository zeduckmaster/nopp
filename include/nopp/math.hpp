#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <cmath>
#include <vector>

namespace nopp
{

using Vec2 = Eigen::Vector2f;
using Vec3 = Eigen::Vector3f;
using Vec4 = Eigen::Vector4f;
using Mat3 = Eigen::Matrix3f;
using Mat4 = Eigen::Matrix4f;

using Aff3 = Eigen::Affine3f;
using Quat = Eigen::Quaternionf;

using Plane = Eigen::Hyperplane<float, 3>;

using Vec2i = Eigen::Matrix<int32_t, 2, 1, 0, 2, 1>;
using Vec3i = Eigen::Matrix<int32_t, 3, 1, 0, 3, 1>;
using Vec4i = Eigen::Matrix<int32_t, 4, 1, 0, 4, 1>;

constexpr auto const Log2 = std::log(2.0f);
constexpr auto const Pi = 3.1415926535897932f;
constexpr auto const HalfPi = 1.5707963267948966f;
constexpr auto const TwoPi = 6.2831853071795865f;
constexpr auto const InvPi = 1.0f / Pi;
constexpr auto const ToRadian = Pi / 180.0f;
constexpr auto const ToDegree = 180.0f / Pi;
constexpr auto const Epsilon = 1e-05f;

template <typename T>
constexpr bool almostEqual(T a, T b)
{
    static_assert(std::is_floating_point<T>::value);
    if (std::abs(a - b) <= Epsilon)
    {
        return true;
    }
    return std::abs(a - b) <= (Epsilon * std::max(std::abs(a), std::abs(b)));
}

inline bool almostEqual(Vec2 const& a, Vec2 const& b) { return a.isApprox(b, Epsilon); }

inline bool almostEqual(Vec3 const& a, Vec3 const& b) { return a.isApprox(b, Epsilon); }

inline bool almostEqual(Vec4 const& a, Vec4 const& b) { return a.isApprox(b, Epsilon); }

template <typename T>
constexpr T lerp(T const& a, T const& b, float w)
{
    return (a + (b - a) * w);
}

template <typename T>
constexpr T clamp(T value, T min, T max)
{
    return std::min<T>(std::max<T>(value, min), max);
}

template <typename T>
constexpr T saturate(T value)
{
    static_assert(std::is_floating_point<T>::value);
    return clamp(value, static_cast<T>(0), static_cast<T>(1));
}

inline Vec2 saturate(Vec2 const& value) { return Vec2(saturate(value.x()), saturate(value.y())); }

inline Vec3 saturate(Vec3 const& value)
{
    return Vec3(saturate(value.x()), saturate(value.y()), saturate(value.z()));
}

inline Vec4 saturate(Vec4 const& value)
{
    return Vec4(saturate(value.x()), saturate(value.y()), saturate(value.z()), saturate(value.w()));
}

inline float frac(float v)
{
    auto intpart = 0.0f;
    return std::modf(v, &intpart);
}

inline Vec2 frac(Vec2 const& v) { return Vec2(frac(v.x()), frac(v.y())); }

inline Vec3 frac(Vec3 const& v) { return Vec3(frac(v.x()), frac(v.y()), frac(v.z())); }

inline Vec4 frac(Vec4 const& v) { return Vec4(frac(v.x()), frac(v.y()), frac(v.z()), frac(v.w())); }

constexpr float log2(float value) { return std::log(value) / Log2; }

constexpr uint32_t reverseBits(uint32_t value)
{
    value = ((value >> 1) & 0x55555555) | ((value & 0x55555555) << 1);
    value = ((value >> 2) & 0x33333333) | ((value & 0x33333333) << 2);
    value = ((value >> 4) & 0x0F0F0F0F) | ((value & 0x0F0F0F0F) << 4);
    value = ((value >> 8) & 0x00FF00FF) | ((value & 0x00FF00FF) << 8);
    value = (value >> 16) | (value << 16);
    return value;
}

// from https://github.com/jhjourdan/SIMD-math-prims/blob/master/simd_math_prims.h
inline float fastexp(float val)
{
    constexpr auto const exp_cst1 = 2139095040.0f;
    constexpr auto const exp_cst2 = 0.0f;
    auto val2 = 12102203.1615614f * val + 1065353216.0f;
    auto val3 = (val2 < exp_cst1) ? val2 : exp_cst1;
    auto val4 = (val3 > exp_cst2) ? val3 : exp_cst2;
    auto val4i = static_cast<int32_t>(val4);
    union
    {
        int32_t i;
        float f;
    } xu, xu2;
    xu.i = val4i & 0x7F800000;
    xu2.i = (val4i & 0x7FFFFF) | 0x3F800000;
    auto b = xu2.f;
    return xu.f *
           (0.509964287281036376953125f + b * (0.3120158612728118896484375f +
                                               b * (0.1666135489940643310546875f +
                                                    b * (-2.12528370320796966552734375e-3f +
                                                         b * 1.3534179888665676116943359375e-2f))));
}

// compute the transform matrix from one axis system to another
inline Mat3 computeMatAxisTransform(Vec3 const& axisX, Vec3 const& axisY, Vec3 const& axisZ)
{
    auto m = Mat3();
    m.block<1, 3>(0, 0) = axisX;
    m.block<1, 3>(1, 0) = axisY;
    m.block<1, 3>(2, 0) = axisZ;
    return m;
}

// get matrix transform from y-up coordinate system to z-up coordinate system
inline Mat3 matYUpToZUp()
{
    auto m = Mat3(Mat3::Zero());
    m(0) = 1.0f;
    m(5) = 1.0f;
    m(7) = -1.0f;
    return m;
}

inline float distance(Vec2 const& p0, Vec2 const& p1) { return Vec2(p0 - p1).norm(); }

inline float distance(Vec3 const& p0, Vec3 const& p1) { return Vec3(p0 - p1).norm(); }

inline float area(Vec3 const& p0, Vec3 const& p1, Vec3 const& p2)
{
    return 0.5f * ((p2 - p0).cross(p2 - p1)).norm();
}

// mat view is the invert of mat world
inline Mat4 computeMatView(Mat4 const& matWorld)
{
    auto m = Mat4();
    m(0) = matWorld(0);
    m(1) = matWorld(4);
    m(2) = matWorld(8);
    m(3) = 0.0f;
    m(4) = matWorld(1);
    m(5) = matWorld(5);
    m(6) = matWorld(9);
    m(7) = 0.0f;
    m(8) = matWorld(2);
    m(9) = matWorld(6);
    m(10) = matWorld(10);
    m(11) = 0.0f;
    m(12) = -(matWorld(0) * matWorld(12) + matWorld(1) * matWorld(13) + matWorld(2) * matWorld(14));
    m(13) = -(matWorld(4) * matWorld(12) + matWorld(5) * matWorld(13) + matWorld(6) * matWorld(14));
    m(14) =
        -(matWorld(8) * matWorld(12) + matWorld(9) * matWorld(13) + matWorld(10) * matWorld(14));
    m(15) = 1.0f;
    return m;
}

//
// struct Ray
//{
//     Vec3 start;
//     Vec3 end;
// };
// inline Ray computeViewportToRay(Vec2 const& viewportPoint, Mat4 const& matView, Mat4 const&
// matProj,
//                                 Viewport const& viewport)
//{
//     auto x = (viewportPoint.x() / viewport.width) * 2.0f - 1.0f;
//     auto y = 1.0f - (viewportPoint.y() / viewport.height) * 2.0f;
//     auto f = Vec4{x, y, -1.0f, 1.0f};
//     auto t = f;
//     t.z() = 1.0f;
//     auto mat1 = matProj * matView;
//     auto mat = mat1.inverse();
//     f = mat * f;
//     auto w = 1.0f / f.w();
//     auto start = Vec3{f.x() * w, f.y() * w, f.z() * w};
//     t = mat * t;
//     w = 1.0f / t.w();
//     auto end = Vec3{t.x() * w, t.y() * w, t.z() * w};
//     return Ray{start, end};
// }

inline Vec3 cameraWorldRight(Mat4 const& matView) { return matView.block<1, 3>(0, 0); }

inline Vec3 cameraWorldUp(Mat4 const& matView) { return matView.block<1, 3>(1, 0); }

inline Vec3 cameraWorldDir(Mat4 const& matView) { return matView.block<1, 3>(2, 0) * -1.0f; }

// computed in view space, according to OpenGL standard, i.e.: z is [-1; 1]
inline std::vector<Plane> cameraFrustumPlanes(Mat4 const& matProj)
{
    auto& m = matProj;
    auto fp = std::vector<Plane>();
    fp.reserve(6);
    // left
    {
        auto a = m(3, 0) + m(0, 0);
        auto b = m(3, 1) + m(0, 1);
        auto c = m(3, 2) + m(0, 2);
        auto d = m(3, 3) + m(0, 3);
        auto p = Plane(Vec3(a, b, c), d);
        p.normalize();
        fp.emplace_back(p);
    }
    // right
    {
        auto a = m(3, 0) - m(0, 0);
        auto b = m(3, 1) - m(0, 1);
        auto c = m(3, 2) - m(0, 2);
        auto d = m(3, 3) - m(0, 3);
        auto p = Plane(Vec3(a, b, c), d);
        p.normalize();
        fp.emplace_back(p);
    }
    // bottom
    {
        auto a = m(3, 0) + m(1, 0);
        auto b = m(3, 1) + m(1, 1);
        auto c = m(3, 2) + m(1, 2);
        auto d = m(3, 3) + m(1, 3);
        auto p = Plane(Vec3(a, b, c), d);
        p.normalize();
        fp.emplace_back(p);
    }
    // top
    {
        auto a = m(3, 0) - m(1, 0);
        auto b = m(3, 1) - m(1, 1);
        auto c = m(3, 2) - m(1, 2);
        auto d = m(3, 3) - m(1, 3);
        auto p = Plane(Vec3(a, b, c), d);
        p.normalize();
        fp.emplace_back(p);
    }
    // near
    {
        auto a = m(3, 0) + m(2, 0);
        auto b = m(3, 1) + m(2, 1);
        auto c = m(3, 2) + m(2, 2);
        auto d = m(3, 3) + m(2, 3);
        auto p = Plane(Vec3(a, b, c), d);
        p.normalize();
        fp.emplace_back(p);
    }
    // far
    {
        auto a = m(3, 0) - m(2, 0);
        auto b = m(3, 1) - m(2, 1);
        auto c = m(3, 2) - m(2, 2);
        auto d = m(3, 3) - m(2, 3);
        auto p = Plane(Vec3(a, b, c), d);
        p.normalize();
        fp.emplace_back(p);
    }
    return fp;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// rendering

constexpr float convertSRGBToLinearApprox(float value) { return std::pow(value, 2.2f); }

inline Vec3 convertSRGBToLinearApprox(Vec3 const& value)
{
    return Vec3{convertSRGBToLinearApprox(value.x()),
                convertSRGBToLinearApprox(value.y()),
                convertSRGBToLinearApprox(value.z())};
}

constexpr float convertSRGBToLinearFast(float value) { return value * value; }

inline Vec3 convertSRGBToLinearFast(Vec3 const& value)
{
    return Vec3{convertSRGBToLinearFast(value.x()),
                convertSRGBToLinearFast(value.y()),
                convertSRGBToLinearFast(value.z())};
}

constexpr float convertLinearToSRGBApprox(float value)
{
    return std::pow(value, 1.0f / 2.2f);
}

inline Vec3 convertLinearToSRGBApprox(Vec3 const& value)
{
    return Vec3{convertLinearToSRGBApprox(value.x()),
                convertLinearToSRGBApprox(value.y()),
                convertLinearToSRGBApprox(value.z())};
}

constexpr float convertLinearToSRGBFast(float value)
{
    return std::sqrt(std::max(value, 0.0f));
}

inline Vec3 convertLinearToSRGBFast(Vec3 const& value)
{
    return Vec3{convertLinearToSRGBFast(value.x()),
                convertLinearToSRGBFast(value.y()),
                convertLinearToSRGBFast(value.z())};
}

constexpr uint32_t nearestPow2(uint32_t value)
{
    auto j = uint32_t(0);
    auto k = uint32_t(0);
    (j = value & 0xFFFF0000) || (j = value);
    (k = j & 0xFF00FF00) || (k = j);
    (j = k & 0xF0F0F0F0) || (j = k);
    (k = j & 0xCCCCCCCC) || (k = j);
    (j = k & 0xAAAAAAAA) || (j = k);
    // return j << 1;
    return j;
}

// assumption: x >= 1
constexpr bool isPow2(int32_t x) { return x == (x & -x); }

} // namespace nopp
