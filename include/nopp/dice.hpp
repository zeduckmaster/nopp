#pragma once

#include <cassert>
#include <random>
#include "evaluate.hpp"

namespace nopp
{

// Roll a dice from [1 .. faceCount]
inline int32_t rollDice(int32_t faceCount)
{
    assert(faceCount > 0);
    static std::mt19937 rng(time(nullptr));
    std::uniform_int_distribution<int32_t> distribution(1, faceCount);
    return distribution(rng);
}

// Roll a dice expression
// Expression can contain :
// - integer positive value
// - operator like * + - /
// - dice ttrpg definition like <number>d<number>, for example 3d6 -> 3 dice with 6 faces
// The result is always >= 0
inline int32_t roll(std::string const& expression)
{
    // convert dices to values
    auto expr =
        regexReplace(expression,
                     std::regex{"[0-9]*d[0-9]+", std::regex::icase | std::regex::ECMAScript},
                     [=](std::smatch const& value)
                     {
                         auto strs = splitString(value.str(), "d");
                         auto mult = 1;
                         auto dice = 6;
                         if (strs.size() > 1)
                         {
                             mult = toInt32(strs[0]);
                             dice = toInt32(strs[1]);
                         }
                         else
                         {
                             dice = toInt32(strs[0]);
                         }
                         auto ret = std::string("(");
                         for (auto n = 0; n < mult; ++n)
                         {
                             ret.append(toString(rollDice(dice)));
                             if (n != (mult - 1))
                             {
                                 ret.push_back('+');
                             }
                         }
                         ret.push_back(')');
                         return ret;
                     });
    // evaluate expression
    return std::max(evaluate(expr), 0);
}

} // namespace nopp
