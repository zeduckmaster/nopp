-------------------------------------------------------------------------------
-- Session

Session = { id = 0xffffffff }

function Session:new(o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function Session:send(msg)
   send(self.id, msg)
end

function Session:quit()
   quit(self.id)
end

-------------------------------------------------------------------------------
-- Command

Command = { description = "" }

function Command:new(o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function Command:f()
   print("Command '"..values.."' not implemented")
end

local function runScript(filepath)
   local script = readFile(filepath)
   load(script)()
end

-------------------------------------------------------------------------------
-- Additional lua script files

runScript(dataDirpath.."room.lua")
runScript(dataDirpath.."character.lua")

-------------------------------------------------------------------------------
-- global data

rooms = {}

function findRoom(roomName)
   local room = rooms[roomName]
   if (room ~= nil) then
      return room
   end
   local filepath = dataDirpath.."rooms/"..roomName..".lua"
   local script = readFile(filepath)
   room = load(script)()
   rooms[roomName] = room
   return room
end

function move(session, roomName)
	local room = findRoom(roomName)
   if (room == nil) then
      print("Can't find room: "..roomName)
      return
   end
   if (session.room ~= nil) then
      session.room:exit(session)
   end
   session.room = room
   room:enter(session)
end

function splitString(str, sep)
   if (sep == nil) then
      sep = "%s"
   end
   local t = {}
   for m in string.gmatch(str, "([^"..sep.."]*)("..sep.."?)") do
      table.insert(t, m)
   end
   return t
end

-------------------------------------------------------------------------------
-- global functions/init

do
   local bootLangFilepath = dataDirpath.."boot_"..lang..".lua"
   local bootLangScript = readFile(bootLangFilepath)
   load(bootLangScript)()
end

function newSession(idValue)
   local st = Session:new{id = idValue}
   move(st, "welcome")
   return st
end

function processCommand(session, command)
   local strs = splitString(command)
   session.room:processCommand(session, strs)
end


