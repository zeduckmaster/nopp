#include "support_sdl.hpp"

using namespace nopp;

void nopp::sdlLogFunction(eLogLevel logLevel, char const* fmt, va_list argList)
{
    auto priority = SDL_LOG_PRIORITY_VERBOSE;
    switch (logLevel)
    {
    case eLogLevel::Info: priority = SDL_LOG_PRIORITY_INFO; break;
    case eLogLevel::Warning: priority = SDL_LOG_PRIORITY_WARN; break;
    case eLogLevel::Debug: priority = SDL_LOG_PRIORITY_DEBUG; break;
    case eLogLevel::Error: priority = SDL_LOG_PRIORITY_ERROR; break;
    default: break;
    }
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, priority, fmt, argList);
}
