#pragma once

#include "enum.hpp"
#include "log.hpp"
#include "string.hpp"

namespace nopp
{

using Real = float;

inline Real toReal(std::string const& value) { return toFloat(value); }

template <typename T>
struct Quantity
{
    using UnitType = T;

    Real value = 0;
    T unit = T::unitless;
    // std::string expression;

    constexpr Quantity() = default;

    constexpr Quantity(Real value) : value(value) {}

    constexpr Quantity(T unit) : unit(unit) {}

    constexpr Quantity(Real value, T unit) : value(value), unit(unit) {}

    // Quantity(QString const& expression)
    //	: expression{expression}
    //{
    //}

    Quantity operator%(T const& dstUnit) const
    {
        if ((unit == T::unitless) || (unit == dstUnit))
        {
            return Quantity(value, dstUnit);
        }
        return Quantity(value * findConversionUnitScale(unit, dstUnit), dstUnit);
    }

    Quantity& operator=(Real const& ivalue)
    {
        value = ivalue;
        return *this;
    }
    // inline Quantity& operator=(QString const& iexpression)
    //{
    //	expression = iexpression;
    //	return *this;
    //}
};

template <typename T>
Quantity<T> operator+(Quantity<T> const& lhs, Real const& rhs)
{
    return Quantity<T>(lhs.value + rhs, lhs.unit);
}

template <typename T>
Quantity<T> operator+(Real const& lhs, Quantity<T> const& rhs)
{
    return Quantity<T>(lhs + rhs.value, rhs.unit);
}

template <typename T>
Quantity<T> operator-(Quantity<T> const& lhs, Real const& rhs)
{
    return Quantity<T>(lhs.value + rhs, lhs.unit);
}

template <typename T>
Quantity<T> operator-(Real const& lhs, Quantity<T> const& rhs)
{
    return Quantity<T>(lhs + rhs.value, rhs.unit);
}

template <typename T>
Quantity<T> operator*(Quantity<T> const& lhs, Real const& rhs)
{
    return Quantity<T>(lhs.value * rhs, lhs.unit);
}

template <typename T>
Quantity<T> operator*(Real const& lhs, Quantity<T> const& rhs)
{
    return Quantity<T>(lhs * rhs.value, rhs.unit);
}

template <typename T>
Quantity<T> operator/(Quantity<T> const& lhs, Real const& rhs)
{
    return Quantity<T>(lhs.value / rhs, lhs.unit);
}

template <typename T>
Quantity<T> operator/(Real const& lhs, Quantity<T> const& rhs)
{
    return Quantity<T>(lhs / rhs.value, rhs.unit);
}

// template<typename T> Quantity<T> operator%(Real const& value, typename
// Quantity<T>::UnitType unitType) { return Quantity<T>{value, unitType}; }

template <typename T>
Quantity<T> add(Quantity<T> const& lhs, Quantity<T> const& rhs, T dstUnit)
{
    return Quantity<T>((lhs % dstUnit).value + (rhs % dstUnit).value, dstUnit);
}

template <typename T>
Quantity<T> sub(Quantity<T> const& lhs, Quantity<T> const& rhs, T dstUnit)
{
    return Quantity<T>((lhs % dstUnit).value - (rhs % dstUnit).value, dstUnit);
}

namespace impl
{

template <typename T>
constexpr uint32_t makeKey(T src, T dst)
{
    return (toType(src) << 16) | (toType(dst));
}

template <typename T>
Real findConversionScale(std::unordered_map<uint32_t, Real> const& table, T srcUnit, T dstUnit)
{
    if ((srcUnit == dstUnit) || (srcUnit == T::unitless) || (dstUnit == T::unitless))
    {
        return 1.0;
    }
    auto iter = table.find(makeKey(srcUnit, dstUnit));
    if (iter != table.cend())
    {
        return iter->second;
    }
    return 0.0;
}

} // namespace impl

////////////////////////////////////////////////////////////////////////////////////////////////////
// length

enum class eLengthUnit : uint16_t
{
    unitless = 0,
    m,
    km,
    cm,
    mm,
    um,
    in, // international inch
    ft, // international foot
    yd, // international yard
    mi  // international mile
};

namespace impl
{
inline auto const mapLengthConvTable = std::unordered_map<uint32_t, Real>{
    {makeKey(eLengthUnit::m, eLengthUnit::km), 0.001},
    {makeKey(eLengthUnit::m, eLengthUnit::cm), 100.0},
    {makeKey(eLengthUnit::m, eLengthUnit::mm), 1000.0},
    {makeKey(eLengthUnit::m, eLengthUnit::um), 1000000.0},
    {makeKey(eLengthUnit::m, eLengthUnit::in), 39.3700787402},
    {makeKey(eLengthUnit::m, eLengthUnit::ft), 3.28083989501},
    {makeKey(eLengthUnit::m, eLengthUnit::yd), 1.09361329834},
    {makeKey(eLengthUnit::m, eLengthUnit::mi), 6.21371192237e-4},

    {makeKey(eLengthUnit::km, eLengthUnit::m), 1000.0},
    {makeKey(eLengthUnit::km, eLengthUnit::cm), 100000.0},
    {makeKey(eLengthUnit::km, eLengthUnit::mm), 1000000.0},
    {makeKey(eLengthUnit::km, eLengthUnit::um), 1000000000.0},
    {makeKey(eLengthUnit::km, eLengthUnit::in), 39370.0787402},
    {makeKey(eLengthUnit::km, eLengthUnit::ft), 3280.83989501},
    {makeKey(eLengthUnit::km, eLengthUnit::yd), 1093.61329834},
    {makeKey(eLengthUnit::km, eLengthUnit::mi), 0.621371192237},

    {makeKey(eLengthUnit::cm, eLengthUnit::m), 0.01},
    {makeKey(eLengthUnit::cm, eLengthUnit::km), 1e-5},
    {makeKey(eLengthUnit::cm, eLengthUnit::mm), 10.0},
    {makeKey(eLengthUnit::cm, eLengthUnit::um), 10000.0},
    {makeKey(eLengthUnit::cm, eLengthUnit::in), 0.393700787402},
    {makeKey(eLengthUnit::cm, eLengthUnit::ft), 0.0328083989501},
    {makeKey(eLengthUnit::cm, eLengthUnit::yd), 0.0109361329834},
    {makeKey(eLengthUnit::cm, eLengthUnit::mi), 6.21371192237e-6},

    {makeKey(eLengthUnit::mm, eLengthUnit::m), 1e-3},
    {makeKey(eLengthUnit::mm, eLengthUnit::km), 1e-6},
    {makeKey(eLengthUnit::mm, eLengthUnit::cm), 0.1},
    {makeKey(eLengthUnit::mm, eLengthUnit::um), 1000.0},
    {makeKey(eLengthUnit::mm, eLengthUnit::in), 0.0393700787402},
    {makeKey(eLengthUnit::mm, eLengthUnit::ft), 3.28083989501e-3},
    {makeKey(eLengthUnit::mm, eLengthUnit::yd), 1.09361329834e-3},
    {makeKey(eLengthUnit::mm, eLengthUnit::mi), 6.21371192237e-7},

    {makeKey(eLengthUnit::um, eLengthUnit::m), 1e-6},
    {makeKey(eLengthUnit::um, eLengthUnit::km), 1e-9},
    {makeKey(eLengthUnit::um, eLengthUnit::cm), 1e-4},
    {makeKey(eLengthUnit::um, eLengthUnit::mm), 1e-3},
    {makeKey(eLengthUnit::um, eLengthUnit::in), 3.93700787402e-5},
    {makeKey(eLengthUnit::um, eLengthUnit::ft), 3.28083989501e-6},
    {makeKey(eLengthUnit::um, eLengthUnit::yd), 1.09361329834e-6},
    {makeKey(eLengthUnit::um, eLengthUnit::mi), 6.21371192237e-10},

    {makeKey(eLengthUnit::in, eLengthUnit::m), 0.0254},
    {makeKey(eLengthUnit::in, eLengthUnit::km), 2.54e-5},
    {makeKey(eLengthUnit::in, eLengthUnit::cm), 2.54},
    {makeKey(eLengthUnit::in, eLengthUnit::mm), 25.4},
    {makeKey(eLengthUnit::in, eLengthUnit::um), 25400.0},
    {makeKey(eLengthUnit::in, eLengthUnit::ft), 0.0833333333333},
    {makeKey(eLengthUnit::in, eLengthUnit::yd), 0.0277777777778},
    {makeKey(eLengthUnit::in, eLengthUnit::mi), 1.57828282828e-5},

    {makeKey(eLengthUnit::ft, eLengthUnit::m), 0.3048},
    {makeKey(eLengthUnit::ft, eLengthUnit::km), 3.048e-4},
    {makeKey(eLengthUnit::ft, eLengthUnit::cm), 30.48},
    {makeKey(eLengthUnit::ft, eLengthUnit::mm), 304.8},
    {makeKey(eLengthUnit::ft, eLengthUnit::um), 304800.0},
    {makeKey(eLengthUnit::ft, eLengthUnit::in), 12.0},
    {makeKey(eLengthUnit::ft, eLengthUnit::yd), 0.333333333333},
    {makeKey(eLengthUnit::ft, eLengthUnit::mi), 1.89393939394e-4},

    {makeKey(eLengthUnit::yd, eLengthUnit::m), 0.9144},
    {makeKey(eLengthUnit::yd, eLengthUnit::km), 9.144e-4},
    {makeKey(eLengthUnit::yd, eLengthUnit::cm), 91.44},
    {makeKey(eLengthUnit::yd, eLengthUnit::mm), 914.4},
    {makeKey(eLengthUnit::yd, eLengthUnit::um), 914400.0},
    {makeKey(eLengthUnit::yd, eLengthUnit::in), 36.0},
    {makeKey(eLengthUnit::yd, eLengthUnit::ft), 3.0},
    {makeKey(eLengthUnit::yd, eLengthUnit::mi), 5.68181818182e-4},

    {makeKey(eLengthUnit::mi, eLengthUnit::m), 1609.344},
    {makeKey(eLengthUnit::mi, eLengthUnit::km), 1.609344},
    {makeKey(eLengthUnit::mi, eLengthUnit::cm), 160934.4},
    {makeKey(eLengthUnit::mi, eLengthUnit::mm), 1609344.0},
    {makeKey(eLengthUnit::mi, eLengthUnit::um), 1609344000.0},
    {makeKey(eLengthUnit::mi, eLengthUnit::in), 63360.0},
    {makeKey(eLengthUnit::mi, eLengthUnit::ft), 5280.0},
    {makeKey(eLengthUnit::mi, eLengthUnit::yd), 1760.0}};
}

inline std::string toString(eLengthUnit lengthUnit)
{
    switch (lengthUnit)
    {
    case eLengthUnit::m: return "m";
    case eLengthUnit::km: return "km";
    case eLengthUnit::cm: return "cm";
    case eLengthUnit::mm: return "mm";
    case eLengthUnit::um: return "um";
    case eLengthUnit::in: return "in";
    case eLengthUnit::ft: return "ft";
    case eLengthUnit::yd: return "yd";
    case eLengthUnit::mi: return "mi";
    default: return std::string();
    }
}

inline eLengthUnit toLengthUnit(std::string const& str)
{
    if (str.empty() == true)
    {
        return eLengthUnit::unitless;
    }
    else if (str == "m")
    {
        return eLengthUnit::m;
    }
    else if (str == "km")
    {
        return eLengthUnit::km;
    }
    else if (str == "cm")
    {
        return eLengthUnit::cm;
    }
    else if (str == "mm")
    {
        return eLengthUnit::mm;
    }
    else if (str == "um")
    {
        return eLengthUnit::um;
    }
    else if (str == "in")
    {
        return eLengthUnit::in;
    }
    else if (str == "ft")
    {
        return eLengthUnit::ft;
    }
    else if (str == "yd")
    {
        return eLengthUnit::yd;
    }
    else if (str == "mi")
    {
        return eLengthUnit::mi;
    }
    else
    {
        logError("toLengthUnit: unknown unit %s", str);
        return eLengthUnit::unitless;
    }
}

inline Real findConversionUnitScale(eLengthUnit srcUnit, eLengthUnit dstUnit)
{
    return impl::findConversionScale<eLengthUnit>(impl::mapLengthConvTable, srcUnit, dstUnit);
}

using Length = Quantity<eLengthUnit>;

inline std::string toString(Length const& value)
{
    // if(value.expression.isEmpty() == false)
    //	return value.expression;
    auto str = toString(value.value);
    if (value.unit != eLengthUnit::unitless)
    {
        str += std::string(" ") + toString(value.unit);
    }
    return str;
}

inline Length toLength(std::string const& value)
{
    auto all = splitString(value, " ");
    auto l = Length();
    l.value = toReal(all[0]);
    if (all.size() > 1)
    {
        l.unit = toLengthUnit(all[1]);
    }
    return l;
}

inline Real convert(Real const& value, eLengthUnit srcUnit, eLengthUnit dstUnit)
{
    return (Length(value, srcUnit) % dstUnit).value;
}

// inline Vec3d convert(Vec3d const& value, eLengthUnit srcUnit, eLengthUnit
// dstUnit)
//{
//	return Vec3d
//	{
//		(Length{value.x(), srcUnit} % dstUnit).value,
//		(Length{value.y(), srcUnit} % dstUnit).value,
//		(Length{value.z(), srcUnit} % dstUnit).value,
//	};
//}

////////////////////////////////////////////////////////////////////////////////////////////////////
// angle

enum class eAngleUnit : uint16_t
{
    unitless = 0,
    rad,
    deg
};

namespace impl
{
auto const mapAngleConvTable = std::unordered_map<uint32_t, Real>{
    {makeKey(eAngleUnit::rad, eAngleUnit::deg), 57.2957795131},
    {makeKey(eAngleUnit::deg, eAngleUnit::rad), 0.0174532925199},
};
}

inline std::string toString(eAngleUnit angleUnit)
{
    switch (angleUnit)
    {
    case eAngleUnit::rad: return "rad";
    case eAngleUnit::deg: return "deg";
    default: return std::string();
    }
}

inline eAngleUnit toAngleUnit(std::string const& str)
{
    if (str.empty() == true)
    {
        return eAngleUnit::unitless;
    }
    else if (str == "rad")
    {
        return eAngleUnit::rad;
    }
    else if (str == "deg")
    {
        return eAngleUnit::deg;
    }
    else
    {
        logError("toAngleUnit: unknown unit %s", str);
        return eAngleUnit::unitless;
    }
}

inline Real findConversionUnitScale(eAngleUnit srcUnit, eAngleUnit dstUnit)
{
    return impl::findConversionScale<eAngleUnit>(impl::mapAngleConvTable, srcUnit, dstUnit);
}

using Angle = Quantity<eAngleUnit>;

inline std::string toString(Angle const& value)
{
    // if(value.expression.isEmpty() == false)
    //	return value.expression;
    auto str = toString(value.value);
    if (value.unit != eAngleUnit::unitless)
    {
        str += std::string(" ") + toString(value.unit);
    }
    return str;
}

Angle toAngle(std::string const& value)
{
    auto all = splitString(value, " ");
    auto l = Angle();
    l.value = toReal(all[0]);
    if (all.size() > 1)
    {
        l.unit = toAngleUnit(all[1]);
    }
    return l;
}

inline Real convert(Real const& value, eAngleUnit srcUnit, eAngleUnit dstUnit)
{
    return (Angle(value, srcUnit) % dstUnit).value;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// mass

enum class eMassUnit : uint16_t
{
    unitless = 0,
    g,
    kg,
    mg,
    t,
    ug,
    lb,
    oz
};

namespace impl
{
auto const mapMassConvTable =
    std::unordered_map<uint32_t, Real>{{makeKey(eMassUnit::g, eMassUnit::kg), 1e-3},
                                       {makeKey(eMassUnit::g, eMassUnit::mg), 1000.0},
                                       {makeKey(eMassUnit::g, eMassUnit::t), 1e-6},
                                       {makeKey(eMassUnit::g, eMassUnit::ug), 1000000.0},
                                       {makeKey(eMassUnit::g, eMassUnit::lb), 2.20462262185e-3},
                                       {makeKey(eMassUnit::g, eMassUnit::oz), 0.0352739619496},

                                       {makeKey(eMassUnit::kg, eMassUnit::g), 1000.0},
                                       {makeKey(eMassUnit::kg, eMassUnit::mg), 1000000.0},
                                       {makeKey(eMassUnit::kg, eMassUnit::t), 1e-3},
                                       {makeKey(eMassUnit::kg, eMassUnit::ug), 1000000000.0},
                                       {makeKey(eMassUnit::kg, eMassUnit::lb), 2.20462262185},
                                       {makeKey(eMassUnit::kg, eMassUnit::oz), 35.2739619496},

                                       {makeKey(eMassUnit::mg, eMassUnit::g), 1e-3},
                                       {makeKey(eMassUnit::mg, eMassUnit::kg), 1e-6},
                                       {makeKey(eMassUnit::mg, eMassUnit::t), 1e-9},
                                       {makeKey(eMassUnit::mg, eMassUnit::ug), 1000.0},
                                       {makeKey(eMassUnit::mg, eMassUnit::lb), 2.20462262185e-6},
                                       {makeKey(eMassUnit::mg, eMassUnit::oz), 3.52739619496e-5},

                                       {makeKey(eMassUnit::t, eMassUnit::g), 1000000.0},
                                       {makeKey(eMassUnit::t, eMassUnit::kg), 1000.0},
                                       {makeKey(eMassUnit::t, eMassUnit::mg), 1000000000.0},
                                       {makeKey(eMassUnit::t, eMassUnit::ug), 1e12},
                                       {makeKey(eMassUnit::t, eMassUnit::lb), 2204.62262185},
                                       {makeKey(eMassUnit::t, eMassUnit::oz), 35273.9619496},

                                       {makeKey(eMassUnit::ug, eMassUnit::g), 1e-6},
                                       {makeKey(eMassUnit::ug, eMassUnit::kg), 1e-9},
                                       {makeKey(eMassUnit::ug, eMassUnit::mg), 1e-3},
                                       {makeKey(eMassUnit::ug, eMassUnit::t), 1e-12},
                                       {makeKey(eMassUnit::ug, eMassUnit::lb), 2.20462262185e-9},
                                       {makeKey(eMassUnit::ug, eMassUnit::oz), 3.52739619496e-8},

                                       {makeKey(eMassUnit::lb, eMassUnit::g), 453.59237},
                                       {makeKey(eMassUnit::lb, eMassUnit::kg), 0.45359237},
                                       {makeKey(eMassUnit::lb, eMassUnit::mg), 453592.37},
                                       {makeKey(eMassUnit::lb, eMassUnit::t), 4.5359237e-4},
                                       {makeKey(eMassUnit::lb, eMassUnit::ug), 453592370.0},
                                       {makeKey(eMassUnit::lb, eMassUnit::oz), 16.0},

                                       {makeKey(eMassUnit::oz, eMassUnit::g), 28.349523125},
                                       {makeKey(eMassUnit::oz, eMassUnit::kg), 0.028349523125},
                                       {makeKey(eMassUnit::oz, eMassUnit::mg), 28349.523125},
                                       {makeKey(eMassUnit::oz, eMassUnit::t), 2.8349523125e-5},
                                       {makeKey(eMassUnit::oz, eMassUnit::ug), 28349523.125},
                                       {makeKey(eMassUnit::oz, eMassUnit::lb), 0.0625}};
}

inline std::string toString(eMassUnit massUnit)
{
    switch (massUnit)
    {
    case eMassUnit::g: return "g";
    case eMassUnit::kg: return "kg";
    case eMassUnit::mg: return "mg";
    case eMassUnit::t: return "t";
    case eMassUnit::ug: return "ug";
    case eMassUnit::lb: return "lb";
    case eMassUnit::oz: return "oz";
    default: return std::string();
    }
}

inline eMassUnit toMassUnit(std::string const& str)
{
    if (str.empty() == true)
    {
        return eMassUnit::unitless;
    }
    else if (str == "g")
    {
        return eMassUnit::g;
    }
    else if (str == "kg")
    {
        return eMassUnit::kg;
    }
    else if (str == "mg")
    {
        return eMassUnit::mg;
    }
    else if (str == "t")
    {
        return eMassUnit::t;
    }
    else if (str == "ug")
    {
        return eMassUnit::ug;
    }
    else if (str == "lb")
    {
        return eMassUnit::lb;
    }
    else if (str == "oz")
    {
        return eMassUnit::oz;
    }
    else
    {
        logError("toMassUnit: unknown unit %s", str);
        return eMassUnit::unitless;
    }
}

inline Real findConversionUnitScale(eMassUnit srcUnit, eMassUnit dstUnit)
{
    return impl::findConversionScale<eMassUnit>(impl::mapMassConvTable, srcUnit, dstUnit);
}

using Mass = Quantity<eMassUnit>;

inline std::string toString(Mass const& value)
{
    // if(value.expression.isEmpty() == false)
    //	return value.expression;
    auto str = toString(value.value);
    if (value.unit != eMassUnit::unitless)
    {
        str += std::string(" ") + toString(value.unit);
    }
    return str;
}

inline Mass toMass(std::string const& value)
{
    auto all = splitString(value, " ");
    auto l = Mass();
    l.value = toReal(all[0]);
    if (all.size() > 1)
    {
        l.unit = toMassUnit(all[1]);
    }
    return l;
}

inline Real convert(Real const& value, eMassUnit srcUnit, eMassUnit dstUnit)
{
    return (Mass(value, srcUnit) % dstUnit).value;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// struct Units
//{
//     eLengthUnit length = eLengthUnit::NbLengthUnit;
//     eAngleUnit angle = eAngleUnit::NbAngleUnit;
//     eMassUnit mass = eMassUnit::NbMassUnit;
// };
//
// QString toString(Units const& units);
// Units toUnits(QString const& str);
// QJsonValue toJson(Units const& value);
// Units toUnits(QJsonValue const& value);
} // namespace nopp
