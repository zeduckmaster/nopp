#pragma once

#include "math.hpp"

namespace nopp
{

struct Viewport final
{
    float x = 0.0f;
    float y = 0.0f;
    float width = 0.0f;
    float height = 0.0f;
    float minDepth = 0.0f;
    float maxDepth = 1.0f;

    constexpr Viewport() = default;

    constexpr Viewport(float x, float y, float width, float height)
        : x(x), y(y), width(width), height(height)
    {
    }

    constexpr bool operator==(Viewport const& vp)
    {
        return (x == vp.x) && (y == vp.y) && (width == vp.width) && (height == vp.height);
    }

    constexpr bool operator!=(Viewport const& vp) { return !(*this == vp); }
};

// uses OpenGL standard, i.e.: z is [-1; 1]
inline Vec2 computeWorldToViewport(Vec3 const& worldPoint,
                                   Mat4 const& matView,
                                   Mat4 const& matProj,
                                   Viewport const& viewport)
{
    auto pos = Vec4(matProj * matView * Vec4(worldPoint.x(), worldPoint.y(), worldPoint.z(), 1.0f));
    auto x = pos.x() / pos.w();
    auto y = pos.y() / pos.w();
    x *= viewport.width;
    y *= viewport.height;
    x = (x + 1.0f) * 0.5f;
    y = (-y + 1.0f) * 0.5f;
    return Vec2{x, y};
}

} // namespace nopp
