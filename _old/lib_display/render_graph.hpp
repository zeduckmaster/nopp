#pragma once

#include <lib_core/math.hpp>

namespace nopp
{
    struct View
    {
        Mat4 matView;
        Mat4 matProj;
        Viewport viewport;
        Frustum frustum;
    };

    using RenderCmd = std::function<void()>;

    struct RenderNode final
    {
        RenderCmd cmd;
        bool enabled = true;
        std::vector<RenderNode> children;
    };


    //struct RenderGraph final
    //{
    //
    //    std::vector<RenderPass> passes;
    //};
}
