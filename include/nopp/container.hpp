#pragma once

#include <algorithm>

namespace nopp
{

template <typename T>
inline void sortAndRemoveDuplicates(T& container)
{
    std::sort(std::begin(container), std::end(container));
    container.erase(std::unique(std::begin(container), std::end(container)), std::end(container));
}

template <typename T, typename Compare>
inline void sortAndRemoveDuplicates(T& container, Compare compare)
{
    std::sort(std::begin(container), std::end(container), compare);
    container.erase(std::unique(std::begin(container), std::end(container)), std::end(container));
}

// it is a generic "contains" function that works on any container using the
// generic "find" function. use the container-provided "find" function when
// available for better performances.
template <typename T, typename U>
inline auto contains(T const& container, U const& value) -> decltype(std::end(container), true)
{
    return std::end(container) != std::find(std::begin(container), std::end(container), value);
}

// search for a value, replace it by the last value in container and pop back.
// this can be used for efficient erase of an element in a big vector without
// using erase. values order is, of course, not conserved. return true if value
// has been found and removed.
template <typename T, typename U>
inline bool swapAndPopBack(T& container, U const& value)
{
    auto iter = std::find(std::begin(container), std::end(container), value);
    if (iter == std::end(container))
    {
        return false;
    }
    std::swap(*iter, container.back());
    container.pop_back();
    return true;
}

// same as previous function but with unary predicate
template <typename T, typename P>
inline bool swapAndPopBackIf(T& container, P predicate)
{
    auto iter = std::find_if(std::begin(container), std::end(container), predicate);
    if (iter == std::end(container))
    {
        return false;
    }
    std::swap(*iter, container.back());
    container.pop_back();
    return true;
}

template <typename T>
inline auto append(T& dstContainer, T const& srcContainer)
{
    return dstContainer.insert(
        std::end(dstContainer), std::begin(srcContainer), std::end(srcContainer));
}

} // namespace nopp
