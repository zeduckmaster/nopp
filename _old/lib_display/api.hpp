#pragma once

#include "camera.hpp"
#include "gpu.hpp"
#include "mesh.hpp"
#include "texture.hpp"

#if defined(WITH_LUA)
#    include "lua/support_lua.hpp"
#endif

#if defined(WITH_IMGUI)
#    include "imgui/support_imgui.hpp"
#endif

#if defined(WITH_GLES2)
#    include "gles2/gpu_gles2.hpp"
#endif
