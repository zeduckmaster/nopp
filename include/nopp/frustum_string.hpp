#pragma once

#include "frustum.hpp"
#include "math_string.hpp"

namespace nopp
{

inline std::string toString(Frustum const& value)
{
    auto str = std::string();
    str.append(toString(value.left));
    str.push_back(' ');
    str.append(toString(value.right));
    str.push_back(' ');
    str.append(toString(value.bottom));
    str.push_back(' ');
    str.append(toString(value.top));
    str.push_back(' ');
    str.append(toString(value.znear));
    str.push_back(' ');
    str.append(toString(value.zfar));
    str.push_back(' ');
    str.append(toString(toType(value.type)));
    return str;
}

inline Frustum toFrustum(std::string const& value)
{
    auto vals = splitString(value, impl::VectorDelims);
    assert(vals.size() == 7);
    auto f = Frustum();
    f.left = toFloat(vals[0]);
    f.right = toFloat(vals[1]);
    f.bottom = toFloat(vals[2]);
    f.top = toFloat(vals[3]);
    f.znear = toFloat(vals[4]);
    f.zfar = toFloat(vals[5]);
    f.type = toEnum<Frustum::eType>(toUInt32(vals[6]));
    return f;
}

} // namespace nopp
