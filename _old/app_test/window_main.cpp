#include "window_main.h"
#include <QVBoxLayout>

void WindowMain::_UI::setup(QMainWindow* w)
{
    w->setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    w->resize(820, 480);
    auto qw = new QWidget{w};
    w->setCentralWidget(qw);
    auto layout = new QVBoxLayout{qw};
    layout->setContentsMargins(0, 0, 0, 0);

    auto font = QFont{"Courier", 12};

    output = new QTextEdit{w};
    output->setReadOnly(true);
    layout->addWidget(output);
    // format
    auto pal = output->palette();
    pal.setColor(QPalette::Active, QPalette::Base, QColor{"#1e1e1e"});
    pal.setColor(QPalette::Active, QPalette::Text, QColor{"#dcdcdc"});
    output->setPalette(pal);
    output->setFont(font);
    output->setLineWrapMode(QTextEdit::LineWrapMode::FixedColumnWidth);
    output->setLineWrapColumnOrWidth(80);
    output->setFrameShape(QFrame::NoFrame);

    input = new QLineEdit{w};
    layout->addWidget(input);
    // format
    input->setPalette(pal);
    input->setFont(font);
    input->setFrame(false);

    actionCommand = new QAction{w};
    actionCommand->setShortcut(Qt::Key_Return);
    input->addAction(actionCommand);
}

void WindowMain::_UI::retranslate()
{

}

////////////////////////////////////////////////////////////////////////////////

WindowMain::WindowMain()
{
    _ui.setup(this);
    _ui.retranslate();

    connect(_ui.actionCommand, &QAction::triggered,
            [&]()
    {
        if(onInput == nullptr)
            return;
        onInput(_ui.input->text().toStdString());
        _ui.input->clear();
    });
}

void WindowMain::onOutput(std::string const& value)
{
    _ui.output->append(QString::fromStdString(value));
}
