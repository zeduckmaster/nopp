#include "../mesh.hpp"
#include "../texture.hpp"
#include "gpu_gles2.hpp"
#include <lib_core/log.hpp>

using namespace nopp;
using namespace nopp::gles2;

namespace
{
std::vector<GLMesh::GLVertexAttribute>
getGLVertexAttributes(GLuint glVBId, std::vector<GLMesh::GLVertexComponent> const& layout)
{
    auto vas = std::vector<GLMesh::GLVertexAttribute>{};
    vas.reserve(layout.size());
    auto offset = 0;
    for (auto& vc : layout)
    {
        auto va = GLMesh::GLVertexAttribute{};
        va.glVBId = glVBId;
        va.size = vc.size;
        va.type = vc.type;
        va.offset = offset;
        switch (va.type)
        {
        case GL_BYTE:
        case GL_UNSIGNED_BYTE: offset += va.size * sizeof(uint8_t); break;
        case GL_SHORT:
        case GL_UNSIGNED_SHORT: offset += va.size * sizeof(uint16_t); break;
        case GL_FLOAT: offset += va.size * sizeof(float); break;
        default: return std::vector<GLMesh::GLVertexAttribute>{}; break;
        }
        vas.emplace_back(va);
    }
    for (auto& va : vas)
    {
        va.stride = static_cast<GLsizei>(offset);
    }
    return vas;
}

GLenum getGLMode(eMeshTopology topology)
{
    switch (topology)
    {
    case eMeshTopology::Points: return GL_POINTS; break;
    case eMeshTopology::Lines: return GL_LINES; break;
    case eMeshTopology::Triangles: return GL_TRIANGLES; break;
    default: return GL_NONE;
    }
}
} // namespace

GLMesh::GLMesh(GL const& gl) : _gl{gl} {}

GLMesh::GLMesh(GL const& gl, GPUData* data) : GLMesh{gl}
{
    assert(data != nullptr);
    glUpdate(data);
}

GLMesh::GLMesh(GL const& gl, std::vector<float> const& vertexData,
               std::vector<GLVertexComponent> const& vertexLayout)
    : GLMesh{gl}
{
    auto glVBId = GLuint{0};
    _gl.glGenBuffers(1, &glVBId);
    _gl.glBindBuffer(GL_ARRAY_BUFFER, glVBId);
    auto vbsize = vertexData.size() * sizeof(vertexData.front());
    _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, vertexData.data(), GL_STATIC_DRAW);
    glVertexAttributes = getGLVertexAttributes(glVBId, vertexLayout);
    glVBIds.emplace_back(glVBId);
    count = static_cast<GLsizei>(vbsize) / glVertexAttributes[0].stride;
}

GLMesh::GLMesh(GL const& gl, std::vector<float> const& vertexData,
               std::vector<GLVertexComponent> const& vertexLayout,
               std::vector<uint16_t> const& indexData)
    : GLMesh{gl}
{
    auto glVBId = GLuint{0};
    _gl.glGenBuffers(1, &glVBId);
    _gl.glBindBuffer(GL_ARRAY_BUFFER, glVBId);
    auto vbsize = vertexData.size() * sizeof(vertexData.front());
    _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, vertexData.data(), GL_STATIC_DRAW);
    glVertexAttributes = getGLVertexAttributes(glVBId, vertexLayout);
    glVBIds.emplace_back(glVBId);

    _gl.glGenBuffers(1, &glIBId);
    _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glIBId);
    auto ibsize = indexData.size() * sizeof(indexData.front());
    _gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibsize, indexData.data(), GL_STATIC_DRAW);
    count = static_cast<GLsizei>(indexData.size());
}

GLMesh::~GLMesh() { _glDelete(); }

void GLMesh::glUpdate(GPUData* data)
{
    assert(data != nullptr);
    assert(data->isTypeOf<MeshData>() == true);
    _glDelete();
    auto meshData = static_cast<MeshData*>(data);
    auto& desc = meshData->desc;

    if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Position) == true)
    {
        auto vbId = GLuint{0};
        _gl.glGenBuffers(1, &vbId);
        _gl.glBindBuffer(GL_ARRAY_BUFFER, vbId);
        auto vbsize = meshData->positions.size() * sizeof(Vec3);
        _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, meshData->positions.data(), GL_STATIC_DRAW);
        glVBIds.emplace_back(vbId);

        auto va = GLVertexAttribute{};
        va.glVBId = vbId;
        va.type = GL_FLOAT;
        va.size = 3;
        va.stride = sizeof(Vec3);
        glVertexAttributes.emplace_back(va);
    }
    else
    {
        glVertexAttributes.emplace_back(GLVertexAttribute{});
    }

    if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Color) == true)
    {
        auto vbId = GLuint{0};
        _gl.glGenBuffers(1, &vbId);
        _gl.glBindBuffer(GL_ARRAY_BUFFER, vbId);
        auto vbsize = meshData->colors.size() * sizeof(Vec4);
        _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, meshData->colors.data(), GL_STATIC_DRAW);
        glVBIds.emplace_back(vbId);

        auto va = GLVertexAttribute{};
        va.glVBId = vbId;
        va.type = GL_FLOAT;
        va.size = 4;
        va.stride = sizeof(Vec4);
        glVertexAttributes.emplace_back(va);
    }
    else
    {
        glVertexAttributes.emplace_back(GLVertexAttribute{});
    }

    if ((hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Normal) == true) ||
        (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Tangent) == true) ||
        (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::TexCoord0) == true) ||
        (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::TexCoord1) == true))
    {
        auto stride = static_cast<GLsizei>(sizeof(MeshData::VtxData));
        auto vbId = GLuint{0};
        _gl.glGenBuffers(1, &vbId);
        _gl.glBindBuffer(GL_ARRAY_BUFFER, vbId);
        auto vbsize = meshData->datas.size() * stride;
        _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, meshData->datas.data(), GL_STATIC_DRAW);
        glVBIds.emplace_back(vbId);

        // for tex coord 0/1
        auto va = GLVertexAttribute{};
        va.glVBId = vbId;
        va.type = GL_FLOAT;
        va.size = 4;
        va.offset = 0;
        va.stride = stride;
        glVertexAttributes.emplace_back(va);
        // for normal
        va = GLVertexAttribute{};
        va.glVBId = vbId;
        va.type = GL_UNSIGNED_BYTE;
        va.size = 4;
        va.offset = sizeof(Vec4);
        va.normalized = GL_TRUE;
        va.stride = stride;
        glVertexAttributes.emplace_back(va);
        // for tangent
        va.offset = sizeof(Vec4) + sizeof(uint32_t);
        glVertexAttributes.emplace_back(va);
    }
    else
    {
        glVertexAttributes.emplace_back(GLVertexAttribute{});
        glVertexAttributes.emplace_back(GLVertexAttribute{});
        glVertexAttributes.emplace_back(GLVertexAttribute{});
    }

    if (desc.indexCount != 0)
    {
        _gl.glGenBuffers(1, &glIBId);
        _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glIBId);
        auto ibsize = meshData->indexes.size() * sizeof(uint16_t);
        _gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibsize, meshData->indexes.data(), GL_STATIC_DRAW);
        count = static_cast<GLsizei>(desc.indexCount);
    }
    else
    {
        count = static_cast<GLsizei>(desc.vertexCount);
    }

    auto nbWireframe = meshData->wireframeIndexes.size();
    if (nbWireframe > 0)
    {
        _gl.glGenBuffers(1, &glIBWireframeId);
        _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glIBWireframeId);
        auto ibsize = nbWireframe * sizeof(uint16_t);
        _gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibsize, meshData->wireframeIndexes.data(),
                         GL_STATIC_DRAW);
        wireframeCount = static_cast<GLsizei>(nbWireframe);
    }
    mode = getGLMode(meshData->desc.primitiveType);
}

void GLMesh::_glDelete()
{
    for (auto& vb : glVBIds)
    {
        _gl.glDeleteBuffers(1, &vb);
    }
    glVBIds.clear();
    if (glIBId != 0)
    {
        _gl.glDeleteBuffers(1, &glIBId);
    }
    glIBId = 0;
    glVertexAttributes.clear();
    count = 0;
    if (glIBWireframeId != 0)
    {
        _gl.glDeleteBuffers(1, &glIBWireframeId);
    }
    wireframeCount = 0;
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
struct GLpf final
{
    GLint internalformat = GL_INVALID_VALUE;
    GLenum format = GL_INVALID_ENUM;
    GLenum type = GL_INVALID_ENUM;
    GLint alignment = 0;
    bool isCompressed = false;
};

GLpf getGLPixelFormat(ePixelFormat pf)
{
    switch (pf)
    {
    case ePixelFormat::RGBx8_UNorm:
    case ePixelFormat::RGBA8_UNorm: return GLpf{GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, 4, false};
    case ePixelFormat::RGB_ETC1:
        return GLpf{GL_ETC1_RGB8_OES, GL_INVALID_ENUM, GL_INVALID_ENUM, 4, true};

    default: return GLpf{};
    }
}

auto maxTextureSize = 0;

} // namespace

GLTexture::GLTexture(GL const& gl) : _gl{gl} {}

GLTexture::GLTexture(GL const& gl, GPUData* data) : GLTexture{gl}
{
    assert(data != nullptr);
    glUpdate(data);
}

GLTexture::GLTexture(GL const& gl, Path const& filepath) : GLTexture{gl}
{
    auto tex = Texture{filepath};
    glUpdate(&tex);
}

GLTexture::~GLTexture() { _glDelete(); }

void GLTexture::glUpdate(GPUData* data)
{
    assert(data != nullptr);
    assert(data->isTypeOf<Texture>() == true);
    if (maxTextureSize == 0)
    {
        _gl.glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
    }

    _glDelete();
    auto src = static_cast<Texture*>(data);

    if (glId == 0)
    {
        glType = (src->desc.isCubemap == true) ? GL_TEXTURE_CUBE_MAP : GL_TEXTURE_2D;
        _gl.glGenTextures(1, &glId);
    }
    auto glpf = getGLPixelFormat(src->desc.format);
    _gl.glBindTexture(glType, glId);

    auto oldUnpackAlignment = GLint{0};
    _gl.glGetIntegerv(GL_UNPACK_ALIGNMENT, &oldUnpackAlignment);
    _gl.glPixelStorei(GL_UNPACK_ALIGNMENT, glpf.alignment);
    auto arraySize = src->desc.arraySize;
    auto levelpos = GLint{0};
    auto texType = (glType == GL_TEXTURE_2D) ? GL_TEXTURE_2D : GL_TEXTURE_CUBE_MAP_POSITIVE_X;
    for (auto& level : src->levels)
    {
        auto sz = level.size;
        for (auto n = 0; n < arraySize; ++n)
        {
            auto data = level.images[n].data();
            if (glpf.isCompressed == true)
            {
                auto imageSize = level.images[n].size();
                _gl.glCompressedTexImage2D(texType + n, levelpos, glpf.internalformat, sz.width,
                                           sz.height, 0, imageSize, data);
            }
            else
            {
                _gl.glTexImage2D(texType + n, levelpos, glpf.internalformat, sz.width, sz.height, 0,
                                 glpf.format, glpf.type, data);
            }
        }
        ++levelpos;
    }

    _gl.glPixelStorei(GL_UNPACK_ALIGNMENT, oldUnpackAlignment);

    if (src->desc.mipLevelCount == -1)
    {
        _gl.glGenerateMipmap(glType);
    }
    glCheckError(_gl, "GLTexture::glUpdate");
}

void GLTexture::_glDelete()
{
    switch (glType)
    {
    case GL_TEXTURE_2D:
    case GL_TEXTURE_CUBE_MAP: _gl.glDeleteTextures(1, &glId); break;
    case GL_RENDERBUFFER: _gl.glDeleteRenderbuffers(1, &glId); break;
    default: break;
    }
    glId = 0;
    glType = 0;
    // size = Size3ui{};
}

////////////////////////////////////////////////////////////////////////////////////////////////////

GLFramebuffer::GLFramebuffer(GL const& gl, bool hasColor, bool hasDepth, bool hasStencil) : _gl{gl}
{
    _gl.glGenFramebuffers(1, &_glId);
    if (hasColor == true)
    {
        _gl.glGenTextures(1, &_glColorId);
    }
    if (hasDepth == true)
    {
        _gl.glGenRenderbuffers(1, &_glDepthId);
    }
    if (hasStencil == true)
    {
        _gl.glGenRenderbuffers(1, &_glStencilId);
    }
}

GLFramebuffer::~GLFramebuffer()
{
    if (_glId != 0)
    {
        _gl.glDeleteFramebuffers(1, &_glId);
    }
    if (_glColorId != 0)
    {
        _gl.glDeleteTextures(1, &_glColorId);
    }
    if (_glDepthId != 0)
    {
        _gl.glDeleteRenderbuffers(1, &_glDepthId);
    }
    if (_glStencilId != 0)
    {
        _gl.glDeleteRenderbuffers(1, &_glStencilId);
    }
}

void GLFramebuffer::glResize(GLsizei width, GLsizei height)
{
    if ((_width == width) && (_height == height))
    {
        return;
    }

    if (_glColorId != 0)
    {
        _gl.glBindTexture(GL_TEXTURE_2D, _glColorId);
        _gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                         nullptr);
    }
    if (_glDepthId != 0)
    {
        _gl.glBindRenderbuffer(GL_RENDERBUFFER, _glDepthId);
        _gl.glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    }
    if (_glStencilId != 0)
    {
        _gl.glBindRenderbuffer(GL_RENDERBUFFER, _glStencilId);
        _gl.glRenderbufferStorage(GL_RENDERBUFFER, GL_STENCIL_INDEX8, width, height);
    }
    _width = width;
    _height = height;

    _gl.glBindFramebuffer(GL_FRAMEBUFFER, _glId);
    if (_glColorId != 0)
    {
        _gl.glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _glColorId,
                                   0);
    }
    if (_glDepthId != 0)
    {
        _gl.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER,
                                      _glDepthId);
    }
    if (_glStencilId != 0)
    {
        _gl.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER,
                                      _glStencilId);
    }
    auto status = _gl.glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        logError("[GL] GLFramebuffer::glCreate: %d", status);
    }
    _gl.glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glCheckError(_gl, "GLFramebuffer::glCreate");
}
