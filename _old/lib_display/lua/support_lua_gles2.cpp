#include "../../lib_core/math.hpp"
#include "../gles2/gpu_gles2.hpp"
#include "support_lua.hpp"

using namespace nopp;
using namespace nopp::gles2;

namespace
{

}

namespace sol
{
template <> struct is_automagical<GL> : std::false_type
{
};
template <> struct is_container<GL> : std::false_type
{
};
template <> struct is_to_stringable<GL> : std::false_type
{
};
} // namespace sol

void nopp::gles2::loadLuaDisplay(sol::state& luaState)
{
    {
        auto t = luaState.new_usertype<GL>("GLES2", sol::no_constructor);

        // enums
        t["GL_NONE"] = sol::var(GL_NONE);
        t["GL_NO_ERROR"] = sol::var(GL_NO_ERROR);
        t["GL_ZERO"] = sol::var(GL_ZERO);
        t["GL_FALSE"] = sol::var(GL_FALSE);
        t["GL_TRUE"] = sol::var(GL_TRUE);
        t["GL_ONE"] = sol::var(GL_ONE);

        t["GL_POINTS"] = sol::var(GL_POINTS);
        t["GL_LINES"] = sol::var(GL_LINES);
        t["GL_LINE_LOOP"] = sol::var(GL_LINE_LOOP);
        t["GL_LINE_STRIP"] = sol::var(GL_LINE_STRIP);
        t["GL_TRIANGLES"] = sol::var(GL_TRIANGLES);
        t["GL_TRIANGLE_STRIP"] = sol::var(GL_TRIANGLE_STRIP);
        t["GL_TRIANGLE_FAN"] = sol::var(GL_TRIANGLE_FAN);

        t["GL_NEVER"] = sol::var(GL_NEVER);
        t["GL_LESS"] = sol::var(GL_LESS);
        t["GL_EQUAL"] = sol::var(GL_EQUAL);
        t["GL_LEQUAL"] = sol::var(GL_LEQUAL);
        t["GL_GREATER"] = sol::var(GL_GREATER);
        t["GL_NOTEQUAL"] = sol::var(GL_NOTEQUAL);
        t["GL_GEQUAL"] = sol::var(GL_GEQUAL);
        t["GL_ALWAYS"] = sol::var(GL_ALWAYS);

        t["GL_SRC_COLOR"] = sol::var(GL_SRC_COLOR);
        t["GL_ONE_MINUS_SRC_COLOR"] = sol::var(GL_ONE_MINUS_SRC_COLOR);
        t["GL_SRC_ALPHA"] = sol::var(GL_SRC_ALPHA);
        t["GL_ONE_MINUS_SRC_ALPHA"] = sol::var(GL_ONE_MINUS_SRC_ALPHA);
        t["GL_DST_ALPHA"] = sol::var(GL_DST_ALPHA);
        t["GL_ONE_MINUS_DST_ALPHA"] = sol::var(GL_ONE_MINUS_DST_ALPHA);
        t["GL_DST_COLOR"] = sol::var(GL_DST_COLOR);
        t["GL_ONE_MINUS_DST_COLOR"] = sol::var(GL_ONE_MINUS_DST_COLOR);
        t["GL_SRC_ALPHA_SATURATE"] = sol::var(GL_SRC_ALPHA_SATURATE);

        t["GL_FRONT"] = sol::var(GL_FRONT);
        t["GL_BACK"] = sol::var(GL_BACK);

        t["GL_INVALID_ENUM"] = sol::var(GL_INVALID_ENUM);
        t["GL_INVALID_VALUE"] = sol::var(GL_INVALID_VALUE);
        t["GL_INVALID_OPERATION"] = sol::var(GL_INVALID_OPERATION);
        t["GL_OUT_OF_MEMORY"] = sol::var(GL_OUT_OF_MEMORY);

        t["GL_CW"] = sol::var(GL_CW);
        t["GL_CCW"] = sol::var(GL_CCW);

        t["GL_CULL_FACE"] = sol::var(GL_CULL_FACE);
        t["GL_DEPTH_TEST"] = sol::var(GL_DEPTH_TEST);
        t["GL_STENCIL_TEST"] = sol::var(GL_STENCIL_TEST);
        t["GL_VIEWPORT"] = sol::var(GL_VIEWPORT);
        t["GL_BLEND"] = sol::var(GL_BLEND);

        t["GL_SCISSOR_BOX"] = sol::var(GL_SCISSOR_BOX);
        t["GL_SCISSOR_TEST"] = sol::var(GL_SCISSOR_TEST);
        t["GL_UNPACK_ALIGNMENT"] = sol::var(GL_UNPACK_ALIGNMENT);

        t["GL_MAX_TEXTURE_SIZE"] = sol::var(GL_MAX_TEXTURE_SIZE);
        t["GL_TEXTURE_2D"] = sol::var(GL_TEXTURE_2D);

        t["GL_DONT_CARE"] = sol::var(GL_DONT_CARE);
        t["GL_FASTEST"] = sol::var(GL_FASTEST);
        t["GL_NICEST"] = sol::var(GL_NICEST);

        t["GL_BYTE"] = sol::var(GL_BYTE);
        t["GL_UNSIGNED_BYTE"] = sol::var(GL_UNSIGNED_BYTE);
        t["GL_SHORT"] = sol::var(GL_SHORT);
        t["GL_UNSIGNED_SHORT"] = sol::var(GL_UNSIGNED_SHORT);
        t["GL_INT"] = sol::var(GL_INT);
        t["GL_UNSIGNED_INT"] = sol::var(GL_UNSIGNED_INT);
        t["GL_FLOAT"] = sol::var(GL_FLOAT);

        t["GL_DEPTH_COMPONENT"] = sol::var(GL_DEPTH_COMPONENT);
        t["GL_RGB"] = sol::var(GL_RGB);
        t["GL_RGBA"] = sol::var(GL_RGBA);

        t["GL_KEEP"] = sol::var(GL_KEEP);
        t["GL_REPLACE"] = sol::var(GL_REPLACE);
        t["GL_INCR"] = sol::var(GL_INCR);
        t["GL_DECR"] = sol::var(GL_DECR);

        t["GL_VENDOR"] = sol::var(GL_VENDOR);
        t["GL_RENDERER"] = sol::var(GL_RENDERER);
        t["GL_VERSION"] = sol::var(GL_VERSION);
        t["GL_EXTENSIONS"] = sol::var(GL_EXTENSIONS);

        t["GL_NEAREST"] = sol::var(GL_NEAREST);
        t["GL_LINEAR"] = sol::var(GL_LINEAR);
        t["GL_NEAREST_MIPMAP_NEAREST"] = sol::var(GL_NEAREST_MIPMAP_NEAREST);
        t["GL_LINEAR_MIPMAP_NEAREST"] = sol::var(GL_LINEAR_MIPMAP_NEAREST);
        t["GL_NEAREST_MIPMAP_LINEAR"] = sol::var(GL_NEAREST_MIPMAP_LINEAR);
        t["GL_LINEAR_MIPMAP_LINEAR"] = sol::var(GL_LINEAR_MIPMAP_LINEAR);
        t["GL_TEXTURE_MAG_FILTER"] = sol::var(GL_TEXTURE_MAG_FILTER);
        t["GL_TEXTURE_MIN_FILTER"] = sol::var(GL_TEXTURE_MIN_FILTER);
        t["GL_TEXTURE_WRAP_S"] = sol::var(GL_TEXTURE_WRAP_S);
        t["GL_TEXTURE_WRAP_T"] = sol::var(GL_TEXTURE_WRAP_T);
        t["GL_REPEAT"] = sol::var(GL_REPEAT);

        t["GL_CONSTANT_COLOR"] = sol::var(GL_CONSTANT_COLOR);
        t["GL_ONE_MINUS_CONSTANT_COLOR"] =
            sol::var(GL_ONE_MINUS_CONSTANT_COLOR);
        t["GL_CONSTANT_ALPHA"] = sol::var(GL_CONSTANT_ALPHA);
        t["GL_ONE_MINUS_CONSTANT_ALPHA"] =
            sol::var(GL_ONE_MINUS_CONSTANT_ALPHA);
        t["GL_BLEND_COLOR"] = sol::var(GL_BLEND_COLOR);
        t["GL_FUNC_ADD"] = sol::var(GL_FUNC_ADD);
        t["GL_BLEND_EQUATION"] = sol::var(GL_BLEND_EQUATION);
        t["GL_BLEND_EQUATION_RGB"] = sol::var(GL_BLEND_EQUATION_RGB);
        t["GL_FUNC_SUBTRACT"] = sol::var(GL_FUNC_SUBTRACT);
        t["GL_FUNC_REVERSE_SUBTRACT"] = sol::var(GL_FUNC_REVERSE_SUBTRACT);

        t["GL_UNSIGNED_SHORT_5_5_5_1"] = sol::var(GL_UNSIGNED_SHORT_5_5_5_1);
        t["GL_POLYGON_OFFSET_FILL"] = sol::var(GL_POLYGON_OFFSET_FILL);
        t["GL_POLYGON_OFFSET_FACTOR"] = sol::var(GL_POLYGON_OFFSET_FACTOR);
        t["GL_RGB5_A1"] = sol::var(GL_RGB5_A1);
        t["GL_TEXTURE_BINDING_2D"] = sol::var(GL_TEXTURE_BINDING_2D);
        t["GL_BLEND_DST_RGB"] = sol::var(GL_BLEND_DST_RGB);
        t["GL_BLEND_SRC_RGB"] = sol::var(GL_BLEND_SRC_RGB);
        t["GL_BLEND_DST_ALPHA"] = sol::var(GL_BLEND_DST_ALPHA);
        t["GL_BLEND_SRC_ALPHA"] = sol::var(GL_BLEND_SRC_ALPHA);

        t["GL_CLAMP_TO_EDGE"] = sol::var(GL_CLAMP_TO_EDGE);
        t["GL_GENERATE_MIPMAP_HINT"] = sol::var(GL_GENERATE_MIPMAP_HINT);
        t["GL_DEPTH_COMPONENT16"] = sol::var(GL_DEPTH_COMPONENT16);
        t["GL_UNSIGNED_SHORT_5_6_5"] = sol::var(GL_UNSIGNED_SHORT_5_6_5);
        t["GL_MIRRORED_REPEAT"] = sol::var(GL_MIRRORED_REPEAT);

        t["GL_TEXTURE0"] = sol::var(GL_TEXTURE0);
        t["GL_ACTIVE_TEXTURE"] = sol::var(GL_ACTIVE_TEXTURE);
        t["GL_TEXTURE_CUBE_MAP"] = sol::var(GL_TEXTURE_CUBE_MAP);
        t["GL_TEXTURE_CUBE_MAP_POSITIVE_X"] =
            sol::var(GL_TEXTURE_CUBE_MAP_POSITIVE_X);
        t["GL_TEXTURE_CUBE_MAP_NEGATIVE_X"] =
            sol::var(GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
        t["GL_TEXTURE_CUBE_MAP_POSITIVE_Y"] =
            sol::var(GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
        t["GL_TEXTURE_CUBE_MAP_NEGATIVE_Y"] =
            sol::var(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
        t["GL_TEXTURE_CUBE_MAP_POSITIVE_Z"] =
            sol::var(GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
        t["GL_TEXTURE_CUBE_MAP_NEGATIVE_Z"] =
            sol::var(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
        t["GL_MAX_CUBE_MAP_TEXTURE_SIZE"] =
            sol::var(GL_MAX_CUBE_MAP_TEXTURE_SIZE);
        t["GL_NUM_COMPRESSED_TEXTURE_FORMATS"] =
            sol::var(GL_NUM_COMPRESSED_TEXTURE_FORMATS);
        t["GL_COMPRESSED_TEXTURE_FORMATS"] =
            sol::var(GL_COMPRESSED_TEXTURE_FORMATS);

        t["GL_BLEND_EQUATION_ALPHA"] = sol::var(GL_BLEND_EQUATION_ALPHA);

        t["GL_ARRAY_BUFFER"] = sol::var(GL_ARRAY_BUFFER);
        t["GL_ELEMENT_ARRAY_BUFFER"] = sol::var(GL_ELEMENT_ARRAY_BUFFER);
        t["GL_ARRAY_BUFFER_BINDING"] = sol::var(GL_ARRAY_BUFFER_BINDING);
        t["GL_ELEMENT_ARRAY_BUFFER_BINDING"] =
            sol::var(GL_ELEMENT_ARRAY_BUFFER_BINDING);
        t["GL_STREAM_DRAW"] = sol::var(GL_STREAM_DRAW);
        t["GL_STATIC_DRAW"] = sol::var(GL_STATIC_DRAW);
        t["GL_DYNAMIC_DRAW"] = sol::var(GL_DYNAMIC_DRAW);

        t["GL_FRAGMENT_SHADER"] = sol::var(GL_FRAGMENT_SHADER);
        t["GL_VERTEX_SHADER"] = sol::var(GL_VERTEX_SHADER);
        t["GL_FLOAT_VEC2"] = sol::var(GL_FLOAT_VEC2);
        t["GL_FLOAT_VEC3"] = sol::var(GL_FLOAT_VEC3);
        t["GL_FLOAT_VEC4"] = sol::var(GL_FLOAT_VEC4);
        t["GL_INT_VEC2"] = sol::var(GL_INT_VEC2);
        t["GL_INT_VEC3"] = sol::var(GL_INT_VEC3);
        t["GL_INT_VEC4"] = sol::var(GL_INT_VEC4);
        t["GL_FLOAT_MAT2"] = sol::var(GL_FLOAT_MAT2);
        t["GL_FLOAT_MAT3"] = sol::var(GL_FLOAT_MAT3);
        t["GL_FLOAT_MAT4"] = sol::var(GL_FLOAT_MAT4);
        t["GL_SAMPLER_2D"] = sol::var(GL_SAMPLER_2D);
        t["GL_SAMPLER_CUBE"] = sol::var(GL_SAMPLER_CUBE);

        t["GL_COMPILE_STATUS"] = sol::var(GL_COMPILE_STATUS);
        t["GL_LINK_STATUS"] = sol::var(GL_LINK_STATUS);
        t["GL_INFO_LOG_LENGTH"] = sol::var(GL_INFO_LOG_LENGTH);
        t["GL_ACTIVE_UNIFORMS"] = sol::var(GL_ACTIVE_UNIFORMS);
        t["GL_ACTIVE_UNIFORM_MAX_LENGTH"] =
            sol::var(GL_ACTIVE_UNIFORM_MAX_LENGTH);
        t["GL_ACTIVE_ATTRIBUTES"] = sol::var(GL_ACTIVE_ATTRIBUTES);
        t["GL_ACTIVE_ATTRIBUTE_MAX_LENGTH"] =
            sol::var(GL_ACTIVE_ATTRIBUTE_MAX_LENGTH);
        t["GL_SHADING_LANGUAGE_VERSION"] =
            sol::var(GL_SHADING_LANGUAGE_VERSION);
        t["GL_CURRENT_PROGRAM"] = sol::var(GL_CURRENT_PROGRAM);

        t["GL_FRAMEBUFFER_BINDING"] = sol::var(GL_FRAMEBUFFER_BINDING);
        t["GL_FRAMEBUFFER_COMPLETE"] = sol::var(GL_FRAMEBUFFER_COMPLETE);
        t["GL_COLOR_ATTACHMENT0"] = sol::var(GL_COLOR_ATTACHMENT0);
        t["GL_DEPTH_ATTACHMENT"] = sol::var(GL_DEPTH_ATTACHMENT);
        t["GL_STENCIL_ATTACHMENT"] = sol::var(GL_STENCIL_ATTACHMENT);
        t["GL_FRAMEBUFFER"] = sol::var(GL_FRAMEBUFFER);
        t["GL_RENDERBUFFER"] = sol::var(GL_RENDERBUFFER);
        t["GL_STENCIL_INDEX8"] = sol::var(GL_STENCIL_INDEX8);

        t["GL_DEPTH_BUFFER_BIT"] = sol::var(GL_DEPTH_BUFFER_BIT);
        t["GL_STENCIL_BUFFER_BIT"] = sol::var(GL_STENCIL_BUFFER_BIT);
        t["GL_COLOR_BUFFER_BIT"] = sol::var(GL_COLOR_BUFFER_BIT);

        // functions A
        t["glActiveTexture"] = [](GL const& gl, GLenum texture) {
            gl.glActiveTexture(texture);
        };
        t["glAttachShader"] = [](GL const& gl, GLuint program, GLuint shader) {
            gl.glAttachShader(program, shader);
        };
        // functions B
        t["glBindAttribLocation"] =
            [](GL const& gl, GLuint program, GLuint index, GLchar const* name) {
                gl.glBindAttribLocation(program, index, name);
            };
        t["glBindBuffer"] = [](GL const& gl, GLenum target, GLuint buffer) {
            gl.glBindBuffer(target, buffer);
        };
        t["glBindFramebuffer"] =
            [](GL const& gl, GLenum target, GLuint framebuffer) {
                gl.glBindFramebuffer(target, framebuffer);
            };
        t["glBindRenderbuffer"] =
            [](GL const& gl, GLenum target, GLuint renderbuffer) {
                gl.glBindRenderbuffer(target, renderbuffer);
            };
        t["glBindTexture"] = [](GL const& gl, GLenum target, GLuint texture) {
            gl.glBindTexture(target, texture);
        };
        t["glBlendColor"] = [](GL const& gl,
                               GLclampf red,
                               GLclampf green,
                               GLclampf blue,
                               GLclampf alpha) {
            gl.glBlendColor(red, green, blue, alpha);
        };
        t["glBlendEquation"] = [](GL const& gl, GLenum mode) {
            gl.glBlendEquation(mode);
        };
        t["glBlendEquationSeparate"] =
            [](GL const& gl, GLenum modeRGB, GLenum modeAlpha) {
                gl.glBlendEquationSeparate(modeRGB, modeAlpha);
            };
        t["glBlendFunc"] = [](GL const& gl, GLenum sfactor, GLenum dfactor) {
            gl.glBlendFunc(sfactor, dfactor);
        };
        t["glBlendFuncSeparate"] = [](GL const& gl,
                                      GLenum srcRGB,
                                      GLenum dstRGB,
                                      GLenum srcAlpha,
                                      GLenum dstAlpha) {
            gl.glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha);
        };
        t["glBufferData"] = [](GL const& gl,
                               GLenum target,
                               GLsizeiptr size,
                               GLvoid const* data,
                               GLenum usage) {
            gl.glBufferData(target, size, data, usage);
        };
        // functions C
        t["glCheckFramebufferStatus"] = [](GL const& gl, GLenum target) {
            return gl.glCheckFramebufferStatus(target);
        };
        t["glClear"] = [](GL const& gl, GLbitfield mask) { gl.glClear(mask); };
        t["glClearColor"] = [](GL const& gl,
                               GLclampf red,
                               GLclampf green,
                               GLclampf blue,
                               GLclampf alpha) {
            gl.glClearColor(red, green, blue, alpha);
        };
        t["glClearDepthf"] = [](GL const& gl, GLclampf depth) {
            gl.glClearDepthf(depth);
        };
        t["glClearStencil"] = [](GL const& gl, GLint s) {
            gl.glClearStencil(s);
        };
        t["glColorMask"] = [](GL const& gl,
                              GLboolean red,
                              GLboolean green,
                              GLboolean blue,
                              GLboolean alpha) {
            gl.glColorMask(red, green, blue, alpha);
        };
        t["glCompileShader"] = [](GL const& gl, GLuint shader) {
            gl.glCompileShader(shader);
        };
        t["glCompressedTexImage2D"] = [](GL const& gl,
                                         GLenum target,
                                         GLint level,
                                         GLint internalformat,
                                         GLsizei width,
                                         GLsizei height,
                                         GLint border,
                                         GLsizei imageSize,
                                         GLvoid const* data) {
            gl.glCompressedTexImage2D(target,
                                      level,
                                      internalformat,
                                      width,
                                      height,
                                      border,
                                      imageSize,
                                      data);
        };
        t["glCreateProgram"] = [](GL const& gl) {
            return gl.glCreateProgram();
        };
        t["glCreateShader"] = [](GL const& gl, GLenum type) {
            return gl.glCreateShader(type);
        };
        t["glCullFace"] = [](GL const& gl, GLenum mode) {
            gl.glCullFace(mode);
        };
        // functions D
        t["glDeleteBuffers"] =
            [](GL const& gl, GLsizei n, GLuint const* buffers) {
                gl.glDeleteBuffers(n, buffers);
            };
        t["glDeleteFramebuffers"] =
            [](GL const& gl, GLsizei n, GLuint const* framebuffers) {
                gl.glDeleteFramebuffers(n, framebuffers);
            };
        t["glDeleteProgram"] = [](GL const& gl, GLuint program) {
            gl.glDeleteProgram(program);
        };
        t["glDeleteRenderbuffers"] =
            [](GL const& gl, GLsizei n, GLuint const* renderbuffers) {
                gl.glDeleteRenderbuffers(n, renderbuffers);
            };
        t["glDeleteShader"] = [](GL const& gl, GLuint shader) {
            gl.glDeleteShader(shader);
        };
        t["glDeleteTextures"] =
            [](GL const& gl, GLsizei n, GLuint const* textures) {
                gl.glDeleteTextures(n, textures);
            };
        t["glDepthFunc"] = [](GL const& gl, GLenum func) {
            gl.glDepthFunc(func);
        };
        t["glDepthMask"] = [](GL const& gl, GLboolean flag) {
            gl.glDepthMask(flag);
        };
        t["glDepthRangef"] = [](GL const& gl, GLclampf zNear, GLclampf zFar) {
            gl.glDepthRangef(zNear, zFar);
        };
        t["glDetachShader"] = [](GL const& gl, GLuint program, GLuint shader) {
            gl.glDetachShader(program, shader);
        };
        t["glDisable"] = [](GL const& gl, GLenum cap) { gl.glDisable(cap); };
        t["glDisableVertexAttribArray"] = [](GL const& gl, GLuint index) {
            gl.glDisableVertexAttribArray(index);
        };
        t["glDrawArrays"] =
            [](GL const& gl, GLenum mode, GLint first, GLsizei count) {
                gl.glDrawArrays(mode, first, count);
            };
        t["glDrawElements"] = [](GL const& gl,
                                 GLenum mode,
                                 GLsizei count,
                                 GLenum type,
                                 GLvoid const* indices) {
            gl.glDrawElements(mode, count, type, indices);
        };
        // functions E
        t["glEnable"] = [](GL const& gl, GLenum cap) { gl.glEnable(cap); };
        t["glEnableVertexAttribArray"] = [](GL const& gl, GLuint index) {
            gl.glEnableVertexAttribArray(index);
        };
        // functions F
        t["glFinish"] = [](GL const& gl) { gl.glFinish(); };
        t["glFlush"] = [](GL const& gl) { gl.glFlush(); };
        t["glFramebufferRenderbuffer"] = [](GL const& gl,
                                            GLenum target,
                                            GLenum attachment,
                                            GLenum renderbuffertarget,
                                            GLuint renderbuffer) {
            gl.glFramebufferRenderbuffer(
                target, attachment, renderbuffertarget, renderbuffer);
        };
        t["glFramebufferTexture2D"] = [](GL const& gl,
                                         GLenum target,
                                         GLenum attachment,
                                         GLenum textarget,
                                         GLuint texture,
                                         GLint level) {
            gl.glFramebufferTexture2D(
                target, attachment, textarget, texture, level);
        };
        t["glFrontFace"] = [](GL const& gl, GLenum mode) {
            gl.glFrontFace(mode);
        };
        // functions G
        t["glGenBuffers"] = [](GL const& gl, GLsizei n, GLuint* buffers) {
            gl.glGenBuffers(n, buffers);
        };
        t["glGenerateMipmap"] = [](GL const& gl, GLenum target) {
            gl.glGenerateMipmap(target);
        };
        t["glGenFramebuffers"] =
            [](GL const& gl, GLsizei n, GLuint* framebuffers) {
                gl.glGenFramebuffers(n, framebuffers);
            };
        t["glGenRenderbuffers"] =
            [](GL const& gl, GLsizei n, GLuint* renderbuffers) {
                gl.glGenRenderbuffers(n, renderbuffers);
            };
        t["glGenTextures"] = [](GL const& gl, GLsizei n, GLuint* textures) {
            gl.glGenTextures(n, textures);
        };
        t["glGetActiveAttrib"] = [](GL const& gl,
                                    GLuint program,
                                    GLuint index,
                                    GLsizei bufsize,
                                    GLsizei* length,
                                    GLint* size,
                                    GLenum* type,
                                    GLchar* name) {
            gl.glGetActiveAttrib(
                program, index, bufsize, length, size, type, name);
        };
        t["glGetActiveUniform"] = [](GL const& gl,
                                     GLuint program,
                                     GLuint index,
                                     GLsizei bufsize,
                                     GLsizei* length,
                                     GLint* size,
                                     GLenum* type,
                                     GLchar* name) {
            gl.glGetActiveUniform(
                program, index, bufsize, length, size, type, name);
        };
        t["glGetAttribLocation"] =
            [](GL const& gl, GLuint program, GLchar const* name) {
                return gl.glGetAttribLocation(program, name);
            };
        t["glGetError"] = [](GL const& gl) { return gl.glGetError(); };
        t["glGetIntegerv"] = [](GL const& gl, GLenum pname, GLint* params) {
            gl.glGetIntegerv(pname, params);
        };
        t["glGetProgramInfoLog"] = [](GL const& gl,
                                      GLuint program,
                                      GLsizei bufSize,
                                      GLsizei* length,
                                      GLchar* infoLog) {
            gl.glGetProgramInfoLog(program, bufSize, length, infoLog);
        };
        t["glGetProgramiv"] =
            [](GL const& gl, GLuint program, GLenum pname, GLint* params) {
                // gl.glGetProgramiv(pname, params);
            };
        t["glGetShaderInfoLog"] = [](GL const& gl,
                                     GLuint shader,
                                     GLsizei bufSize,
                                     GLsizei* length,
                                     GLchar* infoLog) {
            gl.glGetShaderInfoLog(shader, bufSize, length, infoLog);
        };
        t["glGetShaderiv"] =
            [](GL const& gl, GLuint shader, GLenum pname, GLint* params) {
                gl.glGetShaderiv(shader, pname, params);
            };
        t["glGetString"] = [](GL const& gl, GLenum name) {
            return gl.glGetString(name);
        };
        t["glGetUniformLocation"] =
            [](GL const& gl, GLuint program, GLchar const* name) {
                return gl.glGetUniformLocation(program, name);
            };
        // function H
        t["glHint"] = [](GL const& gl, GLenum target, GLenum mode) {
            gl.glHint(target, mode);
        };
        // function I
        t["glIsEnabled"] = [](GL const& gl, GLenum cap) {
            gl.glIsEnabled(cap);
        };
        // function L
        t["glLinkProgram"] = [](GL const& gl, GLuint program) {
            gl.glLinkProgram(program);
        };
        // function P
        t["glPixelStorei"] = [](GL const& gl, GLenum pname, GLint param) {
            gl.glPixelStorei(pname, param);
        };
        t["glPolygonOffset"] = [](GL const& gl, GLfloat factor, GLfloat units) {
            gl.glPolygonOffset(factor, units);
        };
        // function R
        t["glReadPixels"] = [](GL const& gl,
                               GLint x,
                               GLint y,
                               GLsizei width,
                               GLsizei height,
                               GLenum format,
                               GLenum type,
                               GLvoid* pixels) {
            gl.glReadPixels(x, y, width, height, format, type, pixels);
        };
        t["glRenderbufferStorage"] = [](GL const& gl,
                                        GLenum target,
                                        GLenum internalformat,
                                        GLsizei width,
                                        GLsizei height) {
            gl.glRenderbufferStorage(target, internalformat, width, height);
        };
        // function S
        t["glSampleCoverage"] =
            [](GL const& gl, GLclampf value, GLboolean invert) {
                gl.glSampleCoverage(value, invert);
            };
        t["glScissor"] =
            [](GL const& gl, GLint x, GLint y, GLsizei width, GLsizei height) {
                gl.glScissor(x, y, width, height);
            };
        t["glShaderSource"] = [](GL const& gl,
                                 GLuint shader,
                                 GLsizei count,
                                 GLchar const* const* string,
                                 GLint const* length) {
            gl.glShaderSource(shader, count, string, length);
        };
        t["glStencilFunc"] =
            [](GL const& gl, GLenum func, GLint ref, GLuint mask) {
                gl.glStencilFunc(func, ref, mask);
            };
        t["glStencilOp"] =
            [](GL const& gl, GLenum fail, GLenum zfail, GLenum zpass) {
                gl.glStencilFunc(fail, zfail, zpass);
            };
        // function T
        t["glTexImage2D"] = [](GL const& gl,
                               GLenum target,
                               GLint level,
                               GLint internalformat,
                               GLsizei width,
                               GLsizei height,
                               GLint border,
                               GLenum format,
                               GLenum type,
                               void const* pixels) {
            gl.glTexImage2D(target,
                            level,
                            internalformat,
                            width,
                            height,
                            border,
                            format,
                            type,
                            pixels);
        };
        t["glTexParameteri"] =
            [](GL const& gl, GLenum target, GLenum pname, GLint param) {
                gl.glTexParameteri(target, pname, param);
            };
        // function U
        t["glUseProgram"] = [](GL const& gl, GLuint program) {
            gl.glUseProgram(program);
        };
        // function V
        t["glVertexAttribPointer"] = [](GL const& gl,
                                        GLuint index,
                                        GLint size,
                                        GLenum type,
                                        GLboolean normalized,
                                        GLsizei stride,
                                        GLvoid const* ptr) {
            gl.glVertexAttribPointer(
                index, size, type, normalized, stride, ptr);
        };
        t["glViewport"] =
            [](GL const& gl, GLint x, GLint y, GLsizei width, GLsizei height) {
                gl.glViewport(x, y, width, height);
            };

        // specific functions
        t["Program"] =
            sol::overload([](GL const& gl) { return new GLProgram{gl}; },
                          [](GL const& gl, std::string const& filepath) {
                              return new GLProgram{gl, Path{filepath}};
                          });
    }

    // GLProgram
    {
        auto t = luaState.new_usertype<GLProgram>("GLES2Program",
                                                  sol::no_constructor);
        t["glBegin"] = &GLProgram::glBegin;
        t["glEnd"] = &GLProgram::glEnd;
        t["glSetUniform"] = sol::overload(
            [](GLProgram& program, char const* name, float data) {
                program.glSetUniform(name, &data);
            },
            [](GLProgram& program, char const* name, Vec2 const& data) {
                program.glSetUniform(name, data.data());
            },
            [](GLProgram& program, char const* name, Vec3 const& data) {
                program.glSetUniform(name, data.data());
            },
            [](GLProgram& program, char const* name, Vec4 const& data) {
                program.glSetUniform(name, data.data());
            },
            [](GLProgram& program, char const* name, Mat3 const& data) {
                program.glSetUniform(name, data.data());
            },
            [](GLProgram& program, char const* name, Mat4 const& data) {
                program.glSetUniform(name, data.data());
            },
            [](GLProgram& program, char const* name, int data) {
                program.glSetUniform(name, &data);
            });
        t["glSetSampler"] =
            [](GLProgram const& program, GLenum pos, Id textureId) {
                program.glSetSampler(pos, textureId);
            };
        t["glDrawMesh"] = [](GLProgram const& program, Id meshId) {
            program.glDrawMesh(meshId);   
        };
        t["glDrawMeshWireframe"] = [](GLProgram const& program, Id meshId) {
            program.glDrawMeshWireframe(meshId);
        };
    }

    // functions
}
