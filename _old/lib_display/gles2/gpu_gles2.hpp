#pragma once

#include "../gpu.hpp"
#include <cstdint>
#include <lib_core/event.hpp>
#include <lib_core/io.hpp>
#include <lib_core/sdl/support_sdl.hpp>
#include <map>
#include <set>
#include <thread>

#if defined(_WIN32)
#    define GL_APIENTRY __stdcall
#else
#    define GL_APIENTRY
#endif

// OpenGL ES 2.0
namespace nopp
{
namespace gles2
{
using GLboolean = uint8_t;
using GLchar = char;
using GLenum = uint32_t;
using GLint = int32_t;
// using GLintptr = ptrdiff_t;
using GLsizei = int32_t;
using GLsizeiptr = ptrdiff_t;
using GLubyte = uint8_t;
using GLuint = uint32_t;
using GLvoid = void;
using GLfloat = float;
using GLclampf = float;
using GLbitfield = uint32_t;

enum glenums : uint32_t
{
    GL_NONE = 0,
    GL_NO_ERROR = 0,
    GL_ZERO = 0,
    GL_FALSE = 0,
    GL_TRUE = 1,
    GL_ONE = 1,

    GL_POINTS = 0x0000,
    GL_LINES = 0x0001,
    GL_LINE_LOOP = 0x0002,
    GL_LINE_STRIP = 0x0003,
    GL_TRIANGLES = 0x0004,
    GL_TRIANGLE_STRIP = 0x0005,
    GL_TRIANGLE_FAN = 0x0006,

    GL_NEVER = 0x0200,
    GL_LESS = 0x0201,
    GL_EQUAL = 0x0202,
    GL_LEQUAL = 0x0203,
    GL_GREATER = 0x0204,
    GL_NOTEQUAL = 0x0205,
    GL_GEQUAL = 0x0206,
    GL_ALWAYS = 0x0207,

    GL_SRC_COLOR = 0x0300,
    GL_ONE_MINUS_SRC_COLOR = 0x0301,
    GL_SRC_ALPHA = 0x0302,
    GL_ONE_MINUS_SRC_ALPHA = 0x0303,
    GL_DST_ALPHA = 0x0304,
    GL_ONE_MINUS_DST_ALPHA = 0x0305,
    GL_DST_COLOR = 0x0306,
    GL_ONE_MINUS_DST_COLOR = 0x0307,
    GL_SRC_ALPHA_SATURATE = 0x0308,

    GL_FRONT = 0x0404,
    GL_BACK = 0x0405,

    GL_INVALID_ENUM = 0x0500,
    GL_INVALID_VALUE = 0x0501,
    GL_INVALID_OPERATION = 0x0502,
    GL_OUT_OF_MEMORY = 0x0505,

    GL_CW = 0x0900,
    GL_CCW = 0x0901,

    GL_CULL_FACE = 0x0B44,
    GL_DEPTH_TEST = 0x0B71,
    GL_STENCIL_TEST = 0x0B90,
    GL_VIEWPORT = 0x0BA2,
    GL_BLEND = 0x0BE2,

    GL_SCISSOR_BOX = 0x0C10,
    GL_SCISSOR_TEST = 0x0C11,
    GL_UNPACK_ALIGNMENT = 0x0CF5,

    GL_MAX_TEXTURE_SIZE = 0x0D33,
    GL_TEXTURE_2D = 0x0DE1,

    GL_DONT_CARE = 0x1100,
    GL_FASTEST = 0x1101,
    GL_NICEST = 0x1102,

    GL_BYTE = 0x1400,
    GL_UNSIGNED_BYTE = 0x1401,
    GL_SHORT = 0x1402,
    GL_UNSIGNED_SHORT = 0x1403,
    GL_INT = 0x1404,
    GL_UNSIGNED_INT = 0x1405,
    GL_FLOAT = 0x1406,

    GL_DEPTH_COMPONENT = 0x1902,
    GL_RGB = 0x1907,
    GL_RGBA = 0x1908,

    GL_KEEP = 0x1E00,
    GL_REPLACE = 0x1E01,
    GL_INCR = 0x1E02,
    GL_DECR = 0x1E03,

    GL_VENDOR = 0x1F00,
    GL_RENDERER = 0x1F01,
    GL_VERSION = 0x1F02,
    GL_EXTENSIONS = 0x1F03,

    GL_NEAREST = 0x2600,
    GL_LINEAR = 0x2601,
    GL_NEAREST_MIPMAP_NEAREST = 0x2700,
    GL_LINEAR_MIPMAP_NEAREST = 0x2701,
    GL_NEAREST_MIPMAP_LINEAR = 0x2702,
    GL_LINEAR_MIPMAP_LINEAR = 0x2703,
    GL_TEXTURE_MAG_FILTER = 0x2800,
    GL_TEXTURE_MIN_FILTER = 0x2801,
    GL_TEXTURE_WRAP_S = 0x2802,
    GL_TEXTURE_WRAP_T = 0x2803,
    GL_REPEAT = 0x2901,

    GL_CONSTANT_COLOR = 0x8001,
    GL_ONE_MINUS_CONSTANT_COLOR = 0x8002,
    GL_CONSTANT_ALPHA = 0x8003,
    GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004,
    GL_BLEND_COLOR = 0x8005,
    GL_FUNC_ADD = 0x8006,
    GL_BLEND_EQUATION = 0x8009,
    GL_BLEND_EQUATION_RGB = 0x8009,
    GL_FUNC_SUBTRACT = 0x800A,
    GL_FUNC_REVERSE_SUBTRACT = 0x800B,

    GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034,
    GL_POLYGON_OFFSET_FILL = 0x8037,
    GL_POLYGON_OFFSET_FACTOR = 0x8038,
    GL_RGB5_A1 = 0x8057,
    GL_TEXTURE_BINDING_2D = 0x8069,
    GL_BLEND_DST_RGB = 0x80C8,
    GL_BLEND_SRC_RGB = 0x80C9,
    GL_BLEND_DST_ALPHA = 0x80CA,
    GL_BLEND_SRC_ALPHA = 0x80CB,

    GL_CLAMP_TO_EDGE = 0x812F,
    GL_GENERATE_MIPMAP_HINT = 0x8192,
    GL_DEPTH_COMPONENT16 = 0x81A5,
    GL_UNSIGNED_SHORT_5_6_5 = 0x8363,
    GL_MIRRORED_REPEAT = 0x8370,
    // GL_COMPRESSED_RGB_S3TC_DXT1_EXT  = 0x83F0,
    // GL_COMPRESSED_RGBA_S3TC_DXT1_EXT = 0x83F1,
    // GL_COMPRESSED_RGBA_S3TC_DXT3_EXT = 0x83F2,
    // GL_COMPRESSED_RGBA_S3TC_DXT5_EXT = 0x83F3,

    GL_TEXTURE0 = 0x84C0,
    GL_ACTIVE_TEXTURE = 0x84E0,
    GL_TEXTURE_CUBE_MAP = 0x8513,
    GL_TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A,
    GL_MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C,
    GL_NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2,
    GL_COMPRESSED_TEXTURE_FORMATS = 0x86A3,

    GL_BLEND_EQUATION_ALPHA = 0x883D,

    GL_ARRAY_BUFFER = 0x8892,
    GL_ELEMENT_ARRAY_BUFFER = 0x8893,
    GL_ARRAY_BUFFER_BINDING = 0x8894,
    GL_ELEMENT_ARRAY_BUFFER_BINDING = 0x8895,
    GL_STREAM_DRAW = 0x88E0,
    GL_STATIC_DRAW = 0x88E4,
    GL_DYNAMIC_DRAW = 0x88E8,

    GL_FRAGMENT_SHADER = 0x8B30,
    GL_VERTEX_SHADER = 0x8B31,
    GL_FLOAT_VEC2 = 0x8B50,
    GL_FLOAT_VEC3 = 0x8B51,
    GL_FLOAT_VEC4 = 0x8B52,
    GL_INT_VEC2 = 0x8B53,
    GL_INT_VEC3 = 0x8B54,
    GL_INT_VEC4 = 0x8B55,
    GL_FLOAT_MAT2 = 0x8B5A,
    GL_FLOAT_MAT3 = 0x8B5B,
    GL_FLOAT_MAT4 = 0x8B5C,
    GL_SAMPLER_2D = 0x8B5E,
    GL_SAMPLER_CUBE = 0x8B60,

    GL_COMPILE_STATUS = 0x8B81,
    GL_LINK_STATUS = 0x8B82,
    GL_INFO_LOG_LENGTH = 0x8B84,
    GL_ACTIVE_UNIFORMS = 0x8B86,
    GL_ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87,
    GL_ACTIVE_ATTRIBUTES = 0x8B89,
    GL_ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A,
    GL_SHADING_LANGUAGE_VERSION = 0x8B8C,
    GL_CURRENT_PROGRAM = 0x8B8D,

    GL_FRAMEBUFFER_BINDING = 0x8CA6,
    GL_FRAMEBUFFER_COMPLETE = 0x8CD5,
    GL_COLOR_ATTACHMENT0 = 0x8CE0,
    GL_DEPTH_ATTACHMENT = 0x8D00,
    GL_STENCIL_ATTACHMENT = 0x8D20,
    GL_FRAMEBUFFER = 0x8D40,
    GL_RENDERBUFFER = 0x8D41,
    GL_STENCIL_INDEX8 = 0x8D48,

    GL_DEPTH_BUFFER_BIT = 0x00000100,
    GL_STENCIL_BUFFER_BIT = 0x00000400,
    GL_COLOR_BUFFER_BIT = 0x00004000,

    // extensions
    GL_ETC1_RGB8_OES = 0x8D64,
};

class GLData;
struct GL
{
    void(GL_APIENTRY* glActiveTexture)(GLenum texture) = nullptr;
    void(GL_APIENTRY* glAttachShader)(GLuint program, GLuint shader) = nullptr;

    void(GL_APIENTRY* glBindAttribLocation)(GLuint program,
                                            GLuint index,
                                            GLchar const* name) = nullptr;
    void(GL_APIENTRY* glBindBuffer)(GLenum target, GLuint buffer) = nullptr;
    void(GL_APIENTRY* glBindFramebuffer)(GLenum target,
                                         GLuint framebuffer) = nullptr;
    void(GL_APIENTRY* glBindRenderbuffer)(GLenum target,
                                          GLuint renderbuffer) = nullptr;
    void(GL_APIENTRY* glBindTexture)(GLenum target, GLuint texture) = nullptr;
    void(GL_APIENTRY* glBlendColor)(GLclampf red,
                                    GLclampf green,
                                    GLclampf blue,
                                    GLclampf alpha) = nullptr;
    void(GL_APIENTRY* glBlendEquation)(GLenum mode) = nullptr;
    void(GL_APIENTRY* glBlendEquationSeparate)(GLenum modeRGB,
                                               GLenum modeAlpha) = nullptr;
    void(GL_APIENTRY* glBlendFunc)(GLenum sfactor, GLenum dfactor) = nullptr;
    void(GL_APIENTRY* glBlendFuncSeparate)(GLenum srcRGB,
                                           GLenum dstRGB,
                                           GLenum srcAlpha,
                                           GLenum dstAlpha) = nullptr;
    void(GL_APIENTRY* glBufferData)(GLenum target,
                                    GLsizeiptr size,
                                    GLvoid const* data,
                                    GLenum usage) = nullptr;

    GLenum(GL_APIENTRY* glCheckFramebufferStatus)(GLenum target) = nullptr;
    void(GL_APIENTRY* glClear)(GLbitfield mask) = nullptr;
    void(GL_APIENTRY* glClearColor)(GLclampf red,
                                    GLclampf green,
                                    GLclampf blue,
                                    GLclampf alpha) = nullptr;
    void(GL_APIENTRY* glClearDepthf)(GLclampf depth) = nullptr;
    void(GL_APIENTRY* glClearStencil)(GLint s) = nullptr;
    void(GL_APIENTRY* glColorMask)(GLboolean red,
                                   GLboolean green,
                                   GLboolean blue,
                                   GLboolean alpha) = nullptr;
    void(GL_APIENTRY* glCompileShader)(GLuint shader) = nullptr;
    void(GL_APIENTRY* glCompressedTexImage2D)(GLenum target,
                                              GLint level,
                                              GLint internalformat,
                                              GLsizei width,
                                              GLsizei height,
                                              GLint border,
                                              GLsizei imageSize,
                                              GLvoid const* data) = nullptr;
    GLuint(GL_APIENTRY* glCreateProgram)() = nullptr;
    GLuint(GL_APIENTRY* glCreateShader)(GLenum type) = nullptr;
    void(GL_APIENTRY* glCullFace)(GLenum mode) = nullptr;

    void(GL_APIENTRY* glDeleteBuffers)(GLsizei n,
                                       GLuint const* buffers) = nullptr;
    void(GL_APIENTRY* glDeleteFramebuffers)(
        GLsizei n, GLuint const* framebuffers) = nullptr;
    void(GL_APIENTRY* glDeleteProgram)(GLuint program) = nullptr;
    void(GL_APIENTRY* glDeleteRenderbuffers)(
        GLsizei n, GLuint const* renderbuffers) = nullptr;
    void(GL_APIENTRY* glDeleteShader)(GLuint shader) = nullptr;
    void(GL_APIENTRY* glDeleteTextures)(GLsizei n,
                                        GLuint const* textures) = nullptr;
    void(GL_APIENTRY* glDepthFunc)(GLenum func) = nullptr;
    void(GL_APIENTRY* glDepthMask)(GLboolean flag) = nullptr;
    void(GL_APIENTRY* glDepthRangef)(GLclampf zNear, GLclampf zFar) = nullptr;
    void(GL_APIENTRY* glDetachShader)(GLuint program, GLuint shader) = nullptr;
    void(GL_APIENTRY* glDisable)(GLenum cap) = nullptr;
    void(GL_APIENTRY* glDisableVertexAttribArray)(GLuint index) = nullptr;
    void(GL_APIENTRY* glDrawArrays)(GLenum mode,
                                    GLint first,
                                    GLsizei count) = nullptr;
    void(GL_APIENTRY* glDrawElements)(GLenum mode,
                                      GLsizei count,
                                      GLenum type,
                                      GLvoid const* indices) = nullptr;

    void(GL_APIENTRY* glEnable)(GLenum cap) = nullptr;
    void(GL_APIENTRY* glEnableVertexAttribArray)(GLuint index) = nullptr;

    void(GL_APIENTRY* glFinish)() = nullptr;
    void(GL_APIENTRY* glFlush)() = nullptr;
    void(GL_APIENTRY* glFramebufferRenderbuffer)(GLenum target,
                                                 GLenum attachment,
                                                 GLenum renderbuffertarget,
                                                 GLuint renderbuffer) = nullptr;
    void(GL_APIENTRY* glFramebufferTexture2D)(GLenum target,
                                              GLenum attachment,
                                              GLenum textarget,
                                              GLuint texture,
                                              GLint level) = nullptr;
    void(GL_APIENTRY* glFrontFace)(GLenum mode) = nullptr;

    void(GL_APIENTRY* glGenBuffers)(GLsizei n, GLuint* buffers) = nullptr;
    void(GL_APIENTRY* glGenerateMipmap)(GLenum target) = nullptr;
    void(GL_APIENTRY* glGenFramebuffers)(GLsizei n,
                                         GLuint* framebuffers) = nullptr;
    void(GL_APIENTRY* glGenRenderbuffers)(GLsizei n,
                                          GLuint* renderbuffers) = nullptr;
    void(GL_APIENTRY* glGenTextures)(GLsizei n, GLuint* textures) = nullptr;
    void(GL_APIENTRY* glGetActiveAttrib)(GLuint program,
                                         GLuint index,
                                         GLsizei bufsize,
                                         GLsizei* length,
                                         GLint* size,
                                         GLenum* type,
                                         GLchar* name) = nullptr;
    void(GL_APIENTRY* glGetActiveUniform)(GLuint program,
                                          GLuint index,
                                          GLsizei bufsize,
                                          GLsizei* length,
                                          GLint* size,
                                          GLenum* type,
                                          GLchar* name) = nullptr;
    GLint(GL_APIENTRY* glGetAttribLocation)(GLuint program,
                                            GLchar const* name) = nullptr;
    GLenum(GL_APIENTRY* glGetError)() = nullptr;
    void(GL_APIENTRY* glGetIntegerv)(GLenum pname, GLint* params) = nullptr;
    void(GL_APIENTRY* glGetProgramInfoLog)(GLuint program,
                                           GLsizei bufSize,
                                           GLsizei* length,
                                           GLchar* infoLog) = nullptr;
    void(GL_APIENTRY* glGetProgramiv)(GLuint program,
                                      GLenum pname,
                                      GLint* params) = nullptr;
    void(GL_APIENTRY* glGetShaderInfoLog)(GLuint shader,
                                          GLsizei bufSize,
                                          GLsizei* length,
                                          GLchar* infoLog) = nullptr;
    void(GL_APIENTRY* glGetShaderiv)(GLuint shader,
                                     GLenum pname,
                                     GLint* params) = nullptr;
    GLubyte const*(GL_APIENTRY* glGetString)(GLenum name) = nullptr;
    GLint(GL_APIENTRY* glGetUniformLocation)(GLuint program,
                                             GLchar const* name) = nullptr;

    void(GL_APIENTRY* glHint)(GLenum target, GLenum mode) = nullptr;

    GLboolean(GL_APIENTRY* glIsEnabled)(GLenum cap) = nullptr;

    void(GL_APIENTRY* glLinkProgram)(GLuint program) = nullptr;

    void(GL_APIENTRY* glPixelStorei)(GLenum pname, GLint param) = nullptr;
    void(GL_APIENTRY* glPolygonOffset)(GLfloat factor, GLfloat units) = nullptr;

    void(GL_APIENTRY* glReadPixels)(GLint x,
                                    GLint y,
                                    GLsizei width,
                                    GLsizei height,
                                    GLenum format,
                                    GLenum type,
                                    GLvoid* pixels) = nullptr;
    void(GL_APIENTRY* glRenderbufferStorage)(GLenum target,
                                             GLenum internalformat,
                                             GLsizei width,
                                             GLsizei height) = nullptr;

    void(GL_APIENTRY* glSampleCoverage)(GLclampf value,
                                        GLboolean invert) = nullptr;
    void(GL_APIENTRY* glScissor)(GLint x,
                                 GLint y,
                                 GLsizei width,
                                 GLsizei height) = nullptr;
    void(GL_APIENTRY* glShaderSource)(GLuint shader,
                                      GLsizei count,
                                      GLchar const* const* string,
                                      GLint const* length) = nullptr;
    void(GL_APIENTRY* glStencilFunc)(GLenum func,
                                     GLint ref,
                                     GLuint mask) = nullptr;
    void(GL_APIENTRY* glStencilOp)(GLenum fail,
                                   GLenum zfail,
                                   GLenum zpass) = nullptr;

    void(GL_APIENTRY* glTexImage2D)(GLenum target,
                                    GLint level,
                                    GLint internalformat,
                                    GLsizei width,
                                    GLsizei height,
                                    GLint border,
                                    GLenum format,
                                    GLenum type,
                                    void const* pixels) = nullptr;
    void(GL_APIENTRY* glTexParameteri)(GLenum target,
                                       GLenum pname,
                                       GLint param) = nullptr;

    void(GL_APIENTRY* glUniform1fv)(GLint location,
                                    GLsizei count,
                                    GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform2fv)(GLint location,
                                    GLsizei count,
                                    GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform3fv)(GLint location,
                                    GLsizei count,
                                    GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform4fv)(GLint location,
                                    GLsizei count,
                                    GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform1i)(GLint location, GLint x) = nullptr;
    void(GL_APIENTRY* glUniform1iv)(GLint location,
                                    GLsizei count,
                                    GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniform2iv)(GLint location,
                                    GLsizei count,
                                    GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniform3iv)(GLint location,
                                    GLsizei count,
                                    GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniform4iv)(GLint location,
                                    GLsizei count,
                                    GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniformMatrix2fv)(GLint location,
                                          GLsizei count,
                                          GLboolean transpose,
                                          GLfloat const* value) = nullptr;
    void(GL_APIENTRY* glUniformMatrix3fv)(GLint location,
                                          GLsizei count,
                                          GLboolean transpose,
                                          GLfloat const* value) = nullptr;
    void(GL_APIENTRY* glUniformMatrix4fv)(GLint location,
                                          GLsizei count,
                                          GLboolean transpose,
                                          GLfloat const* value) = nullptr;
    void(GL_APIENTRY* glUseProgram)(GLuint program) = nullptr;

    void(GL_APIENTRY* glVertexAttribPointer)(GLuint index,
                                             GLint size,
                                             GLenum type,
                                             GLboolean normalized,
                                             GLsizei stride,
                                             GLvoid const* ptr) = nullptr;
    void(GL_APIENTRY* glViewport)(GLint x,
                                  GLint y,
                                  GLsizei width,
                                  GLsizei height) = nullptr;

    // window system's specific functions
    std::function<void(SDL_Window*)> wglMakeCurrent;
    std::function<void(SDL_Window*)> wglSwapWindow;

    // thread's specific functions
    std::function<GLData*(Id)> glData;
};

////////////////////////////////////////////////////////////////////////////////

class GLData
{
  public:
    virtual ~GLData() = default;

    virtual void glUpdate(GPUData* data) = 0;
};

class GLMesh final : public GLData
{
  public:
    struct GLVertexComponent final
    {
        GLenum type = 0;
        GLint size = 0; // number of values
    };

    struct GLVertexAttribute final
    {
        GLuint glVBId = 0;
        GLint size = 0;
        GLenum type = 0;
        GLboolean normalized = 0;
        GLsizei stride = 0;
        size_t offset = 0;
    };

    GLMesh(GL const& gl);

    GLMesh(GL const& gl, GPUData* data);

    GLMesh(GL const& gl,
           std::vector<float> const& vertexData,
           std::vector<GLVertexComponent> const& vertexLayout);

    GLMesh(GL const& gl,
           std::vector<float> const& vertexData,
           std::vector<GLVertexComponent> const& vertexLayout,
           std::vector<uint16_t> const& indexData);

    ~GLMesh() override;

    void glUpdate(GPUData* data) override;

  public:
    std::vector<GLVertexAttribute> glVertexAttributes;
    std::vector<GLuint> glVBIds;
    GLuint glIBId = 0;
    GLsizei count = 0;
    GLenum mode = GL_TRIANGLES;
    GLuint glIBWireframeId = 0;
    GLsizei wireframeCount = 0;

  private:
    void _glDelete();

    GL const& _gl;
};

class GLTexture : public GLData
{
  public:
    GLTexture(GL const& gl);

    GLTexture(GL const& gl, GPUData* data);

    GLTexture(GL const& gl, Path const& filepath);

    ~GLTexture() override;

    void glUpdate(GPUData* data) override;

  public:
    GLenum glType = 0;
    GLuint glId = 0;

  private:
    void _glDelete();

    GL const& _gl;
};

////////////////////////////////////////////////////////////////////////////////

class GLFramebuffer final
{
  public:
    GLFramebuffer(GL const& gl, bool hasColor, bool hasDepth, bool hasStencil);

    ~GLFramebuffer();

    void glResize(GLsizei width, GLsizei height);

    void glUseFramebuffer() { _gl.glBindFramebuffer(GL_FRAMEBUFFER, _glId); }

  private:
    GL const& _gl;
    GLuint _glId = 0;
    GLuint _glColorId = 0;
    GLuint _glDepthId = 0;
    GLuint _glStencilId = 0;
    GLsizei _width = 0;
    GLsizei _height = 0;
};

GLuint glCreateFramebuffer(GL const& gl,
                           bool hasColor,
                           bool hasDepth,
                           bool hasStencil);

void glDeleteFramebuffer(GL const& gl, GLuint glId);

void glResizeFramebuffer(GL const& gl,
                         GLuint glId,
                         GLsizei width,
                         GLsizei height);

std::string glCompileAndAttachShader(GL const& gl,
                                     GLuint glProgramId,
                                     GLenum glShaderType,
                                     GLchar const* shaderCode);

inline std::string glCompileAndAttachShader(GL const& gl,
                                            GLuint glProgramId,
                                            GLenum glShaderType,
                                            std::string const& shaderCode)
{
    return glCompileAndAttachShader(
        gl, glProgramId, glShaderType, shaderCode.c_str());
}

class GLProgram final
{
  public:
    struct ProgramStatus final
    {
        std::string source;
        std::string intermediate; // intermediate language, depends on platform
        std::vector<uint8_t> binary; // binary version of the program
        uint32_t binaryFormat = 0;   // implementation dependent
        std::string error;           // contains error message if any
    };

    GLProgram(GL const& gl);

    GLProgram(GL const& gl,
              std::string const& code,
              std::vector<Path> const& includeSearchDirpaths);

    GLProgram(GL const& gl, Path const& filepath);

    ~GLProgram();

    void glSetCode(std::string const& code,
                   std::vector<Path> const& includeSearchDirpaths);

    void glBegin() const;

    void glEnd() const;

    void glSetUniform(HashValue name, void const* data) const;

    void glSetSampler(GLenum pos, GLTexture* texture) const;

    void glSetSamplers(std::vector<GLTexture*> textures) const;

    void glDrawMesh(GLMesh* glMesh) const;

    void glDrawMeshWireframe(GLMesh* glMesh) const;

    // convenient functions

    void glSetUniform(std::string const& name, void const* data) const
    {
        glSetUniform(computeHash(name), data);
    }

    void glSetSampler(GLenum pos, Id textureId) const
    {
        glSetSampler(pos, static_cast<GLTexture*>(_gl.glData(textureId)));
    }

    void glDrawMesh(Id meshId) const
    {
        glDrawMesh(static_cast<GLMesh*>(_gl.glData(meshId)));
    }

    void glDrawMeshWireframe(Id meshId) const
    {
        glDrawMeshWireframe(static_cast<GLMesh*>(_gl.glData(meshId)));
    }

  public:
    ProgramStatus status;

  private:
    struct _Uniform final
    {
        HashValue name = HashInvalid;
        GLint location = -1;
        GLenum type = GL_INVALID_ENUM;
        GLsizei count = 0;
    };

    struct _Sampler final
    {
        GLint wrapS = GL_CLAMP_TO_EDGE;
        GLint wrapT = GL_CLAMP_TO_EDGE;
        GLint minFilter = GL_NEAREST;
        GLint magFilter = GL_NEAREST;
    };

    GL const& _gl;
    GLuint _glId = 0;
    std::map<HashValue, _Uniform> _uniforms;
    std::array<_Sampler, 8> _samplers;
    std::vector<GLuint> _attributes;
};

////////////////////////////////////////////////////////////////////////////////

#if defined(NDEBUG)
template <typename... T> void glCheckError(T&&...) {}
#else
void glCheckError(GL const& gl, char const* func);
#endif

struct GLFence final
{
    Event event;

    GLFence() { event.raise(); }

    bool isReached() const { return event.isRaised(); }

    void wait() { event.wait(); }

    void waitFor(int32_t ms) { event.waitFor(ms); }
};

using GLCmd = std::function<void(GL const&)>;

class GLThread : public GPUThread
{
  public:
    GLThread();

    ~GLThread() override;

    void push(std::vector<GLCmd> const& cmds, GLFence* fence);

    void push(std::vector<GLCmd> const& cmds) { push(cmds, nullptr); }

    // void push(GLCmd const& cmd, GLFence* fence) { push({cmd}, fence); }
    // void push(GLCmd const& cmd) { push({cmd}, nullptr); }

    void setOnTerminate(std::vector<GLCmd> const& cmds);

    // void pushOnTerminate(GLCmd const& cmd) { pushOnTerminate({cmd}); }

    // update will be done on gl thread
    virtual void updateData(Id id) override;

    // destroy will be done on gl thread
    virtual void destroyData(Id id) override;

  private:
    void _funcThread();

    void _glInitialize();

    void _glTerminate();

    GLData* _getData(Id id);

    std::vector<GLCmd> _cmdsWait;
    std::vector<GLCmd> _cmdsOnTerminate;
    std::thread* _thread = nullptr;
    std::mutex _swapMutex;
    std::condition_variable _swapCond;
    std::atomic<bool> _isThreadInterrupted{false};

    std::set<Id> _tmpUpdateIds;
    std::set<Id> _updateIds;
    std::mutex _updateMutex;
    std::set<Id> _tmpDestroyIds;
    std::set<Id> _destroyIds;
    std::mutex _destroyMutex;

    // gl data
    SDL_GLContext _glContext = nullptr;
    GL _gl;
    std::vector<GLData*> _glDatas;
};

} // namespace gles2
} // namespace nopp
