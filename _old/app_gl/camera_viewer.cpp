#include "camera_viewer.hpp"

using namespace nopp;

namespace
{
auto const MouseSpeed = 0.003f;
}

void CameraEditorSimple::onMousePress(Vec2 const& mousePos,
                                      bool keyCtrl,
                                      bool keyShift,
                                      bool keyAlt)
{
    if (keyAlt == false)
        return;

    if (keyShift == true)
        _uiMode = _eUIMode::Padding;
    else if (keyCtrl == true)
        _uiMode = _eUIMode::Zooming;
    else
        _uiMode = _eUIMode::Rotating;

    _mouseLastPosition = mousePos;
    switch (_uiMode)
    {
    case _eUIMode::Rotating:
        _cameraLastAngleLateral = lateral();
        _cameraLastAngleLongitudinal = longitudinal();
        break;
    }
}

void CameraEditorSimple::onMouseMove(Vec2 const& mousePos)
{
    switch (_uiMode)
    {
    case _eUIMode::Rotating:
    {
        auto zsign = (up().z() < 0.0f) ? -1.0f : 1.0f;
        auto late =
            zsign * (_mouseLastPosition.x() - mousePos.x()) * MouseSpeed +
            _cameraLastAngleLateral;
        auto longi = (_mouseLastPosition.y() - mousePos.y()) * MouseSpeed +
                     _cameraLastAngleLongitudinal;
        rotate(late, longi);
    }
    break;
    case _eUIMode::Padding:
    {
        auto step = Vec2{_mouseLastPosition.x() - mousePos.x(),
                         _mouseLastPosition.y() - mousePos.y()};
        step *= 0.005f;
        _mouseLastPosition = mousePos;
        pan(step);
    }
    break;
    case _eUIMode::Zooming:
    {
        auto dist =
            distance(Vec2{_mouseLastPosition.x(), _mouseLastPosition.y()},
                     Vec2{mousePos.x(), mousePos.y()});
        auto step = dist *
                    ((_mouseLastPosition.y() > mousePos.y()) ? 1.0f : -1.0f) *
                    0.01f;
        _mouseLastPosition = mousePos;
        zoom(step);
    }
    break;
    }
}

void CameraEditorSimple::onMouseRelease(Vec2 const& mousePos)
{
    switch (_uiMode)
    {
    case _eUIMode::Rotating:
    {
        auto zsign = (up().z() < 0.0f) ? -1.0f : 1.0f;
        auto late =
            zsign * (_mouseLastPosition.x() - mousePos.x()) * MouseSpeed +
            _cameraLastAngleLateral;
        auto longi = (_mouseLastPosition.y() - mousePos.y()) * MouseSpeed +
                     _cameraLastAngleLongitudinal;
        rotate(late, longi);
    }
    break;
    }
    _uiMode = _eUIMode::NbUIMode;
}

void loadLuaCamera(sol::state& luaState)
{
    // camera editor simple
    {
        auto t = luaState.new_usertype<CameraEditorSimple>(
            "CameraEditorSimple",
            sol::call_constructor,
            sol::constructors<CameraEditorSimple()>(),
            sol::base_classes,
            sol::bases<nopp::CameraEditor>());
        t["onMousePress"] = &CameraEditorSimple::onMousePress;
        t["onMouseMove"] = &CameraEditorSimple::onMouseMove;
        t["onMouseRelease"] = &CameraEditorSimple::onMouseRelease;
    }
}
