#include "defaults.hpp"
#include <lib_core/api.hpp>

using namespace nopp;

void glInitializeDefault(gles2::GL const& gl)
{
    logInfo("glInitializeDefault");
}

void glFrameDefault(gles2::GL const& gl)
{
    gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    gl.glClear(gles2::GL_COLOR_BUFFER_BIT);
}

void glTerminateDefault(gles2::GL const& gl) { logInfo("glTerminateDefault"); }

void glImGuiDrawDefault(gles2::GL const& gl) { ImGui::ShowDemoWindow(); }

void worldInitializeDefault() { logInfo("worldInitializeDefault"); }

void worldUpdateDefault() {}

void worldTerminateDefault() { logInfo("worldTerminateDefault"); }

void eventMousePressDefault() {}

void eventMouseReleaseDefault() {}

void eventMouseMoveDefault() {}

void eventKeyPressDefault() {}

void eventKeyReleaseDefault() {}

void eventWindowResize() {}
