#pragma once

#include <cassert>
#include <vector>
#include "size.hpp"

namespace nopp
{

enum class ePixelFormat : uint32_t
{
    Unknown = 0,
    // pixel formats
    RGBA8_UNorm,
    RGBx8_UNorm,
    R8_UNorm,
    RGBA16_Float,
    RGBA32_Float,
    // compressed formats
    RGB_ETC1
};

constexpr bool isPixelFormatCompressed(ePixelFormat pixelFormat)
{
    return (pixelFormat == ePixelFormat::RGB_ETC1);
}

constexpr int32_t toByteSize(Size3i const& size, ePixelFormat pixelFormat)
{
    switch (pixelFormat)
    {
    case ePixelFormat::RGBx8_UNorm:
    case ePixelFormat::RGBA8_UNorm: return size.width * size.height * size.depth * 4;
    case ePixelFormat::R8_UNorm: return size.width * size.height * size.depth;
    case ePixelFormat::RGBA16_Float: return size.width * size.height * size.depth * 8;
    case ePixelFormat::RGBA32_Float: return size.width * size.height * size.depth * 16;
    case ePixelFormat::RGB_ETC1:
        // case ePixelFormat::RGB_S3TC_DXT1:
        {
            auto f = [](int32_t dim) { return std::max<int32_t>(1, ((dim + 3) / 4)); };
            return f(size.width) * f(size.height) * 8;
        }
    default: return -1;
    }
}

constexpr int32_t toByteSize(ePixelFormat pixelFormat)
{
    switch (pixelFormat)
    {
    case ePixelFormat::RGBx8_UNorm:
    case ePixelFormat::RGBA8_UNorm: return 4;
    case ePixelFormat::R8_UNorm: return 1;
    case ePixelFormat::RGBA16_Float: return 8;
    case ePixelFormat::RGBA32_Float: return 16;
    case ePixelFormat::RGB_ETC1: return 8;
    default: return -1;
    }
}

int32_t findMipLevelCount(Size3i const& size)
{
    auto maxDimension = std::max(std::max(size.width, size.height), size.depth);
    auto count = 1;
    while (maxDimension >>= 1)
    {
        ++count;
    }
    return count;
}

} // namespace nopp

#if defined(WITH_SDL)
#include "io.hpp"
#include "support_sdl.hpp"

namespace nopp
{

struct BestPixelFormat
{
    ePixelFormat pixelFormat = ePixelFormat::Unknown;
    uint32_t sdlPixelFormat = 0;
};

constexpr BestPixelFormat findBestPixelFormat(uint32_t sdlPixelFormat)
{
    switch (sdlPixelFormat)
    {
    case SDL_PIXELFORMAT_RGBA32:
        return BestPixelFormat{ePixelFormat::RGBA8_UNorm, SDL_PIXELFORMAT_RGBA32};
    case SDL_PIXELFORMAT_RGB24:
        return BestPixelFormat{ePixelFormat::RGBx8_UNorm, SDL_PIXELFORMAT_RGBA32};
    default: return BestPixelFormat();
    }
}

constexpr uint32_t toSDLPixelFormat(ePixelFormat pixelFormat)
{
    switch (pixelFormat)
    {
    case ePixelFormat::RGBA8_UNorm:
    case ePixelFormat::RGBx8_UNorm: return SDL_PIXELFORMAT_RGBA32; break;
    default: return SDL_PIXELFORMAT_UNKNOWN;
    }
}

} // namespace nopp

#endif // WITH_SDL

namespace nopp
{

struct TextureDesc final
{
    Size3i size;
    ePixelFormat pixelFormat = ePixelFormat::Unknown;
    // negative value => automatic mip generation
    int32_t mipLevelCount = -1;
    int32_t arraySize = 1;
    bool isCubemap = false;

    constexpr TextureDesc() = default;

    constexpr TextureDesc(Size3i const& size, ePixelFormat pixelFormat)
        : size(size), pixelFormat(pixelFormat)
    {
    }

    constexpr TextureDesc(int32_t width, int32_t height, ePixelFormat pixelFormat)
        : size(width, height, 1), pixelFormat(pixelFormat)
    {
    }

    constexpr bool isValid() const
    {
        return (size.isValid() == true) && (pixelFormat != ePixelFormat::Unknown) &&
               (arraySize > 0);
    }

    constexpr bool operator==(TextureDesc const& textureDesc) const
    {
        return (size == textureDesc.size) && (pixelFormat == textureDesc.pixelFormat);
    }

    constexpr bool operator!=(TextureDesc const& textureDesc) const
    {
        return !(*this == textureDesc);
    }

    constexpr int32_t byteSize() const { return toByteSize(size, pixelFormat) * arraySize; }
};

using ImageData = std::vector<uint8_t>;

class Texture final
{
  public:
    Texture() = default;

    Texture(TextureDesc const& textureDesc) { create(textureDesc); }

    Texture(Texture const& texture) { _copy(texture); }

    Texture& operator=(Texture const& texture)
    {
        assert(this != &texture);
        _copy(texture);
        return *this;
    }

    void create(TextureDesc const& textureDesc)
    {
        desc = textureDesc;
        assert(desc.isValid() == true);
        // get correct value for mip level count
        auto mipLevelCount = desc.mipLevelCount;
        if (mipLevelCount == -1)
        {
            mipLevelCount = 1;
        }
        assert(mipLevelCount > 0);
        if (mipLevelCount != 1)
        {
            assert(mipLevelCount == findMipLevelCount(desc.size));
        }
        levels.resize(mipLevelCount);
        auto imgSize = desc.size;
        for (auto& level : levels)
        {
            level.images.resize(desc.arraySize);
            level.size = imgSize;
            auto imgByteSize = toByteSize(level.size, desc.pixelFormat);
            for (auto& image : level.images)
            {
                image.resize(imgByteSize);
            }
            imgSize.width = std::max<int32_t>(1, imgSize.width >> 1);
            imgSize.height = std::max<int32_t>(1, imgSize.height >> 1);
            imgSize.depth = std::max<int32_t>(1, imgSize.depth >> 1);
        }
    }

    bool isValid() const { return (levels.size() != 0) && (desc.isValid() == true); }

    ImageData& data(int32_t level, int32_t pos) { return levels[level].images[pos]; }

    ImageData const& data(int32_t level, int32_t pos) const { return levels[level].images[pos]; }

  public:
    struct MipLevel final
    {
        std::vector<ImageData> images;
        Size3i size;
    };
    std::vector<MipLevel> levels;
    TextureDesc desc;

  private:
    void _copy(Texture const& texture)
    {
        desc = texture.desc;
        levels = texture.levels;
    }

#if defined(WITH_SDL)

  public:
    Texture(SDL_Surface* sdlSurface) { create(sdlSurface); }

    Texture(Path const& filepath) { create(filepath); }

    void create(SDL_Surface* sdlSurface)
    {
        assert(sdlSurface != nullptr);
        if (SDL_LockSurface(sdlSurface) != 0)
        {
            logError("Texture::create: %s", SDL_GetError());
            return;
        }
        auto sdlPixelFormat = sdlSurface->format->format;
        auto pf = findBestPixelFormat(sdlPixelFormat);
        auto newDesc = TextureDesc();
        newDesc.size = Size3i(sdlSurface->w, sdlSurface->h, 1);
        newDesc.pixelFormat = pf.pixelFormat;
        create(newDesc);
        auto& img0 = data(0, 0);
        auto dstPitch = desc.size.width * toByteSize(desc.pixelFormat);
        auto r = SDL_ConvertPixels(sdlSurface->w,
                                   sdlSurface->h,
                                   sdlPixelFormat,
                                   sdlSurface->pixels,
                                   sdlSurface->pitch,
                                   pf.sdlPixelFormat,
                                   img0.data(),
                                   dstPitch);
        if (r != 0)
        {
            logError("Texture::create: %s", SDL_GetError());
        }
        SDL_UnlockSurface(sdlSurface);
    }

    void create(Path const& filepath)
    {
        auto img = IMG_Load(filepath.c_str());
        if (img == nullptr)
        {
            logError("Image::create: %s", SDL_GetError());
            return;
        }
        create(img);
        SDL_FreeSurface(img);
    }

  public:
    SDL_Surface* createSurface(int32_t level, int32_t pos) const
    {
        // auto sdlPixelFormat = toSDLPixelFormat(desc.pixelFormat);
        // if (sdlPixelFormat == SDL_PIXELFORMAT_UNKNOWN)
        //     return nullptr;
        // auto pixSize = toByteSize(desc.pixelFormat);
        // auto depth = pixSize * 8;
        // auto pitch = pixSize * imgSize.width;
        // return SDL_CreateRGBSurfaceWithFormatFrom(const_cast<uint8_t*>//(imgData.data()),
        //                                           imgSize.width,
        //                                           imgSize.height,
        //                                           depth,
        //                                           pitch,
        //                                           sdlPixelFormat);
        return nullptr;
    }

    SDL_Surface* createSurface() const { return createSurface(0, 0); }

#endif // WITH_SDL
};

#if defined(WITH_SDL)

struct TextureDefaultDesc final
{
    void createColor(Texture& texture) const
    {
        // png, 16x16x32
        constexpr uint8_t const dataDefaultColor[] = {
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48,
            0x44, 0x52, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x10, 0x08, 0x06, 0x00, 0x00,
            0x00, 0x1F, 0xF3, 0xFF, 0x61, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48, 0x59, 0x73, 0x00,
            0x00, 0x0B, 0x12, 0x00, 0x00, 0x0B, 0x12, 0x01, 0xD2, 0xDD, 0x7E, 0xFC, 0x00, 0x00,
            0x00, 0x6A, 0x49, 0x44, 0x41, 0x54, 0x38, 0xCB, 0xB5, 0x92, 0x41, 0x12, 0x80, 0x20,
            0x0C, 0x03, 0x37, 0x0C, 0xFF, 0xFF, 0x72, 0x3C, 0x08, 0x1E, 0x14, 0xA4, 0x80, 0xA6,
            0x27, 0x28, 0xC9, 0x24, 0x2D, 0xC2, 0x18, 0xC0, 0x32, 0x77, 0x08, 0xD5, 0xF6, 0x13,
            0x56, 0x79, 0x63, 0xDC, 0x22, 0x87, 0x60, 0x21, 0xE3, 0x45, 0xF6, 0x89, 0xF4, 0xD6,
            0x54, 0xA9, 0x25, 0x81, 0x11, 0xF1, 0x55, 0x20, 0x4A, 0xEE, 0x0A, 0xB8, 0xD4, 0xF6,
            0x0C, 0x46, 0x10, 0x22, 0xCD, 0xD8, 0x6D, 0x39, 0x4D, 0x51, 0xAB, 0xBF, 0x44, 0xF8,
            0x44, 0x20, 0x47, 0xD6, 0x58, 0xCF, 0xAD, 0xB8, 0xB9, 0x37, 0x9C, 0x89, 0x08, 0xDA,
            0x58, 0xE3, 0x35, 0x03, 0x31, 0xFB, 0xA5, 0xEB, 0xED, 0x01, 0x55, 0xBB, 0x1B, 0x23,
            0xBA, 0xA3, 0x2C, 0x50, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42,
            0x60, 0x82};
        _setDefaultData(texture, dataDefaultColor, std::size(dataDefaultColor));
    }

    void createSpecular(Texture& texture) const
    {
        // png, 1x1x32, rgba(11, 11, 11, 1)
        constexpr uint8_t const dataDefaultSpec[] = {
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49,
            0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x08, 0x06,
            0x00, 0x00, 0x00, 0x1F, 0x15, 0xC4, 0x89, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48,
            0x59, 0x73, 0x00, 0x00, 0x0E, 0xC3, 0x00, 0x00, 0x0E, 0xC3, 0x01, 0xC7, 0x6F,
            0xA8, 0x64, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x44, 0x41, 0x54, 0x08, 0xD7, 0x63,
            0xE0, 0xE6, 0xE6, 0x66, 0x04, 0x00, 0x00, 0x69, 0x00, 0x23, 0x54, 0x01, 0xD2,
            0x2F, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82};
        _setDefaultData(texture, dataDefaultSpec, std::size(dataDefaultSpec));
    }

    void createNormal(Texture& texture) const
    {
        // png, 1x1x24, rgb(128, 128, 255)
        constexpr uint8_t const dataDefaultNm[] = {
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49,
            0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x08, 0x02,
            0x00, 0x00, 0x00, 0x90, 0x77, 0x53, 0xDE, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48,
            0x59, 0x73, 0x00, 0x00, 0x0E, 0xC3, 0x00, 0x00, 0x0E, 0xC3, 0x01, 0xC7, 0x6F,
            0xA8, 0x64, 0x00, 0x00, 0x00, 0x0C, 0x49, 0x44, 0x41, 0x54, 0x08, 0xD7, 0x63,
            0x68, 0x68, 0xF8, 0x0F, 0x00, 0x03, 0x83, 0x02, 0x00, 0xD1, 0x86, 0xE4, 0xD6,
            0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82};
        _setDefaultData(texture, dataDefaultNm, std::size(dataDefaultNm));
    }

  private:
    void _setDefaultData(Texture& texture, uint8_t const* data, int32_t dataSize) const
    {
        auto rwops = SDL_RWFromConstMem(data, dataSize);
        auto surface = IMG_LoadPNG_RW(rwops);
        texture.create(surface);
        SDL_FreeSurface(surface);
        SDL_RWclose(rwops);
    }
};

#endif // WITH_SDL

} // namespace nopp
