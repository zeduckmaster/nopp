#pragma once

#include <cassert>
#include <dirent.h>
#include <fstream>
#include <functional>
#include <sys/stat.h>
#include <unistd.h>
#include "log.hpp"
#include "string.hpp"

namespace nopp
{

enum eIOResult : uint32_t
{
    OK = 0,
    InvalidFile,
    InvalidFileFormat,
};

class Path final
{
  public:
    Path() = default;

    Path(std::string const& path) : _path(path) { _ensureSeparator(); }

    Path(char const* path) : _path(path) { _ensureSeparator(); }

    Path& operator=(std::string const& path)
    {
        _path = path;
        _ensureSeparator();
        return *this;
    }

    Path& operator=(char const* path)
    {
        _path = path;
        _ensureSeparator();
        return *this;
    }

    bool operator==(Path const& path) const { return _path == path._path; }

    template <typename T>
    bool operator==(T const& value) const
    {
        return _path == value;
    }

    template <typename T>
    inline bool operator!=(T const& rhs) const
    {
        return !(*this == rhs);
    }

    // append using / separator
    Path& append(Path const& path)
    {
        if (path.isEmpty() == true)
        {
            return *this;
        }
        if (*path._path.begin() != _PathSeparator)
        {
            _appendSeparatorIfNeeded();
        }
        _path += path._path;
        return *this;
    }

    // d:/dir/test.txt -> test.txt
    Path filename() const
    {
        auto pos = _findFilenamePos(_path, _path.size());
        return Path(_path.c_str() + pos);
    }

    // d:/dir/test.txt -> txt
    Path extension() const
    {
        auto name = filename();
        if ((name._path == _DotPath) || (name._path == _DotDotPath))
        {
            return Path();
        }
        auto pos = name._path.rfind(_DotChar);
        if (pos == std::string::npos)
        {
            return Path();
        }
        return Path(name._path.c_str() + pos + 1);
    }

    // d:/dir/test.txt -> test
    Path stem() const
    {
        auto name = filename();
        if ((name._path == _DotPath) || (name._path == _DotDotPath))
        {
            return name;
        }
        auto pos = name._path.rfind(_DotChar);
        if (pos == std::string::npos)
        {
            return name;
        }
        return Path(std::string(name._path.c_str(), name._path.c_str() + pos));
    }

    // d:/dir/test.txt -> d:/dir
    Path parentPath() const
    {
        auto endpos = _parentPathEnd();
        if (endpos == std::string::npos)
        {
            return Path();
        }
        return Path(std::string(_path.c_str(), _path.c_str() + endpos));
    }

    // d:/dir/test.txt -> d:
    Path rootDirectory() const
    {
        auto sz = _path.size();
        if ((sz > 2) && (_path[1] == _ColonChar) && (_path[2] == _PathSeparator))
        {
            return Path(_path.substr(0, 2));
        }
        if ((sz > 0) && (_path[0] == _PathSeparator))
        {
            return Path(_path.substr(0, 1));
        }
        return Path();
    }

    std::string const& string() const { return _path; }

    char const* c_str() const { return _path.c_str(); }

    void clear() { _path.clear(); }

    bool isEmpty() const { return _path.empty(); }

    bool isAbsolute() const { return rootDirectory().isEmpty() == false; }

  private:
    void _ensureSeparator() { std::replace(_path.begin(), _path.end(), '\\', _PathSeparator); }

    void _appendSeparatorIfNeeded()
    {
        if ((_path.empty() == false) && (*(_path.end() - 1) != _PathSeparator))
        {
            _path.push_back(_PathSeparator);
        }
    }

    size_t _parentPathEnd() const
    {
        auto endpos = _findFilenamePos(_path, _path.size());
        auto filenameWasSeparator = (_path.size() != 0) && (_path[endpos] == _PathSeparator);
        for (; (endpos > 0) && (_path[endpos - 1] == _PathSeparator); --endpos)
        {
        }
        if ((endpos == 1) && (filenameWasSeparator == true))
        {
            return std::string::npos;
        }
        return endpos; //(_pathname[endpos]==_PathSeparator)? endpos;
    }

    size_t _rootDirectoryBegin() const
    {
        auto sz = _path.size();
        if ((sz > 2) && (_path[1] == _ColonChar) && (_path[2] == _PathSeparator))
        {
            return 0;
        }
        if ((sz == 2) && (_path[1] == _PathSeparator) && (_path[2] == _PathSeparator))
        {
            return std::string::npos;
        }
        if ((sz > 0) && (_path[0] == _PathSeparator))
        {
            return 0;
        }
        return std::string::npos;
    }

    std::string _path;

    static constexpr auto const _PathSeparator = char('/');
    static constexpr auto const _DotChar = char('.');
    static constexpr auto const _ColonChar = char(':');
    static constexpr auto const _DotPath = std::string_view(".");
    static constexpr auto const _DotDotPath = std::string_view("..");

    static size_t _findFilenamePos(std::string const& str, size_t endPos)
    {
        if ((endPos == 2) && (str[0] == _PathSeparator) && (str[1] == _PathSeparator))
        {
            return 0;
        }
        if ((endPos != 0) && (str[endPos - 1] == _PathSeparator))
        {
            return endPos - 1;
        }
        auto pos = str.find_last_of(_PathSeparator, endPos - 1);
        if (pos == std::string::npos)
        {
            return 0;
        }
        return pos + 1;
    }
};

inline bool operator==(std::string const& lhs, Path const& rhs) { return lhs == rhs.string(); }

inline bool operator==(char const* lhs, Path const& rhs) { return lhs == rhs.string(); }

inline bool operator!=(std::string const& lhs, Path const& rhs) { return !(lhs == rhs); }

inline bool operator!=(char const* lhs, Path const& rhs) { return !(lhs == rhs); }

inline Path getCurrentWorkingPath()
{
    char buffer[512];
    if (getcwd(buffer, sizeof(buffer)) == nullptr)
    {
        logError("getCurrentWorkingPath: getcwd error");
        return Path();
    }
    return Path(buffer);
}

inline std::string readFileContentAsString(Path const& filepath)
{
    auto str = std::string();
    std::ifstream in(filepath.string());
    if (in.is_open() == false)
    {
        logError("readFileAsString: can't open file %s", filepath.c_str());
        return std::string();
    }
    in.seekg(0, std::ios_base::end);
    auto pos = in.tellg();
    in.seekg(0, std::ios_base::beg);
    str.reserve(static_cast<size_t>(pos));
    str.assign(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
    return str;
}

inline std::vector<uint8_t> readFileContentAsBinary(Path const& filepath)
{
    auto buf = std::vector<uint8_t>();
    std::ifstream in(filepath.string(), std::ios_base::in | std::ios_base::binary);
    if (in.is_open() == false)
    {
        logError("readFileAsString: can't open file %s", filepath.c_str());
        return std::vector<uint8_t>{};
    }
    in.seekg(0, std::ios_base::end);
    auto pos = in.tellg();
    in.seekg(0, std::ios_base::beg);
    buf.reserve(static_cast<size_t>(pos));
    buf.assign(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
    return buf;
}

inline Path findFullFilepath(Path const& filepath, std::vector<Path> const& searchDirpaths)
{
    assert(filepath.isEmpty() == false);
    if (access(filepath.c_str(), F_OK) == 0)
    {
        return filepath;
    }
    for (auto& p : searchDirpaths)
    {
        auto tmppath = p;
        tmppath.append(filepath);
        if (access(tmppath.c_str(), F_OK) == 0)
        {
            return tmppath;
        }
    }
    return Path{};
}

inline bool createFullDirpath(Path const& dirpath)
{
    struct stat st;
    if (stat(dirpath.c_str(), &st) == 0)
    {
        return true;
    }
    if (errno != ENOENT)
    {
        return false;
    }
    auto p = Path();
    auto r = splitString(dirpath.string(), "/");
    for (auto& citer : r)
    {
        p.append(citer);
        if (stat(p.c_str(), &st) != 0)
        {
            if (errno != ENOENT)
            {
                return false;
            }
            mode_t procmask = umask(0);
#if defined(_WIN32)
            mkdir(p.c_str());
#else  // _WIN32
            mkdir(p.c_str(), (S_IRWXU | S_IRWXG | S_IRWXO));
#endif // _WIN32
            umask(procmask);
        }
    }
    return true;
}

inline std::vector<Path> findAllFilepaths(Path const& dirpath,
                                          std::function<bool(Path const&)> const predicate,
                                          bool recursive)
{
    auto all = std::vector<Path>();
    auto dirpaths = std::vector<Path>{dirpath};
    while (dirpaths.size() > 0)
    {
        auto curdirpath = dirpaths.back();
        dirpaths.pop_back();
        auto dir = opendir(curdirpath.c_str());
        if (dir == nullptr)
        {
            continue;
        }
        dirent* file = nullptr;
        while ((file = readdir(dir)) != nullptr)
        {
            if ((strcmp(file->d_name, ".") == 0) || (strcmp(file->d_name, "..") == 0))
            {
                continue;
            }
            auto filepath = Path(dirpath).append(file->d_name);
            struct stat info;
            if (stat(filepath.c_str(), &info) < 0)
            {
                logError(
                    "findAllFilepaths: can't access \"%s\", errno \"%d\"", filepath.c_str(), errno);
                continue;
            }
            if (S_ISDIR(info.st_mode))
            {
                dirpaths.push_back(filepath);
            }
            else
            {
                if ((predicate == nullptr) || (predicate(filepath) == true))
                {
                    all.push_back(filepath);
                }
            }
        }
        closedir(dir);
        if (recursive == false)
        {
            return all;
        }
    }
    return all;
}

inline std::vector<Path> findAllFilepaths(Path const& dirpath, bool const recursive)
{
    return findAllFilepaths(dirpath, nullptr, recursive);
}

} // namespace nopp

namespace std
{

template <>
struct hash<nopp::Path>
{
    size_t operator()(nopp::Path const& path) const
    {
        return std::hash<std::string>()(path.string());
    }
};

template <>
struct less<nopp::Path>
{
    bool operator()(nopp::Path const& lhs, nopp::Path const& rhs) const
    {
        return std::less<std::string>()(lhs.string(), rhs.string());
    }
};

} // namespace std
