#pragma once

#include <filesystem>
#include <vector>
#include "io.hpp"
#include "log.hpp"
#include "size.hpp"

namespace nopp
{

enum class ePixelFormat : uint32_t
{
    Unknown = 0,
    // pixel formats
    RGBA8_UNorm,
    RGBx8_UNorm,
    R8_UNorm,
    RGBA16_Float,
    RGBA32_Float,
    // compressed formats
    RGB_ETC1
};

constexpr bool isPixelFormatCompressed(ePixelFormat pixelFormat)
{
    return (pixelFormat == ePixelFormat::RGB_ETC1);
}

constexpr int32_t toByteSize(Size3i const& size, ePixelFormat pixelFormat)
{
    switch (pixelFormat)
    {
    case ePixelFormat::RGBx8_UNorm:
    case ePixelFormat::RGBA8_UNorm: return size.width * size.height * size.depth * 4;
    case ePixelFormat::R8_UNorm: return size.width * size.height * size.depth;
    case ePixelFormat::RGBA16_Float: return size.width * size.height * size.depth * 8;
    case ePixelFormat::RGBA32_Float: return size.width * size.height * size.depth * 16;
    case ePixelFormat::RGB_ETC1:
        // case ePixelFormat::RGB_S3TC_DXT1:
        {
            auto f = [](int32_t const dim) { return std::max<int32_t>(1, ((dim + 3) / 4)); };
            return f(size.width) * f(size.height) * 8;
        }
    default: return -1;
    }
}

constexpr int32_t toByteSize(ePixelFormat const pixelFormat)
{
    switch (pixelFormat)
    {
    case ePixelFormat::RGBx8_UNorm:
    case ePixelFormat::RGBA8_UNorm: return 4;
    case ePixelFormat::R8_UNorm: return 1;
    case ePixelFormat::RGBA16_Float: return 8;
    case ePixelFormat::RGBA32_Float: return 16;
    case ePixelFormat::RGB_ETC1: return 8;
    default: return -1;
    }
}

#if defined(WITH_SDL)
#include "io.hpp"
#include "support_sdl.hpp"

struct BestPixelFormat
{
    ePixelFormat pixelFormat = ePixelFormat::Unknown;
    uint32_t sdlPixelFormat = 0;
};

BestPixelFormat findBestPixelFormat(uint32_t const sdlPixelFormat)
{
    switch (sdlPixelFormat)
    {
    case SDL_PIXELFORMAT_RGBA32:
        return BestPixelFormat{ePixelFormat::RGBA8_UNorm, SDL_PIXELFORMAT_RGBA32};
    case SDL_PIXELFORMAT_RGB24:
        return BestPixelFormat{ePixelFormat::RGBx8_UNorm, SDL_PIXELFORMAT_RGBA32};
    default: return BestPixelFormat{};
    }
}

uint32_t toSDLPixelFormat(ePixelFormat const pixelFormat)
{
    switch (pixelFormat)
    {
    case ePixelFormat::RGBA8_UNorm:
    case ePixelFormat::RGBx8_UNorm: return SDL_PIXELFORMAT_RGBA32; break;
    default: return SDL_PIXELFORMAT_UNKNOWN;
    }
}

#endif // WITH_SDL

struct ImageDesc final
{
    Size3i size;
    ePixelFormat pixelFormat = ePixelFormat::Unknown;

    constexpr ImageDesc() = default;

    constexpr ImageDesc(Size3i const& size, ePixelFormat const pixelFormat)
        : size{size}, pixelFormat{pixelFormat}
    {
    }

    constexpr ImageDesc(int32_t const width, int32_t const height, ePixelFormat const pixelFormat)
        : size{width, height}, pixelFormat{pixelFormat}
    {
    }

    constexpr bool isValid() const
    {
        return (size.isValid() == true) && (pixelFormat != ePixelFormat::Unknown);
    }

    constexpr bool operator==(ImageDesc const& imageDesc) const
    {
        return (size == imageDesc.size) && (pixelFormat == imageDesc.pixelFormat);
    }

    constexpr bool operator!=(ImageDesc const& imageDesc) const { return !(*this == imageDesc); }

    constexpr int32_t byteSize() const { return toByteSize(size, pixelFormat); }
};

using ImageData = std::vector<uint8_t>;

class Image final
{
  public:
    Image() = default;

    Image(ImageDesc const& imageDesc) { create(imageDesc); }

    Image(Image const& image) { _copy(image); }

    Image& operator=(Image const& image)
    {
        assert(this != &image);
        _copy(image);
        return *this;
    }

    void create(ImageDesc const& imageDesc)
    {
        assert(imageDesc.isValid() == true);
        desc = imageDesc;
        auto byteSize = toByteSize(desc.size, desc.pixelFormat);
        data.resize(byteSize);
    }

    bool isValid() const { return (data.size() != 0) && (desc.isValid() == true); }

  public:
    ImageData data;
    ImageDesc desc;

  private:
    void _copy(Image const& image)
    {
        desc = image.desc;
        data = image.data;
    }

#if defined(WITH_SDL)

  public:
    Image(SDL_Surface* const sdlSurface) { create(sdlSurface); }

    Image(Path const& filepath) { create(filepath); }

    void create(SDL_Surface* const sdlSurface)
    {
        assert(sdlSurface != nullptr);
        if (SDL_LockSurface(sdlSurface) != 0)
        {
            logError("Image::create: %s", SDL_GetError());
            return;
        }
        auto sdlPixelFormat = sdlSurface->format->format;
        auto pf = findBestPixelFormat(sdlPixelFormat);
        auto newDesc = ImageDesc{};
        newDesc.size = Size3i{sdlSurface->w, sdlSurface->h, 1};
        newDesc.pixelFormat = pf.pixelFormat;
        create(newDesc);
        auto dstPitch = desc.size.width * toByteSize(desc.pixelFormat);
        auto r = SDL_ConvertPixels(sdlSurface->w,
                                   sdlSurface->h,
                                   sdlPixelFormat,
                                   sdlSurface->pixels,
                                   sdlSurface->pitch,
                                   pf.sdlPixelFormat,
                                   data.data(),
                                   dstPitch);
        if (r != 0)
        {
            logError("Image::create: %s", SDL_GetError());
        }
        SDL_UnlockSurface(sdlSurface);
    }

    void create(Path const& filepath)
    {
        auto img = IMG_Load(filepath.c_str());
        if (img == nullptr)
        {
            logError("Image::create: %s", SDL_GetError());
            return;
        }
        create(img);
        SDL_FreeSurface(img);
    }

    SDL_Surface* createSurface() const
    {
        auto sdlFormat = toSDLPixelFormat(desc.pixelFormat);
        if (sdlFormat == SDL_PIXELFORMAT_UNKNOWN)
        {
            return nullptr;
        }
        auto pixSize = toByteSize(desc.pixelFormat);
        auto depth = pixSize * 8;
        auto pitch = pixSize * desc.size.width;
        return SDL_CreateRGBSurfaceWithFormatFrom(const_cast<uint8_t*>(data.data()),
                                                  desc.size.width,
                                                  desc.size.height,
                                                  depth,
                                                  pitch,
                                                  sdlFormat);
    }

#endif // WITH_SDL
};

#if defined(WITH_SDL)

struct ImageDefaultDesc final
{
    void createColor(Image& image) const
    {
        // png, 16x16x32
        constexpr uint8_t const dataDefaultColor[] = {
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48,
            0x44, 0x52, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x10, 0x08, 0x06, 0x00, 0x00,
            0x00, 0x1F, 0xF3, 0xFF, 0x61, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48, 0x59, 0x73, 0x00,
            0x00, 0x0B, 0x12, 0x00, 0x00, 0x0B, 0x12, 0x01, 0xD2, 0xDD, 0x7E, 0xFC, 0x00, 0x00,
            0x00, 0x6A, 0x49, 0x44, 0x41, 0x54, 0x38, 0xCB, 0xB5, 0x92, 0x41, 0x12, 0x80, 0x20,
            0x0C, 0x03, 0x37, 0x0C, 0xFF, 0xFF, 0x72, 0x3C, 0x08, 0x1E, 0x14, 0xA4, 0x80, 0xA6,
            0x27, 0x28, 0xC9, 0x24, 0x2D, 0xC2, 0x18, 0xC0, 0x32, 0x77, 0x08, 0xD5, 0xF6, 0x13,
            0x56, 0x79, 0x63, 0xDC, 0x22, 0x87, 0x60, 0x21, 0xE3, 0x45, 0xF6, 0x89, 0xF4, 0xD6,
            0x54, 0xA9, 0x25, 0x81, 0x11, 0xF1, 0x55, 0x20, 0x4A, 0xEE, 0x0A, 0xB8, 0xD4, 0xF6,
            0x0C, 0x46, 0x10, 0x22, 0xCD, 0xD8, 0x6D, 0x39, 0x4D, 0x51, 0xAB, 0xBF, 0x44, 0xF8,
            0x44, 0x20, 0x47, 0xD6, 0x58, 0xCF, 0xAD, 0xB8, 0xB9, 0x37, 0x9C, 0x89, 0x08, 0xDA,
            0x58, 0xE3, 0x35, 0x03, 0x31, 0xFB, 0xA5, 0xEB, 0xED, 0x01, 0x55, 0xBB, 0x1B, 0x23,
            0xBA, 0xA3, 0x2C, 0x50, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42,
            0x60, 0x82};
        _setDefaultData(image, dataDefaultColor, std::size(dataDefaultColor));
    }

    void createSpecular(Image& image) const
    {
        // png, 1x1x32, rgba(11, 11, 11, 1)
        constexpr uint8_t const dataDefaultSpec[] = {
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49,
            0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x08, 0x06,
            0x00, 0x00, 0x00, 0x1F, 0x15, 0xC4, 0x89, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48,
            0x59, 0x73, 0x00, 0x00, 0x0E, 0xC3, 0x00, 0x00, 0x0E, 0xC3, 0x01, 0xC7, 0x6F,
            0xA8, 0x64, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x44, 0x41, 0x54, 0x08, 0xD7, 0x63,
            0xE0, 0xE6, 0xE6, 0x66, 0x04, 0x00, 0x00, 0x69, 0x00, 0x23, 0x54, 0x01, 0xD2,
            0x2F, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82};
        _setDefaultData(image, dataDefaultSpec, std::size(dataDefaultSpec));
    }

    void createNormal(Image& image) const
    {
        // png, 1x1x24, rgb(128, 128, 255)
        constexpr uint8_t const dataDefaultNm[] = {
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49,
            0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x08, 0x02,
            0x00, 0x00, 0x00, 0x90, 0x77, 0x53, 0xDE, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48,
            0x59, 0x73, 0x00, 0x00, 0x0E, 0xC3, 0x00, 0x00, 0x0E, 0xC3, 0x01, 0xC7, 0x6F,
            0xA8, 0x64, 0x00, 0x00, 0x00, 0x0C, 0x49, 0x44, 0x41, 0x54, 0x08, 0xD7, 0x63,
            0x68, 0x68, 0xF8, 0x0F, 0x00, 0x03, 0x83, 0x02, 0x00, 0xD1, 0x86, 0xE4, 0xD6,
            0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82};
        _setDefaultData(image, dataDefaultNm, std::size(dataDefaultNm));
    }

  private:
    void _setDefaultData(Image& image, uint8_t const* const data, const int32_t dataSize) const
    {
        auto rwops = SDL_RWFromConstMem(data, dataSize);
        auto surface = IMG_LoadPNG_RW(rwops);
        image.create(surface);
        SDL_FreeSurface(surface);
        SDL_RWclose(rwops);
    }
};

#endif // WITH_SDL

/*void resizeNearest(ImageData& dstData, ImageDesc const& dstDesc, ImageData const& srcData,
                   ImageDesc const& srcDesc)
{
    auto const scale = srcDesc.size / dstDesc.size;
    auto const dstWidth = dstDesc.size.width;
    auto xOffs = std::vector<int32_t>{};
    xOffs.resize(dstWidth);
    for (auto x = 0; x < dstWidth; ++x)
    {
        auto sx = static_cast<int32_t>(std::floor(x * scale.width));
        xOffs[x] = std::min(sx, srcDesc.size.width - 1) * srcDesc.pixelByteSize;
    }

    for (auto y = 0; y < dstDesc.size.height; ++y)
    {
        auto pDstData = dstData.data() + dstDesc.stepByteSize * y;
        auto sy =
            std::min(static_cast<int32_t>(std::floor(y * scale.height)), srcDesc.size.height - 1);
        auto pSrcData = srcData.data() + srcDesc.stepByteSize * sy;
        auto x = 0;
        switch (srcDesc.pixelByteSize)
        {
        case 1:
            for (x = 0; x <= dstWidth - 2; x += 2)
            {
                auto t0 = pSrcData[xOffs[x]];
                auto t1 = pSrcData[xOffs[x + 1]];
                pDstData[x] = t0;
                pDstData[x + 1] = t1;
            }
            for (; x < dstWidth; ++x)
            {
                pDstData[x] = pSrcData[xOffs[x]];
            }
            break;
        case 2:
            for (x = 0; x < dstWidth; ++x)
            {
                *reinterpret_cast<uint16_t*>(pDstData + x * 2) =
                    *reinterpret_cast<uint16_t const*>(pSrcData + xOffs[x]);
            }
            break;
        case 3:
            for (x = 0; x < dstWidth; ++x, pDstData += 3)
            {
                auto sData = pSrcData + xOffs[x];
                pDstData[0] = sData[0];
                pDstData[1] = sData[1];
                pDstData[2] = sData[2];
            }
            break;
        case 4:
            for (x = 0; x < dstWidth; ++x)
            {
                *reinterpret_cast<int32_t*>(pDstData + x * 4) =
                    *reinterpret_cast<int32_t const*>(pSrcData + xOffs[x]);
            }
            break;
        case 6:
            for (x = 0; x < dstWidth; ++x, pDstData += 6)
            {
                auto sData = reinterpret_cast<uint16_t const*>(pSrcData + xOffs[x]);
                auto dData = reinterpret_cast<uint16_t*>(pDstData);
                dData[0] = sData[0];
                dData[1] = sData[1];
                dData[2] = sData[2];
            }
            break;
        case 8:
            for (x = 0; x < dstWidth; ++x, pDstData += 8)
            {
                auto sData = reinterpret_cast<int32_t const*>(pSrcData + xOffs[x]);
                auto dData = reinterpret_cast<int32_t*>(pDstData);
                dData[0] = sData[0];
                dData[1] = sData[1];
            }
            break;
        case 12:
            for (x = 0; x < dstWidth; ++x, pDstData += 12)
            {
                auto sData = reinterpret_cast<int32_t const*>(pSrcData + xOffs[x]);
                auto dData = reinterpret_cast<int32_t*>(pDstData);
                dData[0] = sData[0];
                dData[1] = sData[1];
                dData[2] = sData[2];
            }
            break;
        }
    }
}

enum class eInterpolation
{
    Nearest = 0,
    Linear,
    Cubic
};

ImageData resize(ImageData const& srcData, ImageDesc const& srcDesc, Size3i const& dstSize,
                 eInterpolation interpolation)
{
    auto dstDesc = ImageDesc{dstSize, srcDesc.format};
    auto dstData = ImageData{};
    dstData.resize(dstDesc.totalByteSize);
    if (interpolation == eInterpolation::Nearest)
    {
        resizeNearest(dstData, dstDesc, srcData, srcDesc);
        return dstData;
    }
    return dstData;
}

struct ImageView
{
    ImageData& data;
    ImageDesc desc;

    ImageView(ImageData& data, ImageDesc const& desc) : data{data}, desc{desc} {}
};
*/
} // namespace nopp
