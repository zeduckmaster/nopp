#pragma once

#include "io.hpp"
#include "log.hpp"
#include "mesh.hpp"

namespace nopp
{

struct MeshObj final
{
    eIOResult create(Mesh& mesh, std::string const& fileContent)
    {
        std::istringstream in(fileContent);
        return create(mesh, in);
    }

    eIOResult create(Mesh& mesh, Path const& filepath)
    {
        std::ifstream in(filepath.string());
        if (in.is_open() == false)
        {
            return eIOResult::InvalidFile;
        }
        return create(mesh, in);
    }

    eIOResult create(Mesh& mesh, std::istream& in)
    {
        auto v = std::vector<Vec3>();
        auto vn = std::vector<Vec3>();
        auto vt = std::vector<Vec2>();
        struct Vertex
        {
            int32_t vpos = -1;
            int32_t vnpos = -1;
            int32_t vtpos = -1;
        };
        auto verts = std::vector<Vertex>();
        while (in.eof() == false)
        {
            auto line = std::string();
            std::getline(in, line);
            if ((line.empty() == true) || (line[0] == '#'))
            {
                continue;
            }
            auto const strs = splitString(line, "\t \n\r");
            if (strs[0] == "v")
            {
                v.emplace_back(Vec3(toFloat(strs[1]), toFloat(strs[2]), toFloat(strs[3])));
            }
            else if (strs[0] == "vn")
            {
                vn.emplace_back(Vec3(toFloat(strs[1]), toFloat(strs[2]), toFloat(strs[3])));
            }
            else if (strs[0] == "vt")
            {
                vt.emplace_back(Vec2(toFloat(strs[1]), toFloat(strs[2])));
            }
            else if (strs[0] == "f")
            {
                if (strs.size() == 4)
                {
                    for (auto n = 1; n < 4; ++n)
                    {
                        auto indexes = splitString(strs[n], "/");
                        assert(indexes[0].size() != 0);
                        auto vtx = Vertex();
                        vtx.vpos = toInt32(indexes[0]);
                        if (indexes[1].size() > 0)
                        {
                            vtx.vtpos = toInt32(indexes[1]);
                        }
                        if (indexes[2].size() > 0)
                        {
                            vtx.vnpos = toInt32(indexes[2]);
                        }
                        verts.emplace_back(vtx);
                    }
                }
            }
        }
        auto count = static_cast<int32_t>(verts.size());
        if (count == 0)
        {
            logWarning("OBJ file contains no vertices");
            return eIOResult::OK;
        }
        auto desc = MeshDesc();
        desc.vertexCount = count;
        desc.primitiveType = eMeshTopology::Triangles;
        desc.vertexDeclaration = toType(eVertexDeclaration::Position);
        if (verts[0].vnpos != -1)
        {
            desc.vertexDeclaration |= eVertexDeclaration::Normal;
        }
        if (verts[0].vtpos != -1)
        {
            desc.vertexDeclaration |= eVertexDeclaration::TexCoord0;
        }
        mesh.create(desc);
        for (auto n = 0; n < count; ++n)
        {
            auto& vtx = verts[n];
            mesh.positions[n] = v[vtx.vpos - 1];
            if (vtx.vnpos != -1)
            {
                mesh.datas[n].normal = vn[vtx.vnpos - 1];
            }
            if (vtx.vtpos != -1)
            {
                mesh.datas[n].texCoord0 = vt[vtx.vtpos - 1];
            }
        }
        if (verts[0].vnpos != -1)
        {
            mesh.computeTangents();
        }
        return eIOResult::OK;
    }
};

} // namespace nopp
