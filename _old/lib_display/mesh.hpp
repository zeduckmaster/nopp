#pragma once

#include "gpu.hpp"

#include <lib_core/enum.hpp>
#include <lib_core/io.hpp>
#include <lib_core/math.hpp>
#include <lib_core/ref_object.hpp>
#include <vector>

namespace nopp
{
// store normal in 32bits (uint32_t)
// storage is destructive, i.e.: loose of precision
// normal should be normalized
struct PackedNormal final
{
    union {
        uint32_t value = 0;
        struct
        {
            uint8_t x;
            uint8_t y;
            uint8_t z;
            uint8_t w;
        } v;
    };

    PackedNormal() = default;

    PackedNormal(Vec3 const& normal);

    PackedNormal(float nx, float ny, float nz);

    PackedNormal& operator=(Vec3 const& normal);

    operator Vec3() const;
};

// should be used as a flag
enum class eVertexDeclaration : uint32_t
{
    Position = 0x00000001,
    Normal = 0x00000002,
    Tangent = 0x00000004,
    Color = 0x00000008,
    TexCoord0 = 0x00000010,
    TexCoord1 = 0x00000020,
    Custom0 = 0x00010000,
    // Custom1   = 0x00020000,
    // Custom2   = 0x00040000,
    // Custom3   = 0x00080000,

    All = 0xffffffff
};

using VertexDeclaration = std::underlying_type<eVertexDeclaration>::type;

inline VertexDeclaration& operator|=(VertexDeclaration& decl1,
                                     eVertexDeclaration decl2)
{
    decl1 |= static_cast<VertexDeclaration>(decl2);
    return decl1;
}

inline VertexDeclaration operator|(eVertexDeclaration decl1,
                                   eVertexDeclaration decl2)
{
    return static_cast<VertexDeclaration>(decl1) |
           static_cast<VertexDeclaration>(decl2);
}

inline VertexDeclaration operator|(VertexDeclaration decl1,
                                   eVertexDeclaration decl2)
{
    return decl1 | static_cast<VertexDeclaration>(decl2);
}

enum class eMeshTopology : uint32_t
{
    Points = 0,
    Lines,
    Triangles
};

struct MeshDesc final
{
    VertexDeclaration vertexDeclaration = 0;
    int32_t vertexCount = 0;
    int32_t indexCount = 0;
    eMeshTopology primitiveType = eMeshTopology::Triangles;

    MeshDesc() = default;

    MeshDesc(VertexDeclaration vertexDeclaration, int32_t vertexCount);

    MeshDesc(VertexDeclaration vertexDeclaration,
             int32_t vertexCount,
             int32_t indexCount);

    bool operator==(MeshDesc const& meshDesc) const;

    bool operator!=(MeshDesc const& meshDesc) const
    {
        return !(*this == meshDesc);
    }

    bool hasTexCoord0() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::TexCoord0);
    }

    bool hasNormal() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::Normal);
    }

    bool hasTangent() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::Tangent);
    }

    bool hasColor() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::Color);
    }

    int32_t primitiveCount() const;
};

// vertex data is organized the following way:
// - position vertex buffer (Vec3) to reduce vertex fetching
// - color vertex buffer (Vec4) because it's very specific (i.e.: mainly used
// for lines)
// - data vertex buffer:
//   * texCoord0 (Vec2)
//   * texCoord1 (Vec2)
//   * normal (uint32_t packed)
//   * tangent (uint32_t packed)
// - custom vertex buffer (Vec4) for custom specific data
class MeshData final : public GPUData
{
    FAST_RTTI_CLASS(MeshData, GPUData)

  public:
    MeshData() = default;

    MeshData(MeshDesc const& meshDesc);

    MeshData(Path const& filepath);

    MeshData(MeshData const& meshData);

    MeshData(MeshData&& meshData);

    MeshData& operator=(MeshData const& meshData);

    MeshData& operator=(MeshData&& meshData);

    void create(MeshDesc const& meshDesc);

    void create(Path const& filepath);

    // set the same color for all vertices
    void setVertexColor(Vec4 const& color);

    // only valid for triangles
    void computeTangents();

    // only valid for triangles
    void computeWireframe();

  public:
    struct VtxData final
    {
        Vec2 texCoord0;
        Vec2 texCoord1;
        PackedNormal normal;
        PackedNormal tangent;
    };

    std::vector<Vec3> positions;
    std::vector<Vec4> colors;
    std::vector<VtxData> datas;
    std::vector<Vec4> customs;
    std::vector<uint16_t> indexes;
    std::vector<uint16_t> wireframeIndexes;
    MeshDesc desc;

  private:
    void _copy(MeshData const& meshData);
};

eIOResult loadOBJ(MeshData& meshData, Path const& filepath);

inline Bound computeBound(MeshData const& meshData)
{
    return Bound{}.extend(meshData.positions);
}

////////////////////////////////////////////////////////////////////////////////
// complex creation functions

// grid as a collection of colored lines and z-up aligned
// each vertex contains:
// - position
// - color
struct MeshGridDesc final
{
    int32_t lineCount = 10;
    float scale = 1.0f;
    Vec4 colorLine = Vec4{0.5f, 0.5f, 0.5f, 1.0f};
    Vec4 colorAxisX = Vec4::UnitX();
    Vec4 colorAxisY = Vec4::UnitY();
    Vec4 colorAxisZ = Vec4::UnitZ();

    MeshGridDesc() = default;

    MeshGridDesc(int32_t lineCount, float scale);

    void create(MeshData& meshData) const;
};

// a cube
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshCubeDesc final
{
    Vec3 center = Vec3::Zero();
    float size = 1.0f;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration =
        eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;

    MeshCubeDesc() = default;

    MeshCubeDesc(Vec3 const& center, float size);

    MeshCubeDesc(Vec3 const& center, float size, uint32_t vertexDeclaration);

    void create(MeshData& meshData) const;
};

// a quad z-up aligned
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshQuadDesc final
{
    Vec3 center = Vec3::Zero();
    float width = 1.0f;
    float height = 1.0f;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration =
        eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;
    bool isTwoSided = false; // if true, quad will have also a back face

    MeshQuadDesc() = default;

    MeshQuadDesc(Vec3 const& center, float width, float height);

    MeshQuadDesc(Vec3 const& center,
                 float width,
                 float height,
                 uint32_t vertexDeclaration);

    MeshQuadDesc(Vec3 const& center,
                 float width,
                 float height,
                 uint32_t vertexDeclaration,
                 bool isTwoSided);

    void create(MeshData& meshData) const;
};

// a cylinder aligned along z axis
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshCylinderDesc final
{
    enum class eCapType : uint32_t
    {
        None = 0, // no cap at the end
        Normal,   // normal triangulation
        Flipped,  // flipped triangulation, useful for light shape
    };

    Vec3 center = Vec3::Zero();
    float radiusTop = 1.0f;
    float radiusBottom = 1.0f;
    float height = 1.0f;
    uint32_t radialSegments = 8;
    uint32_t heightSegments = 1;
    float thetaStart = 0.0f;
    float thetaLength = TwoPi;
    eCapType capTop = eCapType::Normal;
    eCapType capBottom = eCapType::Normal;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration =
        eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;

    MeshCylinderDesc() = default;

    MeshCylinderDesc(float radiusTop, float radiusBottom, float height);

    MeshCylinderDesc(float radiusTop,
                     float radiusBottom,
                     float height,
                     uint32_t vertexDeclaration);

    void createMesh(MeshData& meshData) const;
};

// an icosphere
// each vertex contains:
// - position
// - texture coordinates (opt.)
// - normal (opt.)
// - color (opt.)
struct MeshIcosphereDesc final
{
    Vec3 center = Vec3::Zero();
    uint32_t subdivisions = 1;
    float size = 1.0f;
    Vec4 color = Vec4::Ones();
    uint32_t vertexDeclaration =
        eVertexDeclaration::Position | eVertexDeclaration::TexCoord0;

    MeshIcosphereDesc() = default;

    MeshIcosphereDesc(Vec3 const& center, uint32_t subdivisions, float size);

    MeshIcosphereDesc(Vec3 const& center,
                      uint32_t subdivisions,
                      float size,
                      uint32_t vertexDeclaration);

    void create(MeshData& meshData) const;
};

} // namespace nopp
