#pragma once

#include <cstdarg>
#include <cstdint>
#include <functional>
#include <iostream>

#if defined(WITH_SDL)
#include "support_sdl.hpp"
#include "unused.hpp"
#endif

namespace nopp
{

enum class eLogLevel : uint32_t
{
    Info = 0,
    Warning,
    Debug,
    Error,
};

inline void logFunctionToStdCout(eLogLevel logLevel, char const* fmt, va_list argList)
{
#if defined(_DEBUG)
    constexpr auto BufSize = size_t(2048);
#else  // _DEBUG
    constexpr auto BufSize = size_t(256);
#endif // _DEBUG
    char buf[BufSize];
    vsnprintf(buf, BufSize - 1, fmt, argList);
    buf[BufSize - 1] = '\0';
    auto slog = std::string();
    switch (logLevel)
    {
    case eLogLevel::Info: slog = "[I]"; break;
    case eLogLevel::Debug: slog = "[D]"; break;
    case eLogLevel::Warning: slog = "[W]"; break;
    case eLogLevel::Error: slog = "[E]"; break;
    default: break;
    }
    slog.append(buf);
    slog.push_back('\n');
    std::cout << slog << std::flush;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
using FuncLog = std::function<void(eLogLevel, char const*, va_list)>;
#pragma GCC diagnostic pop

inline FuncLog logFunction = logFunctionToStdCout;

template <eLogLevel L>
void log(char const* fmt, ...)
{
    if (logFunction == nullptr)
    {
        return;
    }
    va_list args;
    va_start(args, fmt);
    logFunction(L, fmt, args);
    va_end(args);
}

template <typename... Args>
inline auto logInfo(Args&&... args) -> decltype(log<eLogLevel::Info>(std::forward<Args>(args)...))
{
    return log<eLogLevel::Info>(std::forward<Args>(args)...);
}

template <typename... Args>
inline auto logWarning(Args&&... args)
    -> decltype(log<eLogLevel::Warning>(std::forward<Args>(args)...))
{
    return log<eLogLevel::Warning>(std::forward<Args>(args)...);
}

template <typename... Args>
inline auto logError(Args&&... args) -> decltype(log<eLogLevel::Error>(std::forward<Args>(args)...))
{
    return log<eLogLevel::Error>(std::forward<Args>(args)...);
}

#if defined(NDEBUG)
#define logDebug(...) ((void)0)
#else  // NDEBUG
template <typename... Args>
inline auto logDebug(Args&&... args) -> decltype(log<eLogLevel::Debug>(std::forward<Args>(args)...))
{
    return log<eLogLevel::Debug>(std::forward<Args>(args)...);
}
#endif // NDEBUG

} // namespace nopp

#if defined(WITH_SDL)

#include "support_sdl.hpp"

namespace nopp
{

// Log function to channel all logs to SDL_Log
// This function should be assigned to nopp::logFunction
inline void logFunctionToSDLLog(eLogLevel logLevel, char const* fmt, va_list argList)
{
    auto priority = SDL_LOG_PRIORITY_VERBOSE;
    switch (logLevel)
    {
    case eLogLevel::Info: priority = SDL_LOG_PRIORITY_INFO; break;
    case eLogLevel::Warning: priority = SDL_LOG_PRIORITY_WARN; break;
    case eLogLevel::Debug: priority = SDL_LOG_PRIORITY_DEBUG; break;
    case eLogLevel::Error: priority = SDL_LOG_PRIORITY_ERROR; break;
    default: break;
    }
    SDL_LogMessageV(SDL_LOG_CATEGORY_TEST, priority, fmt, argList);
}

inline std::string toString(SDL_LogCategory category)
{
    switch (category)
    {
    case SDL_LOG_CATEGORY_APPLICATION: return "APPLICATION";
    case SDL_LOG_CATEGORY_ERROR: return "ERROR";
    case SDL_LOG_CATEGORY_ASSERT: return "ASSERT";
    case SDL_LOG_CATEGORY_SYSTEM: return "SYSTEM";
    case SDL_LOG_CATEGORY_AUDIO: return "AUDIO";
    case SDL_LOG_CATEGORY_VIDEO: return "VIDEO";
    case SDL_LOG_CATEGORY_RENDER: return "RENDER";
    case SDL_LOG_CATEGORY_INPUT: return "INPUT";
    case SDL_LOG_CATEGORY_TEST: return "TEST";
    default: return "Unknown";
    }
}

// Output function to channel SDL_Log to log
// This function should be set to SDL_LogSetOutputFunction API
inline void
logSDLLogOutputFunction(void* userdata, int category, SDL_LogPriority priority, const char* message)
{
    unused(userdata);
    auto categoryStr = toString(static_cast<SDL_LogCategory>(category));
    switch (priority)
    {
    case SDL_LOG_PRIORITY_INFO: logInfo("[SDL][%s]%s", categoryStr.c_str(), message); break;
    case SDL_LOG_PRIORITY_DEBUG: logDebug("[SDL][%s]%s", categoryStr.c_str(), message); break;
    case SDL_LOG_PRIORITY_WARN: logWarning("[SDL][%s]%s", categoryStr.c_str(), message); break;
    case SDL_LOG_PRIORITY_ERROR: logError("[SDL][%s]%s", categoryStr.c_str(), message); break;
    default: break;
    }
}

} // namespace nopp

#endif // WITH_SDL
