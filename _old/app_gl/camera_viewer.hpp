#pragma once

#include <lib_display/api.hpp>

class CameraEditorSimple final : public nopp::CameraEditor
{
  public:
    void onMousePress(nopp::Vec2 const& mousePos,
                      bool keyCtrl,
                      bool keyShift,
                      bool keyAlt);

    void onMouseMove(nopp::Vec2 const& mousePos);

    void onMouseRelease(nopp::Vec2 const& mousePos);

  private:
    enum class _eUIMode : uint32_t
    {
        Rotating = 0,
        Padding,
        Zooming,

        NbUIMode
    };
    _eUIMode _uiMode = _eUIMode::NbUIMode;
    nopp::Vec2 _mouseLastPosition = nopp::Vec2::Zero();
    float _cameraLastAngleLateral = 0.0f;
    float _cameraLastAngleLongitudinal = 0.0f;
};

void loadLuaCamera(sol::state& luaState);
