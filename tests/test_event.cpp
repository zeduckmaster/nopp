#include <catch2/catch.hpp>
#include <future>
#include <thread>
#include "nopp/event.hpp"

using namespace nopp;

TEST_CASE("event", "[event]")
{
    auto event = Event{};
    REQUIRE(event.isRaised() == false);
    auto const threadCount = 4;
    auto promises = std::vector<std::promise<bool>>{};
    promises.resize(threadCount);
    auto futures = std::vector<std::shared_future<bool>>();
    auto threads = std::vector<std::thread>{};
    for (auto i = 0; i < threadCount; ++i)
    {
        auto& promise = promises[i];
        futures.push_back(promise.get_future());
        threads.emplace_back(
            [&]()
            {
                event.wait();
                promise.set_value(true);
            });
    }
    event.raise();
    for (auto& future : futures)
    {
        auto status = future.wait_for(std::chrono::seconds(1));
        REQUIRE(status == std::future_status::ready);
        REQUIRE(future.get() == true);
    }
    for (auto& thread : threads)
    {
        thread.join();
    }
}
