#pragma once

#include <cmath>
#include <cstdint>

namespace nopp
{

// A helper class to store a 32 bits float value
struct Float32 final
{
    union
    {
        float value = 0.0f;
        struct
        {
            uint32_t mantissa :23;
            uint32_t exponent :8;
            uint32_t sign     :1;
        } c;
    };

    Float32() = default;

    constexpr Float32(float v) : value(v) {}
};

// A class to store a 16 bits float value
// The 16 bit float components and conversion:
// IEEE float 16 represented by 10-bit mantissa M, 5-bit exponent E, and 1-bit sign S
// E=0, M=0      == 0.0
// E=0, M!=0     == S * 2^-14 * (M / 2^10)  : (subnormal value)
// 0<E<31, M=any == S * 2^(E-15) * (1 + M / 2^10)
// E=31, M=0     == Infinity
// E=31, M!=0    == NaN
struct Float16 final
{
    union
    {
        uint16_t value = 0;
        struct
        {
            uint16_t mantissa :10;
            uint16_t exponent :5;
            uint16_t sign     :1;
        } c;
    };

    Float16() = default;

    constexpr Float16(uint16_t v) : value(v) {}

    Float16(float v) { set(v); }

    constexpr operator uint16_t() const { return value; }

    operator float() const { return get(); }

    constexpr Float16& operator=(uint16_t v)
    {
        value = v;
        return *this;
    }

    Float16& operator=(float v)
    {
        set(v);
        return *this;
    }

    constexpr void set(float v)
    {
        auto f32 = Float32(v);
        c.sign = f32.c.sign;
        // too small exponent? (0+127-15)
        if (f32.c.exponent <= 112)
        {
            c.exponent = 0;
            c.mantissa = 0;
        }
        // too large exponent? (31+127-15)
        else if (f32.c.exponent >= 143)
        {
            c.exponent = 30;
            c.mantissa = 1023;
        }
        else
        {
            c.exponent = static_cast<int32_t>(f32.c.exponent) - 127 + 15;
            c.mantissa = static_cast<uint32_t>(f32.c.mantissa) >> 13;
        }
    }

    float get() const
    {
        auto f32 = Float32();
        f32.c.sign = c.sign;
        if (c.exponent == 0)
        {
            auto mantissa = static_cast<uint32_t>(c.mantissa);
            if (mantissa == 0)
            {
                f32.c.exponent = 0;
                f32.c.mantissa = 0;
            }
            else
            {
                auto mShift = 10 - static_cast<uint32_t>(std::log(static_cast<float>(mantissa)) *
                                                         1.44269502f);
                f32.c.exponent = 127 - (15 - 1) - mShift;
                f32.c.mantissa = mantissa << (mShift + 23 - 10);
            }
        }
        else if (c.exponent == 31) // 2^5 - 1
        {
            // infinity or NaN. set to 65504.0
            f32.c.exponent = 142;
            f32.c.mantissa = 8380416;
        }
        else
        {
            // normal number.
            // stored exponents are biased by half their range.
            f32.c.exponent = static_cast<int32_t>(c.exponent) - 15 + 127;
            f32.c.mantissa = static_cast<uint32_t>(c.mantissa) << 13;
        }
        return f32.value;
    }
};

static_assert(sizeof(Float32) == sizeof(float));
static_assert(sizeof(Float16) == sizeof(uint16_t));

} // namespace nopp
