#include "../frustum.hpp"
#include "../frustum_string.hpp"
#include "../math.hpp"
#include "../math_string.hpp"
#include "../size.hpp"
#include "../transform.hpp"
#include "../viewport.hpp"
#include "../viewport_string.hpp"
#include "support_lua.hpp"

using namespace nopp;

namespace
{

}

namespace sol
{
template <> struct is_automagical<Vec2> : std::false_type
{
};
template <> struct is_container<Vec2> : std::false_type
{
};
template <> struct is_automagical<Vec3> : std::false_type
{
};
template <> struct is_container<Vec3> : std::false_type
{
};
template <> struct is_automagical<Vec4> : std::false_type
{
};
template <> struct is_container<Vec4> : std::false_type
{
};
template <> struct is_automagical<Mat3> : std::false_type
{
};
template <> struct is_container<Mat3> : std::false_type
{
};
template <> struct is_automagical<Mat4> : std::false_type
{
};
template <> struct is_container<Mat4> : std::false_type
{
};
template <> struct is_automagical<Transform> : std::false_type
{
};
template <> struct is_automagical<Frustum> : std::false_type
{
};
template <> struct is_automagical<Viewport> : std::false_type
{
};
} // namespace sol

namespace nopp
{
void loadLuaCoreMath(sol::state& luaState)
{
    // Vec2
    {
        auto t = luaState.new_usertype<Vec2>(
            "Vec2", sol::call_constructor,
            sol::constructors<Vec2(), Vec2(Vec2::Scalar, Vec2::Scalar)>());
        t["x"] = sol::property([](Vec2 const& v) -> Vec2::Scalar { return v.x(); },
                               [](Vec2& v, Vec2::Scalar value) { v.x() = value; });
        t["y"] = sol::property([](Vec2 const& v) -> Vec2::Scalar { return v.y(); },
                               [](Vec2& v, Vec2::Scalar value) { v.y() = value; });
        t[sol::meta_function::addition] = [](Vec2 const& v1, Vec2 const& v2) -> Vec2
        { return v1 + v2; };
        t[sol::meta_function::subtraction] = [](Vec2 const& v1, Vec2 const& v2) -> Vec2
        { return v1 - v2; };
        t[sol::meta_function::multiplication] = sol::overload(
            [](Vec2 const& v1, Vec2 const& v2) -> Vec2 { return v1.cwiseProduct(v2); },
            [](Vec2 const& v, Vec2::Scalar k) -> Vec2 { return v * k; });
        t[sol::meta_function::equal_to] = [](Vec2 const& v1, Vec2 const& v2) -> bool
        { return v1 == v2; };
        t["norm"] = &Vec2::norm;
        t["normalize"] = &Vec2::normalize;
        t["dot"] = [](Vec2 const& v1, Vec2 const& v2) -> Vec2::Scalar { return v1.dot(v2); };
        t[sol::meta_function::to_string] = [](Vec2 const& v) { return toString(v); };
    }
    // Vec3
    {
        auto t = luaState.new_usertype<Vec3>(
            "Vec3", sol::call_constructor,
            sol::constructors<Vec3(), Vec3(Vec3::Scalar, Vec3::Scalar, Vec3::Scalar)>());
        t["x"] = sol::property([](Vec3 const& v) -> Vec3::Scalar { return v.x(); },
                               [](Vec3& v, Vec3::Scalar value) { v.x() = value; });
        t["y"] = sol::property([](Vec3 const& v) -> Vec3::Scalar { return v.y(); },
                               [](Vec3& v, Vec3::Scalar value) { v.y() = value; });
        t["z"] = sol::property([](Vec3 const& v) -> Vec3::Scalar { return v.z(); },
                               [](Vec3& v, Vec3::Scalar value) { v.z() = value; });
        t[sol::meta_function::addition] = [](Vec3 const& v1, Vec3 const& v2) -> Vec3
        { return v1 + v2; };
        t[sol::meta_function::subtraction] = [](Vec3 const& v1, Vec3 const& v2) -> Vec3
        { return v1 - v2; };
        t[sol::meta_function::multiplication] = sol::overload(
            [](Vec3 const& v1, Vec3 const& v2) -> Vec3 { return v1.cwiseProduct(v2); },
            [](Vec3 const& v, Vec3::Scalar k) -> Vec3 { return v * k; });
        t[sol::meta_function::equal_to] = [](Vec3 const& v1, Vec3 const& v2) -> bool
        { return v1 == v2; };
        t["norm"] = &Vec3::norm;
        t["normalize"] = &Vec3::normalize;
        t["dot"] = [](Vec3 const& v1, Vec3 const& v2) -> Vec3::Scalar { return v1.dot(v2); };
        t["cross"] = [](Vec3 const& v1, Vec3 const& v2) -> Vec3 { return v1.cross(v2); };
        t[sol::meta_function::to_string] = [](Vec3 const& v) { return toString(v); };
    }
    // Vec4
    {
        auto t = luaState.new_usertype<Vec4>(
            "Vec4", sol::call_constructor,
            sol::constructors<Vec4(),
                              Vec4(Vec4::Scalar, Vec4::Scalar, Vec4::Scalar, Vec4::Scalar)>());
        t["x"] = sol::property([](Vec4 const& v) -> Vec4::Scalar { return v.x(); },
                               [](Vec4& v, Vec4::Scalar value) { v.x() = value; });
        t["y"] = sol::property([](Vec4 const& v) -> Vec4::Scalar { return v.y(); },
                               [](Vec4& v, Vec4::Scalar value) { v.y() = value; });
        t["z"] = sol::property([](Vec4 const& v) -> Vec4::Scalar { return v.z(); },
                               [](Vec4& v, Vec4::Scalar value) { v.z() = value; });
        t["w"] = sol::property([](Vec4 const& v) -> Vec4::Scalar { return v.w(); },
                               [](Vec4& v, Vec4::Scalar value) { v.w() = value; });
        t[sol::meta_function::addition] = [](Vec4 const& v1, Vec4 const& v2) -> Vec4
        { return v1 + v2; };
        t[sol::meta_function::subtraction] = [](Vec4 const& v1, Vec4 const& v2) -> Vec4
        { return v1 - v2; };
        t[sol::meta_function::multiplication] = sol::overload(
            [](Vec4 const& v1, Vec4 const& v2) -> Vec4 { return v1.cwiseProduct(v2); },
            [](Vec4 const& v, Vec4::Scalar k) -> Vec4 { return v * k; });
        t[sol::meta_function::equal_to] = [](Vec4 const& v1, Vec4 const& v2) -> bool
        { return v1 == v2; };
        t["norm"] = &Vec4::norm;
        t["normalize"] = &Vec4::normalize;
        t["dot"] = [](Vec4 const& v1, Vec4 const& v2) -> Vec4::Scalar { return v1.dot(v2); };
        t[sol::meta_function::to_string] = [](Vec4 const& v) { return toString(v); };
    }
    // Mat3
    {
        auto t =
            luaState.new_usertype<Mat3>("Mat3", sol::call_constructor, sol::constructors<Mat3()>());
        t[sol::meta_function::addition] = [](Mat3 const& m1, Mat3 const& m2) -> Mat3
        { return m1 + m2; };
        t[sol::meta_function::to_string] = [](Mat3 const& m) { return toString(m); };
    }

    // Mat4
    {
        auto t =
            luaState.new_usertype<Mat4>("Mat4", sol::call_constructor, sol::constructors<Mat4()>());
        t[sol::meta_function::to_string] = [](Mat4 const& m) { return toString(m); };
    }

    luaState["Log2"] = Log2;
    luaState["Pi"] = Pi;
    luaState["HalfPi"] = HalfPi;
    luaState["TwoPi"] = TwoPi;
    luaState["InvPi"] = InvPi;
    luaState["ToRadian"] = ToRadian;
    luaState["ToDegree"] = ToDegree;
    luaState["Epsilon"] = Epsilon;

    luaState["lerp"] =
        sol::overload(lerp<float>, lerp<Vec2>, lerp<Vec3>, lerp<Vec4>);
    luaState["clamp"] = sol::overload(clamp<float>);
    luaState["distance"] = sol::overload(
        [](Vec2 const& v1, Vec2 const& v2) { return distance(v1, v2); },
        [](Vec3 const& v1, Vec3 const& v2) { return distance(v1, v2); });

    // Size3i
    {
        auto t = luaState.new_usertype<Size3i>(
            "Size3i", sol::call_constructor,
            sol::constructors<Size3i(), Size3i(int32_t, int32_t, int32_t)>());
        t["width"] = &Size3i::width;
        t["height"] = &Size3i::height;
        t["depth"] = &Size3i::depth;
        t[sol::meta_function::equal_to] = &Size3i::operator==;
    }

    // Transform
    {
        auto t = luaState.new_usertype<Transform>(
            "Transform", sol::call_constructor,
            sol::constructors<Transform(), Transform(Mat3 const&, Vec3 const&),
                              Transform(Mat3 const&, Vec3 const&, Vec3 const&)>());
        t["translation"] = &Transform::translation;
        t["rotation"] = &Transform::rotation;
        t["scale"] = &Transform::scale;
        t["matrix"] = &Transform::matrix;
    }

    // Frustum
    {
        auto t = luaState.new_usertype<Frustum>("Frustum", sol::call_constructor,
                                                sol::constructors<Frustum()>());
        t["left"] = &Frustum::left;
        t["right"] = &Frustum::right;
        t["bottom"] = &Frustum::bottom;
        t["top"] = &Frustum::top;
        t["znear"] = &Frustum::znear;
        t["zfar"] = &Frustum::zfar;
        t["type"] = &Frustum::type;
        t[sol::meta_function::to_string] = [](Frustum const& f) { return toString(f); };
    }

    // Viewport
    {
        auto t = luaState.new_usertype<Viewport>(
            "Viewport", sol::call_constructor,
            sol::constructors<Viewport(), Viewport(float, float, float, float)>());
        t["x"] = &Viewport::x;
        t["y"] = &Viewport::y;
        t["width"] = &Viewport::width;
        t["height"] = &Viewport::height;
        t["minDepth"] = &Viewport::minDepth;
        t["maxDepth"] = &Viewport::maxDepth;
        t[sol::meta_function::to_string] = [](Viewport const& v) { return toString(v); };
    }

    luaState["computeMatView"] = computeMatView;
    luaState["computeViewLookDir"] = computeViewLookDir;
    luaState["computeViewLookAt"] = computeViewLookAt;
    luaState["computeMatProj"] = computeMatProj;
    luaState["computeMatProjAlt"] = computeMatProjAlt;
    luaState["computeProjPerspFOVH"] = computeProjPerspFOVH;
    luaState["computeProjPerspFOVV"] = computeProjPerspFOVV;
    luaState["computeWorldToViewport"] = computeWorldToViewport;

    luaState["cameraWorldRight"] = cameraWorldRight;
    luaState["cameraWorldUp"] = cameraWorldUp;
    luaState["cameraWorldDir"] = cameraWorldDir;

    // Ray
    //{
    //    auto t =
    //        luaState.new_usertype<Ray>("Ray", sol::call_constructor, sol::constructors<Ray()>());
    //    t["start"] = &Ray::start;
    //    t["end"] = &Ray::end;
    //}

    // luaState["computeViewportToRay"] = computeViewportToRay;
}
} // namespace nopp
