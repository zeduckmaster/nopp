#pragma once

#include <cstdint>
#include <string>

namespace nopp
{
    int32_t rollDice(int32_t faceCount);
    int32_t roll(std::string const& expression);
}
