#pragma once

#include <lib_core/fast_rtti.hpp>
#include <lib_core/id.hpp>
#include <lib_core/ref_object.hpp>

namespace nopp
{

class GPUData : public RefObject
{
    FAST_RTTI_CLASS_ROOT(GPUData)

  public:
    ~GPUData() override;

    GPUData(GPUData const&) = delete;

    GPUData(GPUData&& gpuData);

    GPUData& operator=(GPUData const&) = delete;

    GPUData& operator=(GPUData&& gpuData);

    Id id() const { return _id; }

    operator Id() const { return _id; }

    void commit();

  protected:
    GPUData();

  private:
    Id _id;
};

bool isGPUDataIdValid(Id id);

GPUData* getGPUData(Id id);

////////////////////////////////////////////////////////////////////////////////

class GPUThread
{
  public:
    virtual ~GPUThread();

    virtual void updateData(Id id) = 0;

    virtual void destroyData(Id id) = 0;

    void updateData(GPUData* gpuData)
    {
        assert(gpuData != nullptr);
        updateData(gpuData->id());
    }

    void destroyData(GPUData* gpuData)
    {
        assert(gpuData != nullptr);
        destroyData(gpuData->id());
    }

  protected:
    GPUThread();
};

} // namespace nopp
