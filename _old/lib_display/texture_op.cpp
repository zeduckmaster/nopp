#include "texture.hpp"

using namespace nopp;

namespace
{
struct ImageDesc
{
    Size3i size;
    int32_t totalByteSize = 0;
    int32_t pixelByteSize = 0;
    int32_t stepByteSize = 0;

    ImageDesc(TextureDesc const& desc) : size{desc.size}
    {
        totalByteSize = imageByteSize(size, desc.format);
        pixelByteSize = ::pixelByteSize(desc.format);
        stepByteSize = size.width * pixelByteSize;
    }

    ImageDesc(TextureDesc const& desc, Size3i const& size) : size{size}
    {
        totalByteSize = imageByteSize(size, desc.format);
        pixelByteSize = ::pixelByteSize(desc.format);
        stepByteSize = size.width * pixelByteSize;
    }
};

void resizeNearest(ImageData& dstData,
                   ImageDesc const& dstDesc,
                   ImageData const& srcData,
                   ImageDesc const& srcDesc)
{
    auto scale = srcDesc.size / dstDesc.size;
    auto dstWidth = dstDesc.size.width;
    auto xOffs = std::vector<int32_t>{};
    xOffs.resize(dstWidth);
    for (auto x = 0; x < dstWidth; ++x)
    {
        auto sx = static_cast<int32_t>(std::floor(x * scale.width));
        xOffs[x] = std::min(sx, srcDesc.size.width - 1) * srcDesc.pixelByteSize;
    }

    for (auto y = 0; y < dstDesc.size.height; ++y)
    {
        auto pDstData = dstData.data() + dstDesc.stepByteSize * y;
        auto sy = std::min(static_cast<int32_t>(std::floor(y * scale.height)),
                           srcDesc.size.height - 1);
        auto pSrcData = srcData.data() + srcDesc.stepByteSize * sy;
        auto x = 0;

        switch (srcDesc.pixelByteSize)
        {
        case 1:
            for (x = 0; x <= dstWidth - 2; x += 2)
            {
                auto t0 = pSrcData[xOffs[x]];
                auto t1 = pSrcData[xOffs[x + 1]];
                pDstData[x] = t0;
                pDstData[x + 1] = t1;
            }
            for (; x < dstWidth; ++x)
                pDstData[x] = pSrcData[xOffs[x]];
            break;
        case 2:
            for (x = 0; x < dstWidth; ++x)
            {
                *reinterpret_cast<uint16_t*>(pDstData + x * 2) =
                    *reinterpret_cast<uint16_t const*>(pSrcData + xOffs[x]);
            }
            break;
        case 3:
            for (x = 0; x < dstWidth; ++x, pDstData += 3)
            {
                auto sData = pSrcData + xOffs[x];
                pDstData[0] = sData[0];
                pDstData[1] = sData[1];
                pDstData[2] = sData[2];
            }
            break;
        case 4:
            for (x = 0; x < dstWidth; ++x)
            {
                *reinterpret_cast<int32_t*>(pDstData + x * 4) =
                    *reinterpret_cast<int32_t const*>(pSrcData + xOffs[x]);
            }
            break;
        case 6:
            for (x = 0; x < dstWidth; ++x, pDstData += 6)
            {
                auto sData =
                    reinterpret_cast<uint16_t const*>(pSrcData + xOffs[x]);
                auto dData = reinterpret_cast<uint16_t*>(pDstData);
                dData[0] = sData[0];
                dData[1] = sData[1];
                dData[2] = sData[2];
            }
            break;
        case 8:
            for (x = 0; x < dstWidth; ++x, pDstData += 8)
            {
                auto sData =
                    reinterpret_cast<int32_t const*>(pSrcData + xOffs[x]);
                auto dData = reinterpret_cast<int32_t*>(pDstData);
                dData[0] = sData[0];
                dData[1] = sData[1];
            }
            break;
        case 12:
            for (x = 0; x < dstWidth; ++x, pDstData += 12)
            {
                auto sData =
                    reinterpret_cast<int32_t const*>(pSrcData + xOffs[x]);
                auto dData = reinterpret_cast<int32_t*>(pDstData);
                dData[0] = sData[0];
                dData[1] = sData[1];
                dData[2] = sData[2];
            }
            break;
        }
    }
}

ImageData resize(ImageData const& srcData,
                 TextureDesc const& srcTextureDesc,
                 Size3i const& dstSize,
                 eInterpolation interpolation)
{
    auto srcImageDesc = ImageDesc{srcTextureDesc};
    auto dstImageDesc = ImageDesc{srcTextureDesc, dstSize};
    auto dstData = ImageData{};
    dstData.resize(dstImageDesc.totalByteSize);

    if (interpolation == eInterpolation::Nearest)
    {
        resizeNearest(dstData, dstImageDesc, srcData, srcImageDesc);
        return dstData;
    }

    return dstData;
}
} // namespace

void TextureData::resize(Size3i const& newSize, eInterpolation interpolation)
{
    // easy case
    if (desc.size == newSize)
        return;

    // desc.size = newSize;
    for (auto& imageData : levels[0].images)
    {
        auto newData = ::resize(imageData, desc, newSize, interpolation);
        imageData = newData;
    }
    levels[0].size = newSize;
    desc.size = newSize;
    // recompute mipmaps if needed
    if (desc.mipLevelCount > 1)
        generateMipmap(interpolation);
}

void TextureData::generateMipmap(eInterpolation interpolation) {}

void TextureData::removeMipmap() {}
