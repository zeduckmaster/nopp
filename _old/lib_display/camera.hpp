#pragma once

#include <lib_core/math.hpp>

namespace nopp
{
class CameraEditor
{
  public:
    enum class eViewPreset : uint32_t
    {
        Front = 0,
        Top,
        Right
    };

    CameraEditor();

    void setViewPreset(eViewPreset viewPreset);

    void fitView(Vec3 const& min, Vec3 const& max);

    void setProjType(Frustum::eType type);

    void
    setPerspFOVV(float fovvRad, float aspectRatio, float znear, float zfar);

    void setOrtho(float left,
                  float right,
                  float bottom,
                  float top,
                  float znear,
                  float zfar);

    void setAspectRatio(float aspectRatio);

    void setViewport(Viewport const& viewport); // updates also aspect ratio

    void lookAt(Vec3 const& pos, Vec3 const& at, Vec3 const& up);

    void zoom(float step);

    void pan(Vec2 const& step);

    void rotate(float lateral, float longitudinal);

    Vec3 const& pos() const { return _pos; }

    Vec3 const& at() const { return _at; }

    Vec3 const& up() const { return _up; }

    float lateral() const { return _lateral; }

    float longitudinal() const { return _longitudinal; }

    Frustum::eType projType() const { return _frustum.type; }

    Transform const& transform() const { return _transform; }

    Frustum const& frustum() const { return _frustum; }

    Mat4 const& matView() const { return _matView; }

    Mat4 const& matProj() const { return _matProj; }

    Viewport const& viewport() const { return _viewport; }

  private:
    void _update();

    Mat4 _matView;
    Mat4 _matProj;
    Frustum _frustum;
    Viewport _viewport;
    Transform _transform;
    float _fovv = ToRadian * 55.0f; // in radians
    Vec3 _pos = Vec3{0.0f, -2.0f, 0.0f};
    Vec3 _at = Vec3{0.0f, 0.0f, 0.0f};
    Vec3 _up = Vec3{0.0f, 0.0f, 1.0f};
    Vec3 _right = Vec3{1.0f, 0.0f, 0.0f};
    float _lateral = 0.0f;
    float _longitudinal = 0.0f;
    float _targetOffset = 2.0f; // how far from target
};

} // namespace nopp
