#include "gpu_gl.h"
#include "helpers.h"
#include "log.h"
#include "mesh.h"
#include <thread>

using namespace nopp;
using namespace nopp::gl;

namespace
{
    using ProcGLActiveTexture = void (GL_APIENTRY*)(GLenum texture);
    using ProcGLAttachShader = void (GL_APIENTRY*)(GLuint program, GLuint shader);
    using ProcGLBindBuffer = void (GL_APIENTRY*)(GLenum target, GLuint buffer);
    using ProcGLBindBufferBase = void (GL_APIENTRY*)(GLenum target, GLuint index, GLuint buffer);
    using ProcGLBindFramebuffer = void (GL_APIENTRY*)(GLenum target, GLuint framebuffer);
    using ProcGLBindSampler = void (GL_APIENTRY*)(GLuint unit, GLuint sampler);
    using ProcGLBindTexture = void (GL_APIENTRY*)(GLenum target, GLuint texture);
    using ProcGLBindVertexArray = void (GL_APIENTRY*)(GLuint array);
    using ProcGLBindVertexBuffer = void (GL_APIENTRY*)(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
    using ProcGLBufferData = void (GL_APIENTRY*)(GLenum target, GLsizeiptr size, void const* data, GLenum usage);
    using ProcGLCheckFramebufferStatus = GLenum (GL_APIENTRY*)(GLenum target);
    using ProcGLClear = void (GL_APIENTRY*)(GLbitfield mask);
    using ProcGLClearColor = void (GL_APIENTRY*)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
    using ProcGLClearDepthf = void (GL_APIENTRY*)(GLfloat depth);
    using ProcGLClearStencil = void (GL_APIENTRY*)(GLint s);
    using ProcGLCompileShader = void (GL_APIENTRY*)(GLuint shader);
    using ProcGLCreateProgram = GLuint(GL_APIENTRY*)();
    using ProcGLCreateShader = GLuint(GL_APIENTRY*)(GLenum type);
    using ProcGLCullFace = void (GL_APIENTRY*)(GLenum mode);
    using ProcGLDebugMessageCallback = void (GL_APIENTRY*)(GLDEBUGPROC callback, void const* userParam);
    using ProcGLDeleteBuffers = void (GL_APIENTRY*)(GLsizei n, GLuint const* buffers);
    using ProcGLDeleteFramebuffers = void (GL_APIENTRY*)(GLsizei n, GLuint const* framebuffers);
    using ProcGLDeleteProgram = void (GL_APIENTRY*)(GLuint program);
    using ProcGLDeleteSamplers = void (GL_APIENTRY*)(GLsizei count, GLuint const* samplers);
    using ProcGLDeleteShader = void (GL_APIENTRY*)(GLuint shader);
    using ProcGLDeleteTextures = void (GL_APIENTRY*)(GLsizei n, GLuint const* textures);
    using ProcGLDeleteVertexArrays = void (GL_APIENTRY*)(GLsizei n, GLuint const* arrays);
    using ProcGLDepthFunc = void (GL_APIENTRY*)(GLenum func);
    using ProcGLDepthMask = void (GL_APIENTRY*)(GLboolean flag);
    using ProcGLDepthRangef = void (GL_APIENTRY*)(GLfloat nearVal, GLfloat farVal);
    using ProcGLDisable = void (GL_APIENTRY*)(GLenum cap);
    using ProcGLDrawArrays = void (GL_APIENTRY*)(GLenum mode, GLint first, GLsizei count);
    using ProcGLDrawElements = void (GL_APIENTRY*)(GLenum mode, GLsizei count, GLenum type, void const* indices);
    using ProcGLEnable = void (GL_APIENTRY*)(GLenum cap);
    using ProcGLEnableVertexAttribArray = void (GL_APIENTRY*)(GLuint index);
    using ProcGLFlush = void (GL_APIENTRY*)();
    using ProcGLFramebufferParameteri = void (GL_APIENTRY*)(GLenum target, GLenum pname, GLint param);
    using ProcGLFramebufferTexture = void (GL_APIENTRY*)(GLenum target, GLenum attachment, GLuint texture, GLint level);
    using ProcGLFrontFace = void (GL_APIENTRY*)(GLenum mode);
    using ProcGLGenBuffers = void (GL_APIENTRY*)(GLsizei n, GLuint* buffers);
    using ProcGLGenFramebuffers = void (GL_APIENTRY*)(GLsizei n, GLuint* framebuffers);
    using ProcGLGenSamplers = void (GL_APIENTRY*)(GLsizei count, GLuint* samplers);
    using ProcGLGenTextures = void (GL_APIENTRY*)(GLsizei n, GLuint* textures);
    using ProcGLGenVertexArrays = void (GL_APIENTRY*)(GLsizei n, GLuint* arrays);
    using ProcGLGetError = GLenum(GL_APIENTRY*)();
    using ProcGLGetIntegerv = void (GL_APIENTRY*)(GLenum pname, GLint* data);
    using ProcGLGetFramebufferParameteriv = void (GL_APIENTRY*)(GLenum target, GLenum pname, GLint* params);
    using ProcGLGetProgramBinary = void (GL_APIENTRY*)(GLuint program, GLsizei bufSize, GLsizei* length, GLenum* binaryFormat, void* binary);
    using ProcGLGetProgramInfoLog = void (GL_APIENTRY*)(GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
    using ProcGLGetProgramInterfaceiv = void (GL_APIENTRY*)(GLuint program, GLenum programInterface, GLenum pname, GLint* params);
    using ProcGLGetProgramiv = void (GL_APIENTRY*)(GLuint program, GLenum pname, GLint* params);
    using ProcGLGetProgramResourceName = void (GL_APIENTRY*)(GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei* length, GLchar* name);
    using ProcGLGetShaderInfoLog = void (GL_APIENTRY*)(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
    using ProcGLGetShaderiv = void (GL_APIENTRY*)(GLuint shader, GLenum pname, GLint* params);
    using ProcGLGetString = GLubyte const* (GL_APIENTRY*)(GLenum name);
    using ProcGLGetStringi = GLubyte const* (GL_APIENTRY*)(GLenum name, GLuint index);
    using ProcGLGetUniformBlockIndex = GLuint(GL_APIENTRY*)(GLuint program, GLchar const* uniformBlockName);
    using ProcGLLinkProgram = void (GL_APIENTRY*)(GLuint program);
    using ProcGLPixelStorei = void (GL_APIENTRY*)(GLenum pname, GLint param);
    using ProcGLSamplerParameteri = void (GL_APIENTRY*)(GLuint sampler, GLenum pname, GLint param);
    using ProcGLShaderSource = void (GL_APIENTRY*)(GLuint shader, GLsizei count, GLchar const* const* string, GLint const* length);
    using ProcGLTexImage2D = void (GL_APIENTRY*)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, void const* pixels);
    using ProcGLUseProgram = void (GL_APIENTRY*)(GLuint program);
    using ProcGLVertexAttribBinding = void (GL_APIENTRY*)(GLuint attribindex, GLuint bindingindex);
    using ProcGLVertexAttribFormat = void (GL_APIENTRY*)(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset);
    using ProcGLViewport = void (GL_APIENTRY*)(GLint x, GLint y, GLsizei width, GLsizei height);

    GL createGL()
    {
        auto hal = GL{};
        hal.glActiveTexture = reinterpret_cast<ProcGLActiveTexture>(SDL_GL_GetProcAddress("glActiveTexture"));
        hal.glAttachShader = reinterpret_cast<ProcGLAttachShader>(SDL_GL_GetProcAddress("glAttachShader"));
        hal.glBindBuffer = reinterpret_cast<ProcGLBindBuffer>(SDL_GL_GetProcAddress("glBindBuffer"));
        hal.glBindBufferBase = reinterpret_cast<ProcGLBindBufferBase>(SDL_GL_GetProcAddress("glBindBufferBase"));
        hal.glBindFramebuffer = reinterpret_cast<ProcGLBindFramebuffer>(SDL_GL_GetProcAddress("glBindFramebuffer"));
        hal.glBindSampler = reinterpret_cast<ProcGLBindSampler>(SDL_GL_GetProcAddress("glBindSampler"));
        hal.glBindTexture = reinterpret_cast<ProcGLBindTexture>(SDL_GL_GetProcAddress("glBindTexture"));
        hal.glBindVertexArray = reinterpret_cast<ProcGLBindVertexArray>(SDL_GL_GetProcAddress("glBindVertexArray"));
        hal.glBindVertexBuffer = reinterpret_cast<ProcGLBindVertexBuffer>(SDL_GL_GetProcAddress("glBindVertexBuffer"));
        hal.glBufferData = reinterpret_cast<ProcGLBufferData>(SDL_GL_GetProcAddress("glBufferData"));
        hal.glCheckFramebufferStatus = reinterpret_cast<ProcGLCheckFramebufferStatus>(SDL_GL_GetProcAddress("glCheckFramebufferStatus"));
        hal.glClear = reinterpret_cast<ProcGLClear>(SDL_GL_GetProcAddress("glClear"));
        hal.glClearColor = reinterpret_cast<ProcGLClearColor>(SDL_GL_GetProcAddress("glClearColor"));
        hal.glClearDepthf = reinterpret_cast<ProcGLClearDepthf>(SDL_GL_GetProcAddress("glClearDepthf"));
        hal.glClearStencil = reinterpret_cast<ProcGLClearStencil>(SDL_GL_GetProcAddress("glClearStencil"));
        hal.glCompileShader = reinterpret_cast<ProcGLCompileShader>(SDL_GL_GetProcAddress("glCompileShader"));
        hal.glCreateProgram = reinterpret_cast<ProcGLCreateProgram>(SDL_GL_GetProcAddress("glCreateProgram"));
        hal.glCreateShader = reinterpret_cast<ProcGLCreateShader>(SDL_GL_GetProcAddress("glCreateShader"));
        hal.glCullFace = reinterpret_cast<ProcGLCullFace>(SDL_GL_GetProcAddress("glCullFace"));
        hal.glDebugMessageCallback = reinterpret_cast<ProcGLDebugMessageCallback>(SDL_GL_GetProcAddress("glDebugMessageCallback"));
        hal.glDeleteBuffers = reinterpret_cast<ProcGLDeleteBuffers>(SDL_GL_GetProcAddress("glDeleteBuffers"));
        hal.glDeleteFramebuffers = reinterpret_cast<ProcGLDeleteFramebuffers>(SDL_GL_GetProcAddress("glDeleteFramebuffers"));
        hal.glDeleteProgram = reinterpret_cast<ProcGLDeleteProgram>(SDL_GL_GetProcAddress("glDeleteProgram"));
        hal.glDeleteSamplers = reinterpret_cast<ProcGLDeleteSamplers>(SDL_GL_GetProcAddress("glDeleteSamplers"));
        hal.glDeleteShader = reinterpret_cast<ProcGLDeleteShader>(SDL_GL_GetProcAddress("glDeleteShader"));
        hal.glDeleteTextures = reinterpret_cast<ProcGLDeleteTextures>(SDL_GL_GetProcAddress("glDeleteTextures"));
        hal.glDeleteVertexArrays = reinterpret_cast<ProcGLDeleteVertexArrays>(SDL_GL_GetProcAddress("glDeleteVertexArrays"));
        hal.glDepthFunc = reinterpret_cast<ProcGLDepthFunc>(SDL_GL_GetProcAddress("glDepthFunc"));
        hal.glDepthMask = reinterpret_cast<ProcGLDepthMask>(SDL_GL_GetProcAddress("glDepthMask"));
        hal.glDepthRangef = reinterpret_cast<ProcGLDepthRangef>(SDL_GL_GetProcAddress("glDepthRangef"));
        hal.glDisable = reinterpret_cast<ProcGLDisable>(SDL_GL_GetProcAddress("glDisable"));
        hal.glDrawArrays = reinterpret_cast<ProcGLDrawArrays>(SDL_GL_GetProcAddress("glDrawArrays"));
        hal.glDrawElements = reinterpret_cast<ProcGLDrawElements>(SDL_GL_GetProcAddress("glDrawElements"));
        hal.glEnable = reinterpret_cast<ProcGLEnable>(SDL_GL_GetProcAddress("glEnable"));
        hal.glEnableVertexAttribArray = reinterpret_cast<ProcGLEnableVertexAttribArray>(SDL_GL_GetProcAddress("glEnableVertexAttribArray"));
        hal.glFlush = reinterpret_cast<ProcGLFlush>(SDL_GL_GetProcAddress("glFlush"));
        hal.glFramebufferParameteri = reinterpret_cast<ProcGLFramebufferParameteri>(SDL_GL_GetProcAddress("glFramebufferParameteri"));
        hal.glFramebufferTexture = reinterpret_cast<ProcGLFramebufferTexture>(SDL_GL_GetProcAddress("glFramebufferTexture"));
        hal.glFrontFace = reinterpret_cast<ProcGLFrontFace>(SDL_GL_GetProcAddress("glFrontFace"));
        hal.glGenBuffers = reinterpret_cast<ProcGLGenBuffers>(SDL_GL_GetProcAddress("glGenBuffers"));
        hal.glGenFramebuffers = reinterpret_cast<ProcGLGenFramebuffers>(SDL_GL_GetProcAddress("glGenFramebuffers"));
        hal.glGenSamplers = reinterpret_cast<ProcGLGenSamplers>(SDL_GL_GetProcAddress("glGenSamplers"));
        hal.glGenTextures = reinterpret_cast<ProcGLGenTextures>(SDL_GL_GetProcAddress("glGenTextures"));
        hal.glGenVertexArrays = reinterpret_cast<ProcGLGenVertexArrays>(SDL_GL_GetProcAddress("glGenVertexArrays"));
        hal.glGetError = reinterpret_cast<ProcGLGetError>(SDL_GL_GetProcAddress("glGetError"));
        hal.glGetIntegerv = reinterpret_cast<ProcGLGetIntegerv>(SDL_GL_GetProcAddress("glGetIntegerv"));
        hal.glGetFramebufferParameteriv = reinterpret_cast<ProcGLGetFramebufferParameteriv>(SDL_GL_GetProcAddress("glGetFramebufferParameteriv"));
        hal.glGetProgramBinary = reinterpret_cast<ProcGLGetProgramBinary>(SDL_GL_GetProcAddress("glGetProgramBinary"));
        hal.glGetProgramInfoLog = reinterpret_cast<ProcGLGetProgramInfoLog>(SDL_GL_GetProcAddress("glGetProgramInfoLog"));
        hal.glGetProgramInterfaceiv = reinterpret_cast<ProcGLGetProgramInterfaceiv>(SDL_GL_GetProcAddress("glGetProgramInterfaceiv"));
        hal.glGetProgramiv = reinterpret_cast<ProcGLGetProgramiv>(SDL_GL_GetProcAddress("glGetProgramiv"));
        hal.glGetProgramResourceName = reinterpret_cast<ProcGLGetProgramResourceName>(SDL_GL_GetProcAddress("glGetProgramResourceName"));
        hal.glGetShaderInfoLog = reinterpret_cast<ProcGLGetShaderInfoLog>(SDL_GL_GetProcAddress("glGetShaderInfoLog"));
        hal.glGetShaderiv = reinterpret_cast<ProcGLGetShaderiv>(SDL_GL_GetProcAddress("glGetShaderiv"));
        hal.glGetString = reinterpret_cast<ProcGLGetString>(SDL_GL_GetProcAddress("glGetString"));
        hal.glGetStringi = reinterpret_cast<ProcGLGetStringi>(SDL_GL_GetProcAddress("glGetStringi"));
        hal.glGetUniformBlockIndex = reinterpret_cast<ProcGLGetUniformBlockIndex>(SDL_GL_GetProcAddress("glGetUniformBlockIndex"));
        hal.glLinkProgram = reinterpret_cast<ProcGLLinkProgram>(SDL_GL_GetProcAddress("glLinkProgram"));
        hal.glPixelStorei = reinterpret_cast<ProcGLPixelStorei>(SDL_GL_GetProcAddress("glPixelStorei"));
        hal.glSamplerParameteri = reinterpret_cast<ProcGLSamplerParameteri>(SDL_GL_GetProcAddress("glSamplerParameteri"));
        hal.glShaderSource = reinterpret_cast<ProcGLShaderSource>(SDL_GL_GetProcAddress("glShaderSource"));
        hal.glTexImage2D = reinterpret_cast<ProcGLTexImage2D>(SDL_GL_GetProcAddress("glTexImage2D"));
        hal.glUseProgram = reinterpret_cast<ProcGLUseProgram>(SDL_GL_GetProcAddress("glUseProgram"));
        hal.glVertexAttribBinding = reinterpret_cast<ProcGLVertexAttribBinding>(SDL_GL_GetProcAddress("glVertexAttribBinding"));
        hal.glVertexAttribFormat = reinterpret_cast<ProcGLVertexAttribFormat>(SDL_GL_GetProcAddress("glVertexAttribFormat"));
        hal.glViewport = reinterpret_cast<ProcGLViewport>(SDL_GL_GetProcAddress("glViewport"));
        return hal;
    }

    void GL_APIENTRY funcGLDebugProc(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, GLchar const* message, void const* userParam)
    {
        unused(id);
        unused(length);
        unused(userParam);

        auto ssource = std::string{};
        switch(source)
        {
        case GL_DEBUG_SOURCE_API: ssource = "API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB: ssource = "WINDOW_SYSTEM"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: ssource = "SHADER_COMPILER"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY: ssource = "THIRD_PARTY"; break;
        case GL_DEBUG_SOURCE_APPLICATION: ssource = "APPLICATION"; break;
        case GL_DEBUG_SOURCE_OTHER:
        default:
            ssource = "OTHER";
            break;
        }
        auto stype = std::string{};
        switch(type)
        {
        case GL_DEBUG_TYPE_ERROR: stype = "ERROR"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: stype = "DEPRECATED_BEHAVIOR"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: stype = "UNDEFINED_BEHAVIOR"; break;
        case GL_DEBUG_TYPE_PORTABILITY: stype = "PORTABILITY"; break;
        case GL_DEBUG_TYPE_PERFORMANCE: stype = "PERFORMANCE"; break;
        case GL_DEBUG_TYPE_MARKER: stype = "MARKER"; break;
        case GL_DEBUG_TYPE_PUSH_GROUP: stype = "PUSH_GROUP"; break;
        case GL_DEBUG_TYPE_POP_GROUP: stype= "POP_GROUP"; break;
        case GL_DEBUG_TYPE_OTHER:
        default:
            stype = "OTHER";
            break;
        }
        auto sseverity = std::string{};
        switch(severity)
        {
        case GL_DEBUG_SEVERITY_HIGH: sseverity = "HIGH"; break;
        case GL_DEBUG_SEVERITY_MEDIUM: sseverity = "MEDIUM"; break;
        case GL_DEBUG_SEVERITY_LOW: sseverity = "LOW"; break;
        default:
            sseverity = "OTHER";
            break;
        }
        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "[GL][%s][%s][%s] %s", ssource.c_str(), stype.c_str(), sseverity.c_str(), message);
    }
}

////////////////////////////////////////////////////////////////////////////////

#if !defined(NDEBUG)
void nopp::gl::glCheckError(GL const& gl, char const* func)
{
    auto glerr = gl.glGetError();
    if(glerr == GL_NO_ERROR)
        return;
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[GL] %s: \"%#06x\"", func, glerr);
}
#endif

////////////////////////////////////////////////////////////////////////////////

GLThread::GLThread()
{
    _glDatas.reserve(1024);
    _thread = new std::thread(&GLThread::_funcThread, this);
}

GLThread::~GLThread()
{
    _isThreadInterrupted = true;
    _swapCond.notify_one();
    if(_thread->joinable())
        _thread->join();
    else
        logError("GLThread::~GLThread: thread not joinable");
    delete _thread;
}

void GLThread::submit(std::vector<GLCmd> const& cmds, GLFence* fence)
{
    std::lock_guard<std::mutex> lock{_swapMutex};
    append(_cmdsWait, cmds);
    if(fence != nullptr)
    {
        fence->event.reset();
        _cmdsWait.emplace_back([=](GL const& gl) { fence->event.raise(); });
    }
    _swapCond.notify_one();
}

void GLThread::addTerminateCmds(std::vector<GLCmd> const& cmds)
{
    append(_cmdsTerminate, cmds);
}

GLData* GLThread::addData(Id gpuDataId)
{
    auto index = gpuDataId.index();
    if(_glDatas.size() <= index)
        _glDatas.resize(index + 1);
    auto& dst = _glDatas[index];
    assert(dst == nullptr);
    auto gpuData = getGPUData(gpuDataId);
    if(gpuData->isTypeOf<MeshData>() == true)
        dst = new GLMesh{};
    return dst;
}

void GLThread::removeData(Id gpuDataId)
{

}

void GLThread::_funcThread()
{
    _glInitialize();
    while(true)
    {
        auto cmdsWork = std::vector<GLCmd>{};
        // swap data from main thread
        {
            std::unique_lock<std::mutex> lock{_swapMutex};
            while((_cmdsWait.empty() == true) && (_isThreadInterrupted == false))
                _swapCond.wait(lock);
            if(_isThreadInterrupted == true)
            {
                _glTerminate();
                logDebug("stopping opengl thread");
                return;
            }
            cmdsWork.reserve(_cmdsWait.size()); // small optim
            cmdsWork.swap(_cmdsWait);
        }
        for(auto& cmd : cmdsWork)
            cmd(_gl);
    }
}

void GLThread::_glInitialize()
{
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    auto tmpWindow = SDL_CreateWindow("", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 16, 15, SDL_WINDOW_OPENGL|SDL_WINDOW_HIDDEN);
    _glContext = SDL_GL_CreateContext(tmpWindow);
    _gl = createGL();
    _gl.wglMakeCurrent = [&](SDL_Window* window)
    {
        assert(window != nullptr);
        auto curWindow = SDL_GL_GetCurrentWindow();
        auto curContext = SDL_GL_GetCurrentContext();
        if((curWindow == window) && (curContext == _glContext))
            return;
        auto r = SDL_GL_MakeCurrent(window, _glContext);
    };
    _gl.wglSwapWindow = [&](SDL_Window* window)
    {
        SDL_GL_SwapWindow(window);
    };
    _gl.glDebugMessageCallback(funcGLDebugProc, nullptr);
    _gl.glEnable(GL_DEBUG_OUTPUT);
    logInfo("[GL] vendor: %s", _gl.glGetString(GL_VENDOR));
    logInfo("[GL] renderer: %s", _gl.glGetString(GL_RENDERER));
    logInfo("[GL] version: %s", _gl.glGetString(GL_VERSION));
    logInfo("[GL] shading language: %s", _gl.glGetString(GL_SHADING_LANGUAGE_VERSION));
    //auto nbExtensions = GLint{0};
    //_gl.glGetIntegerv(GL_NUM_EXTENSIONS, &nbExtensions);
    //auto strExtensions = std::string{};
    //for(auto n = 0; n < nbExtensions; ++n)
    //{
    //    strExtensions.append(reinterpret_cast<char const*>(_gl.glGetStringi(GL_EXTENSIONS, n)));
    //    strExtensions.push_back(' ');
    //}
    //SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "[GL] extensions: %s", strExtensions.c_str());
    SDL_DestroyWindow(tmpWindow);
}

void GLThread::_glTerminate()
{
    // call all terminate commands
    for(auto& cmd : _cmdsTerminate)
        cmd(_gl);

    // delete all remaining data if any
    for(auto& data : _glDatas)
    {
        if(data != nullptr)
        {
            delete data;
            data = nullptr;
        }
    }

    // delete context
    if(_glContext != nullptr)
    {
        _gl = GL{};
        SDL_GL_DeleteContext(_glContext);
        _glContext = nullptr;
    }
    logInfo("[GL] terminate");

    //if(_hGLRC != nullptr)
    //{
    //    delete _gl;
    //    _gl = nullptr;
    //    delete _wgl;
    //    _wgl = nullptr;
    //    wglDeleteContext(_hGLRC);
    //    _hGLRC = nullptr;
    //}
    //logInfo("shutdown GL");
    ////if(_glOffFramebuffer != 0)
    ////{
    ////	glDeleteFramebuffers(1, &_glOffFramebuffer);
    ////	glDeleteTextures(1, &_glOffColor);
    ////	glDeleteRenderbuffers(1, &_glOffDepth);
    ////}
    //
    //
    ////checkGLError("GLThread::_glShutdown");
    ////_eglShutdown();
}
