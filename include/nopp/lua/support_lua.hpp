#pragma once

#define SOL_SAFE_FUNCTION 1
#define SOL_NO_EXCEPTIONS 1
#include <sol/sol.hpp>

namespace nopp
{
int32_t luaPrint(lua_State* L);

sol::protected_function_result runLuaScript(sol::state& luaState, std::string const& script);

void loadLuaCore(sol::state& luaState);
} // namespace nopp
