#pragma once

#include "bound.hpp"
#include "enum.hpp"
#include "math.hpp"
#include "packed_normal.hpp"

namespace nopp
{

// should be used as a flag
enum class eVertexDeclaration : uint32_t
{
    Position = 0x00000001,
    Normal = 0x00000002,
    Tangent = 0x00000004,
    Color = 0x00000008,
    TexCoord0 = 0x00000010,
    TexCoord1 = 0x00000020,
    Custom0 = 0x00010000,
    // Custom1   = 0x00020000,
    // Custom2   = 0x00040000,
    // Custom3   = 0x00080000,

    All = 0xffffffff
};

using VertexDeclaration = std::underlying_type<eVertexDeclaration>::type;

constexpr VertexDeclaration& operator|=(VertexDeclaration& decl1, eVertexDeclaration const decl2)
{
    decl1 |= static_cast<VertexDeclaration>(decl2);
    return decl1;
}

constexpr VertexDeclaration operator|(eVertexDeclaration const decl1,
                                      eVertexDeclaration const decl2)
{
    return static_cast<VertexDeclaration>(decl1) | static_cast<VertexDeclaration>(decl2);
}

constexpr VertexDeclaration operator|(VertexDeclaration const decl1, eVertexDeclaration const decl2)
{
    return decl1 | static_cast<VertexDeclaration>(decl2);
}

enum class eMeshTopology : uint32_t
{
    Points = 0,
    Lines,
    Triangles
};

struct MeshDesc final
{
    VertexDeclaration vertexDeclaration = 0;
    int32_t vertexCount = 0;
    int32_t indexCount = 0;
    eMeshTopology primitiveType = eMeshTopology::Triangles;

    constexpr MeshDesc() = default;

    constexpr MeshDesc(VertexDeclaration vertexDeclaration, int32_t vertexCount)
        : vertexDeclaration(vertexDeclaration), vertexCount(vertexCount)
    {
    }

    constexpr MeshDesc(VertexDeclaration vertexDeclaration, int32_t vertexCount, int32_t indexCount)
        : vertexDeclaration(vertexDeclaration), vertexCount(vertexCount), indexCount(indexCount)
    {
    }

    constexpr bool operator==(MeshDesc const& meshDesc) const
    {
        return (vertexDeclaration == meshDesc.vertexDeclaration) &&
               (vertexCount == meshDesc.vertexCount) && (indexCount == meshDesc.indexCount) &&
               (primitiveType == meshDesc.primitiveType);
    }

    constexpr bool operator!=(MeshDesc const& meshDesc) const { return !(*this == meshDesc); }

    constexpr bool hasTexCoord0() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::TexCoord0);
    }

    constexpr bool hasNormal() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::Normal);
    }

    constexpr bool hasTangent() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::Tangent);
    }

    constexpr bool hasColor() const
    {
        return hasEnumFlag(vertexDeclaration, eVertexDeclaration::Color);
    }

    constexpr int32_t primitiveCount() const
    {
        auto div = 0;
        switch (primitiveType)
        {
        case eMeshTopology::Points: div = 1; break;
        case eMeshTopology::Lines: div = 2; break;
        case eMeshTopology::Triangles: div = 3; break;
        default: return 0;
        }
        return (indexCount > 0) ? indexCount / div : vertexCount / div;
    }
};

// vertex data is organized the following way:
// - position vertex buffer (Vec3) to reduce vertex fetching
// - color vertex buffer (Vec4) because it's very specific (i.e.: mainly used
// for lines)
// - data vertex buffer:
//   * texCoord0 (Vec2)
//   * texCoord1 (Vec2)
//   * normal (uint32_t packed)
//   * tangent (uint32_t packed)
// - custom vertex buffer (Vec4) for custom specific data
class Mesh final
{
  public:
    Mesh() = default;

    Mesh(MeshDesc const& meshDesc) { create(meshDesc); }

    Mesh(Mesh const& mesh) { _copy(mesh); }

    Mesh(Mesh&& mesh) { *this = std::move(mesh); }

    Mesh& operator=(Mesh const& mesh)
    {
        assert(this != &mesh);
        _copy(mesh);
        return *this;
    }

    Mesh& operator=(Mesh&& mesh)
    {
        assert(this != &mesh);
        positions = std::move(mesh.positions);
        colors = std::move(mesh.colors);
        datas = std::move(mesh.datas);
        customs = std::move(mesh.customs);
        indexes = std::move(mesh.indexes);
        wireframeIndexes = std::move(mesh.wireframeIndexes);
        desc = std::move(mesh.desc);
        return *this;
    }

    void create(MeshDesc const& meshDesc)
    {
        desc = meshDesc;
        if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Position) == true)
        {
            positions.resize(desc.vertexCount, Vec3::Zero());
        }
        else
        {
            positions.clear();
        }
        if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Color) == true)
        {
            colors.resize(desc.vertexCount, Vec4::Zero());
        }
        else
        {
            colors.clear();
        }
        if (_hasVertexData(desc.vertexDeclaration) == true)
        {
            datas.resize(desc.vertexCount);
        }
        else
        {
            datas.clear();
        }
        if (desc.indexCount > 0)
        {
            indexes.resize(desc.indexCount);
        }
        else
        {
            indexes.clear();
        }
        wireframeIndexes.clear();
    }

    // set the same color for all vertices
    void setVertexColor(Vec4 const& color)
    {
        desc.vertexDeclaration |= eVertexDeclaration::Color;
        colors.assign(positions.size(), color);
    }

    // only valid for triangles
    void computeTangents()
    {
        assert(desc.primitiveType == eMeshTopology::Triangles);
        auto funcGetIndex = [this](size_t n) { return (desc.indexCount == 0) ? n : indexes[n]; };
        auto count = desc.primitiveCount();
        auto tans = std::vector<Vec3>{};
        tans.resize(desc.vertexCount, Vec3::Zero());
        auto tans2 = std::vector<Vec3>{};
        tans2.resize(desc.vertexCount, Vec3::Zero());

        for (auto n = decltype(count)(0); n < count; ++n)
        {
            auto n0 = n * 3;
            auto n1 = n0 + 1;
            auto n2 = n0 + 2;
            auto i0 = funcGetIndex(n0);
            auto i1 = funcGetIndex(n1);
            auto i2 = funcGetIndex(n2);

            auto& p0 = positions[i0];
            auto& p1 = positions[i1];
            auto& p2 = positions[i2];

            auto& uv0 = datas[i0].texCoord0;
            auto& uv1 = datas[i1].texCoord0;
            auto& uv2 = datas[i2].texCoord0;

            auto x0 = p1.x() - p0.x();
            auto x1 = p2.x() - p0.x();
            auto y0 = p1.y() - p0.y();
            auto y1 = p2.y() - p0.y();
            auto z0 = p1.z() - p0.z();
            auto z1 = p2.z() - p0.z();

            auto s0 = uv1.x() - uv0.x();
            auto s1 = uv2.x() - uv0.x();
            auto t0 = uv1.y() - uv0.y();
            auto t1 = uv2.y() - uv0.y();

            auto r = 1.0f / (s0 * t1 - s1 * t0);
            auto sdir =
                Vec3((t1 * x0 - t0 * x1) * r, (t1 * y0 - t0 * y1) * r, (t1 * z0 - t0 * z1) * r);
            auto tdir =
                Vec3((s0 * x1 - s1 * x0) * r, (s0 * y1 - s1 * y0) * r, (s0 * z1 - s1 * z0) * r);

            tans[i0] += sdir;
            tans[i1] += sdir;
            tans[i2] += sdir;
            tans2[i0] += tdir;
            tans2[i1] += tdir;
            tans2[i2] += tdir;
        }
        auto vtxCount = desc.vertexCount;
        for (auto n = decltype(vtxCount)(0); n < vtxCount; ++n)
        {
            Vec3 nm = datas[n].normal;
            auto t = tans[n];
            auto tan = Vec3{(t - nm * nm.dot(t))};
            tan.normalize();
            datas[n].tangent = tan;
        }
        desc.vertexDeclaration |= eVertexDeclaration::Tangent;
    }

    // only valid for triangles
    void computeWireframe()
    {
        assert(desc.primitiveType == eMeshTopology::Triangles);
        auto funcGetIndex = [this](size_t n) { return (desc.indexCount == 0) ? n : indexes[n]; };
        auto count = desc.primitiveCount();
        wireframeIndexes.resize(count * 6, 0);
        for (auto n = decltype(count)(0); n < count; ++n)
        {
            auto n0 = n * 3;
            auto n1 = n0 + 1;
            auto n2 = n0 + 2;
            auto i0 = funcGetIndex(n0);
            auto i1 = funcGetIndex(n1);
            auto i2 = funcGetIndex(n2);

            auto wn = n * 6;
            wireframeIndexes[wn] = static_cast<uint16_t>(i0);
            wireframeIndexes[wn + 1] = static_cast<uint16_t>(i1);
            wireframeIndexes[wn + 2] = static_cast<uint16_t>(i1);
            wireframeIndexes[wn + 3] = static_cast<uint16_t>(i2);
            wireframeIndexes[wn + 4] = static_cast<uint16_t>(i2);
            wireframeIndexes[wn + 5] = static_cast<uint16_t>(i0);
        }
        if (colors.empty() == true)
        {
            setVertexColor(Vec4::Ones());
        }
    }

    Bound computeBound() const { return Bound().extend(positions); }

    bool isEmpty() const { return positions.size() == 0; }

  public:
    struct VtxData final
    {
        Vec2 texCoord0;
        Vec2 texCoord1;
        PackedNormal normal;
        PackedNormal tangent;
    };

    std::vector<Vec3> positions;
    std::vector<Vec4> colors;
    std::vector<VtxData> datas;
    std::vector<Vec4> customs;
    std::vector<uint16_t> indexes;
    std::vector<uint16_t> wireframeIndexes;
    MeshDesc desc;

  private:
    static bool _hasVertexData(VertexDeclaration const vdecl)
    {
        return hasEnumFlag(vdecl, eVertexDeclaration::TexCoord0) ||
               hasEnumFlag(vdecl, eVertexDeclaration::TexCoord1) ||
               hasEnumFlag(vdecl, eVertexDeclaration::Normal) ||
               hasEnumFlag(vdecl, eVertexDeclaration::Tangent);
    }

    void _copy(Mesh const& mesh)
    {
        positions = mesh.positions;
        colors = mesh.colors;
        datas = mesh.datas;
        customs = mesh.customs;
        indexes = mesh.indexes;
        wireframeIndexes = mesh.wireframeIndexes;
        desc = mesh.desc;
    }
};

} // namespace nopp
