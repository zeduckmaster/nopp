#include "gpu_gl.h"
#include "helpers.h"
#include "log.h"
#include "mesh.h"

using namespace nopp;
using namespace nopp::gl;

namespace
{
    GLint findGLWrapMode(std::string const& str)
    {
        if(str == "GL_REPEAT")
            return GL_REPEAT;
        else if(str == "GL_MIRRORED_REPEAT")
            return GL_MIRRORED_REPEAT;
        else if(str == "GL_CLAMP_TO_EDGE")
            return GL_CLAMP_TO_EDGE;
        else if(str == "GL_MIRROR_CLAMP_TO_EDGE")
            return GL_MIRROR_CLAMP_TO_EDGE;
        logError("findGLWrapMode: invalid value \"%s\"", str.c_str());
        return GL_CLAMP_TO_EDGE;
    }

    GLint findGLFilter(std::string const& str)
    {
        if(str == "GL_NEAREST")
            return GL_NEAREST;
        else if(str == "GL_LINEAR")
            return GL_LINEAR;
        else if(str == "GL_NEAREST_MIPMAP_NEAREST")
            return GL_NEAREST_MIPMAP_NEAREST;
        else if(str == "GL_NEAREST_MIPMAP_LINEAR")
            return GL_NEAREST_MIPMAP_LINEAR;
        else if(str == "GL_LINEAR_MIPMAP_NEAREST")
            return GL_LINEAR_MIPMAP_NEAREST;
        else if(str == "GL_LINEAR_MIPMAP_LINEAR")
            return GL_LINEAR_MIPMAP_LINEAR;
        logError("findGLFilter: invalid value \"%s\"", str.c_str());
        return GL_NEAREST;
    }

    struct SamplerData final
    {
        GLint unit;
        GLint wrapS;
        GLint wrapT;
        GLint wrapR;
        GLint minFilter;
        GLint magFilter;
    };

    SamplerData parseSampler(std::string const& strsampler)
    {
        auto parts = splitString(strsampler, " \t\r\n,()");
        assert(parts.size() == 6);
        auto sampler = SamplerData{};
        sampler.unit = toInt32(parts[0]);
        sampler.wrapS = findGLWrapMode(parts[1]);
        sampler.wrapT = findGLWrapMode(parts[2]);
        sampler.wrapR = findGLWrapMode(parts[3]);
        sampler.minFilter = findGLFilter(parts[4]);
        sampler.magFilter = findGLFilter(parts[5]);
        return sampler;
    }

    struct ShaderData
    {
        GLenum type = 0;
        std::string code;
        std::map<GLint, SamplerData> samplers;

        ShaderData() = default;
        ShaderData(GLenum type)
            : type{type}
        {}

        // process includes, parse samplers and clean code
        void update()
        {
            auto newCode = std::string{};
            std::istringstream iss{code};
            auto codeout = std::string{};
            while(iss)
            {
                auto line = std::string{};
                std::getline(iss, line);
                auto pos = line.find("#pragma");
                if(pos != std::string::npos)
                {
                    auto parts = splitString(line, " \t\r\n");
                    if((pos = line.find("sampler")) != std::string::npos)
                    {
                        auto sampler = parseSampler(line.substr(pos + std::string{"sampler"}.size()));
                        samplers[sampler.unit] = sampler;
                    }
                    else
                    {
                        newCode.append(line);
                        newCode.push_back('\n');
                    }
                }
                else
                {
                    newCode.append(line);
                    newCode.push_back('\n');
                }
            }
            code = newCode;
        }
    };

    struct ProgramData
    {
        std::vector<ShaderData> shaders;
        std::map<HashValue, SamplerData> samplers;
    };

    ProgramData parseProgram(std::string const& strprogram, std::vector<Path> const& includeSearchPaths)
    {
        auto program = ProgramData{};
        std::istringstream iss{strprogram};
        auto finished = false;
        std::string* cur_s{nullptr};
        while((iss) && (finished != true))
        {
            auto line = std::string{};
            std::getline(iss, line);
            auto parts = splitString(line, " \t\r\n");
            if((parts.size() != 0) && (parts[0] == "#pragma"))
            {
                if(parts[1].find("shader_vertex_begin") != std::string::npos)
                {
                    program.shaders.emplace_back(ShaderData{GL_VERTEX_SHADER});
                    cur_s = &program.shaders.back().code;
                    continue;
                }
                else if(parts[1].find("shader_fragment_begin") != std::string::npos)
                {
                    program.shaders.emplace_back(ShaderData{GL_FRAGMENT_SHADER});
                    cur_s = &program.shaders.back().code;
                    continue;
                }
                else if((parts.size() > 2)
                     && (cur_s != nullptr)
                     && (parts[1].find("include") != std::string::npos))
                {
                    auto includeFilepath = findFullFilepath(parts[2], includeSearchPaths);
                    if(includeFilepath.isEmpty() == true)
                    {
                        logError("parseProgram: can't find include file %s", parts[2].c_str());
                        continue;
                    }
                    auto includeStr = readFileContentAsString(includeFilepath);
                    auto includeMsg = std::string{"\n//include: "} + parts[2] + std::string{"\n"};
                    cur_s->append(includeMsg);
                    cur_s->append(includeStr);
                    cur_s->append(includeMsg);
                    continue;
                }
                else if((parts[1].find("shader_vertex_end") != std::string::npos)
                     || (parts[1].find("shader_fragment_end") != std::string::npos))
                {
                    cur_s = nullptr;
                    continue;
                }
            }
            if(cur_s != nullptr)
            {
                cur_s->append(line);
                cur_s->push_back('\n');
            }
        }
        for(auto& shaderParse : program.shaders)
        {
            shaderParse.update();
            program.samplers.insert(shaderParse.samplers.begin(), shaderParse.samplers.end());
        }
        return program;
    }

    void compileAndAttachShader(GL const& gl, GLuint glProgramId, std::string const& shaderCode, GLenum glShaderType, GLProgram::CompileProgramStatus& status)
    {
        auto glShaderId = gl.glCreateShader(glShaderType);
        auto src = shaderCode.c_str();
        gl.glShaderSource(glShaderId, 1, &src, NULL);
        gl.glCompileShader(glShaderId);
        auto compiled = GLint{GL_FALSE};
        gl.glGetShaderiv(glShaderId, GL_COMPILE_STATUS, &compiled);
        if(compiled == GL_FALSE)
        {
            auto infoLen = GLint{0};
            gl.glGetShaderiv(glShaderId, GL_INFO_LOG_LENGTH, &infoLen);
            if(infoLen > 1)
            {
                auto infoLog = new GLchar[infoLen];
                gl.glGetShaderInfoLog(glShaderId, infoLen, NULL, infoLog);
                status.error.append(infoLog);
                status.error.push_back('\n');
                delete[] infoLog;
            }
            gl.glDeleteShader(glShaderId);
            return;
        }
        gl.glAttachShader(glProgramId, glShaderId);
    }

    std::vector<GLProgram::GLVertexAttrib> createAttribs(VertexDeclaration vertexDeclaration)
    {
        auto attribs = std::vector<GLProgram::GLVertexAttrib>{};
        if(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) == true)
        {
            auto va = GLProgram::GLVertexAttrib{};
            va.attribindex = 0;
            va.size = 3;
            va.type = GL_FLOAT;
            va.normalized = GL_FALSE;
            va.relativeoffset = 0;
            va.bindingindex = 0;
            attribs.emplace_back(va);
        }
        if(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Color) == true)
        {
            auto va = GLProgram::GLVertexAttrib{};
            va.attribindex = 1;
            va.size = 4;
            va.type = GL_FLOAT;
            va.normalized = GL_FALSE;
            va.relativeoffset = 0;
            va.bindingindex = 1;
            attribs.emplace_back(va);
        }
        if((hasEnumFlag(vertexDeclaration, eVertexDeclaration::TexCoord0) == true)
        || (hasEnumFlag(vertexDeclaration, eVertexDeclaration::TexCoord1) == true))
        {
            auto va = GLProgram::GLVertexAttrib{};
            va.attribindex = 2;
            va.size = 4;
            va.type = GL_FLOAT;
            va.normalized = GL_FALSE;
            va.relativeoffset = 0;
            va.bindingindex = 2;
            attribs.emplace_back(va);
        }
        if(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Normal) == true)
        {
            auto va = GLProgram::GLVertexAttrib{};
            va.attribindex = 3;
            va.size = 4;
            va.type = GL_UNSIGNED_BYTE;
            va.normalized = GL_TRUE;
            va.relativeoffset = static_cast<GLuint>(sizeof(Vec4));
            va.bindingindex = 2;
            attribs.emplace_back(va);
        }
        if(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Tangent) == true)
        {
            auto va = GLProgram::GLVertexAttrib{};
            va.attribindex = 4;
            va.size = 4;
            va.type = GL_UNSIGNED_BYTE;
            va.normalized = GL_TRUE;
            va.relativeoffset = static_cast<GLuint>(sizeof(Vec4)+sizeof(uint32_t));
            va.bindingindex = 2;
            attribs.emplace_back(va);
        }
        return attribs;
    }

    GLProgram::GLVAO createVAO(GL const& gl, std::vector<GLProgram::GLVertexAttrib> const& attribs)
    {
        auto vao = GLProgram::GLVAO{};
        gl.glGenVertexArrays(1, &vao.glId);
        gl.glBindVertexArray(vao.glId);
        for(auto& va : attribs)
        {
            gl.glVertexAttribFormat(va.attribindex, va.size, va.type, va.normalized, va.relativeoffset);
            gl.glVertexAttribBinding(va.attribindex, va.bindingindex);
            gl.glEnableVertexAttribArray(va.attribindex);
            vao.bindings.insert(va.bindingindex);
        }
        gl.glBindVertexArray(0);
        return vao;
    }
}

GLProgram::GLProgram(GL const& gl)
    : _gl{gl}
{
}

GLProgram::GLProgram(GL const& gl, std::string const& code, std::vector<Path> const& includeSearchDirpaths)
    : GLProgram{gl}
{
    glSetCode(code, includeSearchDirpaths);
}

GLProgram::GLProgram(GL const& gl, Path const& filepath)
    : GLProgram{gl}
{
    auto code = readFileContentAsString(filepath);
    if(code.empty() == true)
        return;
    glSetCode(code, {filepath.parentPath()});
}

GLProgram::~GLProgram()
{
    if(_glSamplers.size() != 0)
        _gl.glDeleteSamplers(static_cast<GLsizei>(_glSamplers.size()), _glSamplers.data());
    if(_glVAO.glId != 0)
        _gl.glDeleteVertexArrays(1, &_glVAO.glId);
    if(_glId != 0)
        _gl.glDeleteProgram(_glId);
}

void GLProgram::glSetCode(std::string const& code, std::vector<Path> const& includeSearchDirpaths)
{
    status = CompileProgramStatus{};
    _glId = _gl.glCreateProgram();
    auto pp = parseProgram(code, includeSearchDirpaths);
    status.source = code;
    for(auto& shader : pp.shaders)
    {
        compileAndAttachShader(_gl, _glId, shader.code, shader.type, status);
        if(status.error.empty() == false)
        {
            logError("GLProgram::glCompile: %s", status.error.c_str());
            return;
        }
    }

    // link program
    _gl.glLinkProgram(_glId);
    auto linked = GLint{GL_FALSE};
    _gl.glGetProgramiv(_glId, GL_LINK_STATUS, &linked);
    if(linked == GL_FALSE)
    {
        auto infoLen = GLint{0};
        _gl.glGetProgramiv(_glId, GL_INFO_LOG_LENGTH, &infoLen);
        if(infoLen > 1)
        {
            auto infoLog = new GLchar[infoLen];
            _gl.glGetProgramInfoLog(_glId, infoLen, nullptr, infoLog);
            status.error.append(infoLog);
            status.error.push_back('\n');
            logError("GLProgram::glCompile: %s", infoLog);
            delete[] infoLog;
        }
        _gl.glDeleteProgram(_glId);
        return;
    }

    status.compiled = true;
    {
        auto length = GLint{0};
        _gl.glGetProgramiv(_glId, GL_PROGRAM_BINARY_LENGTH, &length);
        status.binary.resize(length, 0);
        _gl.glGetProgramBinary(_glId, static_cast<GLsizei>(status.binary.size()), &length, &status.binaryFormat, status.binary.data());
    }

    // get input and create vao
    //if(attribs.size() == 0)
    {
        auto maxLength = GLint{0};
        _gl.glGetProgramInterfaceiv(_glId, GL_PROGRAM_INPUT, GL_MAX_NAME_LENGTH, &maxLength);
        auto buf = std::string{};
        buf.resize(maxLength);
        auto nb = GLint{0};
        _gl.glGetProgramInterfaceiv(_glId, GL_PROGRAM_INPUT, GL_ACTIVE_RESOURCES, &nb);
        auto vertexDeclaration = VertexDeclaration{0};
        for(auto n = 0; n < nb; ++n)
        {
            auto length = GLsizei{0};
            auto ptr = &buf[0];
            _gl.glGetProgramResourceName(_glId, GL_PROGRAM_INPUT, n, maxLength, &length, ptr);
            if(buf.find("inPosition") != std::string::npos)
                vertexDeclaration |= eVertexDeclaration::Position;
            else if(buf.find("inColor") != std::string::npos)
                vertexDeclaration |= eVertexDeclaration::Color;
            else if(buf.find("inTexCoord01") != std::string::npos)
                vertexDeclaration |= eVertexDeclaration::TexCoord0 | eVertexDeclaration::TexCoord1;
            else if(buf.find("inNormal") != std::string::npos)
                vertexDeclaration |= eVertexDeclaration::Normal;
            else if(buf.find("inTangent") != std::string::npos)
                vertexDeclaration |= eVertexDeclaration::Tangent;
        }
        _glVAO = createVAO(_gl, createAttribs(vertexDeclaration));
    }
    //else
    //_glVAO = createVAO(gl, attribs);

    // create samplers
    {
        auto maxIndex = 0;
        for(auto& sampler : pp.samplers)
        {
            if(maxIndex < sampler.first)
                maxIndex = sampler.first;
        }
        _glSamplers.resize(maxIndex + 1, 0);
        for(auto& iter : pp.samplers)
        {
            auto& sampler = iter.second;
            auto& glid = _glSamplers[sampler.unit];
            _gl.glGenSamplers(1, &glid);
            _gl.glSamplerParameteri(glid, GL_TEXTURE_WRAP_R, sampler.wrapR);
            _gl.glSamplerParameteri(glid, GL_TEXTURE_WRAP_S, sampler.wrapS);
            _gl.glSamplerParameteri(glid, GL_TEXTURE_WRAP_T, sampler.wrapT);
            _gl.glSamplerParameteri(glid, GL_TEXTURE_MIN_FILTER, sampler.minFilter);
            _gl.glSamplerParameteri(glid, GL_TEXTURE_MAG_FILTER, sampler.magFilter);
        }
    }

    // get uniform blocks
    {
        _glUBlockLocations.clear();
        auto maxLength = GLint{0};
        _gl.glGetProgramInterfaceiv(_glId, GL_UNIFORM_BLOCK, GL_MAX_NAME_LENGTH, &maxLength);
        auto buf = std::string{};
        buf.resize(maxLength);
        auto nb = GLint{0};
        _gl.glGetProgramInterfaceiv(_glId, GL_UNIFORM_BLOCK, GL_ACTIVE_RESOURCES, &nb);
        for(auto n = 0; n < nb; ++n)
        {
            auto length = GLsizei{0};
            auto ptr = &buf[0];
            _gl.glGetProgramResourceName(_glId, GL_UNIFORM_BLOCK, n, maxLength, &length, ptr);
            auto loc = _gl.glGetUniformBlockIndex(_glId, ptr);
            auto hashname = computeHash(buf);
            _glUBlockLocations[hashname] = loc;
        }
    }
}


