#pragma once

#include <algorithm>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#if defined(_MSC_VER)
#define snprintf _snprintf
#endif

namespace nopp
{

inline std::vector<std::string> splitString(std::string const& value, std::string const& delimiters)
{
    auto result = std::vector<std::string>();
    auto current = 0;
    size_t next = -1;
    do
    {
        next = value.find_first_not_of(delimiters, next + 1);
        if (next == std::string::npos)
        {
            break;
        }
        current = next;
        next = value.find_first_of(delimiters, current);
        result.emplace_back(value.substr(current, next - current));
    } while (next != std::string::npos);
    return result;
}

inline std::vector<std::string> splitString(std::string const& value, std::regex const& expr)
{
    auto result = std::vector<std::string>();
    auto iter = std::sregex_token_iterator(value.begin(), value.end(), expr, -1);
    auto end = std::sregex_token_iterator();
    while (iter != end)
    {
        result.emplace_back(*iter);
        ++iter;
    }
    return result;
}

inline std::string joinStrings(std::vector<std::string> const& value, std::string const& delimiter)
{
    auto all = std::string();
    for (auto& str : value)
    {
        if (all.empty() == false)
        {
            all = all + delimiter;
        }
        all = all + str;
    }
    return all;
}

inline std::string toLower(std::string const& str)
{
    auto tmp = str;
    std::transform(tmp.begin(), tmp.end(), tmp.begin(), tolower);
    return tmp;
}

inline std::string toUpper(std::string const& str)
{
    auto tmp = str;
    std::transform(tmp.begin(), tmp.end(), tmp.begin(), toupper);
    return tmp;
}

inline std::vector<std::string> tokenizeString(std::string const& value,
                                               std::string const& operators)
{
    auto result = std::vector<std::string>();
    auto current = size_t(0);
    auto next = size_t(-1);
    do
    {
        next = value.find_first_of(operators, next + 1);
        if (next == std::string::npos)
        {
            next = value.size();
        }
        if (next > current)
        {
            result.emplace_back(value.substr(current, next - current));
        }
        if (next == value.size())
        {
            break;
        }
        result.emplace_back(value.substr(next, 1));
        current = next + 1;
    } while (next != std::string::npos);
    return result;
}

inline std::vector<std::string> tokenizeString(std::string const& value,
                                               std::vector<std::string> const& operators)
{
    struct Found
    {
        size_t pos = std::string::npos;
        std::string match;
    };
    auto findFirstOfAlt = [](std::string const& value,
                             std::vector<std::string> const& matchs,
                             size_t const pos) -> Found
    {
        auto ret = Found();
        for (auto& match : matchs)
        {
            auto p = value.find(match, pos);
            if (p < ret.pos)
            {
                ret.pos = p;
                ret.match = match;
            }
        }
        return ret;
    };

    auto result = std::vector<std::string>();
    auto current = size_t(0);
    auto next = Found();
    next.pos = 0;
    // next.match = " ";
    do
    {
        next = findFirstOfAlt(value, operators, next.pos + next.match.size());
        if (next.pos == std::string::npos)
        {
            next.pos = value.size();
        }
        if (next.pos > current)
        {
            result.emplace_back(value.substr(current, next.pos - current));
        }
        if (next.pos == value.size())
        {
            break;
        }
        result.emplace_back(value.substr(next.pos, next.match.size()));
        current = next.pos + next.match.size();
    } while (next.pos != std::string::npos);
    return result;
}

inline std::string toString(bool value) { return (value) ? "true" : "false"; }

inline std::string toString(int32_t value)
{
    char buf[64];
    snprintf(buf, 64, "%d", value);
    return std::string(buf);
}

inline std::string toString(uint32_t value)
{
    auto s = std::string();
    s.reserve(10);
    if (value < 10)
    {
        s.push_back('0' + (value % 10));
    }
    else if (value < 100)
    {
        s.push_back('0' + (value / 10));
        s.push_back('0' + (value % 10));
    }
    else if (value < 1000)
    {
        s.push_back('0' + (value / 100));
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    else if (value < 10000)
    {
        s.push_back('0' + (value / 1000));
        s.push_back('0' + (value % 1000) / 100);
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    else if (value < 100000)
    {
        s.push_back('0' + (value / 10000));
        s.push_back('0' + (value % 10000) / 1000);
        s.push_back('0' + (value % 1000) / 100);
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    else if (value < 1000000)
    {
        s.push_back('0' + (value / 100000));
        s.push_back('0' + (value % 100000) / 10000);
        s.push_back('0' + (value % 10000) / 1000);
        s.push_back('0' + (value % 1000) / 100);
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    else if (value < 10000000)
    {
        s.push_back('0' + (value / 1000000));
        s.push_back('0' + (value % 1000000) / 100000);
        s.push_back('0' + (value % 100000) / 10000);
        s.push_back('0' + (value % 10000) / 1000);
        s.push_back('0' + (value % 1000) / 100);
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    else if (value < 100000000)
    {
        s.push_back('0' + (value / 10000000));
        s.push_back('0' + (value % 10000000) / 1000000);
        s.push_back('0' + (value % 1000000) / 100000);
        s.push_back('0' + (value % 100000) / 10000);
        s.push_back('0' + (value % 10000) / 1000);
        s.push_back('0' + (value % 1000) / 100);
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    else if (value < 1000000000)
    {
        s.push_back('0' + (value / 100000000));
        s.push_back('0' + (value % 100000000) / 10000000);
        s.push_back('0' + (value % 10000000) / 1000000);
        s.push_back('0' + (value % 1000000) / 100000);
        s.push_back('0' + (value % 100000) / 10000);
        s.push_back('0' + (value % 10000) / 1000);
        s.push_back('0' + (value % 1000) / 100);
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    else
    {
        s.push_back('0' + (value / 1000000000));
        s.push_back('0' + (value % 1000000000) / 100000000);
        s.push_back('0' + (value % 100000000) / 10000000);
        s.push_back('0' + (value % 10000000) / 1000000);
        s.push_back('0' + (value % 1000000) / 100000);
        s.push_back('0' + (value % 100000) / 10000);
        s.push_back('0' + (value % 10000) / 1000);
        s.push_back('0' + (value % 1000) / 100);
        s.push_back('0' + (value % 100) / 10);
        s.push_back('0' + (value % 10));
    }
    return s;
}

inline std::string toString(int64_t value)
{
    char buf[64];
    snprintf(buf, 64, "%lld", static_cast<long long int>(value));
    return std::string(buf);
}

inline std::string toString(float value)
{
    // char buf[64];
    // snprintf(buf, 64, "%f", value);
    // return std::string{buf};
    std::ostringstream sstream;
    sstream << value;
    return sstream.str();
}

inline std::string toString(double value)
{
    std::ostringstream sstream;
    sstream << value;
    return sstream.str();
}

inline bool toBool(std::string const& value)
{
    auto str = value;
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    return (str.find("true") != std::string::npos);
}

inline int32_t toInt32(std::string const& value, size_t* idx = nullptr, int base = 10)
{
    char* eptr = nullptr;
    errno = 0;
    auto ptr = value.c_str();
    auto ans = strtol(ptr, &eptr, base);
    if (ptr == eptr)
    {
        // logError("toInt32: invalid argument");
    }
    if (errno == ERANGE)
    {
        // logError("toInt32: argument out of range");
    }
    if (idx != nullptr)
    {
        *idx = static_cast<size_t>(eptr - ptr);
    }
    return static_cast<int32_t>(ans);
}

inline uint32_t toUInt32(std::string const& value, size_t* idx = nullptr, int base = 10)
{
    char* eptr = nullptr;
    errno = 0;
    auto ptr = value.c_str();
    auto ans = strtoul(ptr, &eptr, base);
    if (ptr == eptr)
    {
        // logError("toUInt32: invalid argument");
    }
    if (errno == ERANGE)
    {
        // logError("toUInt32: argument out of range");
    }
    if (idx != nullptr)
    {
        *idx = static_cast<size_t>(eptr - ptr);
    }
    return static_cast<uint32_t>(ans);
}

inline int64_t toInt64(std::string const& value, size_t* idx = nullptr, int base = 10)
{
    char* eptr = nullptr;
    errno = 0;
    auto ptr = value.c_str();
    auto ans = strtoll(ptr, &eptr, base);
    if (ptr == eptr)
    {
        // logError("toInt64: invalid argument");
    }
    if (errno == ERANGE)
    {
        // logError("toInt64: argument out of range");
    }
    if (idx != 0)
    {
        *idx = static_cast<size_t>(eptr - ptr);
    }
    return static_cast<int64_t>(ans);
}

inline double toDouble(std::string const& value, size_t* idx = nullptr)
{
    char* eptr = nullptr;
    errno = 0;
    auto ptr = value.c_str();
    auto ans = strtod(ptr, &eptr);
    if (ptr == eptr)
    {
        // logError("toDouble: invalid argument");
    }
    if (errno == ERANGE)
    {
        // logError("toDouble: argument out of range");
    }
    if (idx != 0)
    {
        *idx = static_cast<size_t>(eptr - ptr);
    }
    return ans;
}

inline float toFloat(std::string const& value, size_t* idx = nullptr)
{
    return static_cast<float>(toDouble(value, idx));
}

// inline std::string toString(std::wstring const& value)
//{
// return std::string{value.begin(), value.end()};
//}

inline size_t
findFirstOf(std::string const& value, std::vector<std::string> const& matchs, size_t pos)
{
    auto ret = std::string::npos;
    for (auto& match : matchs)
    {
        auto p = value.find(match, pos);
        if (p < ret)
        {
            ret = p;
        }
    }
    return ret;
}

inline size_t findFirstOf(std::string const& value, std::vector<std::string> const& matchs)
{
    return findFirstOf(value, matchs, 0);
}

template <typename UnaryFunction>
std::string regexReplace(std::string const& value, std::regex const& regex, UnaryFunction function)
{
    auto str = std::string();
    auto positionOfLastMatch = std::smatch::difference_type(0);
    auto endOfLastMatch = value.cbegin();
    auto callback = [&](std::smatch const& match)
    {
        auto positionOfThisMatch = match.position(0);
        auto diff = positionOfThisMatch - positionOfLastMatch;
        auto startOfThisMatch = endOfLastMatch;
        std::advance(startOfThisMatch, diff);
        str.append(endOfLastMatch, startOfThisMatch);
        str.append(function(match));
        auto lengthOfMatch = match.length(0);
        positionOfLastMatch = positionOfThisMatch + lengthOfMatch;
        endOfLastMatch = startOfThisMatch;
        std::advance(endOfLastMatch, lengthOfMatch);
    };

    auto begin = std::sregex_iterator(value.cbegin(), value.cend(), regex);
    auto end = std::sregex_iterator();
    std::for_each(begin, end, callback);
    str.append(endOfLastMatch, value.cend());
    return str;
}

} // namespace nopp
