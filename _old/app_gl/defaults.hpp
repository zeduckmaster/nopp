#pragma once

#include <lib_display/api.hpp>

void glInitializeDefault(nopp::gles2::GL const& gl);

void glFrameDefault(nopp::gles2::GL const& gl);

void glTerminateDefault(nopp::gles2::GL const& gl);

void glImGuiDrawDefault(nopp::gles2::GL const& gl);

void worldInitializeDefault();

void worldUpdateDefault();

void worldTerminateDefault();

void eventMousePressDefault();

void eventMouseReleaseDefault();

void eventMouseMoveDefault();

void eventKeyPressDefault();

void eventKeyReleaseDefault();

void eventWindowResize();
