#pragma once

#include "frustum.hpp"
#include "math.hpp"
#include "transform.hpp"
#include "viewport.hpp"

namespace nopp
{

// Handles a camera editor type:
//
class CameraEditor
{
  public:
    enum class eViewPreset : uint32_t
    {
        Front = 0,
        Top,
        Right
    };

    CameraEditor()
    {
        // default camera
        _frustum = Frustum(-1.0f, 1.0f, -1.0f, 1.0f, 0.1f, 100.0f, Frustum::eType::Orthographic);
        setPerspFOVV(_fovv, 1.33f, _frustum.znear, _frustum.zfar);
        lookAt(_pos, _at, _up);
        _update();
    }

    void setViewPreset(eViewPreset viewPreset)
    {
        switch (viewPreset)
        {
        case eViewPreset::Front:
            _longitudinal = 0.0f;
            _lateral = 0.0f;
            break;
        case eViewPreset::Top:
            _longitudinal = -HalfPi;
            _lateral = 0;
            break;
        case eViewPreset::Right:
            _longitudinal = 0.0f;
            _lateral = HalfPi;
            break;
        }
        _update();
    }

    void fitView(Vec3 const& min, Vec3 const& max)
    {
        auto radius = (max - min).norm() * 0.5f;
        if (radius == 0.0f)
        {
            return;
        }
        auto center = Vec3((max + min) * 0.5f);
        // logDebug("center(%.2f, %.2f, %.2f) radius(%.2f)", center.x(), center.y(),
        // center.z(), radius);
        auto matView = computeMatView(_transform.matrix());
        auto vcenter = Vec4(matView * Vec4(center.x(), center.y(), center.z(), 1.0f));
        _at += (_right * vcenter.x()) + (_up * vcenter.y());
        _targetOffset = radius / std::tan(_fovv * 0.5f);
        if (_frustum.type == Frustum::eType::Orthographic)
        {
            auto aspectRatio = _frustum.aspectRatio();
            setOrtho(-radius * aspectRatio,
                     radius * aspectRatio,
                     -radius,
                     radius,
                     _frustum.znear,
                     _frustum.zfar);
        }
        _update();
    }

    void setProjType(Frustum::eType type)
    {
        if (_frustum.type == type)
        {
            return;
        }
        auto aspectRatio = _frustum.aspectRatio();
        switch (type)
        {
        case Frustum::eType::Perspective:
            setPerspFOVV(_fovv, aspectRatio, _frustum.znear, _frustum.zfar);
            break;
        case Frustum::eType::Orthographic:
        {
            auto d = (_at - _pos).norm() * tanf(_fovv * 0.5f);
            setOrtho(-d * aspectRatio, d * aspectRatio, -d, d, _frustum.znear, _frustum.zfar);
        }
        break;
        }
    }

    void setPerspFOVV(float fovvRad, float aspectRatio, float znear, float zfar)
    {
        _fovv = fovvRad;
        _frustum = computeProjPerspFOVV(_fovv, aspectRatio, znear, zfar);
        _matProj = computeMatProj(_frustum);
    }

    void setOrtho(float left, float right, float bottom, float top, float znear, float zfar)
    {
        _frustum = Frustum(left, right, bottom, top, znear, zfar, Frustum::eType::Orthographic);
        _matProj = computeMatProj(_frustum);
    }

    void setAspectRatio(float aspectRatio)
    {
        if (_frustum.type == Frustum::eType::Orthographic)
        {
            setOrtho(-_frustum.top * aspectRatio,
                     _frustum.top * aspectRatio,
                     -_frustum.top,
                     _frustum.top,
                     _frustum.znear,
                     _frustum.zfar);
        }
        else
        {
            _frustum = computeProjPerspFOVV(_fovv, aspectRatio, _frustum.znear, _frustum.zfar);
        }
        _matProj = computeMatProj(_frustum);
    }

    // updates also aspect ratio
    void setViewport(Viewport const& viewport)
    {
        _viewport = viewport;
        setAspectRatio(_viewport.width / _viewport.height);
    }

    void lookAt(Vec3 const& pos, Vec3 const& at, Vec3 const& up)
    {
        _at = at;
        _pos = pos;
        _up = up;
        _targetOffset = (_pos - _at).norm();
        _transform = computeViewLookAt(_pos, _at, _up);
        _matView = computeMatView(_transform.matrix());
    }

    void zoom(float step)
    {
        auto dir = Vec3(_pos - _at);
        auto dn = dir.norm();
        auto l = dn - (step * dn);
        if (l < _frustum.znear)
        {
            l = _frustum.znear;
        }
        else if (l > _frustum.zfar)
        {
            l = _frustum.zfar;
        }
        dir.normalize();
        _pos = (dir * l) + _at;
        lookAt(_pos, _at, _up);
        if (_frustum.type == Frustum::eType::Orthographic)
        {
            auto aspectratio = _frustum.aspectRatio();
            auto d = (_at - _pos).norm() * std::tan(_fovv * 0.5f);
            setOrtho(-d * aspectratio, d * aspectratio, -d, d, _frustum.znear, _frustum.zfar);
        }
    }

    void pan(Vec2 const& step)
    {
        _at += (_right * step.x()) - (_up * step.y());
        _update();
    }

    void rotate(float lateral, float longitudinal)
    {
        _lateral = lateral;
        _longitudinal = longitudinal;
        _update();
    }

    Vec3 const& pos() const { return _pos; }

    Vec3 const& at() const { return _at; }

    Vec3 const& up() const { return _up; }

    constexpr float lateral() const { return _lateral; }

    constexpr float longitudinal() const { return _longitudinal; }

    constexpr Frustum::eType projType() const { return _frustum.type; }

    Transform const& transform() const { return _transform; }

    Frustum const& frustum() const { return _frustum; }

    Mat4 const& matView() const { return _matView; }

    Mat4 const& matProj() const { return _matProj; }

    Viewport const& viewport() const { return _viewport; }

  private:
    static void _updateOrbital(Vec3& pos,
                               Vec3& up,
                               Vec3& right,
                               Vec3 const& at,
                               float targetOffset,
                               float lateral,
                               float longitudinal)
    {
        auto q = Quat(Eigen::AngleAxisf(lateral, Vec3::UnitZ()) *
                      Eigen::AngleAxisf(longitudinal, Vec3::UnitX()));
        auto rot = q.toRotationMatrix();
        up = rot * Vec3::UnitZ();
        up.normalize();
        right = rot * Vec3::UnitX();
        right.normalize();
        auto dir = Vec3(rot * Vec3(0.0f, 1.0, 0.0f));
        dir.normalize();
        pos = at - dir * targetOffset;
    }

    void _update()
    {
        _updateOrbital(_pos, _up, _right, _at, _targetOffset, _lateral, _longitudinal);
        lookAt(_pos, _at, _up);
    }

    Mat4 _matView;
    Mat4 _matProj;
    Frustum _frustum;
    Viewport _viewport;
    Transform _transform;
    float _fovv = ToRadian * 55.0f; // in radians
    Vec3 _pos = Vec3(0.0f, -2.0f, 0.0f);
    Vec3 _at = Vec3(0.0f, 0.0f, 0.0f);
    Vec3 _up = Vec3(0.0f, 0.0f, 1.0f);
    Vec3 _right = Vec3(1.0f, 0.0f, 0.0f);
    float _lateral = 0.0f;
    float _longitudinal = 0.0f;
    float _targetOffset = 2.0f; // how far from target
};

} // namespace nopp
