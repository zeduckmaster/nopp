#include "dice.hpp"
#include "helpers.hpp"
#include "math.hpp"
#include <ctime>
#include <random>
#include <regex>

using namespace nopp;

namespace
{
    std::mt19937 rng(time(nullptr));
}

int32_t nopp::rollDice(int32_t faceCount)
{
    std::uniform_int_distribution<int32_t> distribution(1, faceCount);
    return distribution(rng);
}

int32_t nopp::roll(std::string const& expression)
{
    // convert dices to values
    auto expr = regexReplace(expression, std::regex{"[0-9]*d[0-9]+", std::regex::icase|std::regex::ECMAScript}, [=](std::smatch const& value)
    {
        auto strs = splitString(value.str(), "d");
        auto mult = 1;
        auto dice = 6;
        if(strs.size() > 1)
        {
            mult = toInt32(strs[0]);
            dice = toInt32(strs[1]);
        }
        else
            dice = toInt32(strs[0]);
        auto ret = std::string{"("};
        for(auto n = 0; n < mult; ++n)
        {
            ret.append(toString(rollDice(dice)));
            if(n != (mult - 1))
                ret.push_back('+');
        }
        ret.push_back(')');
        return ret;
    });
    // evaluate expression
    return evaluate(expr);
}
