#include "mesh.hpp"

using namespace nopp;

static_assert(sizeof(PackedNormal) == sizeof(uint32_t), "");

PackedNormal::PackedNormal(Vec3 const& normal) { *this = normal; }

PackedNormal::PackedNormal(float nx, float ny, float nz)
    : PackedNormal{Vec3{nx, ny, nz}}
{
}

PackedNormal& PackedNormal::operator=(Vec3 const& normal)
{
    v.x = clamp<uint8_t>(
        static_cast<uint8_t>(normal.x() * 127.5f + 127.5f), 0, 255);
    v.y = clamp<uint8_t>(
        static_cast<uint8_t>(normal.y() * 127.5f + 127.5f), 0, 255);
    v.z = clamp<uint8_t>(
        static_cast<uint8_t>(normal.z() * 127.5f + 127.5f), 0, 255);
    return *this;
}

PackedNormal::operator Vec3() const
{
    auto f = 1.0f / 127.5f;
    return Vec3{(static_cast<float>(v.x) - 127.5f) * f,
                (static_cast<float>(v.y) - 127.5f) * f,
                (static_cast<float>(v.z) - 127.5f) * f};
}

////////////////////////////////////////////////////////////////////////////////

MeshDesc::MeshDesc(VertexDeclaration vertexDeclaration, int32_t vertexCount)
    : vertexDeclaration{vertexDeclaration}, vertexCount{vertexCount}
{
}

MeshDesc::MeshDesc(VertexDeclaration vertexDeclaration,
                   int32_t vertexCount,
                   int32_t indexCount)
    : vertexDeclaration{vertexDeclaration}, vertexCount{vertexCount},
      indexCount{indexCount}
{
}

bool MeshDesc::operator==(MeshDesc const& meshDesc) const
{
    return (vertexDeclaration == meshDesc.vertexDeclaration) &&
           (vertexCount == meshDesc.vertexCount) &&
           (indexCount == meshDesc.indexCount) &&
           (primitiveType == meshDesc.primitiveType);
}

int32_t MeshDesc::primitiveCount() const
{
    auto div = 0;
    switch (primitiveType)
    {
    case eMeshTopology::Points: div = 1; break;
    case eMeshTopology::Lines: div = 2; break;
    case eMeshTopology::Triangles: div = 3; break;
    default: return 0;
    }
    return (indexCount > 0) ? indexCount / div : vertexCount / div;
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
bool hasVertexData(VertexDeclaration vdecl)
{
    return hasEnumFlag(vdecl, eVertexDeclaration::TexCoord0) ||
           hasEnumFlag(vdecl, eVertexDeclaration::TexCoord1) ||
           hasEnumFlag(vdecl, eVertexDeclaration::Normal) ||
           hasEnumFlag(vdecl, eVertexDeclaration::Tangent);
}
} // namespace

MeshData::MeshData(MeshDesc const& meshDesc) { create(meshDesc); }

MeshData::MeshData(Path const& filepath) { create(filepath); }

MeshData::MeshData(MeshData const& meshData) { _copy(meshData); }

MeshData::MeshData(MeshData&& meshData) { *this = std::move(meshData); }

// MeshData::MeshData(MeshData&& meshData)
//{
//    positions = std::move(meshData.positions);
//    colors = std::move(meshData.colors);
//    datas = std::move(meshData.datas);
//    customs = std::move(meshData.customs);
//    indexes = std::move(meshData.indexes);
//    wireframeIndexes = std::move(meshData.wireframeIndexes);
//    desc = std::move(meshData.desc);
//    _id
//}

MeshData& MeshData::operator=(MeshData const& meshData)
{
    assert(this != &meshData);
    _copy(meshData);
    return *this;
}

MeshData& MeshData::operator=(MeshData&& meshData)
{
    assert(this != &meshData);
    GPUData::operator=(std::move(meshData));
    positions = std::move(meshData.positions);
    colors = std::move(meshData.colors);
    datas = std::move(meshData.datas);
    customs = std::move(meshData.customs);
    indexes = std::move(meshData.indexes);
    wireframeIndexes = std::move(meshData.wireframeIndexes);
    desc = std::move(meshData.desc);
    return *this;
}

void MeshData::create(MeshDesc const& meshDesc)
{
    desc = meshDesc;
    if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Position) ==
        true)
        positions.resize(desc.vertexCount, Vec3::Zero());
    else
        positions.clear();

    if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Color) == true)
        colors.resize(desc.vertexCount, Vec4::Zero());
    else
        colors.clear();

    if (hasVertexData(desc.vertexDeclaration) == true)
        datas.resize(desc.vertexCount);
    else
        datas.clear();

    if (desc.indexCount > 0)
        indexes.resize(desc.indexCount);
    else
        indexes.clear();

    wireframeIndexes.clear();
}

void MeshData::create(Path const& filepath)
{
    auto ext = filepath.extension();
    if (ext.string() == "obj")
        loadOBJ(*this, filepath);
}

void MeshData::setVertexColor(Vec4 const& color)
{
    desc.vertexDeclaration |= eVertexDeclaration::Color;
    colors.assign(positions.size(), color);
}

void MeshData::computeTangents()
{
    assert(desc.primitiveType == eMeshTopology::Triangles);
    auto funcGetIndex = [this](size_t n) {
        return (desc.indexCount == 0) ? n : indexes[n];
    };
    auto count = desc.primitiveCount();
    auto tans = std::vector<Vec3>{};
    tans.resize(desc.vertexCount, Vec3::Zero());
    auto tans2 = std::vector<Vec3>{};
    tans2.resize(desc.vertexCount, Vec3::Zero());

    for (auto n = decltype(count){0}; n < count; ++n)
    {
        auto n0 = n * 3;
        auto n1 = n0 + 1;
        auto n2 = n0 + 2;
        auto i0 = funcGetIndex(n0);
        auto i1 = funcGetIndex(n1);
        auto i2 = funcGetIndex(n2);

        auto& p0 = positions[i0];
        auto& p1 = positions[i1];
        auto& p2 = positions[i2];

        auto& uv0 = datas[i0].texCoord0;
        auto& uv1 = datas[i1].texCoord0;
        auto& uv2 = datas[i2].texCoord0;

        auto x0 = p1.x() - p0.x();
        auto x1 = p2.x() - p0.x();
        auto y0 = p1.y() - p0.y();
        auto y1 = p2.y() - p0.y();
        auto z0 = p1.z() - p0.z();
        auto z1 = p2.z() - p0.z();

        auto s0 = uv1.x() - uv0.x();
        auto s1 = uv2.x() - uv0.x();
        auto t0 = uv1.y() - uv0.y();
        auto t1 = uv2.y() - uv0.y();

        auto r = 1.0f / (s0 * t1 - s1 * t0);
        auto sdir = Vec3{(t1 * x0 - t0 * x1) * r,
                         (t1 * y0 - t0 * y1) * r,
                         (t1 * z0 - t0 * z1) * r};
        auto tdir = Vec3{(s0 * x1 - s1 * x0) * r,
                         (s0 * y1 - s1 * y0) * r,
                         (s0 * z1 - s1 * z0) * r};

        tans[i0] += sdir;
        tans[i1] += sdir;
        tans[i2] += sdir;

        tans2[i0] += tdir;
        tans2[i1] += tdir;
        tans2[i2] += tdir;
    }
    auto vtxCount = desc.vertexCount;
    for (auto n = decltype(vtxCount){0}; n < vtxCount; ++n)
    {
        Vec3 nm = datas[n].normal;
        auto t = tans[n];

        auto tan = Vec3{(t - nm * nm.dot(t))};
        tan.normalize();
        datas[n].tangent = tan;
    }
    desc.vertexDeclaration |= eVertexDeclaration::Tangent;
}

void MeshData::computeWireframe()
{
    assert(desc.primitiveType == eMeshTopology::Triangles);
    auto funcGetIndex = [this](size_t n) {
        return (desc.indexCount == 0) ? n : indexes[n];
    };
    auto count = desc.primitiveCount();
    wireframeIndexes.resize(count * 6, 0);
    for (auto n = decltype(count){0}; n < count; ++n)
    {
        auto n0 = n * 3;
        auto n1 = n0 + 1;
        auto n2 = n0 + 2;
        auto i0 = funcGetIndex(n0);
        auto i1 = funcGetIndex(n1);
        auto i2 = funcGetIndex(n2);

        auto wn = n * 6;
        wireframeIndexes[wn] = static_cast<uint16_t>(i0);
        wireframeIndexes[wn + 1] = static_cast<uint16_t>(i1);
        wireframeIndexes[wn + 2] = static_cast<uint16_t>(i1);
        wireframeIndexes[wn + 3] = static_cast<uint16_t>(i2);
        wireframeIndexes[wn + 4] = static_cast<uint16_t>(i2);
        wireframeIndexes[wn + 5] = static_cast<uint16_t>(i0);
    }
    if (colors.empty() == true)
        setVertexColor(Vec4::Ones());
}

void MeshData::_copy(MeshData const& meshData)
{
    positions = meshData.positions;
    colors = meshData.colors;
    datas = meshData.datas;
    customs = meshData.customs;
    indexes = meshData.indexes;
    wireframeIndexes = meshData.wireframeIndexes;
    desc = meshData.desc;
}
