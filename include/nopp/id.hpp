#pragma once

#include <cassert>
#include <cstdint>
#include <deque>
#include <vector>

namespace nopp
{

// allows 16 777 216 different ids at the same time
class Id final
{
  public:
    constexpr Id() = default;

    constexpr explicit Id(uint32_t value) : _value(value) {}

    constexpr uint32_t value() const { return _value; }

    constexpr int32_t index() const { return static_cast<int32_t>(_value & _IndexMask); }

    constexpr uint8_t gen() const
    {
        return static_cast<uint8_t>((_value >> _IndexBits) & _GenMask);
    }

    constexpr bool operator==(Id const& id) const { return _value == id._value; }

    constexpr bool operator!=(Id const& id) const { return _value != id._value; }

    constexpr bool operator<(Id const& id) const { return _value < id._value; }

    constexpr static Id make(int32_t index, uint8_t gen)
    {
        assert(index < (1 << _IndexBits));
        return Id(static_cast<uint32_t>(((gen & _GenMask) << _IndexBits) | (index & _IndexMask)));
    }

  private:
    uint32_t _value = 0xFFFFFFFF;

    static constexpr auto const _IndexBits = uint32_t(24);
    static constexpr auto const _IndexMask = uint32_t((1 << _IndexBits) - 1);
    static constexpr auto const _GenBits = uint32_t(8);
    static constexpr auto const _GenMask = uint32_t((1 << _GenBits) - 1);
};

constexpr auto IdInvalid = Id(0xFFFFFFFF);

////////////////////////////////////////////////////////////////////////////////////////////////////
// A simple manager to associate an Id to some data

struct SimpleIdDataRemover
{
    void operator()(Id) {}
};

template <typename T, typename DataRemover = SimpleIdDataRemover>
class SimpleIdDataManager
{
  public:
    SimpleIdDataManager() = default;

    SimpleIdDataManager(size_t reserveSize) { reserve(reserveSize); }

    Id add(T const& data)
    {
        auto index = 0;
        if (_freeIds.size() >= idRecycleLimit)
        {
            index = _freeIds.front();
            _freeIds.pop_front();
        }
        else
        {
            _idDatas.emplace_back(_IdData());
            index = _idDatas.size() - 1;
        }
        _idDatas[index].data = data;
        return Id::make(index, _idDatas[index].gen);
    }

    T remove(Id id)
    {
        auto index = id.index();
        auto& sd = _idDatas[index];
        auto data = sd.data;
        sd.data = T{};
        ++sd.gen;
        _freeIds.push_back(index);
        SimpleIdDataRemover()(id);
        return data;
    }

    bool isIdValid(Id id) const
    {
        auto index = id.index();
        return (index < static_cast<int32_t>(_idDatas.size())) && (id.gen() == _idDatas[index].gen);
    }

    T const& data(Id id) const
    {
        assert(isIdValid(id));
        return _idDatas[id.index()].data;
    }

    T& data(Id id)
    {
        assert(isIdValid(id));
        return _idDatas[id.index()].data;
    }

    void reserve(size_t size) { _idDatas.reserve(size); }

    size_t size() const { return _idDatas.size(); }

  public:
    size_t idRecycleLimit = 1024;

  private:
    struct _IdData
    {
        T data = {};
        uint8_t gen = 0;
    };
    std::vector<_IdData> _idDatas;
    std::deque<int32_t> _freeIds;
};

} // namespace nopp
