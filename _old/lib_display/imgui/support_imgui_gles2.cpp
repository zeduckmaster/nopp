#include "support_imgui.hpp"

using namespace nopp;
using namespace nopp::gles2;

namespace
{
struct GLStateBackup
{
    GLStateBackup(GL const& gl) : _gl{gl}
    {
        _gl.glGetIntegerv(GL_ACTIVE_TEXTURE,
                          reinterpret_cast<GLint*>(&_activeTexture));
        _gl.glGetIntegerv(GL_CURRENT_PROGRAM, &_program);
        _gl.glGetIntegerv(GL_TEXTURE_BINDING_2D, &_texture);
        _gl.glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &_arrayBuffer);
        _gl.glGetIntegerv(GL_VIEWPORT, _viewport);
        _gl.glGetIntegerv(GL_SCISSOR_BOX, _scissorBox);
        _gl.glGetIntegerv(GL_BLEND_SRC_RGB,
                          reinterpret_cast<GLint*>(&_blendSrcRgb));
        _gl.glGetIntegerv(GL_BLEND_DST_RGB,
                          reinterpret_cast<GLint*>(&_blendDstRgb));
        _gl.glGetIntegerv(GL_BLEND_SRC_ALPHA,
                          reinterpret_cast<GLint*>(&_blendSrcAlpha));
        _gl.glGetIntegerv(GL_BLEND_DST_ALPHA,
                          reinterpret_cast<GLint*>(&_blendDstAlpha));
        _gl.glGetIntegerv(GL_BLEND_EQUATION_RGB,
                          reinterpret_cast<GLint*>(&_blendEquationRgb));
        _gl.glGetIntegerv(GL_BLEND_EQUATION_ALPHA,
                          reinterpret_cast<GLint*>(&_blendEquationAlpha));
        _enableBlend = _gl.glIsEnabled(GL_BLEND);
        _enableCullFace = _gl.glIsEnabled(GL_CULL_FACE);
        _enableDepthTest = _gl.glIsEnabled(GL_DEPTH_TEST);
        _enableScissorTest = _gl.glIsEnabled(GL_SCISSOR_TEST);
    }

    void enable(GLenum cap, bool flag)
    {
        if (flag == true)
            _gl.glEnable(cap);
        else
            _gl.glDisable(cap);
    }

    ~GLStateBackup()
    {
        _gl.glUseProgram(_program);
        _gl.glBindTexture(GL_TEXTURE_2D, _texture);
        _gl.glActiveTexture(_activeTexture);
        _gl.glBindBuffer(GL_ARRAY_BUFFER, _arrayBuffer);
        _gl.glBlendEquationSeparate(_blendEquationRgb, _blendEquationAlpha);
        _gl.glBlendFuncSeparate(
            _blendSrcRgb, _blendDstRgb, _blendSrcAlpha, _blendDstAlpha);
        enable(GL_BLEND, _enableBlend);
        enable(GL_CULL_FACE, _enableCullFace);
        enable(GL_DEPTH_TEST, _enableDepthTest);
        enable(GL_SCISSOR_TEST, _enableScissorTest);
        _gl.glViewport(_viewport[0],
                       _viewport[1],
                       static_cast<GLsizei>(_viewport[2]),
                       static_cast<GLsizei>(_viewport[3]));
        _gl.glScissor(_scissorBox[0],
                      _scissorBox[1],
                      static_cast<GLsizei>(_scissorBox[2]),
                      static_cast<GLsizei>(_scissorBox[3]));
    }

  private:
    GL const& _gl;
    // todo check types
    GLenum _activeTexture = 0;
    GLint _program = 0;
    GLint _texture = 0;
    GLint _arrayBuffer = 0;
    GLint _viewport[4];
    GLint _scissorBox[4];
    GLenum _blendSrcRgb = 0;
    GLenum _blendDstRgb = 0;
    GLenum _blendSrcAlpha = 0;
    GLenum _blendDstAlpha = 0;
    GLenum _blendEquationRgb = 0;
    GLenum _blendEquationAlpha = 0;
    GLboolean _enableBlend = false;
    GLboolean _enableCullFace = false;
    GLboolean _enableDepthTest = false;
    GLboolean _enableScissorTest = false;
};

struct GLObjects
{
    GLuint program = 0;
    GLuint fontTexture = 0;
    GLint locTex = 0;
    GLint locProjMtx = 0;
    GLint locVtxPos = 0;
    GLint locVtxUV = 0;
    GLint locVtxColor = 0;
    GLuint vertexBuffer = 0;
    GLuint indexBuffer = 0;

    bool create(GL const& gl)
    {
        GLchar const* vertexShaderSrc =
            "#version 100\n"
            "uniform mat4 ProjMtx;\n"
            "attribute vec2 Position;\n"
            "attribute vec2 UV;\n"
            "attribute vec4 Color;\n"
            "varying vec2 Frag_UV;\n"
            "varying vec4 Frag_Color;\n"
            "void main()\n"
            "{\n"
            "    Frag_UV = UV;\n"
            "    Frag_Color = Color;\n"
            "    gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
            "}\n";
        GLchar const* fragmentShaderSrc =
            "#version 100\n"
            "#ifdef GL_ES\n"
            "    precision mediump float;\n"
            "#endif\n"
            "uniform sampler2D Texture;\n"
            "varying vec2 Frag_UV;\n"
            "varying vec4 Frag_Color;\n"
            "void main()\n"
            "{\n"
            "    gl_FragColor = Frag_Color * texture2D(Texture, Frag_UV.st);\n"
            "}\n";
        program = gl.glCreateProgram();
        auto error = glCompileAndAttachShader(
            gl, program, GL_VERTEX_SHADER, vertexShaderSrc);
        error = glCompileAndAttachShader(
            gl, program, GL_FRAGMENT_SHADER, fragmentShaderSrc);
        gl.glLinkProgram(program);

        locTex = gl.glGetUniformLocation(program, "Texture");
        locProjMtx = gl.glGetUniformLocation(program, "ProjMtx");
        locVtxPos = gl.glGetAttribLocation(program, "Position");
        locVtxUV = gl.glGetAttribLocation(program, "UV");
        locVtxColor = gl.glGetAttribLocation(program, "Color");

        gl.glGenBuffers(1, &vertexBuffer);
        gl.glGenBuffers(1, &indexBuffer);

        if (createFontTexture(gl) == false)
            return false;

        return true;
    }

    bool createFontTexture(GL const& gl)
    {
        auto& io = ImGui::GetIO();
        unsigned char* pixels = nullptr;
        auto width = 0;
        auto height = 0;
        io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

        auto tex = GLint{0};
        gl.glGetIntegerv(GL_TEXTURE_BINDING_2D, &tex);
        gl.glGenTextures(1, &fontTexture);
        gl.glBindTexture(GL_TEXTURE_2D, fontTexture);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        gl.glTexImage2D(GL_TEXTURE_2D,
                        0,
                        GL_RGBA,
                        width,
                        height,
                        0,
                        GL_RGBA,
                        GL_UNSIGNED_BYTE,
                        pixels);
        io.Fonts->TexID = reinterpret_cast<ImTextureID>(fontTexture);
        gl.glBindTexture(GL_TEXTURE_2D, tex);
        return true;
    }

    void destroy(GL const& gl)
    {
        if (vertexBuffer != 0)
        {
            gl.glDeleteBuffers(1, &vertexBuffer);
            vertexBuffer = 0;
        }
        if (indexBuffer != 0)
        {
            gl.glDeleteBuffers(1, &indexBuffer);
            indexBuffer = 0;
        }
        if (program != 0)
        {
            gl.glDeleteProgram(program);
            program = 0;
        }
        if (fontTexture != 0)
        {
            gl.glDeleteTextures(1, &fontTexture);
            fontTexture = 0;
            auto& io = ImGui::GetIO();
            io.Fonts->TexID = 0;
        }
    }
};
GLObjects glObjects;

void setupGLState(GL const& gl,
                  ImDrawData* drawData,
                  int32_t fbWidth,
                  int32_t fbHeight)
{
    gl.glEnable(GL_BLEND);
    gl.glBlendEquation(GL_FUNC_ADD);
    gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    gl.glDisable(GL_CULL_FACE);
    gl.glDisable(GL_DEPTH_TEST);
    gl.glEnable(GL_SCISSOR_TEST);

    // setup viewport, orthographic projection matrix
    // our visible imgui space lies from drawData->DisplayPos (top left) to
    // drawData->DisplayPos+drawData->DisplaySize (bottom right).
    // displayPos is (0,0) for single viewport apps.
    gl.glViewport(
        0, 0, static_cast<GLsizei>(fbWidth), static_cast<GLsizei>(fbHeight));
    auto L = drawData->DisplayPos.x;
    auto R = drawData->DisplayPos.x + drawData->DisplaySize.x;
    auto T = drawData->DisplayPos.y;
    auto B = drawData->DisplayPos.y + drawData->DisplaySize.y;
    float const ortho_projection[4][4] = {
        {2.0f / (R - L), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (T - B), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {(R + L) / (L - R), (T + B) / (B - T), 0.0f, 1.0f}};
    gl.glUseProgram(glObjects.program);
    gl.glUniform1i(glObjects.locTex, 0);
    gl.glUniformMatrix4fv(
        glObjects.locProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);

    gl.glBindBuffer(GL_ARRAY_BUFFER, glObjects.vertexBuffer);
    gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glObjects.indexBuffer);
    gl.glEnableVertexAttribArray(glObjects.locVtxPos);
    gl.glEnableVertexAttribArray(glObjects.locVtxUV);
    gl.glEnableVertexAttribArray(glObjects.locVtxColor);
    gl.glVertexAttribPointer(glObjects.locVtxPos,
                             2,
                             GL_FLOAT,
                             GL_FALSE,
                             sizeof(ImDrawVert),
                             (GLvoid*)IM_OFFSETOF(ImDrawVert, pos));
    gl.glVertexAttribPointer(glObjects.locVtxUV,
                             2,
                             GL_FLOAT,
                             GL_FALSE,
                             sizeof(ImDrawVert),
                             (GLvoid*)IM_OFFSETOF(ImDrawVert, uv));
    gl.glVertexAttribPointer(glObjects.locVtxColor,
                             4,
                             GL_UNSIGNED_BYTE,
                             GL_TRUE,
                             sizeof(ImDrawVert),
                             (GLvoid*)IM_OFFSETOF(ImDrawVert, col));
}
} // namespace

void nopp::gles2::glImGuiInit(GL const& gl)
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    auto& io = ImGui::GetIO();
    ImGui::StyleColorsDark();
    io.BackendRendererName = "imgui_impl_opengles2";
    glObjects.create(gl);
    glCheckError(gl, "glImGuiInit");
}

void nopp::gles2::glImGuiRenderFrame(GL const& gl)
{
    auto drawData = ImGui::GetDrawData();
    // avoid rendering when minimized, scale coordinates for retina displays
    // (screen coordinates != framebuffer coordinates)
    auto fbWidth = static_cast<int>(drawData->DisplaySize.x *
                                    drawData->FramebufferScale.x);
    auto fbHeight = static_cast<int>(drawData->DisplaySize.y *
                                     drawData->FramebufferScale.y);
    if ((fbWidth <= 0) || (fbHeight <= 0))
        return;

    // backup gl state
    auto glStateBackup = GLStateBackup{gl};

    // setup gl state
    setupGLState(gl, drawData, fbWidth, fbHeight);

    // render
    // will project scissor/clipping rectangles into framebuffer space
    // (0,0) unless using multi-viewports
    auto clipOff = drawData->DisplayPos;
    // (1,1) unless using retina display which are often (2,2)
    auto clipScale = drawData->FramebufferScale;

    for (auto n = 0; n < drawData->CmdListsCount; ++n)
    {
        auto cmdList = drawData->CmdLists[n];
        // upload vertex/index buffers
        auto size = static_cast<GLsizeiptr>(cmdList->VtxBuffer.Size *
                                            sizeof(ImDrawVert));
        auto data = static_cast<GLvoid const*>(cmdList->VtxBuffer.Data);
        gl.glBufferData(GL_ARRAY_BUFFER, size, data, GL_STREAM_DRAW);

        size = static_cast<GLsizeiptr>(cmdList->IdxBuffer.Size *
                                       sizeof(ImDrawIdx));
        data = static_cast<GLvoid const*>(cmdList->IdxBuffer.Data);
        gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STREAM_DRAW);

        for (auto cmdPos = 0; cmdPos < cmdList->CmdBuffer.Size; ++cmdPos)
        {
            auto pcmd = &cmdList->CmdBuffer[cmdPos];
            if (pcmd->UserCallback != nullptr)
            {
                // user callback, registered via ImDrawList::AddCallback()
                // (ImDrawCallback_ResetRenderState is a special callback value
                // used by the user to request the renderer to reset render
                // state.)
                if (pcmd->UserCallback == ImDrawCallback_ResetRenderState)
                    setupGLState(gl, drawData, fbWidth, fbHeight);
                else
                    pcmd->UserCallback(cmdList, pcmd);
            }
            else
            {
                // project scissor/clipping rectangles into framebuffer space
                auto clipRect =
                    ImVec4{(pcmd->ClipRect.x - clipOff.x) * clipScale.x,
                           (pcmd->ClipRect.y - clipOff.y) * clipScale.y,
                           (pcmd->ClipRect.z - clipOff.x) * clipScale.x,
                           (pcmd->ClipRect.w - clipOff.y) * clipScale.y};

                if ((clipRect.x < fbWidth) && (clipRect.y < fbHeight) &&
                    (clipRect.z >= 0.0f) && (clipRect.w >= 0.0f))
                {
                    auto x = static_cast<GLint>(clipRect.x);
                    auto y = static_cast<GLint>(fbHeight - clipRect.w);
                    auto width = static_cast<GLsizei>(clipRect.z - clipRect.x);
                    auto height = static_cast<GLsizei>(clipRect.w - clipRect.y);

                    gl.glScissor(x, y, width, height);

                    // Bind texture, Draw
                    auto texture = reinterpret_cast<intptr_t>(pcmd->TextureId);
                    gl.glBindTexture(GL_TEXTURE_2D, texture);

                    auto count = static_cast<GLsizei>(pcmd->ElemCount);
                    auto type = sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT
                                                       : GL_UNSIGNED_INT;
                    auto indices = reinterpret_cast<GLvoid const*>(
                        pcmd->IdxOffset * sizeof(ImDrawIdx));
                    gl.glDrawElements(GL_TRIANGLES, count, type, indices);
                }
            }
        }
    }
}

void nopp::gles2::glImGuiTerminate(GL const& gl) {}
