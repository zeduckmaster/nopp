#include "gpu_gles2.hpp"

#include <array>
#include <lib_core/log.hpp>
#include <lib_core/string.hpp>

using namespace nopp;
using namespace nopp::gles2;

namespace
{
GLint findGLWrapMode(std::string const& str)
{
    if (str == "GL_REPEAT")
        return GL_REPEAT;
    else if (str == "GL_MIRRORED_REPEAT")
        return GL_MIRRORED_REPEAT;
    else if (str == "GL_CLAMP_TO_EDGE")
        return GL_CLAMP_TO_EDGE;
    logError("findGLWrapMode: invalid value \"%s\"", str.c_str());
    return GL_CLAMP_TO_EDGE;
}

GLint findGLFilter(std::string const& str)
{
    if (str == "GL_NEAREST")
        return GL_NEAREST;
    else if (str == "GL_LINEAR")
        return GL_LINEAR;
    else if (str == "GL_NEAREST_MIPMAP_NEAREST")
        return GL_NEAREST_MIPMAP_NEAREST;
    else if (str == "GL_NEAREST_MIPMAP_LINEAR")
        return GL_NEAREST_MIPMAP_LINEAR;
    else if (str == "GL_LINEAR_MIPMAP_NEAREST")
        return GL_LINEAR_MIPMAP_NEAREST;
    else if (str == "GL_LINEAR_MIPMAP_LINEAR")
        return GL_LINEAR_MIPMAP_LINEAR;
    logError("findGLFilter: invalid value \"%s\"", str.c_str());
    return GL_NEAREST;
}

struct SamplerData final
{
    HashValue name;
    GLint unit;
    GLint wrapS;
    GLint wrapT;
    GLint minFilter;
    GLint magFilter;
};

SamplerData parseSampler(std::string const& str)
{
    auto parts = splitString(str, " \t\r\n,()");
    assert(parts.size() == 6);
    auto sampler = SamplerData{};
    sampler.name = computeHash(parts[0]);
    sampler.unit = toInt32(parts[1]);
    sampler.wrapS = findGLWrapMode(parts[2]);
    sampler.wrapT = findGLWrapMode(parts[3]);
    sampler.minFilter = findGLFilter(parts[4]);
    sampler.magFilter = findGLFilter(parts[5]);
    return sampler;
}

struct ShaderData
{
    GLenum type = 0;
    std::string code;

    ShaderData() = default;
    ShaderData(GLenum type) : type{type} {}
};

struct ProgramParse final
{
    std::vector<ShaderData> shaders;
    std::map<HashValue, SamplerData> samplers;
};

ProgramParse parseProgram(std::string const& str,
                          std::vector<Path> const& includeSearchPaths)
{
    auto program = ProgramParse{};
    std::istringstream iss{str};
    auto finished = false;
    std::string* cur_s{nullptr};
    while ((iss) && (finished != true))
    {
        auto line = std::string{};
        std::getline(iss, line);
        auto parts = splitString(line, " \t\r\n");
        auto pos = std::string::npos;
        if ((parts.size() != 0) && (parts[0] == "#pragma"))
        {
            if (parts[1].find("shader_vertex_begin") != std::string::npos)
            {
                program.shaders.emplace_back(ShaderData{GL_VERTEX_SHADER});
                cur_s = &program.shaders.back().code;
                continue;
            }
            else if (parts[1].find("shader_fragment_begin") !=
                     std::string::npos)
            {
                program.shaders.emplace_back(ShaderData{GL_FRAGMENT_SHADER});
                cur_s = &program.shaders.back().code;
                continue;
            }
            else if ((parts.size() > 2) && (cur_s != nullptr) &&
                     (parts[1].find("include") != std::string::npos))
            {
                auto includeFilepath =
                    findFullFilepath(parts[2], includeSearchPaths);
                if (includeFilepath.isEmpty() == true)
                {
                    logError("parseProgram: can't find include file %s",
                             parts[2].c_str());
                    continue;
                }
                auto includeStr = readFileContentAsString(includeFilepath);
                auto includeMsg =
                    std::string{"\n//include: "} + parts[2] + std::string{"\n"};
                cur_s->append(includeMsg);
                cur_s->append(includeStr);
                cur_s->append(includeMsg);
                continue;
            }
            else if ((pos = line.find("sampler")) != std::string::npos)
            {
                auto sampler = parseSampler(
                    line.substr(pos + std::string{"sampler"}.size()));
                program.samplers[sampler.name] = sampler;
            }
            else if ((parts[1].find("shader_vertex_end") !=
                      std::string::npos) ||
                     (parts[1].find("shader_fragment_end") !=
                      std::string::npos))
            {
                cur_s = nullptr;
                continue;
            }
        }
        if (cur_s != nullptr)
        {
            cur_s->append(line);
            cur_s->push_back('\n');
        }
    }
    return program;
}

std::array<std::string, 5> const AttributeNames{
    "inPosition", "inColor", "inTexCoord01", "inNormal", "inTangent"};
} // namespace

std::string nopp::gles2::glCompileAndAttachShader(GL const& gl,
                                                  GLuint glProgramId,
                                                  GLenum glShaderType,
                                                  GLchar const* shaderCode)
{
    auto glShaderId = gl.glCreateShader(glShaderType);
    gl.glShaderSource(glShaderId, 1, &shaderCode, nullptr);
    gl.glCompileShader(glShaderId);
    auto compiled = GLint{GL_FALSE};
    gl.glGetShaderiv(glShaderId, GL_COMPILE_STATUS, &compiled);
    if (compiled == GL_FALSE)
    {
        auto infoLen = GLint{0};
        gl.glGetShaderiv(glShaderId, GL_INFO_LOG_LENGTH, &infoLen);
        auto error = std::string{};
        if (infoLen > 1)
        {
            auto infoLog = new GLchar[infoLen];
            gl.glGetShaderInfoLog(glShaderId, infoLen, nullptr, infoLog);
            error.append(infoLog);
            error.push_back('\n');
            delete[] infoLog;
        }
        gl.glDeleteShader(glShaderId);
        return error;
    }
    // if(glGetTranslatedShaderSourceANGLE != nullptr)
    //	getGLShaderTranslatedSource(glShaderId, status);
    gl.glAttachShader(glProgramId, glShaderId);
    return std::string{};
}

GLProgram::GLProgram(GL const& gl) : _gl{gl} {}

GLProgram::GLProgram(GL const& gl,
                     std::string const& code,
                     std::vector<Path> const& includeSearchDirpaths)
    : GLProgram{gl}
{
    glSetCode(code, includeSearchDirpaths);
}

GLProgram::GLProgram(GL const& gl, Path const& filepath) : GLProgram{gl}
{
    auto code = readFileContentAsString(filepath);
    if (code.empty() == true)
        return;
    glSetCode(code, {filepath.parentPath()});
}

GLProgram::~GLProgram()
{
    if (_glId != 0)
        _gl.glDeleteProgram(_glId);
}

void GLProgram::glSetCode(std::string const& code,
                          std::vector<Path> const& includeSearchDirpaths)
{
    status = ProgramStatus{};
    _glId = _gl.glCreateProgram();
    auto pp = parseProgram(code, includeSearchDirpaths);
    status.source = code;
    for (auto& shader : pp.shaders)
    {
        auto error =
            glCompileAndAttachShader(_gl, _glId, shader.type, shader.code);
        if (error.empty() == false)
        {
            status.error.append(error);
            logError("GLProgram::glSetCode: %s", error.c_str());
            return;
        }
    }
    // force attribute location
    // names should match attribute names in vertex shader
    auto n = GLuint{0};
    for (auto& name : AttributeNames)
        _gl.glBindAttribLocation(_glId, n++, name.c_str());

    // link program
    _gl.glLinkProgram(_glId);
    auto linked = GLint{GL_FALSE};
    _gl.glGetProgramiv(_glId, GL_LINK_STATUS, &linked);
    if (linked == GL_FALSE)
    {
        auto infoLen = GLint{0};
        _gl.glGetProgramiv(_glId, GL_INFO_LOG_LENGTH, &infoLen);
        if (infoLen > 1)
        {
            auto infoLog = new GLchar[infoLen];
            _gl.glGetProgramInfoLog(_glId, infoLen, nullptr, infoLog);
            status.error.append(infoLog);
            status.error.push_back('\n');
            logError("GLProgram::glSetCode: %s", infoLog);
            delete[] infoLog;
        }
        _gl.glDeleteProgram(_glId);
        return;
    }

    // get binary
    // if(glGetProgramBinaryOES != nullptr)
    //	getGLProgramBinary(_glId, status);

    // check attribute locations
    _attributes.clear();
    auto nbAA = GLint{0};
    _gl.glGetProgramiv(_glId, GL_ACTIVE_ATTRIBUTES, &nbAA);
    if (nbAA != 0)
    {
        _attributes.reserve(nbAA);
        auto lAA = GLint{0};
        _gl.glGetProgramiv(_glId, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &lAA);
        auto buf = new GLchar[lAA];
        for (auto n = 0; n < nbAA; ++n)
        {
            auto size = GLint{0};
            auto type = GLenum{0};
            _gl.glGetActiveAttrib(_glId,
                                  n,
                                  lAA,
                                  nullptr,
                                  &size,
                                  &type,
                                  reinterpret_cast<GLchar*>(buf));
            auto loc = _gl.glGetAttribLocation(_glId, buf);
            auto checkloc = GLint{0};
            for (auto& name : AttributeNames)
            {
                if (name == buf)
                    break;
                checkloc++;
            }
            assert(loc == checkloc);
            _attributes.emplace_back(loc);
        }
        delete[] buf;
    }

    _gl.glUseProgram(_glId);
    // get all active uniforms
    auto nbAU = GLint{0};
    _gl.glGetProgramiv(_glId, GL_ACTIVE_UNIFORMS, &nbAU);
    _uniforms.clear();
    _samplers.fill(_Sampler{});
    if (nbAU != 0)
    {
        auto lAU = GLint{0};
        _gl.glGetProgramiv(_glId, GL_ACTIVE_UNIFORM_MAX_LENGTH, &lAU);
        auto buf = new GLchar[lAU];
        for (auto n = 0; n < nbAU; ++n)
        {
            auto size = GLint{0};
            auto type = GLenum{0};
            _gl.glGetActiveUniform(_glId, n, lAU, nullptr, &size, &type, buf);
            auto uloc = _gl.glGetUniformLocation(_glId, buf);
            auto name = computeHash(buf);
            if ((type == GL_SAMPLER_2D) || (type == GL_SAMPLER_CUBE))
            {
                auto iter = pp.samplers.find(name);
                assert(iter != pp.samplers.cend());
                auto& sp = iter->second;
                auto sampler = _Sampler{};
                sampler.wrapS = sp.wrapS;
                sampler.wrapT = sp.wrapT;
                sampler.minFilter = sp.minFilter;
                sampler.magFilter = sp.magFilter;
                _samplers[sp.unit] = sampler;
                _gl.glUniform1i(uloc, sp.unit);
            }
            else
            {
                auto u = _Uniform{};
                u.name = name;
                u.location = uloc;
                u.type = type;
                u.count = size;
                _uniforms[u.name] = u;
            }
        }
        delete[] buf;
    }
    _gl.glUseProgram(0);
    glCheckError(_gl, "GLProgram::glSetCode");
}

void GLProgram::glBegin() const
{
    _gl.glUseProgram(_glId);
    for (auto& attribute : _attributes)
        _gl.glEnableVertexAttribArray(attribute);
    glCheckError(_gl, "GLProgram::glBegin");
}

void GLProgram::glEnd() const
{
    _gl.glUseProgram(0);
    for (auto& attribute : _attributes)
        _gl.glDisableVertexAttribArray(attribute);
    glCheckError(_gl, "GLProgram::glEnd");
}

void GLProgram::glSetUniform(HashValue name, void const* data) const
{
    auto iter = _uniforms.find(name);
    if (iter == _uniforms.cend())
    {
        logDebug("GLProgram::glSetUniform: uniform \"%u\" doesn't exists",
                 name);
        return;
    }
    auto location = iter->second.location;
    auto count = iter->second.count;
    switch (iter->second.type)
    {
    case GL_FLOAT:
        _gl.glUniform1fv(location, count, static_cast<GLfloat const*>(data));
        break;
    case GL_FLOAT_VEC2:
        _gl.glUniform2fv(location, count, static_cast<GLfloat const*>(data));
        break;
    case GL_FLOAT_VEC3:
        _gl.glUniform3fv(location, count, static_cast<GLfloat const*>(data));
        break;
    case GL_FLOAT_VEC4:
        _gl.glUniform4fv(location, count, static_cast<GLfloat const*>(data));
        break;
    case GL_INT:
        _gl.glUniform1iv(location, count, static_cast<GLint const*>(data));
        break;
    case GL_INT_VEC2:
        _gl.glUniform2iv(location, count, static_cast<GLint const*>(data));
        break;
    case GL_INT_VEC3:
        _gl.glUniform3iv(location, count, static_cast<GLint const*>(data));
        break;
    case GL_INT_VEC4:
        _gl.glUniform4iv(location, count, static_cast<GLint const*>(data));
        break;
    case GL_FLOAT_MAT2:
        _gl.glUniformMatrix2fv(
            location, count, GL_FALSE, static_cast<GLfloat const*>(data));
        break;
    case GL_FLOAT_MAT3:
        _gl.glUniformMatrix3fv(
            location, count, GL_FALSE, static_cast<GLfloat const*>(data));
        break;
    case GL_FLOAT_MAT4:
        _gl.glUniformMatrix4fv(
            location, count, GL_FALSE, static_cast<GLfloat const*>(data));
        break;
    default: break;
    }
    glCheckError(_gl, "GLProgram::glSetUniform");
}

void GLProgram::glSetSampler(GLenum pos, GLTexture* texture) const
{
    assert(pos < _samplers.size());
    auto& sampler = _samplers[pos];
    _gl.glActiveTexture(GL_TEXTURE0 + pos);
    if (texture != nullptr)
    {
        _gl.glBindTexture(texture->glType, texture->glId);
        _gl.glTexParameteri(texture->glType, GL_TEXTURE_WRAP_S, sampler.wrapS);
        _gl.glTexParameteri(texture->glType, GL_TEXTURE_WRAP_T, sampler.wrapT);
        _gl.glTexParameteri(
            texture->glType, GL_TEXTURE_MIN_FILTER, sampler.minFilter);
        _gl.glTexParameteri(
            texture->glType, GL_TEXTURE_MAG_FILTER, sampler.magFilter);
    }
    else
        _gl.glBindTexture(GL_TEXTURE_2D, 0);
    glCheckError(_gl, "GLProgram::glSetSampler");
}

void GLProgram::glSetSamplers(std::vector<GLTexture*> textures) const
{
    assert(textures.size() <= _samplers.size());
    auto pos = GLenum{0};
    for (auto& texture : textures)
    {
        auto& sampler = _samplers[pos];
        if (texture != nullptr)
        {
            _gl.glActiveTexture(GL_TEXTURE0 + pos);
            _gl.glBindTexture(texture->glType, texture->glId);
            _gl.glTexParameteri(
                texture->glType, GL_TEXTURE_WRAP_S, sampler.wrapS);
            _gl.glTexParameteri(
                texture->glType, GL_TEXTURE_WRAP_T, sampler.wrapT);
            _gl.glTexParameteri(
                texture->glType, GL_TEXTURE_MIN_FILTER, sampler.minFilter);
            _gl.glTexParameteri(
                texture->glType, GL_TEXTURE_MAG_FILTER, sampler.magFilter);
        }
        ++pos;
    }
    glCheckError(_gl, "GLProgram::glSetSamplers");
}

void GLProgram::glDrawMesh(GLMesh* mesh) const
{
    assert(mesh != nullptr);
    auto& vas = mesh->glVertexAttributes;
    if (vas.size() == 0)
        return;
    for (auto& attribute : _attributes)
    {
        auto& va = vas[attribute];
        if (va.glVBId == 0)
            continue;
        _gl.glBindBuffer(GL_ARRAY_BUFFER, va.glVBId);
        _gl.glVertexAttribPointer(attribute,
                                  va.size,
                                  va.type,
                                  va.normalized,
                                  va.stride,
                                  reinterpret_cast<GLvoid*>(va.offset));
    }

    // bind index buffer if any
    if (mesh->glIBId == 0)
    {
        _gl.glDrawArrays(mesh->mode, 0, mesh->count);
    }
    else
    {
        _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->glIBId);
        _gl.glDrawElements(mesh->mode, mesh->count, GL_UNSIGNED_SHORT, nullptr);
    }
    glCheckError(_gl, "GLProgram::glDrawMesh");
}

void GLProgram::glDrawMeshWireframe(GLMesh* mesh) const
{
    assert(mesh != nullptr);
    assert(mesh->glIBWireframeId != 0);
    auto& vas = mesh->glVertexAttributes;
    for (auto& attribute : _attributes)
    {
        auto& va = vas[attribute];
        if (va.glVBId == 0)
            continue;
        _gl.glBindBuffer(GL_ARRAY_BUFFER, va.glVBId);
        _gl.glVertexAttribPointer(attribute,
                                  va.size,
                                  va.type,
                                  va.normalized,
                                  va.stride,
                                  reinterpret_cast<GLvoid*>(va.offset));
    }

    // bind wireframe index buffer
    _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->glIBWireframeId);
    _gl.glDrawElements(
        GL_LINES, mesh->wireframeCount, GL_UNSIGNED_SHORT, nullptr);
    glCheckError(_gl, "GLProgram::glDrawMeshWireframe");
}
