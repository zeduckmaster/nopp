#include <catch2/catch.hpp>
#include "nopp/bound.hpp"

using namespace nopp;

TEST_CASE("contains", "[bound]")
{
    auto bound2d = Bound{Vec3{1.0f, 1.0f, 0.0f}, 1.0f};
    auto bound3d = Bound{Vec3{1.0f, 1.0f, 1.0f}, 1.0f};

    REQUIRE(bound2d.contains(Vec3{1.0f, 1.0f, 0.0f}) == true);
    auto r2 = (std::sqrt(2.0f) - 1.0f) / std::sqrt(2.0f);
    REQUIRE(bound2d.contains(Vec3{r2, r2, 0.0f}) == true);
    REQUIRE(bound2d.contains(Vec3::Zero()) == false);
    REQUIRE(bound2d.contains(Bound{Vec3{1.0f, 1.0f, 0.0f}, 0.7f}) == true);
    REQUIRE(bound2d.contains(Bound{Vec3{0.5f, 0.5f, 0.0f}, 1.0f - std::sqrt(2.0f) / 2.0f}) == true);
    REQUIRE(bound2d.contains(Bound{Vec3{0.5f, 0.5f, 0.0f}, 1.0f}) == false);
    REQUIRE(bound3d.contains(Vec3{1.0f, 1.0f, 1.0f}) == true);
    auto r3 = (std::sqrt(3.0f) - 1.0f) / std::sqrt(3.0f);
    REQUIRE(bound3d.contains(Vec3{r3, r3, r3}) == true);
    REQUIRE(bound3d.contains(Vec3::Zero()) == false);
    REQUIRE(bound3d.contains(Bound{Vec3{1.0f, 1.0f, 1.0f}, 0.7f}) == true);
}

TEST_CASE("extend", "[bound]")
{
    auto bound2d = Bound{Vec3{1.0f, 1.0f, 0.0f}, 1.0f};

    REQUIRE(bound2d.extend({Vec3{1.0f, 1.0f, 0.0f}}) == Bound{Vec3{1.0f, 1.0f, 0.0f}, 1.0f});
    REQUIRE(bound2d.extend(Bound{Vec3{1.1f, 1.2f, 0.0f}, 0.5f}) ==
            Bound{Vec3{1.0f, 1.0f, 0.0f}, 1.0f});
    REQUIRE(bound2d.extend({Vec3{0.5f, 0.5f, 0.5f}}) == Bound{Vec3{1.0f, 1.0f, 0.0f}, 1.0f});
}
