#include <catch2/catch.hpp>
#include "nopp/time.hpp"

using namespace nopp;

TEST_CASE("PrecisionTimer", "[time]")
{
    auto timer = PrecisionTimer{};
    timer.start();
    REQUIRE(timer.timeMicroSec() > 0.0);
}

TEST_CASE("isLeapYear", "[time]")
{
    REQUIRE(isLeapYear(2012) == true);
    REQUIRE(isLeapYear(1978) == false);
}

TEST_CASE("monthDayCount", "[time]")
{
    REQUIRE(monthDaysCount(4, 1978) == 30);
    REQUIRE(monthDaysCount(2, 2012) == 29);
}
