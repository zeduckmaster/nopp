#include <iostream>
#include <QApplication>
#include "window_main.h"
#include "io.h"
#include "helpers.h"
#include "mud.h"
//#include "math.h"
//#include "clock.h"

using namespace nopp;

auto dataPath = Path{"c:/users/david/documents/nopp/app_test_data/"};
//auto dataPath = Path{"/home/duckmaster/Documents/nopp/app_test_data/"};

#if defined(__cplusplus)
extern "C"
#endif
int main(int argc, char* argv[])
{
    World world{dataPath};
    QApplication qtApp{argc, argv};
    auto r = 0;
    {
        WindowMain w{};
        auto output = [&](std::string const& msg) { w.onOutput(msg); };
        auto id = world.addSession(output);
        auto input = [&](std::string const& msg) { world.sendInput(id, msg); };
        w.onInput = input;
        world.move(id, "welcome");
        w.show();
        r = qtApp.exec();
    }
    return r;
}

