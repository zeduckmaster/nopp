#include "gpu.hpp"

#include <deque>
#include <lib_core/container.hpp>
#include <vector>

using namespace nopp;

namespace
{
struct _GPUData
{
    GPUData* data = nullptr;
    uint8_t gen = 0;
};
std::vector<_GPUData> gpuDatas;
std::deque<int32_t> freeIds;
std::vector<GPUThread*> gpuThreads;

Id addData(GPUData* data)
{
    auto index = 0;
    if (freeIds.size() >= 1024)
    {
        index = freeIds.front();
        freeIds.pop_front();
    }
    else
    {
        gpuDatas.emplace_back(_GPUData{});
        index = gpuDatas.size() - 1;
    }
    gpuDatas[index].data = data;
    return Id::make(index, gpuDatas[index].gen);
}

GPUData* removeData(Id id)
{
    auto index = id.index();
    auto& sd = gpuDatas[index];
    auto data = sd.data;
    sd.data = nullptr;
    ++sd.gen;
    freeIds.push_back(index);
    for (auto& gpuThread : gpuThreads)
        gpuThread->destroyData(id);
    return data;
}
} // namespace

GPUData::GPUData() { _id = addData(this); }

GPUData::GPUData(GPUData&& gpuData) : _id{std::move(gpuData._id)}
{
    gpuData._id = IdInvalid;
}

GPUData::~GPUData()
{
    if (_id == IdInvalid)
        return;

    auto p = removeData(_id);
    assert(p == this);
}

GPUData& GPUData::operator=(GPUData&& gpuData)
{
    assert(this != &gpuData);
    if (_id != IdInvalid)
    {
        auto p = removeData(_id);
        assert(p == this);
    }
    _id = gpuData._id;
    gpuData._id = IdInvalid;
    return *this;
}

void GPUData::commit()
{
    for (auto& gpuThread : gpuThreads)
        gpuThread->updateData(_id);
}

bool nopp::isGPUDataIdValid(Id id)
{
    auto index = id.index();
    return (index < static_cast<int32_t>(gpuDatas.size())) &&
           (id.gen() == gpuDatas[index].gen) &&
           (gpuDatas[index].data != nullptr);
}

GPUData* nopp::getGPUData(Id id) { return gpuDatas[id.index()].data; }

////////////////////////////////////////////////////////////////////////////////

GPUThread::GPUThread() { gpuThreads.emplace_back(this); }

GPUThread::~GPUThread() { swapAndPopBack(gpuThreads, this); }
