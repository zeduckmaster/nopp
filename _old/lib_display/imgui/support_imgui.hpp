#pragma once

#if defined(__MINGW32__)
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wattributes"
#endif

#include <imgui.hpp>

#if defined(__MINGW32__)
#    pragma GCC diagnostic pop
#endif

#if defined(WITH_GLES2)
#    include "../gles2/gpu_gles2.hpp"

namespace nopp
{
namespace gles2
{
void glImGuiInit(GL const& gl);

void glImGuiRenderFrame(GL const& gl);

void glImGuiTerminate(GL const& gl);
}
} // namespace nopp

#endif
