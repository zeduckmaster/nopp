#include <catch2/catch.hpp>
#include "nopp/base64.hpp"

using namespace nopp;

TEST_CASE("encoding/decoding some basic data", "[base64]")
{
    auto src = std::vector<uint8_t>{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
    REQUIRE(encodeBase64(src) == "MTIzNDU2Nzg5MA==");
    auto result = decodeBase64("MTIzNDU2Nzg5MA==");
    REQUIRE(std::equal(src.begin(), src.end(), result.begin()) == true);
}

TEST_CASE("encoding/decoding http address", "[base64]")
{
    auto src = std::vector<uint8_t>{'h', 't', 't', 'p', ':', '/', '/', 'f', 'o', 'o'};
    REQUIRE(encodeBase64(src, true) == "aHR0cDovL2Zvbw..");
    auto result = decodeBase64("aHR0cDovL2Zvbw..");
    REQUIRE(std::equal(src.begin(), src.end(), result.begin()) == true);
}
