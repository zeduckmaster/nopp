#include <catch2/catch.hpp>
#include "nopp/image.hpp"

using namespace nopp;

TEST_CASE("isPixelFormatCompressed", "[image]")
{
    REQUIRE(isPixelFormatCompressed(ePixelFormat::RGB_ETC1) == true);
}

TEST_CASE("toByteSize", "[image]")
{
    REQUIRE(toByteSize(ePixelFormat::Unknown) == -1);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::Unknown) == -1);
    REQUIRE(toByteSize(ePixelFormat::RGBA8_UNorm) == 4);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBA8_UNorm) == 13500);
    REQUIRE(toByteSize(ePixelFormat::RGBx8_UNorm) == 4);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBx8_UNorm) == 13500);
    REQUIRE(toByteSize(ePixelFormat::R8_UNorm) == 1);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::R8_UNorm) == 3375);
    REQUIRE(toByteSize(ePixelFormat::RGBA16_Float) == 8);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBA16_Float) == 27000);
    REQUIRE(toByteSize(ePixelFormat::RGBA32_Float) == 16);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBA32_Float) == 54000);
    REQUIRE(toByteSize(ePixelFormat::RGB_ETC1) == 8);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGB_ETC1) == 128);
}

TEST_CASE("ImageDesc", "[image]")
{
    // isValid & constructor
    REQUIRE(ImageDesc{}.isValid() == false);
    REQUIRE(ImageDesc{Size3i{}, ePixelFormat::RGBA8_UNorm}.isValid() == false);
    REQUIRE(ImageDesc{15, 13, ePixelFormat::RGBx8_UNorm}.isValid() == true);
    REQUIRE(ImageDesc{Size3i{15, 11, 1}, ePixelFormat::RGBx8_UNorm}.isValid() == true);
    // operator== & operator!=
    REQUIRE(ImageDesc{} == ImageDesc{});
    REQUIRE(ImageDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float} ==
            ImageDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float});
    REQUIRE(ImageDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float} !=
            ImageDesc{Size3i{11, 10, 1}, ePixelFormat::RGBA16_Float});
    REQUIRE(ImageDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float} !=
            ImageDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA32_Float});
    // byteSize()
    REQUIRE(ImageDesc{9, 10, ePixelFormat::RGBA32_Float}.byteSize() == 1440);
}

TEST_CASE("Image", "[image]")
{
    REQUIRE(Image{}.isValid() == false);
    auto imgDesc = ImageDesc{15, 13, ePixelFormat::RGBx8_UNorm};
    auto img = Image{imgDesc};
    REQUIRE(img.isValid() == true);
    REQUIRE(img.desc == imgDesc);
    //REQUIRE(img.data.size() == )
}

#if defined(WITH_SDL)

namespace
{

void* sdlUserData = nullptr;
auto sdlInfoLog = std::string{};
auto sdlDebugLog = std::string{};
auto sdlWarningLog = std::string{};
auto sdlErrorLog = std::string{};

void sdlOutputFunction(void* userdata, int category, SDL_LogPriority priority, const char* message)
{
    sdlUserData = userdata;
    switch (priority)
    {
    case SDL_LOG_PRIORITY_INFO: sdlInfoLog = message; break;
    case SDL_LOG_PRIORITY_DEBUG: sdlDebugLog = message; break;
    case SDL_LOG_PRIORITY_WARN: sdlWarningLog = message; break;
    case SDL_LOG_PRIORITY_ERROR: sdlErrorLog = message; break;
    default: break;
    }
}

bool initSDL()
{
    if (SDL_WasInit(SDL_INIT_EVENTS) == SDL_INIT_EVENTS)
    {
        return true;
    }
    if (SDL_Init(SDL_INIT_EVENTS) != 0)
    {
        return false;
    }
    nopp::logFunction = sdlLogFunction;
    SDL_LogSetOutputFunction(sdlOutputFunction, nullptr);
    return true;
}

bool hasErrors()
{
    return (sdlErrorLog.empty() == false);
}

} // namespace

TEST_CASE("findBestPixelFormat", "[image]")
{
    auto pf = findBestPixelFormat(SDL_PIXELFORMAT_UNKNOWN);
    REQUIRE(pf.pixelFormat == ePixelFormat::Unknown);
    REQUIRE(pf.sdlPixelFormat == SDL_PIXELFORMAT_UNKNOWN);
    pf = findBestPixelFormat(SDL_PIXELFORMAT_RGBA32);
    REQUIRE(pf.pixelFormat == ePixelFormat::RGBA8_UNorm);
    REQUIRE(pf.sdlPixelFormat == SDL_PIXELFORMAT_RGBA32);
    pf = findBestPixelFormat(SDL_PIXELFORMAT_RGB24);
    REQUIRE(pf.pixelFormat == ePixelFormat::RGBx8_UNorm);
    REQUIRE(pf.sdlPixelFormat == SDL_PIXELFORMAT_RGBA32);
}

TEST_CASE("Image::create", "[image]")
{
    auto dataPath = getCurrentWorkingPath().append("data/");
    REQUIRE(initSDL() == true);
    auto img = Image{};
    // jpeg loading
    img.create(Path{dataPath}.append("lena.jpg"));
    REQUIRE(hasErrors() == false);
    REQUIRE(img.isValid() == true);
    REQUIRE(img.desc == ImageDesc{512, 512, ePixelFormat::RGBx8_UNorm});

    //REQUIRE(img.data.size() == 0);
}

TEST_CASE("ImageDefaultDesc", "[image]")
{
    REQUIRE(initSDL() == true);
    auto img = Image{};
    ImageDefaultDesc{}.createColor(img);
    REQUIRE(img.desc == ImageDesc{16, 16, ePixelFormat::RGBA8_UNorm});
    REQUIRE(img.data.size() == 1024);
    ImageDefaultDesc{}.createSpecular(img);
    REQUIRE(img.desc == ImageDesc{1, 1, ePixelFormat::RGBA8_UNorm});
    REQUIRE(img.data.size() == 4);
    ImageDefaultDesc{}.createNormal(img);
    REQUIRE(img.desc == ImageDesc{1, 1, ePixelFormat::RGBx8_UNorm});
    REQUIRE(img.data.size() == 4);
}

#endif // WITH_SDL