#include <catch2/catch.hpp>
#include "nopp/dice.hpp"

using namespace nopp;

TEST_CASE("rollDice", "[dice]")
{
    REQUIRE(rollDice(1) == 1);
    auto result = rollDice(3);
    REQUIRE(((result >= 1) && (result <= 3)));
    result = rollDice(20);
    REQUIRE(((result >= 1) && (result <= 20)));
}

TEST_CASE("roll", "[dice]")
{
    REQUIRE(roll("2d4-12") == 0);
    auto result = roll("2d4");
    REQUIRE(((result >= 2) && (result <= 8)));
    result = roll("3d6-2");
    REQUIRE(((result >= 1) && (result <= 16)));
}
