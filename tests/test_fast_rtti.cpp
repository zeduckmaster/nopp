#include <catch2/catch.hpp>
#include "nopp/fast_rtti.hpp"

using namespace nopp;

namespace tmp
{
class Foo
{
    FAST_RTTI_CLASS_ROOT(tmp::Foo)

  public:
    virtual ~Foo() = default;
};

class Bar : public Foo
{
    FAST_RTTI_CLASS(tmp::Bar, tmp::Foo)
};
} // namespace tmp

TEST_CASE("TypeTraits base types", "[fast_rtti]")
{
    REQUIRE(TypeTraits<bool>::name() == std::string("bool"));
    REQUIRE(TypeTraits<int8_t>::name() == std::string("int8_t"));
    REQUIRE(TypeTraits<uint8_t>::name() == std::string("uint8_t"));
    REQUIRE(TypeTraits<int16_t>::name() == std::string("int16_t"));
    REQUIRE(TypeTraits<uint16_t>::name() == std::string("uint16_t"));
    REQUIRE(TypeTraits<int32_t>::name() == std::string("int32_t"));
    REQUIRE(TypeTraits<uint32_t>::name() == std::string("uint32_t"));
    REQUIRE(TypeTraits<int64_t>::name() == std::string("int64_t"));
    REQUIRE(TypeTraits<uint64_t>::name() == std::string("uint64_t"));
    REQUIRE(TypeTraits<float>::name() == std::string("float"));
    REQUIRE(TypeTraits<double>::name() == std::string("double"));
    REQUIRE(TypeTraits<std::string>::name() == std::string("std::string"));
}

TEST_CASE("TypeTraits custom types", "[fast_rtti]")
{
    REQUIRE(TypeTraits<tmp::Foo>::name() == std::string{"tmp::Foo"});
    auto ptr = std::make_unique<tmp::Bar>();
    REQUIRE(ptr->isTypeOf<tmp::Foo>() == true);
}
