#include <catch2/catch.hpp>
#include "nopp/evaluate.hpp"

using namespace nopp;

TEST_CASE("evaluate", "[evaluate]")
{
    REQUIRE(evaluate("7 + 3") == 10);
    REQUIRE(evaluate("7-3 ") == 4);
    REQUIRE(evaluate("3-7") == -4);
    REQUIRE(evaluate("  7 *3") == 21);
    REQUIRE(evaluate(" 20 /5  ") == 4);
    REQUIRE(evaluate("(2+  3)* 4") == 20);
    REQUIRE(evaluate("30/  ( 5 + 1 )") == 5);
}
