#include "clock.hpp"
#include <cstdlib>

using namespace nopp;

TimeValue nopp::systemNow()
{
    auto now = std::chrono::system_clock::now();
    return std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
}

TimeStamp nopp::toTimeStamp(TimeValue const& value)
{
    // 1y = 365d, 1d = 24h, 1h = 60m, 1m = 60s
    auto ts = TimeStamp{};
    auto s = value.count();
    auto r = std::div(s, static_cast<decltype(s)>(31536000));
    ts.years = r.quot + 1;
    r = std::div(r.rem, static_cast<decltype(s)>(86400));
    ts.days = r.quot + 1;
    r = std::div(r.rem, static_cast<decltype(s)>(3600));
    ts.hours = r.quot;
    r = std::div(r.rem, static_cast<decltype(s)>(60));
    ts.minutes = r.quot;
    ts.seconds = r.rem;
    return ts;
}

TimeValue nopp::toTimeValue(TimeStamp const& value)
{
    return TimeValue(((value.years - 1) * 31536000) + ((value.days - 1) * 86400) + (value.hours * 3600) + (value.minutes * 60) + value.seconds);
}

////////////////////////////////////////////////////////////////////////////////

void Clock::start()
{
    _start = systemNow();
    _isStopped = false;
}

void Clock::stop()
{
    _isStopped = true;
    _stop = systemNow();
}

TimeValue Clock::time()
{
    auto stop = (_isStopped == true)? _stop : systemNow();
    return stop - _start;
}
