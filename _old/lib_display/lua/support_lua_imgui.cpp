#include "../imgui/support_imgui.hpp"
#include "support_lua.hpp"
#include <lib_core/math.hpp>

using namespace nopp;

void nopp::loadLuaDisplayImGui(sol::state& luaState)
{
    auto t = luaState["ImGui"].get_or_create<sol::table>(sol::new_table());

    // enum window flag
    t["ImGuiWindowFlags_None"] = sol::var(ImGuiWindowFlags_None);
    t["ImGuiWindowFlags_NoTitleBar"] = sol::var(ImGuiWindowFlags_NoTitleBar);
    t["ImGuiWindowFlags_NoResize"] = sol::var(ImGuiWindowFlags_NoResize);
    t["ImGuiWindowFlags_NoMove"] = sol::var(ImGuiWindowFlags_NoMove);
    t["ImGuiWindowFlags_NoScrollbar"] = sol::var(ImGuiWindowFlags_NoScrollbar);
    t["ImGuiWindowFlags_NoScrollWithMouse"] =
        sol::var(ImGuiWindowFlags_NoScrollWithMouse);
    t["ImGuiWindowFlags_NoCollapse"] = sol::var(ImGuiWindowFlags_NoCollapse);
    t["ImGuiWindowFlags_AlwaysAutoResize"] =
        sol::var(ImGuiWindowFlags_AlwaysAutoResize);
    t["ImGuiWindowFlags_NoBackground"] =
        sol::var(ImGuiWindowFlags_NoBackground);
    t["ImGuiWindowFlags_NoSavedSettings"] =
        sol::var(ImGuiWindowFlags_NoSavedSettings);
    t["ImGuiWindowFlags_NoMouseInputs"] =
        sol::var(ImGuiWindowFlags_NoMouseInputs);
    t["ImGuiWindowFlags_MenuBar"] = sol::var(ImGuiWindowFlags_MenuBar);
    t["ImGuiWindowFlags_HorizontalScrollbar"] =
        sol::var(ImGuiWindowFlags_HorizontalScrollbar);
    t["ImGuiWindowFlags_NoFocusOnAppearing"] =
        sol::var(ImGuiWindowFlags_NoFocusOnAppearing);
    t["ImGuiWindowFlags_NoBringToFrontOnFocus"] =
        sol::var(ImGuiWindowFlags_NoBringToFrontOnFocus);
    t["ImGuiWindowFlags_AlwaysVerticalScrollbar"] =
        sol::var(ImGuiWindowFlags_AlwaysVerticalScrollbar);
    t["ImGuiWindowFlags_AlwaysHorizontalScrollbar"] =
        sol::var(ImGuiWindowFlags_AlwaysHorizontalScrollbar);
    t["ImGuiWindowFlags_AlwaysUseWindowPadding"] =
        sol::var(ImGuiWindowFlags_AlwaysUseWindowPadding);
    t["ImGuiWindowFlags_NoNavInputs"] = sol::var(ImGuiWindowFlags_NoNavInputs);
    t["ImGuiWindowFlags_NoNavFocus"] = sol::var(ImGuiWindowFlags_NoNavFocus);
    t["ImGuiWindowFlags_UnsavedDocument"] =
        sol::var(ImGuiWindowFlags_UnsavedDocument);
    t["ImGuiWindowFlags_NoNav"] = sol::var(ImGuiWindowFlags_NoNav);
    t["ImGuiWindowFlags_NoDecoration"] =
        sol::var(ImGuiWindowFlags_NoDecoration);
    t["ImGuiWindowFlags_NoInputs"] = sol::var(ImGuiWindowFlags_NoInputs);

    // enum color edit
    t["ImGuiColorEditFlags_None"] = sol::var(ImGuiColorEditFlags_None);
    t["ImGuiColorEditFlags_NoAlpha"] = sol::var(ImGuiColorEditFlags_NoAlpha);
    t["ImGuiColorEditFlags_NoPicker"] = sol::var(ImGuiColorEditFlags_NoPicker);
    t["ImGuiColorEditFlags_NoOptions"] =
        sol::var(ImGuiColorEditFlags_NoOptions);
    t["ImGuiColorEditFlags_NoSmallPreview"] =
        sol::var(ImGuiColorEditFlags_NoSmallPreview);
    t["ImGuiColorEditFlags_NoInputs"] = sol::var(ImGuiColorEditFlags_NoInputs);
    t["ImGuiColorEditFlags_NoTooltip"] =
        sol::var(ImGuiColorEditFlags_NoTooltip);
    t["ImGuiColorEditFlags_NoLabel"] = sol::var(ImGuiColorEditFlags_NoLabel);
    t["ImGuiColorEditFlags_NoSidePreview"] =
        sol::var(ImGuiColorEditFlags_NoSidePreview);
    t["ImGuiColorEditFlags_NoDragDrop"] =
        sol::var(ImGuiColorEditFlags_NoDragDrop);
    t["ImGuiColorEditFlags_AlphaBar"] = sol::var(ImGuiColorEditFlags_AlphaBar);
    t["ImGuiColorEditFlags_AlphaPreview"] =
        sol::var(ImGuiColorEditFlags_AlphaPreview);
    t["ImGuiColorEditFlags_AlphaPreviewHalf"] =
        sol::var(ImGuiColorEditFlags_AlphaPreviewHalf);
    t["ImGuiColorEditFlags_HDR"] = sol::var(ImGuiColorEditFlags_HDR);
    t["ImGuiColorEditFlags_DisplayRGB"] =
        sol::var(ImGuiColorEditFlags_DisplayRGB);
    t["ImGuiColorEditFlags_DisplayHSV"] =
        sol::var(ImGuiColorEditFlags_DisplayHSV);
    t["ImGuiColorEditFlags_DisplayHex"] =
        sol::var(ImGuiColorEditFlags_DisplayHex);
    t["ImGuiColorEditFlags_Uint8"] = sol::var(ImGuiColorEditFlags_Uint8);
    t["ImGuiColorEditFlags_Float"] = sol::var(ImGuiColorEditFlags_Float);
    t["ImGuiColorEditFlags_PickerHueBar"] =
        sol::var(ImGuiColorEditFlags_PickerHueBar);
    t["ImGuiColorEditFlags_PickerHueWheel"] =
        sol::var(ImGuiColorEditFlags_PickerHueWheel);
    t["ImGuiColorEditFlags_InputRGB"] = sol::var(ImGuiColorEditFlags_InputRGB);
    t["ImGuiColorEditFlags_InputHSV"] = sol::var(ImGuiColorEditFlags_InputHSV);
    t["ImGuiColorEditFlags__OptionsDefault"] =
        sol::var(ImGuiColorEditFlags__OptionsDefault);

    // windows
    t["Begin"] = sol::overload([](char const* name) { ImGui::Begin(name); },
                               [](char const* name, int flags) {
                                   ImGui::Begin(name, nullptr, flags);
                               });
    t["End"] = &ImGui::End;

    // windows utilities
    t["IsWindowAppearing"] = &ImGui::IsWindowAppearing;
    t["IsWindowCollapsed"] = &ImGui::IsWindowCollapsed;
    t["GetWindowPos"] = []() -> Vec2 {
        auto pos = ImGui::GetWindowPos();
        return Vec2{pos.x, pos.y};
    };
    t["GetWindowSize"] = []() -> Vec2 {
        auto pos = ImGui::GetWindowSize();
        return Vec2{pos.x, pos.y};
    };
    t["GetWindowWidth"] = &ImGui::GetWindowWidth;
    t["GetWindowHeight"] = &ImGui::GetWindowHeight;
    t["SetNextWindowPos"] = [](Vec2 const& pos) {
        ImGui::SetNextWindowPos(ImVec2{pos.x(), pos.y()});
    };
    t["SetNextWindowSize"] = [](Vec2 const& size) {
        ImGui::SetNextWindowSize(ImVec2{size.x(), size.y()});
    };
    t["SetNextWindowSizeConstraints"] = [](Vec2 const& sizeMin,
                                           Vec2 const& sizeMax) {
        ImGui::SetNextWindowSizeConstraints(ImVec2{sizeMin.x(), sizeMin.y()},
                                            ImVec2{sizeMax.x(), sizeMax.y()});
    };
    t["SetNextWindowContentSize"] = [](Vec2 const& size) {
        ImGui::SetNextWindowContentSize(ImVec2{size.x(), size.y()});
    };
    t["SetNextWindowCollapsed"] = &ImGui::SetNextWindowCollapsed;
    t["SetNextWindowFocus"] = &ImGui::SetNextWindowFocus;

    // widget: text
    t["Text"] = [](char const* text) {
        ImGui::TextUnformatted(text); };
    t["TextColored"] = [](char const* text, Vec4 const& color) {
        ImGui::PushStyleColor(
            ImGuiCol_Text, ImVec4{color.x(), color.y(), color.z(), color.w()});
        ImGui::TextUnformatted(text);
        ImGui::PopStyleColor();
    };

    // widget : color
    t["ColorEdit3"] =
        [](char const* label, Vec3 const& color, int flags) -> Vec3 {
        float rgb[3] = {color.x(), color.y(), color.z()};
        ImGui::ColorEdit3(label, rgb, flags);
        return Vec3{rgb[0], rgb[1], rgb[2]};
    };
    t["ColorEdit4"] =
        [](char const* label, Vec4 const& color, int flags) -> Vec4 {
        float rgb[4] = {color.x(), color.y(), color.z(), color.w()};
        ImGui::ColorEdit4(label, rgb, flags);
        return Vec4{rgb[0], rgb[1], rgb[2], rgb[3]};
    };

    // widget : sliders
    t["SliderFloat"] = [](const char* label,
                          float value,
                          float valueMin,
                          float valueMax) -> float {
        ImGui::SliderFloat(label, &value, valueMin, valueMax);
        return value;
    };

    // widget : combo
    t["Combo"] = [](const char* label, int current, const char* data) -> int {
        ImGui::Combo(label, &current, data);
        return current;
    };

    // widgets : others
    t["Checkbox"] = [](const char* label, bool value) -> bool {
        ImGui::Checkbox(label, &value);
        return value;
    };

    // layout
    t["Separator"] = &ImGui::Separator;

    // focus, activation
    t["SetItemDefaultFocus"] = &ImGui::SetItemDefaultFocus;
}
