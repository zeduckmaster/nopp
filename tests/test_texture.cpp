#include <catch2/catch.hpp>
#include "nopp/texture.hpp"

using namespace nopp;

TEST_CASE("isPixelFormatCompressed", "[texture]")
{
    REQUIRE(isPixelFormatCompressed(ePixelFormat::RGB_ETC1) == true);
}

TEST_CASE("toByteSize", "[texture]")
{
    REQUIRE(toByteSize(ePixelFormat::Unknown) == -1);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::Unknown) == -1);
    REQUIRE(toByteSize(ePixelFormat::RGBA8_UNorm) == 4);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBA8_UNorm) == 13500);
    REQUIRE(toByteSize(ePixelFormat::RGBx8_UNorm) == 4);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBx8_UNorm) == 13500);
    REQUIRE(toByteSize(ePixelFormat::R8_UNorm) == 1);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::R8_UNorm) == 3375);
    REQUIRE(toByteSize(ePixelFormat::RGBA16_Float) == 8);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBA16_Float) == 27000);
    REQUIRE(toByteSize(ePixelFormat::RGBA32_Float) == 16);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGBA32_Float) == 54000);
    REQUIRE(toByteSize(ePixelFormat::RGB_ETC1) == 8);
    REQUIRE(toByteSize(Size3i{15, 15, 15}, ePixelFormat::RGB_ETC1) == 128);
}

TEST_CASE("TextureDesc", "[texture]")
{
    // isValid & constructor
    REQUIRE(TextureDesc{}.isValid() == false);
    REQUIRE(TextureDesc{Size3i{}, ePixelFormat::RGBA8_UNorm}.isValid() == false);
    REQUIRE(TextureDesc{15, 13, ePixelFormat::RGBx8_UNorm}.isValid() == true);
    REQUIRE(TextureDesc{Size3i{15, 11, 1}, ePixelFormat::RGBx8_UNorm}.isValid() == true);
    // operator== & operator!=
    REQUIRE(TextureDesc{} == TextureDesc{});
    REQUIRE(TextureDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float} ==
            TextureDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float});
    REQUIRE(TextureDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float} !=
            TextureDesc{Size3i{11, 10, 1}, ePixelFormat::RGBA16_Float});
    REQUIRE(TextureDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA16_Float} !=
            TextureDesc{Size3i{11, 10, 2}, ePixelFormat::RGBA32_Float});
    // byteSize()
    REQUIRE(TextureDesc{9, 10, ePixelFormat::RGBA32_Float}.byteSize() == 1440);
}

TEST_CASE("Texture", "[texture]")
{
    REQUIRE(Texture{}.isValid() == false);
    auto textureDesc = TextureDesc{15, 13, ePixelFormat::RGBx8_UNorm};
    auto texture = Texture{textureDesc};
    REQUIRE(texture.isValid() == true);
    REQUIRE(texture.desc == textureDesc);
}

#if defined(WITH_SDL)

namespace
{

void* sdlUserData = nullptr;
auto sdlInfoLog = std::string{};
auto sdlDebugLog = std::string{};
auto sdlWarningLog = std::string{};
auto sdlErrorLog = std::string{};

void sdlOutputFunction(void* userdata, int category, SDL_LogPriority priority, const char* message)
{
    sdlUserData = userdata;
    switch (priority)
    {
    case SDL_LOG_PRIORITY_INFO: sdlInfoLog = message; break;
    case SDL_LOG_PRIORITY_DEBUG: sdlDebugLog = message; break;
    case SDL_LOG_PRIORITY_WARN: sdlWarningLog = message; break;
    case SDL_LOG_PRIORITY_ERROR: sdlErrorLog = message; break;
    default: break;
    }
}

bool initSDL()
{
    if (SDL_WasInit(SDL_INIT_EVENTS) == SDL_INIT_EVENTS)
    {
        return true;
    }
    if (SDL_Init(SDL_INIT_EVENTS) != 0)
    {
        return false;
    }
    nopp::logFunction = logFunctionToSDLLog;
    SDL_LogSetOutputFunction(sdlOutputFunction, nullptr);
    return true;
}

bool hasErrors()
{
    return (sdlErrorLog.empty() == false);
}

} // namespace

TEST_CASE("findBestPixelFormat", "[texture]")
{
    auto pf = findBestPixelFormat(SDL_PIXELFORMAT_UNKNOWN);
    REQUIRE(pf.pixelFormat == ePixelFormat::Unknown);
    REQUIRE(pf.sdlPixelFormat == SDL_PIXELFORMAT_UNKNOWN);
    pf = findBestPixelFormat(SDL_PIXELFORMAT_RGBA32);
    REQUIRE(pf.pixelFormat == ePixelFormat::RGBA8_UNorm);
    REQUIRE(pf.sdlPixelFormat == SDL_PIXELFORMAT_RGBA32);
    pf = findBestPixelFormat(SDL_PIXELFORMAT_RGB24);
    REQUIRE(pf.pixelFormat == ePixelFormat::RGBx8_UNorm);
    REQUIRE(pf.sdlPixelFormat == SDL_PIXELFORMAT_RGBA32);
}

TEST_CASE("Texture::create", "[texture]")
{
    auto dataPath = getCurrentWorkingPath().append("data/");
    REQUIRE(initSDL() == true);
    auto texture = Texture{};
    // jpeg loading
    texture.create(Path{dataPath}.append("lena.jpg"));
    REQUIRE(hasErrors() == false);
    REQUIRE(texture.isValid() == true);
    REQUIRE(texture.desc == TextureDesc{512, 512, ePixelFormat::RGBx8_UNorm});

    //REQUIRE(img.data.size() == 0);
}

TEST_CASE("TextureDefaultDesc", "[texture]")
{
    REQUIRE(initSDL() == true);
    auto texture = Texture{};
    TextureDefaultDesc{}.createColor(texture);
    REQUIRE(texture.desc == TextureDesc{16, 16, ePixelFormat::RGBA8_UNorm});
    REQUIRE(texture.data(0, 0).size() == 1024);
    TextureDefaultDesc{}.createSpecular(texture);
    REQUIRE(texture.desc == TextureDesc{1, 1, ePixelFormat::RGBA8_UNorm});
    REQUIRE(texture.data(0, 0).size() == 4);
    TextureDefaultDesc{}.createNormal(texture);
    REQUIRE(texture.desc == TextureDesc{1, 1, ePixelFormat::RGBx8_UNorm});
    REQUIRE(texture.data(0, 0).size() == 4);
}

#endif // WITH_SDL
