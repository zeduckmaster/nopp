#pragma once

#include <string>
#include <vector>

namespace nopp
{

// An encoding function to base 64
inline std::string encodeBase64(std::vector<uint8_t> const& value, bool isURL)
{
    char const* const base64Chars[2] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "abcdefghijklmnopqrstuvwxyz"
                                        "0123456789"
                                        "+/",

                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "abcdefghijklmnopqrstuvwxyz"
                                        "0123456789"
                                        "-_"};
    auto len = value.size();
    auto lenEncoded = (len + 2) / 3 * 4;
    auto trailingChar = (isURL == true) ? '.' : '=';
    auto base64Chars_ = (isURL == true) ? base64Chars[1] : base64Chars[0];
    auto result = std::string();
    result.reserve(lenEncoded);
    auto pos = 0u;
    while (pos < len)
    {
        result.push_back(base64Chars_[(value[pos + 0] & 0xfc) >> 2]);
        if ((pos + 1) < len)
        {
            result.push_back(
                base64Chars_[((value[pos + 0] & 0x03) << 4) + ((value[pos + 1] & 0xf0) >> 4)]);

            if ((pos + 2) < len)
            {
                result.push_back(
                    base64Chars_[((value[pos + 1] & 0x0f) << 2) + ((value[pos + 2] & 0xc0) >> 6)]);
                result.push_back(base64Chars_[value[pos + 2] & 0x3f]);
            }
            else
            {
                result.push_back(base64Chars_[(value[pos + 1] & 0x0f) << 2]);
                result.push_back(trailingChar);
            }
        }
        else
        {
            result.push_back(base64Chars_[(value[pos + 0] & 0x03) << 4]);
            result.push_back(trailingChar);
            result.push_back(trailingChar);
        }
        pos += 3;
    }
    return result;
}

// A decoding function from base 64 from a binary value
inline std::string encodeBase64(std::vector<uint8_t> const& value)
{
    return encodeBase64(value, false);
}

// A decoding function from base 64 from a string value
inline std::vector<uint8_t> decodeBase64(std::string const& value)
{
    if (value.empty() == true)
    {
        return std::vector<uint8_t>();
    }
    auto len = value.size();
    auto result = std::vector<uint8_t>();
    result.reserve(len / 4 * 3);
    auto pos = 0u;
    auto findValue = [](unsigned char const chr) -> uint32_t
    {
        if ((chr >= 'A') && (chr <= 'Z'))
        {
            return chr - 'A';
        }
        else if ((chr >= 'a') && (chr <= 'z'))
        {
            return chr - 'a' + ('Z' - 'A') + 1;
        }
        else if ((chr >= '0') && (chr <= '9'))
        {
            return chr - '0' + ('Z' - 'A') + ('z' - 'a') + 2;
        }
        else if ((chr == '+') || (chr == '-'))
        {
            return 62;
        }
        else if ((chr == '/') || (chr == '_'))
        {
            return 63;
        }
        return -1;
    };
    while (pos < len)
    {
        auto v1 = findValue(value[pos + 1]);
        result.emplace_back(
            static_cast<uint8_t>(((findValue(value[pos + 0])) << 2) + ((v1 & 0x30) >> 4)));

        if ((value[pos + 2] != '=') && (value[pos + 2] != '.'))
        {
            auto v2 = findValue(value[pos + 2]);
            result.emplace_back(static_cast<uint8_t>(((v1 & 0x0f) << 4) + ((v2 & 0x3c) >> 2)));

            if ((value[pos + 3] != '=') && (value[pos + 3] != '.'))
            {
                result.emplace_back(
                    static_cast<uint8_t>(((v2 & 0x03) << 6) + findValue(value[pos + 3])));
            }
        }
        pos += 4;
    }
    return result;
}

} // namespace nopp
