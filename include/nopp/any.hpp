#pragma once

#include "fast_rtti.hpp"

namespace nopp
{

class Any final
{
  public:
    Any() = default;

    template <typename T>
    Any(T const& value) : _content(new _Holder<T>(value))
    {
    }

    Any(Any const& any)
        : _content((any.isEmpty() == true) ? nullptr : any._content->clone())
    {
    }

    ~Any() { delete _content; }

    Any& swap(Any& any)
    {
        std::swap(_content, any._content);
        return *this;
    }

    template <typename T>
    Any& operator=(T const& value)
    {
        auto any = Any(value);
        return swap(any);
    }

    Any& operator=(Any const& any)
    {
        auto a = Any(any);
        return swap(a);
    }

    TypeId typeId() const { return (_content != nullptr) ? _content->typeId() : TypeIdInvalid; }

    std::string typeName() const
    {
        return (_content != nullptr) ? _content->typeName() : std::string();
    }

    bool isEmpty() const { return _content == nullptr; }

    void clear()
    {
        auto any = Any();
        swap(any);
    }

    template <typename T>
    bool is() const
    {
        return TypeTraits<T>::id() == typeId();
    }

    template <typename T>
    T to() const
    {
        assert(TypeTraits<T>::id() == typeId());
        return static_cast<_Holder<T>*>(_content)->held;
    }

  private:
    struct _PlaceHolder
    {
        virtual ~_PlaceHolder() = default;

        virtual TypeId typeId() const = 0;

        virtual std::string typeName() const = 0;

        virtual _PlaceHolder* clone() const = 0;
    };

    template <typename T>
    struct _Holder final : public _PlaceHolder
    {
        T held;

        _Holder(T const& value) : held(value) {}

        TypeId typeId() const override { return TypeTraits<T>::id(); }

        std::string typeName() const override { return TypeTraits<T>::name(); }

        _PlaceHolder* clone() const override { return new _Holder(held); }
    };

    _PlaceHolder* _content = nullptr;
};

} // namespace nopp
