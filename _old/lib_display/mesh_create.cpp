#include "mesh.hpp"

using namespace nopp;

MeshGridDesc::MeshGridDesc(int32_t lineCount, float scale)
    : lineCount{lineCount}, scale{scale}
{
}

void MeshGridDesc::create(MeshData& meshData) const
{
    struct GridLine
    {
        Vec3 from;
        Vec3 to;
        Vec4 color;
    };

    auto gridLines = std::vector<GridLine>{};
    auto l = GridLine{};
    auto size = static_cast<float>(lineCount / 2) * scale;
    auto nb = static_cast<int>(lineCount / 2);
    for (auto n = -nb; n <= nb; ++n)
    {
        auto f = static_cast<float>(n) * scale;

        l.from = Vec3{f, -size, 0.0f};
        l.to = Vec3{f, size, 0.0f};
        l.color = colorLine;
        if (n == 0)
            l.color = colorAxisY;
        gridLines.emplace_back(l);
        l.from = Vec3{-size, f, 0.0f};
        l.to = Vec3{size, f, 0.0f};
        l.color = colorLine;
        if (n == 0)
            l.color = colorAxisX;
        gridLines.push_back(l);
    }
    // z axis
    // l.from = Vec3f{0.0f, 0.0f, -size};
    // l.to = Vec3f{0.0f, 0.0f, size};
    // l.color = params.colorAxisZ;
    // gridLines.push_back(l);

    auto totalLineCount = static_cast<int32_t>(gridLines.size());
    auto meshDesc = MeshDesc{};
    meshDesc.vertexDeclaration |=
        eVertexDeclaration::Position | eVertexDeclaration::Color;
    meshDesc.vertexCount = totalLineCount * 2;
    meshDesc.primitiveType = eMeshTopology::Lines;
    meshData.create(meshDesc);
    for (auto n = 0; n < totalLineCount; ++n)
    {
        auto& l = gridLines[n];
        meshData.positions[n * 2] = l.from;
        meshData.colors[n * 2] = l.color;
        meshData.positions[n * 2 + 1] = l.to;
        meshData.colors[n * 2 + 1] = l.color;
    }
}

////////////////////////////////////////////////////////////////////////////////

namespace
{

auto const cubePositions = std::array<Vec3, 24>{
    Vec3{-0.5f, -0.5f, -0.5f}, Vec3{0.5f, -0.5f, -0.5f},
    Vec3{-0.5f, -0.5f, 0.5f},  Vec3{0.5f, -0.5f, 0.5f}, // front
    Vec3{0.5f, -0.5f, -0.5f},  Vec3{0.5f, 0.5f, -0.5f},
    Vec3{0.5f, -0.5f, 0.5f},   Vec3{0.5f, 0.5f, 0.5f}, // right
    Vec3{0.5f, 0.5f, -0.5f},   Vec3{-0.5f, 0.5f, -0.5f},
    Vec3{0.5f, 0.5f, 0.5f},    Vec3{-0.5f, 0.5f, 0.5f}, // back
    Vec3{-0.5f, 0.5f, -0.5f},  Vec3{-0.5f, -0.5f, -0.5f},
    Vec3{-0.5f, 0.5f, 0.5f},   Vec3{-0.5f, -0.5f, 0.5f}, // left
    Vec3{-0.5f, -0.5f, 0.5f},  Vec3{0.5f, -0.5f, 0.5f},
    Vec3{-0.5f, 0.5f, 0.5f},   Vec3{0.5f, 0.5f, 0.5f}, // top
    Vec3{-0.5f, 0.5f, -0.5f},  Vec3{0.5f, 0.5f, -0.5f},
    Vec3{-0.5f, -0.5f, -0.5f}, Vec3{0.5f, -0.5f, -0.5f} // bottom
};

auto const cubeTexCoord0 = std::array<Vec2, 24>{
    Vec2{0.0f, 0.0f}, Vec2{1.0f, 0.0f},
    Vec2{0.0f, 1.0f}, Vec2{1.0f, 1.0f}, // front
    Vec2{0.0f, 0.0f}, Vec2{1.0f, 0.0f},
    Vec2{0.0f, 1.0f}, Vec2{1.0f, 1.0f}, // right
    Vec2{0.0f, 0.0f}, Vec2{1.0f, 0.0f},
    Vec2{0.0f, 1.0f}, Vec2{1.0f, 1.0f}, // back
    Vec2{0.0f, 0.0f}, Vec2{1.0f, 0.0f},
    Vec2{0.0f, 1.0f}, Vec2{1.0f, 1.0f}, // left
    Vec2{0.0f, 0.0f}, Vec2{1.0f, 0.0f},
    Vec2{0.0f, 1.0f}, Vec2{1.0f, 1.0f}, // top
    Vec2{0.0f, 0.0f}, Vec2{1.0f, 0.0f},
    Vec2{0.0f, 1.0f}, Vec2{1.0f, 1.0f}, // bottom
};

auto const cubeNormal = std::array<PackedNormal, 24>{
    Vec3{0.0f, -1.0f, 0.0f}, Vec3{0.0f, -1.0f, 0.0f},
    Vec3{0.0f, -1.0f, 0.0f}, Vec3{0.0f, -1.0f, 0.0f}, // front
    Vec3{1.0f, 0.0f, 0.0f},  Vec3{1.0f, 0.0f, 0.0f},
    Vec3{1.0f, 0.0f, 0.0f},  Vec3{1.0f, 0.0f, 0.0f}, // right
    Vec3{0.0f, 1.0f, 0.0f},  Vec3{0.0f, 1.0f, 0.0f},
    Vec3{0.0f, 1.0f, 0.0f},  Vec3{0.0f, 1.0f, 0.0f}, // back
    Vec3{-1.0f, 0.0f, 0.0f}, Vec3{-1.0f, 0.0f, 0.0f},
    Vec3{-1.0f, 0.0f, 0.0f}, Vec3{-1.0f, 0.0f, 0.0f}, // left
    Vec3{0.0f, 0.0f, 1.0f},  Vec3{0.0f, 0.0f, 1.0f},
    Vec3{0.0f, 0.0f, 1.0f},  Vec3{0.0f, 0.0f, 1.0f}, // top
    Vec3{0.0f, 0.0f, -1.0f}, Vec3{0.0f, 0.0f, -1.0f},
    Vec3{0.0f, 0.0f, -1.0f}, Vec3{0.0f, 0.0f, -1.0f} // bottom
};

auto const cubeIndexes = std::vector<uint16_t>{
    0,  1,  2,  2,  1,  3,  // front
    4,  5,  6,  6,  5,  7,  // right
    8,  9,  10, 10, 9,  11, // back
    12, 13, 14, 14, 13, 15, // left
    16, 17, 18, 18, 17, 19, // top
    20, 21, 22, 22, 21, 23  // bottom
};

} // namespace

MeshCubeDesc::MeshCubeDesc(Vec3 const& center, float size)
    : center{center}, size{size}
{
}
MeshCubeDesc::MeshCubeDesc(Vec3 const& center,
                           float size,
                           uint32_t vertexDeclaration)
    : center{center}, size{size}, vertexDeclaration{vertexDeclaration}
{
}

void MeshCubeDesc::create(MeshData& meshData) const
{
    assert((cubePositions.size() == cubeTexCoord0.size()) &&
           (cubePositions.size() == cubeNormal.size()));
    assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) ==
           true);
    auto meshDesc = MeshDesc{};
    meshDesc.vertexDeclaration = vertexDeclaration;
    meshDesc.vertexCount = cubePositions.size();
    meshDesc.indexCount = cubeIndexes.size();
    meshDesc.primitiveType = eMeshTopology::Triangles;

    auto nb = static_cast<int32_t>(cubePositions.size());
    meshData.create(meshDesc);
    auto hasTexCoord =
        hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::TexCoord0);
    auto hasNormal =
        hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Normal);
    auto hasColor =
        hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Color);
    for (auto n = 0; n < nb; ++n)
    {
        meshData.positions[n] = (cubePositions[n] * size) + center;
        if (hasTexCoord == true)
            meshData.datas[n].texCoord0 = cubeTexCoord0[n];
        if (hasNormal == true)
            meshData.datas[n].normal = cubeNormal[n];
        if (hasColor == true)
            meshData.colors[n] = color;
    }
    meshData.indexes = cubeIndexes;
}

////////////////////////////////////////////////////////////////////////////////

namespace
{

auto const quadPositions = std::array<Vec3, 4>{Vec3{-0.5f, -0.5f, 0.0f},
                                               Vec3{0.5f, -0.5f, 0.0f},
                                               Vec3{-0.5f, 0.5f, 0.0f},
                                               Vec3{0.5f, 0.5f, 0.0f}};

auto const quadTexCoord0 = std::array<Vec2, 4>{
    Vec2{0.0f, 0.0f}, Vec2{1.0f, 0.0f}, Vec2{0.0f, 1.0f}, Vec2{1.0f, 1.0f}};

auto const quadNormal =
    std::array<PackedNormal, 4>{PackedNormal{0.0f, 0.0f, 1.0f},
                                PackedNormal{0.0f, 0.0f, 1.0f},
                                PackedNormal{0.0f, 0.0f, 1.0f},
                                PackedNormal{0.0f, 0.0f, 1.0f}};

auto const quadIndexes = std::vector<uint16_t>{0, 1, 2, 2, 1, 3};

auto const quadTwoSidedIndexes =
    std::vector<uint16_t>{0, 1, 2, 2, 1, 3, 0, 2, 1, 1, 2, 3};

} // namespace

MeshQuadDesc::MeshQuadDesc(Vec3 const& center, float width, float height)
    : center{center}, width{width}, height{height}
{
}

MeshQuadDesc::MeshQuadDesc(Vec3 const& center,
                           float width,
                           float height,
                           uint32_t vertexDeclaration)
    : center{center}, width{width}, height{height}, vertexDeclaration{
                                                        vertexDeclaration}
{
}

MeshQuadDesc::MeshQuadDesc(Vec3 const& center,
                           float width,
                           float height,
                           uint32_t vertexDeclaration,
                           bool isTwoSided)
    : center{center}, width{width}, height{height},
      vertexDeclaration{vertexDeclaration}, isTwoSided{isTwoSided}
{
}

void MeshQuadDesc::create(MeshData& meshData) const
{
    assert((quadPositions.size() == quadTexCoord0.size()) &&
           (quadPositions.size() == quadNormal.size()));
    assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) ==
           true);
    auto meshDesc = MeshDesc{};
    meshDesc.vertexDeclaration = vertexDeclaration;
    meshDesc.vertexCount = quadPositions.size();
    meshDesc.indexCount =
        (isTwoSided == true) ? quadTwoSidedIndexes.size() : quadIndexes.size();
    meshDesc.primitiveType = eMeshTopology::Triangles;

    auto nb = static_cast<int32_t>(quadPositions.size());
    meshData.create(meshDesc);
    auto hasTexCoord =
        hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::TexCoord0);
    auto hasNormal =
        hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Normal);
    auto hasColor =
        hasEnumFlag(meshDesc.vertexDeclaration, eVertexDeclaration::Color);
    auto scale = Vec3{width, height, 1.0f};
    for (auto n = 0; n < nb; ++n)
    {
        meshData.positions[n] = quadPositions[n].cwiseProduct(scale) + center;
        if (hasTexCoord == true)
            meshData.datas[n].texCoord0 = quadTexCoord0[n];
        if (hasNormal == true)
            meshData.datas[n].normal = quadNormal[n];
        if (hasColor == true)
            meshData.colors[n] = color;
    }
    meshData.indexes = (isTwoSided == true) ? quadTwoSidedIndexes : quadIndexes;
}

////////////////////////////////////////////////////////////////////////////////

MeshCylinderDesc::MeshCylinderDesc(float radiusTop,
                                   float radiusBottom,
                                   float height)
    : radiusTop{radiusTop}, radiusBottom(radiusBottom), height{height}
{
}

MeshCylinderDesc::MeshCylinderDesc(float radiusTop,
                                   float radiusBottom,
                                   float height,
                                   uint32_t vertexDeclaration)
    : radiusTop{radiusTop},
      radiusBottom(radiusBottom), height{height}, vertexDeclaration{
                                                      vertexDeclaration}
{
}

void MeshCylinderDesc::createMesh(MeshData& meshData) const
{
    assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) ==
           true);

    // create the mesh
    auto meshDesc = MeshDesc{};
    meshDesc.vertexCount = (radialSegments + 1) * (heightSegments + 1);
    meshDesc.indexCount = radialSegments * heightSegments * 2 * 3;
    if (capTop != MeshCylinderDesc::eCapType::None)
    {
        meshDesc.vertexCount += radialSegments * 2 + 1;
        meshDesc.indexCount += radialSegments * 3;
    }
    if (capBottom != MeshCylinderDesc::eCapType::None)
    {
        meshDesc.vertexCount += radialSegments * 2 + 1;
        meshDesc.indexCount += radialSegments * 3;
    }
    meshDesc.primitiveType = eMeshTopology::Triangles;
    meshDesc.vertexDeclaration = vertexDeclaration;
    meshData.create(meshDesc);

    auto hasTexCoord = meshDesc.hasTexCoord0();
    auto hasNormal = meshDesc.hasNormal();
    auto hasColor = meshDesc.hasColor();

    // generate vertices, normals and uvs
    auto tanTheta = (radiusBottom - radiusTop) / height;
    auto index = size_t{0};
    auto halfHeight = height * 0.5f;
    auto indexArray = std::vector<std::vector<int32_t>>{};
    for (auto y = 0; y <= heightSegments; ++y)
    {
        auto indexRow = std::vector<int32_t>{};
        auto v = static_cast<float>(y) / static_cast<float>(heightSegments);
        // calculate the radius of the current row
        auto radius = v * (radiusBottom - radiusTop) + radiusTop;
        for (auto x = 0; x <= radialSegments; ++x, ++index)
        {
            auto u = static_cast<float>(x) / static_cast<float>(radialSegments);
            // vertex
            auto pos = Vec3{radius * std::sin(u * thetaLength + thetaStart),
                            -v * height + halfHeight,
                            radius * std::cos(u * thetaLength + thetaStart)};
            meshData.positions[index] = Vec3{pos.x() + center.x(),
                                             -pos.z() + center.y(),
                                             pos.y() + center.z()};
            // normal
            if (hasNormal == true)
            {
                auto nm = pos;
                // handle special case if radiusTop/radiusBottom is zero
                if (((radiusTop == 0.0f) && (y == 0)) ||
                    ((radiusBottom == 0.0f) && (y == heightSegments)))
                {
                    nm.x() = std::sin(u * thetaLength + thetaStart);
                    nm.z() = std::cos(u * thetaLength + thetaStart);
                }
                nm.y() =
                    std::sqrt(nm.x() * nm.x() + nm.z() * nm.z()) * tanTheta;
                nm.normalize();
                meshData.datas[index].normal = Vec3{nm.x(), -nm.z(), nm.y()};
            }
            // uv
            if (hasTexCoord == true)
                meshData.datas[index].texCoord0 = Vec2{u, 1.0f - v};
            // color
            if (hasColor == true)
                meshData.colors[index] = color;
            // save index of vertex in respective row
            indexRow.push_back(index);
        }
        // now save vertices of the row in our index array
        indexArray.emplace_back(indexRow);
    }

    auto indexOffset = 0;
    for (auto x = 0; x < radialSegments; ++x)
    {
        for (auto y = 0; y < heightSegments; ++y)
        {
            // we use the index array to access the correct indices
            auto i1 = static_cast<uint16_t>(indexArray[y][x]);
            auto i2 = static_cast<uint16_t>(indexArray[y + 1][x]);
            auto i3 = static_cast<uint16_t>(indexArray[y + 1][x + 1]);
            auto i4 = static_cast<uint16_t>(indexArray[y][x + 1]);
            // face one
            meshData.indexes[indexOffset++] = i1;
            meshData.indexes[indexOffset++] = i2;
            meshData.indexes[indexOffset++] = i4;
            // face two
            meshData.indexes[indexOffset++] = i2;
            meshData.indexes[indexOffset++] = i3;
            meshData.indexes[indexOffset++] = i4;
        }
    }

    auto funcCap = [&](bool top, MeshCylinderDesc::eCapType capType) {
        auto radius = (top == true) ? radiusTop : radiusBottom;
        auto sign = (top == true) ? 1.0f : -1.0f;
        auto centerIndexStart =
            index; // save the index of the first center vertex
        // first we generate the center vertex data of the cap.
        // because the geometry needs one set of uvs per face,
        // we must generate a center vertex per face/segment
        for (auto x = 1; x <= radialSegments; ++x, ++index)
        {
            meshData.positions[index] =
                Vec3{center.x(), center.y(), halfHeight * sign + center.z()};
            if (hasColor == true)
                meshData.colors[index] = color;

            if (hasNormal == true)
            {
                meshData.datas[index].normal = Vec3{
                    0.0f,
                    0.0f,
                    sign * ((capType == MeshCylinderDesc::eCapType::Flipped)
                                ? -1.0f
                                : 1.0f)};
            }

            if (hasTexCoord == true)
            {
                if (top == true)
                {
                    meshData.datas[index].texCoord0 =
                        Vec2{static_cast<float>(x) /
                                 static_cast<float>(radialSegments),
                             0.0f};
                }
                else
                {
                    meshData.datas[index].texCoord0 =
                        Vec2{static_cast<float>(x - 1) /
                                 static_cast<float>(radialSegments),
                             0.0f};
                }
            }
        }
        auto centerIndexEnd = index; // save the index of the last center vertex
        // now we generate the surrounding vertices, normals and uvs
        for (auto x = 0; x <= radialSegments; ++x, ++index)
        {
            auto u = static_cast<float>(x) / static_cast<float>(radialSegments);
            auto pos = Vec3{};
            pos.x() = radius * std::sin(u * thetaLength + thetaStart);
            pos.y() = halfHeight * sign;
            pos.z() = radius * std::cos(u * thetaLength + thetaStart);
            meshData.positions[index] = Vec3{pos.x() + center.x(),
                                             -pos.z() + center.y(),
                                             pos.y() + center.z()};
            if (hasColor == true)
                meshData.colors[index] = color;

            if (hasNormal == true)
            {
                meshData.datas[index].normal = Vec3{
                    0.0f,
                    0.0f,
                    sign * ((capType == MeshCylinderDesc::eCapType::Flipped)
                                ? -1.0f
                                : 1.0f)};
            }

            if (hasTexCoord == true)
            {
                meshData.datas[index].texCoord0 =
                    Vec2{u, (top == true) ? 1.0f : 0.0f};
            }
        }
        // generate indices
        for (auto x = 0; x < radialSegments; ++x)
        {
            auto c = static_cast<uint16_t>(centerIndexStart + x);
            auto i = static_cast<uint16_t>(centerIndexEnd + x);
            if (top == true)
            {
                meshData.indexes[indexOffset++] = i;
                if (capType == MeshCylinderDesc::eCapType::Normal)
                {
                    meshData.indexes[indexOffset++] = i + 1;
                    meshData.indexes[indexOffset++] = c;
                }
                else if (capType == MeshCylinderDesc::eCapType::Flipped)
                {
                    meshData.indexes[indexOffset++] = c;
                    meshData.indexes[indexOffset++] = i + 1;
                }
            }
            else
            {
                meshData.indexes[indexOffset++] = i + 1;
                if (capType == MeshCylinderDesc::eCapType::Normal)
                {
                    meshData.indexes[indexOffset++] = i;
                    meshData.indexes[indexOffset++] = c;
                }
                else if (capType == MeshCylinderDesc::eCapType::Flipped)
                {
                    meshData.indexes[indexOffset++] = c;
                    meshData.indexes[indexOffset++] = i;
                }
            }
        }
    };

    if ((capTop != MeshCylinderDesc::eCapType::None) && (radiusTop > 0.0f))
        funcCap(true, capTop);
    if ((capBottom != MeshCylinderDesc::eCapType::None) &&
        (radiusBottom > 0.0f))
        funcCap(false, capBottom);
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
// {  0.000000f,  0.000000f, -1.000000f } 0
// {  0.723600f, -0.525720f, -0.447215f } 1
// { -0.276385f, -0.850640f, -0.447215f } 2
// { -0.894425f,  0.000000f, -0.447215f } 3
// { -0.276385f,  0.850640f, -0.447215f } 4
// {  0.723600f,  0.525720f, -0.447215f } 5
// {  0.276385f, -0.850640f,  0.447215f } 6
// { -0.723600f, -0.525720f,  0.447215f } 7
// { -0.723600f,  0.525720f,  0.447215f } 8
// {  0.276385f,  0.850640f,  0.447215f } 9
// {  0.894425f,  0.000000f,  0.447215f } 10
// {  0.000000f,  0.000000f,  1.000000f } 11

auto const icospherePositions = std::vector<Vec3>{
    {0.000000f, 0.000000f, -1.000000f},   {0.723600f, -0.525720f, -0.447215f},
    {-0.276385f, -0.850640f, -0.447215f}, // 0, 1, 2,
    {0.000000f, 0.000000f, -1.000000f},   {-0.276385f, -0.850640f, -0.447215f},
    {-0.894425f, 0.000000f, -0.447215f}, // 0, 2, 3,
    {0.000000f, 0.000000f, -1.000000f},   {-0.894425f, 0.000000f, -0.447215f},
    {-0.276385f, 0.850640f, -0.447215f}, // 0, 3, 4,
    {0.000000f, 0.000000f, -1.000000f},   {-0.276385f, 0.850640f, -0.447215f},
    {0.723600f, 0.525720f, -0.447215f}, // 0, 4, 5,
    {0.000000f, 0.000000f, -1.000000f},   {0.723600f, 0.525720f, -0.447215f},
    {0.723600f, -0.525720f, -0.447215f}, // 0, 5, 1,

    {0.723600f, -0.525720f, -0.447215f},  {0.894425f, 0.000000f, 0.447215f},
    {0.276385f, -0.850640f, 0.447215f}, // 1, 10, 6,
    {0.276385f, -0.850640f, 0.447215f},   {-0.276385f, -0.850640f, -0.447215f},
    {0.723600f, -0.525720f, -0.447215f}, // 6, 2, 1,
    {-0.276385f, -0.850640f, -0.447215f}, {0.276385f, -0.850640f, 0.447215f},
    {-0.723600f, -0.525720f, 0.447215f}, // 2, 6, 7,
    {-0.723600f, -0.525720f, 0.447215f},  {-0.894425f, 0.000000f, -0.447215f},
    {-0.276385f, -0.850640f, -0.447215f}, // 7, 3, 2,
    {-0.894425f, 0.000000f, -0.447215f},  {-0.723600f, -0.525720f, 0.447215f},
    {-0.723600f, 0.525720f, 0.447215f}, // 3, 7, 8,
    {-0.723600f, 0.525720f, 0.447215f},   {-0.276385f, 0.850640f, -0.447215f},
    {-0.894425f, 0.000000f, -0.447215f}, // 8, 4, 3,
    {-0.276385f, 0.850640f, -0.447215f},  {-0.723600f, 0.525720f, 0.447215f},
    {0.276385f, 0.850640f, 0.447215f}, // 4, 8, 9,
    {0.276385f, 0.850640f, 0.447215f},    {0.723600f, 0.525720f, -0.447215f},
    {-0.276385f, 0.850640f, -0.447215f}, // 9, 5, 4,
    {0.723600f, 0.525720f, -0.447215f},   {0.276385f, 0.850640f, 0.447215f},
    {0.894425f, 0.000000f, 0.447215f}, // 5, 9, 10,
    {0.894425f, 0.000000f, 0.447215f},    {0.723600f, -0.525720f, -0.447215f},
    {0.723600f, 0.525720f, -0.447215f}, // 10, 1, 5,

    {0.000000f, 0.000000f, 1.000000f},    {0.894425f, 0.000000f, 0.447215f},
    {0.276385f, 0.850640f, 0.447215f}, // 11, 10, 9,
    {0.000000f, 0.000000f, 1.000000f},    {0.276385f, 0.850640f, 0.447215f},
    {-0.723600f, 0.525720f, 0.447215f}, // 11, 9, 8,
    {0.000000f, 0.000000f, 1.000000f},    {-0.723600f, 0.525720f, 0.447215f},
    {-0.723600f, -0.525720f, 0.447215f}, // 11, 8, 7,
    {0.000000f, 0.000000f, 1.000000f},    {-0.723600f, -0.525720f, 0.447215f},
    {0.276385f, -0.850640f, 0.447215f}, // 11, 7, 6,
    {0.000000f, 0.000000f, 1.000000f},    {0.276385f, -0.850640f, 0.447215f},
    {0.894425f, 0.000000f, 0.447215f} // 11, 6, 10
};

auto const icosphereTexCoord0 = std::vector<Vec2>{
    {0.0f, 0.0f}, {1.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 0.0f}, {0.0f, 1.0f},
    {1.0f, 1.0f}, {0.0f, 0.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, 0.0f},
    {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 0.0f}, {1.0f, 1.0f}, {1.0f, 0.0f},

    {1.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 0.0f}, {0.0f, 0.0f}, {0.0f, 1.0f},
    {1.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 0.0f},
    {1.0f, 1.0f}, {0.0f, 1.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, 1.0f},
    {0.0f, 1.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, 1.0f},
    {0.0f, 0.0f}, {0.0f, 0.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {1.0f, 1.0f},
    {0.0f, 0.0f}, {0.0f, 1.0f}, {0.0f, 1.0f}, {1.0f, 0.0f}, {1.0f, 1.0f},

    {1.0f, 1.0f}, {0.0f, 1.0f}, {0.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 0.0f},
    {0.0f, 1.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}, {1.0f, 0.0f}, {1.0f, 1.0f},
    {1.0f, 0.0f}, {0.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 0.0f}, {0.0f, 1.0f},
};

auto const icosphereNormal = std::vector<PackedNormal>{
    {0.187600f, -0.577350f, -0.794650f},  {0.187600f, -0.577350f, -0.794650f},
    {0.187600f, -0.577350f, -0.794650f},  {-0.491120f, -0.356830f, -0.794650f},
    {-0.491120f, -0.356830f, -0.794650f}, {-0.491120f, -0.356830f, -0.794650f},
    {-0.491120f, 0.356830f, -0.794650f},  {-0.491120f, 0.356830f, -0.794650f},
    {-0.491120f, 0.356830f, -0.794650f},  {0.187660f, 0.577350f, -0.794650f},
    {0.187660f, 0.577350f, -0.794650f},   {0.187660f, 0.577350f, -0.794650f},
    {0.607060f, 0.000000f, -0.794650f},   {0.607060f, 0.000000f, -0.794650f},
    {0.607060f, 0.000000f, -0.794650f},

    {0.794645f, -0.577360f, 0.187585f},   {0.794645f, -0.577360f, 0.187585f},
    {0.794645f, -0.577360f, 0.187585f},   {0.303535f, -0.934170f, -0.187585f},
    {0.303535f, -0.934170f, -0.187585f},  {0.303535f, -0.934170f, -0.187585f},
    {-0.303535f, -0.934170f, 0.187585f},  {-0.303535f, -0.934170f, 0.187585f},
    {-0.303535f, -0.934170f, 0.187585f},  {-0.794645f, -0.577360f, -0.187585f},
    {-0.794645f, -0.577360f, -0.187585f}, {-0.794645f, -0.577360f, -0.187585f},
    {-0.982245f, 0.000000f, 0.187585f},   {-0.982245f, 0.000000f, 0.187585f},
    {-0.982245f, 0.000000f, 0.187585f},   {-0.794645f, 0.577360f, -0.187585f},
    {-0.794645f, 0.577360f, -0.187585f},  {-0.794645f, 0.577360f, -0.187585f},
    {-0.303535f, 0.934170f, 0.187585f},   {-0.303535f, 0.934170f, 0.187585f},
    {-0.303535f, 0.934170f, 0.187585f},   {0.303535f, 0.934170f, -0.187585f},
    {0.303535f, 0.934170f, -0.187585f},   {0.303535f, 0.934170f, -0.187585f},
    {0.794645f, 0.577360f, 0.187585f},    {0.794645f, 0.577360f, 0.187585f},
    {0.794645f, 0.577360f, 0.187585f},    {0.982245f, -0.000000f, -0.187585f},
    {0.982245f, -0.000000f, -0.187585f},  {0.982245f, -0.000000f, -0.187585f},

    {0.491120f, 0.356830f, 0.794650f},    {0.491120f, 0.356830f, 0.794650f},
    {0.491120f, 0.356830f, 0.794650f},    {-0.187600f, 0.577350f, 0.794650f},
    {-0.187600f, 0.577350f, 0.794650f},   {-0.187600f, 0.577350f, 0.794650f},
    {-0.607060f, 0.000000f, 0.794650f},   {-0.607060f, 0.000000f, 0.794650f},
    {-0.607060f, 0.000000f, 0.794650f},   {-0.187600f, -0.577350f, 0.794650f},
    {-0.187600f, -0.577350f, 0.794650f},  {-0.187600f, -0.577350f, 0.794650f},
    {0.491120f, -0.356830f, 0.794650f},   {0.491120f, -0.356830f, 0.794650f},
    {0.491120f, -0.356830f, 0.794650f},
};

} // namespace

MeshIcosphereDesc::MeshIcosphereDesc(Vec3 const& center,
                                     uint32_t subdivisions,
                                     float size)
    : center{center}, subdivisions{subdivisions}, size{size}
{
}

MeshIcosphereDesc::MeshIcosphereDesc(Vec3 const& center,
                                     uint32_t subdivisions,
                                     float size,
                                     uint32_t vertexDeclaration)
    : center{center}, subdivisions{subdivisions}, size{size},
      vertexDeclaration{vertexDeclaration}
{
}

void MeshIcosphereDesc::create(MeshData& meshData) const
{
    assert(subdivisions < 7);
    assert(subdivisions > 0);
    assert(hasEnumFlag(vertexDeclaration, eVertexDeclaration::Position) ==
           true);
    auto meshDesc = MeshDesc{};
    meshDesc.vertexDeclaration = vertexDeclaration;
    meshDesc.vertexCount =
        static_cast<int32_t>(std::pow(4, subdivisions - 1)) * 60;
    // meshInfo.indexCount = static_cast<int32_t>(std::pow(4,
    // meshIcosphereInfo.subdivisions - 1)) * 60;
    meshDesc.primitiveType = eMeshTopology::Triangles;
    meshData.create(meshDesc);
    auto hasTexCoord = meshDesc.hasTexCoord0();
    auto hasNormal = meshDesc.hasNormal();
    auto hasColor = meshDesc.hasColor();

    // create base Icosahedron
    for (auto n = 0; n < 60; ++n)
    {
        meshData.positions[n] = (icospherePositions[n] * size) + center;
        if (hasTexCoord == true)
            meshData.datas[n].texCoord0 = icosphereTexCoord0[n];
        if (hasNormal == true)
            meshData.datas[n].normal = icosphereNormal[n];
        if (hasColor == true)
            meshData.colors[n] = color;
    }

    // subdivide
    auto faces = 60;
    auto nextPositions = 59;
    for (auto i = 1; i < subdivisions; ++i)
    {
        for (auto j = 2; j < faces; j += 3)
        {
            auto x = meshData.positions[j - 2];
            auto y = meshData.positions[j - 1];
            auto z = meshData.positions[j - 0];

            auto a = Vec3{((y - x) * 0.5f) + x - center};
            auto b = Vec3{((z - y) * 0.5f) + y - center};
            auto c = Vec3{((x - z) * 0.5f) + z - center};

            a = a.normalized() * size + center;
            b = b.normalized() * size + center;
            c = c.normalized() * size + center;

            meshData.positions[j - 2] = a;
            meshData.positions[j - 1] = b;
            meshData.positions[j - 0] = c;

            if (hasNormal == true)
            {
                auto normal = ((b - a).cross(c - a)).normalized();
                meshData.datas[j - 2].normal = normal;
                meshData.datas[j - 1].normal = normal;
                meshData.datas[j - 0].normal = normal;
            }

            meshData.positions[++nextPositions] = x;
            meshData.positions[++nextPositions] = a;
            meshData.positions[++nextPositions] = c;

            if (hasNormal == true)
            {
                auto normal = ((a - x).cross(c - x)).normalized();
                meshData.datas[nextPositions - 2].normal = normal;
                meshData.datas[nextPositions - 1].normal = normal;
                meshData.datas[nextPositions - 0].normal = normal;
            }

            meshData.positions[++nextPositions] = y;
            meshData.positions[++nextPositions] = b;
            meshData.positions[++nextPositions] = a;

            if (hasNormal == true)
            {
                auto normal = ((b - y).cross(a - y)).normalized();
                meshData.datas[nextPositions - 2].normal = normal;
                meshData.datas[nextPositions - 1].normal = normal;
                meshData.datas[nextPositions - 0].normal = normal;
            }

            meshData.positions[++nextPositions] = z;
            meshData.positions[++nextPositions] = c;
            meshData.positions[++nextPositions] = b;

            if (hasNormal == true)
            {
                auto normal = ((c - z).cross(b - z)).normalized();
                meshData.datas[nextPositions - 2].normal = normal;
                meshData.datas[nextPositions - 1].normal = normal;
                meshData.datas[nextPositions - 0].normal = normal;
            }
        }
        faces *= 4;
    }

    if (hasTexCoord == true)
    {
        for (auto i = 0; i < meshData.datas.size(); ++i)
        {
            // meshData.indexes[i] = static_cast<uint16_t>(i);
            meshData.datas[i].texCoord0 = icosphereTexCoord0[i % 60];
        }
    }
    if (hasColor == true)
        meshData.setVertexColor(color);
}
