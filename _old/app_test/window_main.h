#pragma once

#include <QMainWindow>
#include <QTextEdit>
#include <QLineEdit>
#include <QAction>
#include "mud.h"

class WindowMain : public QMainWindow
{
    Q_OBJECT;

public:
    WindowMain();
    virtual ~WindowMain() = default;
    void onOutput(std::string const& value);

public:
    std::function<void(std::string)> onInput;

private:
    struct _UI
    {
        QTextEdit* output = nullptr;
        QLineEdit* input = nullptr;

        QAction* actionCommand = nullptr;

        void setup(QMainWindow* w);
        void retranslate();
    };
    _UI _ui;
};
