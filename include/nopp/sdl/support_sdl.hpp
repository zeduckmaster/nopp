#pragma once

#if defined(_WIN32)
#    define SDL_MAIN_HANDLED
#    include <SDL.h>
#    include <SDL_image.h>
#    include <windows.h>
#elif defined(__linux__)
#    include <SDL2/SDL.h>
#    include <SDL2/SDL_image.h>
#elif defined(__APPLE__)
#    include <SDL.h>
#endif

#include "../log.hpp"

namespace nopp
{

void sdlLogFunction(eLogLevel logLevel, char const* fmt, va_list argList);

}
