Errors =
{
   InvalidCommand = "Commande invalide ",
   InvalidArgument = "Argument invalide ",
}

CommandAliases =
{
   regarder = "look",
   aide = "help",
   quitter = "quit",
   q = "quit"
}
CommandAliases["dé"] = "roll"

CommandDescriptions =
{
   look = "Regarder l'",
   help = "Afficher de l'aide sur un sujet. Si aucun sujet n'est spécifié, affiche la liste des commandes disponibles.\nUtilisation: aide [sujet].",
   quit = "Quitter l'application."
}
CommandDescriptions["dé"] = "Effectue un lancer de dés.\nUtilisation: dé [expression]\nExemple: dé 1d6+3"