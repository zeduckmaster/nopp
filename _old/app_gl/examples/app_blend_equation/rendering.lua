--------------------------------------------------------------------------------
-- rendering

local tmp = Transform()
tmp:matrix()

local mesh = MeshData()
MeshQuadDesc():create(mesh)

local bgQuadTex = TextureData()
TextureDefaultDesc():createColor(bgQuadTex)
local quadTex = TextureData(dataPath() .. "/../common/kueken7_RGBA8.dds")
--TextureDefaultDesc():createColor(quadTex)

local glProg
-- background
local bgColor = Vec3(0, 0, 0)
local bgQuadDraw = false
local bgQuadScale = 2
-- blend
local enableBlend = true
local blendColor = Vec4(0, 0, 0, 1)
local imodeRGB = 0
local imodeAlpha = 0
local isrcRGB = 1
local idstRGB = 0
local isrcAlpha = 1
local idstAlpha = 0
local vBlendEquation
local vBlendFunction

local glInit = function(gl)
  --print("glInit")
  glProg = gl:Program(dataPath() .. "/prg_test.gles100")
  vBlendEquation = { gl.GL_FUNC_ADD, gl.GL_FUNC_SUBTRACT, gl.GL_FUNC_REVERSE_SUBTRACT }
  vBlendFunction = { gl.GL_ZERO, gl.GL_ONE, gl.GL_SRC_COLOR, gl.GL_ONE_MINUS_SRC_COLOR, gl.GL_DST_COLOR, gl.GL_ONE_MINUS_DST_COLOR, gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA, gl.GL_DST_ALPHA, gl.GL_ONE_MINUS_DST_ALPHA, gl.GL_CONSTANT_COLOR, gl.GL_ONE_MINUS_CONSTANT_COLOR, gl.GL_CONSTANT_ALPHA, gl.GL_ONE_MINUS_CONSTANT_ALPHA, gl.GL_SRC_ALPHA_SATURATE }
end

local glFrame = function(gl)
  local frame = frameData()

  -- clear background
  gl:glClearColor(bgColor.x, bgColor.y, bgColor.z, 1)
  gl:glClear(gl.GL_COLOR_BUFFER_BIT)
  gl:glViewport(0, 0, frame.windowSize.x, frame.windowSize.y)

  gl:glDisable(gl.GL_BLEND)

  -- draw bottom quad
  if (bgQuadDraw == true) then
    glProg:glBegin()
    glProg:glSetSampler(0, bgQuadTex.id)
    glProg:glSetUniform("scale", bgQuadScale)
    glProg:glDrawMesh(mesh.id)
    glProg:glEnd()
  end

  -- set blend
  if (enableBlend == true) then
    gl:glBlendColor(blendColor.x, blendColor.y, blendColor.z, blendColor.w)
    gl:glEnable(gl.GL_BLEND)
  end
  gl:glBlendEquationSeparate(vBlendEquation[imodeRGB + 1], vBlendEquation[imodeAlpha + 1])
  gl:glBlendFuncSeparate(vBlendFunction[isrcRGB + 1], vBlendFunction[idstRGB + 1], vBlendFunction[isrcAlpha + 1], vBlendFunction[idstAlpha + 1])

  -- draw top quad
  glProg:glBegin()
  glProg:glSetSampler(0, quadTex.id)
  glProg:glSetUniform("scale", 1.0)
  glProg:glDrawMesh(mesh.id)
  glProg:glEnd()
end

local glTerminate = function(gl)
  --print("glTerminate")
end

setRendering(glInit, glFrame, glTerminate)

--------------------------------------------------------------------------------
-- gui

local firstRun = true
local blendEqValues = "GL_FUNC_ADD\0GL_FUNC_SUBTRACT\0GL_FUNC_REVERSE_SUBTRACT\0\0"
local blendFuncValues = "GL_ZERO\0GL_ONE\0GL_SRC_COLOR\0GL_ONE_MINUS_SRC_COLOR\0GL_DST_COLOR\0GL_ONE_MINUS_DST_COLOR\0GL_SRC_ALPHA\0GL_ONE_MINUS_SRC_ALPHA\0GL_DST_ALPHA\0GL_ONE_MINUS_DST_ALPHA\0GL_CONSTANT_COLOR\0GL_ONE_MINUS_CONSTANT_COLOR\0GL_CONSTANT_ALPHA\0GL_ONE_MINUS_CONSTANT_ALPHA\0GL_SRC_ALPHA_SATURATE\0\0"

local glGuiDraw = function(gl)

  if (firstRun == true) then
    ImGui.SetNextWindowPos(Vec2(0, 0))
    firstRun = false
  end

  local frame = frameData()
  ImGui.Begin("Blend Equation", ImGui.ImGuiWindowFlags_AlwaysAutoResize)
  bgColor = ImGui.ColorEdit3("Background Color", bgColor, ImGui.ImGuiColorEditFlags__OptionsDefault)
  bgQuadDraw = ImGui.Checkbox("Draw Background Quad", bgQuadDraw)
  bgQuadScale = ImGui.SliderFloat("Background Quad Scale", bgQuadScale, 0, 2)
  ImGui.Separator()
  enableBlend = ImGui.Checkbox("glEnable(GL_BLEND)", enableBlend)
  ImGui.Text("glBlendEquationSeparate(modeRGB, modeAlpha)")
  imodeRGB = ImGui.Combo("modeRGB", imodeRGB, blendEqValues)
  imodeAlpha = ImGui.Combo("modeAlpha", imodeAlpha, blendEqValues)
  ImGui.Text("glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha)")
  isrcRGB = ImGui.Combo("srcRGB", isrcRGB, blendFuncValues)
  idstRGB = ImGui.Combo("dstRGB", idstRGB, blendFuncValues)
  isrcAlpha = ImGui.Combo("srcAlpha", isrcAlpha, blendFuncValues)
  idstAlpha = ImGui.Combo("dstAlpha", idstAlpha, blendFuncValues)
  blendColor = ImGui.ColorEdit4("GL_CONSTANT_COLOR", blendColor, ImGui.ImGuiColorEditFlags__OptionsDefault)
  ImGui.End()
end

setImGuiDraw(glGuiDraw)
