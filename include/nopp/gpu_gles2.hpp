#pragma once

#include <set>
#include <thread>
#include "event.hpp"
#include "gpu.hpp"
#include "log.hpp"

#if defined(WITH_SDL)
#include "support_sdl.hpp"
#endif

#if defined(_WIN32)
#define GL_APIENTRY __stdcall
#else // _WIN32
#define GL_APIENTRY
#endif // _WIN32

namespace nopp
{
namespace gles2
{

using GLboolean = uint8_t;
using GLchar = char;
using GLenum = uint32_t;
using GLint = int32_t;
// using GLintptr = ptrdiff_t;
using GLsizei = int32_t;
using GLsizeiptr = ptrdiff_t;
using GLubyte = uint8_t;
using GLuint = uint32_t;
using GLvoid = void;
using GLfloat = float;
using GLclampf = float;
using GLbitfield = uint32_t;

enum glenums : uint32_t
{
    GL_NONE = 0,
    GL_NO_ERROR = 0,
    GL_ZERO = 0,
    GL_FALSE = 0,
    GL_TRUE = 1,
    GL_ONE = 1,

    GL_POINTS = 0x0000,
    GL_LINES = 0x0001,
    GL_LINE_LOOP = 0x0002,
    GL_LINE_STRIP = 0x0003,
    GL_TRIANGLES = 0x0004,
    GL_TRIANGLE_STRIP = 0x0005,
    GL_TRIANGLE_FAN = 0x0006,

    GL_NEVER = 0x0200,
    GL_LESS = 0x0201,
    GL_EQUAL = 0x0202,
    GL_LEQUAL = 0x0203,
    GL_GREATER = 0x0204,
    GL_NOTEQUAL = 0x0205,
    GL_GEQUAL = 0x0206,
    GL_ALWAYS = 0x0207,

    GL_SRC_COLOR = 0x0300,
    GL_ONE_MINUS_SRC_COLOR = 0x0301,
    GL_SRC_ALPHA = 0x0302,
    GL_ONE_MINUS_SRC_ALPHA = 0x0303,
    GL_DST_ALPHA = 0x0304,
    GL_ONE_MINUS_DST_ALPHA = 0x0305,
    GL_DST_COLOR = 0x0306,
    GL_ONE_MINUS_DST_COLOR = 0x0307,
    GL_SRC_ALPHA_SATURATE = 0x0308,

    GL_FRONT = 0x0404,
    GL_BACK = 0x0405,

    GL_INVALID_ENUM = 0x0500,
    GL_INVALID_VALUE = 0x0501,
    GL_INVALID_OPERATION = 0x0502,
    GL_OUT_OF_MEMORY = 0x0505,

    GL_CW = 0x0900,
    GL_CCW = 0x0901,

    GL_CULL_FACE = 0x0B44,
    GL_DEPTH_TEST = 0x0B71,
    GL_STENCIL_TEST = 0x0B90,
    GL_VIEWPORT = 0x0BA2,
    GL_BLEND = 0x0BE2,

    GL_SCISSOR_BOX = 0x0C10,
    GL_SCISSOR_TEST = 0x0C11,
    GL_UNPACK_ALIGNMENT = 0x0CF5,

    GL_MAX_TEXTURE_SIZE = 0x0D33,
    GL_TEXTURE_2D = 0x0DE1,

    GL_DONT_CARE = 0x1100,
    GL_FASTEST = 0x1101,
    GL_NICEST = 0x1102,

    GL_BYTE = 0x1400,
    GL_UNSIGNED_BYTE = 0x1401,
    GL_SHORT = 0x1402,
    GL_UNSIGNED_SHORT = 0x1403,
    GL_INT = 0x1404,
    GL_UNSIGNED_INT = 0x1405,
    GL_FLOAT = 0x1406,

    GL_DEPTH_COMPONENT = 0x1902,
    GL_RGB = 0x1907,
    GL_RGBA = 0x1908,

    GL_KEEP = 0x1E00,
    GL_REPLACE = 0x1E01,
    GL_INCR = 0x1E02,
    GL_DECR = 0x1E03,

    GL_VENDOR = 0x1F00,
    GL_RENDERER = 0x1F01,
    GL_VERSION = 0x1F02,
    GL_EXTENSIONS = 0x1F03,

    GL_NEAREST = 0x2600,
    GL_LINEAR = 0x2601,
    GL_NEAREST_MIPMAP_NEAREST = 0x2700,
    GL_LINEAR_MIPMAP_NEAREST = 0x2701,
    GL_NEAREST_MIPMAP_LINEAR = 0x2702,
    GL_LINEAR_MIPMAP_LINEAR = 0x2703,
    GL_TEXTURE_MAG_FILTER = 0x2800,
    GL_TEXTURE_MIN_FILTER = 0x2801,
    GL_TEXTURE_WRAP_S = 0x2802,
    GL_TEXTURE_WRAP_T = 0x2803,
    GL_REPEAT = 0x2901,

    GL_CONSTANT_COLOR = 0x8001,
    GL_ONE_MINUS_CONSTANT_COLOR = 0x8002,
    GL_CONSTANT_ALPHA = 0x8003,
    GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004,
    GL_BLEND_COLOR = 0x8005,
    GL_FUNC_ADD = 0x8006,
    GL_BLEND_EQUATION = 0x8009,
    GL_BLEND_EQUATION_RGB = 0x8009,
    GL_FUNC_SUBTRACT = 0x800A,
    GL_FUNC_REVERSE_SUBTRACT = 0x800B,

    GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034,
    GL_POLYGON_OFFSET_FILL = 0x8037,
    GL_POLYGON_OFFSET_FACTOR = 0x8038,
    GL_RGB5_A1 = 0x8057,
    GL_TEXTURE_BINDING_2D = 0x8069,
    GL_BLEND_DST_RGB = 0x80C8,
    GL_BLEND_SRC_RGB = 0x80C9,
    GL_BLEND_DST_ALPHA = 0x80CA,
    GL_BLEND_SRC_ALPHA = 0x80CB,

    GL_CLAMP_TO_EDGE = 0x812F,
    GL_GENERATE_MIPMAP_HINT = 0x8192,
    GL_DEPTH_COMPONENT16 = 0x81A5,
    GL_UNSIGNED_SHORT_5_6_5 = 0x8363,
    GL_MIRRORED_REPEAT = 0x8370,
    // GL_COMPRESSED_RGB_S3TC_DXT1_EXT  = 0x83F0,
    // GL_COMPRESSED_RGBA_S3TC_DXT1_EXT = 0x83F1,
    // GL_COMPRESSED_RGBA_S3TC_DXT3_EXT = 0x83F2,
    // GL_COMPRESSED_RGBA_S3TC_DXT5_EXT = 0x83F3,

    GL_TEXTURE0 = 0x84C0,
    GL_ACTIVE_TEXTURE = 0x84E0,
    GL_TEXTURE_CUBE_MAP = 0x8513,
    GL_TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A,
    GL_MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C,
    GL_NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2,
    GL_COMPRESSED_TEXTURE_FORMATS = 0x86A3,

    GL_BLEND_EQUATION_ALPHA = 0x883D,

    GL_ARRAY_BUFFER = 0x8892,
    GL_ELEMENT_ARRAY_BUFFER = 0x8893,
    GL_ARRAY_BUFFER_BINDING = 0x8894,
    GL_ELEMENT_ARRAY_BUFFER_BINDING = 0x8895,
    GL_STREAM_DRAW = 0x88E0,
    GL_STATIC_DRAW = 0x88E4,
    GL_DYNAMIC_DRAW = 0x88E8,

    GL_FRAGMENT_SHADER = 0x8B30,
    GL_VERTEX_SHADER = 0x8B31,
    GL_FLOAT_VEC2 = 0x8B50,
    GL_FLOAT_VEC3 = 0x8B51,
    GL_FLOAT_VEC4 = 0x8B52,
    GL_INT_VEC2 = 0x8B53,
    GL_INT_VEC3 = 0x8B54,
    GL_INT_VEC4 = 0x8B55,
    GL_FLOAT_MAT2 = 0x8B5A,
    GL_FLOAT_MAT3 = 0x8B5B,
    GL_FLOAT_MAT4 = 0x8B5C,
    GL_SAMPLER_2D = 0x8B5E,
    GL_SAMPLER_CUBE = 0x8B60,

    GL_COMPILE_STATUS = 0x8B81,
    GL_LINK_STATUS = 0x8B82,
    GL_INFO_LOG_LENGTH = 0x8B84,
    GL_ACTIVE_UNIFORMS = 0x8B86,
    GL_ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87,
    GL_ACTIVE_ATTRIBUTES = 0x8B89,
    GL_ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A,
    GL_SHADING_LANGUAGE_VERSION = 0x8B8C,
    GL_CURRENT_PROGRAM = 0x8B8D,

    GL_FRAMEBUFFER_BINDING = 0x8CA6,
    GL_FRAMEBUFFER_COMPLETE = 0x8CD5,
    GL_COLOR_ATTACHMENT0 = 0x8CE0,
    GL_DEPTH_ATTACHMENT = 0x8D00,
    GL_STENCIL_ATTACHMENT = 0x8D20,
    GL_FRAMEBUFFER = 0x8D40,
    GL_RENDERBUFFER = 0x8D41,
    GL_STENCIL_INDEX8 = 0x8D48,

    GL_DEPTH_BUFFER_BIT = 0x00000100,
    GL_STENCIL_BUFFER_BIT = 0x00000400,
    GL_COLOR_BUFFER_BIT = 0x00004000,

    // extensions
    GL_ETC1_RGB8_OES = 0x8D64,
};

class GLData;
struct GL
{
    void(GL_APIENTRY* glActiveTexture)(GLenum texture) = nullptr;
    void(GL_APIENTRY* glAttachShader)(GLuint program, GLuint shader) = nullptr;

    void(GL_APIENTRY* glBindAttribLocation)(GLuint program,
                                            GLuint index,
                                            GLchar const* name) = nullptr;
    void(GL_APIENTRY* glBindBuffer)(GLenum target, GLuint buffer) = nullptr;
    void(GL_APIENTRY* glBindFramebuffer)(GLenum target, GLuint framebuffer) = nullptr;
    void(GL_APIENTRY* glBindRenderbuffer)(GLenum target, GLuint renderbuffer) = nullptr;
    void(GL_APIENTRY* glBindTexture)(GLenum target, GLuint texture) = nullptr;
    void(GL_APIENTRY* glBlendColor)(GLclampf red,
                                    GLclampf green,
                                    GLclampf blue,
                                    GLclampf alpha) = nullptr;
    void(GL_APIENTRY* glBlendEquation)(GLenum mode) = nullptr;
    void(GL_APIENTRY* glBlendEquationSeparate)(GLenum modeRGB, GLenum modeAlpha) = nullptr;
    void(GL_APIENTRY* glBlendFunc)(GLenum sfactor, GLenum dfactor) = nullptr;
    void(GL_APIENTRY* glBlendFuncSeparate)(GLenum srcRGB,
                                           GLenum dstRGB,
                                           GLenum srcAlpha,
                                           GLenum dstAlpha) = nullptr;
    void(GL_APIENTRY* glBufferData)(GLenum target,
                                    GLsizeiptr size,
                                    GLvoid const* data,
                                    GLenum usage) = nullptr;

    GLenum(GL_APIENTRY* glCheckFramebufferStatus)(GLenum target) = nullptr;
    void(GL_APIENTRY* glClear)(GLbitfield mask) = nullptr;
    void(GL_APIENTRY* glClearColor)(GLclampf red,
                                    GLclampf green,
                                    GLclampf blue,
                                    GLclampf alpha) = nullptr;
    void(GL_APIENTRY* glClearDepthf)(GLclampf depth) = nullptr;
    void(GL_APIENTRY* glClearStencil)(GLint s) = nullptr;
    void(GL_APIENTRY* glColorMask)(GLboolean red,
                                   GLboolean green,
                                   GLboolean blue,
                                   GLboolean alpha) = nullptr;
    void(GL_APIENTRY* glCompileShader)(GLuint shader) = nullptr;
    void(GL_APIENTRY* glCompressedTexImage2D)(GLenum target,
                                              GLint level,
                                              GLint internalformat,
                                              GLsizei width,
                                              GLsizei height,
                                              GLint border,
                                              GLsizei imageSize,
                                              GLvoid const* data) = nullptr;
    GLuint(GL_APIENTRY* glCreateProgram)() = nullptr;
    GLuint(GL_APIENTRY* glCreateShader)(GLenum type) = nullptr;
    void(GL_APIENTRY* glCullFace)(GLenum mode) = nullptr;

    void(GL_APIENTRY* glDeleteBuffers)(GLsizei n, GLuint const* buffers) = nullptr;
    void(GL_APIENTRY* glDeleteFramebuffers)(GLsizei n, GLuint const* framebuffers) = nullptr;
    void(GL_APIENTRY* glDeleteProgram)(GLuint program) = nullptr;
    void(GL_APIENTRY* glDeleteRenderbuffers)(GLsizei n, GLuint const* renderbuffers) = nullptr;
    void(GL_APIENTRY* glDeleteShader)(GLuint shader) = nullptr;
    void(GL_APIENTRY* glDeleteTextures)(GLsizei n, GLuint const* textures) = nullptr;
    void(GL_APIENTRY* glDepthFunc)(GLenum func) = nullptr;
    void(GL_APIENTRY* glDepthMask)(GLboolean flag) = nullptr;
    void(GL_APIENTRY* glDepthRangef)(GLclampf zNear, GLclampf zFar) = nullptr;
    void(GL_APIENTRY* glDetachShader)(GLuint program, GLuint shader) = nullptr;
    void(GL_APIENTRY* glDisable)(GLenum cap) = nullptr;
    void(GL_APIENTRY* glDisableVertexAttribArray)(GLuint index) = nullptr;
    void(GL_APIENTRY* glDrawArrays)(GLenum mode, GLint first, GLsizei count) = nullptr;
    void(GL_APIENTRY* glDrawElements)(GLenum mode,
                                      GLsizei count,
                                      GLenum type,
                                      GLvoid const* indices) = nullptr;

    void(GL_APIENTRY* glEnable)(GLenum cap) = nullptr;
    void(GL_APIENTRY* glEnableVertexAttribArray)(GLuint index) = nullptr;

    void(GL_APIENTRY* glFinish)() = nullptr;
    void(GL_APIENTRY* glFlush)() = nullptr;
    void(GL_APIENTRY* glFramebufferRenderbuffer)(GLenum target,
                                                 GLenum attachment,
                                                 GLenum renderbuffertarget,
                                                 GLuint renderbuffer) = nullptr;
    void(GL_APIENTRY* glFramebufferTexture2D)(
        GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) = nullptr;
    void(GL_APIENTRY* glFrontFace)(GLenum mode) = nullptr;

    void(GL_APIENTRY* glGenBuffers)(GLsizei n, GLuint* buffers) = nullptr;
    void(GL_APIENTRY* glGenerateMipmap)(GLenum target) = nullptr;
    void(GL_APIENTRY* glGenFramebuffers)(GLsizei n, GLuint* framebuffers) = nullptr;
    void(GL_APIENTRY* glGenRenderbuffers)(GLsizei n, GLuint* renderbuffers) = nullptr;
    void(GL_APIENTRY* glGenTextures)(GLsizei n, GLuint* textures) = nullptr;
    void(GL_APIENTRY* glGetActiveAttrib)(GLuint program,
                                         GLuint index,
                                         GLsizei bufsize,
                                         GLsizei* length,
                                         GLint* size,
                                         GLenum* type,
                                         GLchar* name) = nullptr;
    void(GL_APIENTRY* glGetActiveUniform)(GLuint program,
                                          GLuint index,
                                          GLsizei bufsize,
                                          GLsizei* length,
                                          GLint* size,
                                          GLenum* type,
                                          GLchar* name) = nullptr;
    GLint(GL_APIENTRY* glGetAttribLocation)(GLuint program, GLchar const* name) = nullptr;
    GLenum(GL_APIENTRY* glGetError)() = nullptr;
    void(GL_APIENTRY* glGetIntegerv)(GLenum pname, GLint* params) = nullptr;
    void(GL_APIENTRY* glGetProgramInfoLog)(GLuint program,
                                           GLsizei bufSize,
                                           GLsizei* length,
                                           GLchar* infoLog) = nullptr;
    void(GL_APIENTRY* glGetProgramiv)(GLuint program, GLenum pname, GLint* params) = nullptr;
    void(GL_APIENTRY* glGetShaderInfoLog)(GLuint shader,
                                          GLsizei bufSize,
                                          GLsizei* length,
                                          GLchar* infoLog) = nullptr;
    void(GL_APIENTRY* glGetShaderiv)(GLuint shader, GLenum pname, GLint* params) = nullptr;
    GLubyte const*(GL_APIENTRY* glGetString)(GLenum name) = nullptr;
    GLint(GL_APIENTRY* glGetUniformLocation)(GLuint program, GLchar const* name) = nullptr;

    void(GL_APIENTRY* glHint)(GLenum target, GLenum mode) = nullptr;

    GLboolean(GL_APIENTRY* glIsEnabled)(GLenum cap) = nullptr;

    void(GL_APIENTRY* glLinkProgram)(GLuint program) = nullptr;

    void(GL_APIENTRY* glPixelStorei)(GLenum pname, GLint param) = nullptr;
    void(GL_APIENTRY* glPolygonOffset)(GLfloat factor, GLfloat units) = nullptr;

    void(GL_APIENTRY* glReadPixels)(GLint x,
                                    GLint y,
                                    GLsizei width,
                                    GLsizei height,
                                    GLenum format,
                                    GLenum type,
                                    GLvoid* pixels) = nullptr;
    void(GL_APIENTRY* glRenderbufferStorage)(GLenum target,
                                             GLenum internalformat,
                                             GLsizei width,
                                             GLsizei height) = nullptr;

    void(GL_APIENTRY* glSampleCoverage)(GLclampf value, GLboolean invert) = nullptr;
    void(GL_APIENTRY* glScissor)(GLint x, GLint y, GLsizei width, GLsizei height) = nullptr;
    void(GL_APIENTRY* glShaderSource)(GLuint shader,
                                      GLsizei count,
                                      GLchar const* const* string,
                                      GLint const* length) = nullptr;
    void(GL_APIENTRY* glStencilFunc)(GLenum func, GLint ref, GLuint mask) = nullptr;
    void(GL_APIENTRY* glStencilOp)(GLenum fail, GLenum zfail, GLenum zpass) = nullptr;

    void(GL_APIENTRY* glTexImage2D)(GLenum target,
                                    GLint level,
                                    GLint internalformat,
                                    GLsizei width,
                                    GLsizei height,
                                    GLint border,
                                    GLenum format,
                                    GLenum type,
                                    void const* pixels) = nullptr;
    void(GL_APIENTRY* glTexParameteri)(GLenum target, GLenum pname, GLint param) = nullptr;

    void(GL_APIENTRY* glUniform1fv)(GLint location, GLsizei count, GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform2fv)(GLint location, GLsizei count, GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform3fv)(GLint location, GLsizei count, GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform4fv)(GLint location, GLsizei count, GLfloat const* v) = nullptr;
    void(GL_APIENTRY* glUniform1i)(GLint location, GLint x) = nullptr;
    void(GL_APIENTRY* glUniform1iv)(GLint location, GLsizei count, GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniform2iv)(GLint location, GLsizei count, GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniform3iv)(GLint location, GLsizei count, GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniform4iv)(GLint location, GLsizei count, GLint const* v) = nullptr;
    void(GL_APIENTRY* glUniformMatrix2fv)(GLint location,
                                          GLsizei count,
                                          GLboolean transpose,
                                          GLfloat const* value) = nullptr;
    void(GL_APIENTRY* glUniformMatrix3fv)(GLint location,
                                          GLsizei count,
                                          GLboolean transpose,
                                          GLfloat const* value) = nullptr;
    void(GL_APIENTRY* glUniformMatrix4fv)(GLint location,
                                          GLsizei count,
                                          GLboolean transpose,
                                          GLfloat const* value) = nullptr;
    void(GL_APIENTRY* glUseProgram)(GLuint program) = nullptr;

    void(GL_APIENTRY* glVertexAttribPointer)(GLuint index,
                                             GLint size,
                                             GLenum type,
                                             GLboolean normalized,
                                             GLsizei stride,
                                             GLvoid const* ptr) = nullptr;
    void(GL_APIENTRY* glViewport)(GLint x, GLint y, GLsizei width, GLsizei height) = nullptr;

    // window system's specific functions
#if defined(WITH_SDL)
    std::function<void(SDL_Window*)> wglMakeCurrent;
    std::function<void(SDL_Window*)> wglSwapWindow;
#endif

    // thread's specific functions
    std::function<GLData*(Id)> glData;
};

#if defined(NDEBUG)
template <typename... T>
void glCheckError(T&&...)
{
}
#else  // NDEBUG
void glCheckError(GL const& gl, char const* func)
{
    auto glerr = gl.glGetError();
    if (glerr == GL_NO_ERROR)
    {
        return;
    }
    logError("[GL]%s: \"%#06x\"", func, glerr);
}
#endif // NDEBUG

// The base class of all data/resource from OpenGL
class GLData
{
  public:
    virtual ~GLData() = default;

    virtual void glUpdate(GPUData* gpuData) = 0;
};

// Mirror the MeshData class with
// - one or more vertex buffer
// - one index buffer
// - a vertex layout (with attributes)
// - a dedicated index buffer for wireframe
class GLMesh final : public GLData
{
  public:
    struct GLVertexComponent final
    {
        GLenum type = 0;
        GLint size = 0; // number of values
    };

    struct GLVertexAttribute final
    {
        GLuint glVBId = 0;
        GLint size = 0;
        GLenum type = 0;
        GLboolean normalized = 0;
        GLsizei stride = 0;
        size_t offset = 0;
    };

    GLMesh(GL const& gl) : _gl(gl) {}

    // Creates a mesh with the data of a MeshData instance
    GLMesh(GL const& gl, GPUData* gpuData) : GLMesh(gl)
    {
        assert(gpuData != nullptr);
        glUpdate(gpuData);
    }

    // To create a mesh with only one vertex buffer of floats with a specific layout
    GLMesh(GL const& gl,
           std::vector<float> const& vertexData,
           std::vector<GLVertexComponent> const& vertexLayout)
        : GLMesh(gl)
    {
        // create the (single) vertex buffer
        auto glVBId = GLuint(0);
        _gl.glGenBuffers(1, &glVBId);
        _gl.glBindBuffer(GL_ARRAY_BUFFER, glVBId);
        // fill the vertex buffer with the vertex data
        auto vbsize = vertexData.size() * sizeof(vertexData.front());
        _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, vertexData.data(), GL_STATIC_DRAW);
        // convert the layout to attributes
        glVertexAttributes = _getGLVertexAttributes(glVBId, vertexLayout);
        // add the vertex buffer to the mesh
        glVBIds.emplace_back(glVBId);
        count = static_cast<GLsizei>(vbsize) / glVertexAttributes[0].stride;
    }

    // To create a mesh with one vertex buffer and one index buffer with a specific layout
    GLMesh(GL const& gl,
           std::vector<float> const& vertexData,
           std::vector<GLVertexComponent> const& vertexLayout,
           std::vector<uint16_t> const& indexData)
        : GLMesh(gl)
    {
        // create the vertex buffer
        auto glVBId = GLuint(0);
        _gl.glGenBuffers(1, &glVBId);
        _gl.glBindBuffer(GL_ARRAY_BUFFER, glVBId);
        // fill the vertex buffer with the vertex data
        auto vbsize = vertexData.size() * sizeof(vertexData.front());
        _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, vertexData.data(), GL_STATIC_DRAW);
        // convert the layout to attributes
        glVertexAttributes = _getGLVertexAttributes(glVBId, vertexLayout);
        // add the vertex buffer to the mesh
        glVBIds.emplace_back(glVBId);
        // create the index buffer
        _gl.glGenBuffers(1, &glIBId);
        _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glIBId);
        // fill the index buffer with the index data
        auto ibsize = indexData.size() * sizeof(indexData.front());
        _gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibsize, indexData.data(), GL_STATIC_DRAW);
        count = static_cast<GLsizei>(indexData.size());
    }

    ~GLMesh() override { _glDelete(); }

    // update the data of the GLMesh with the data of a GPUMesh instance
    void glUpdate(GPUData* gpuData) override
    {
        assert(gpuData != nullptr);
        // if the data is not from the type of GPUMesh, then something gone wrong
        assert(gpuData->isTypeOf<GPUMesh>() == true);
        // delete previous data (if any)
        _glDelete();
        auto& mesh = static_cast<GPUMesh*>(gpuData)->mesh;
        auto& desc = mesh.desc;
        // create and fill each vertex buffer and attribute with according data from the Mesh
        if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Position) == true)
        {
            auto vbId = GLuint(0);
            _gl.glGenBuffers(1, &vbId);
            _gl.glBindBuffer(GL_ARRAY_BUFFER, vbId);
            auto vbsize = mesh.positions.size() * sizeof(Vec3);
            _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, mesh.positions.data(), GL_STATIC_DRAW);
            glVBIds.emplace_back(vbId);
            // create attribute for position
            auto va = GLVertexAttribute();
            va.glVBId = vbId;
            va.type = GL_FLOAT;
            va.size = 3;
            va.stride = sizeof(Vec3);
            glVertexAttributes.emplace_back(va);
        }
        else
        {
            glVertexAttributes.emplace_back(GLVertexAttribute());
        }

        if (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Color) == true)
        {
            auto vbId = GLuint(0);
            _gl.glGenBuffers(1, &vbId);
            _gl.glBindBuffer(GL_ARRAY_BUFFER, vbId);
            auto vbsize = mesh.colors.size() * sizeof(Vec4);
            _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, mesh.colors.data(), GL_STATIC_DRAW);
            glVBIds.emplace_back(vbId);
            // create attribute for color
            auto va = GLVertexAttribute();
            va.glVBId = vbId;
            va.type = GL_FLOAT;
            va.size = 4;
            va.stride = sizeof(Vec4);
            glVertexAttributes.emplace_back(va);
        }
        else
        {
            glVertexAttributes.emplace_back(GLVertexAttribute());
        }

        if ((hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Normal) == true) ||
            (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::Tangent) == true) ||
            (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::TexCoord0) == true) ||
            (hasEnumFlag(desc.vertexDeclaration, eVertexDeclaration::TexCoord1) == true))
        {
            auto stride = static_cast<GLsizei>(sizeof(Mesh::VtxData));
            auto vbId = GLuint(0);
            _gl.glGenBuffers(1, &vbId);
            _gl.glBindBuffer(GL_ARRAY_BUFFER, vbId);
            auto vbsize = mesh.datas.size() * stride;
            _gl.glBufferData(GL_ARRAY_BUFFER, vbsize, mesh.datas.data(), GL_STATIC_DRAW);
            glVBIds.emplace_back(vbId);
            // create attribute for tex coord 0/1
            auto va = GLVertexAttribute();
            va.glVBId = vbId;
            va.type = GL_FLOAT;
            va.size = 4;
            va.offset = 0;
            va.stride = stride;
            glVertexAttributes.emplace_back(va);
            // create attribute for normal
            va = GLVertexAttribute();
            va.glVBId = vbId;
            va.type = GL_UNSIGNED_BYTE;
            va.size = 4;
            va.offset = sizeof(Vec4);
            va.normalized = GL_TRUE;
            va.stride = stride;
            glVertexAttributes.emplace_back(va);
            // create attribute for tangent
            va.offset = sizeof(Vec4) + sizeof(uint32_t);
            glVertexAttributes.emplace_back(va);
        }
        else
        {
            glVertexAttributes.emplace_back(GLVertexAttribute());
            glVertexAttributes.emplace_back(GLVertexAttribute());
            glVertexAttributes.emplace_back(GLVertexAttribute());
        }
        // create and fill the index buffer
        if (desc.indexCount != 0)
        {
            _gl.glGenBuffers(1, &glIBId);
            _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glIBId);
            auto ibsize = mesh.indexes.size() * sizeof(uint16_t);
            _gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibsize, mesh.indexes.data(), GL_STATIC_DRAW);
            count = static_cast<GLsizei>(desc.indexCount);
        }
        else
        {
            count = static_cast<GLsizei>(desc.vertexCount);
        }
        // create and fill the wireframe index buffer
        auto nbWireframe = mesh.wireframeIndexes.size();
        if (nbWireframe > 0)
        {
            _gl.glGenBuffers(1, &glIBWireframeId);
            _gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glIBWireframeId);
            auto ibsize = nbWireframe * sizeof(uint16_t);
            _gl.glBufferData(
                GL_ELEMENT_ARRAY_BUFFER, ibsize, mesh.wireframeIndexes.data(), GL_STATIC_DRAW);
            wireframeCount = static_cast<GLsizei>(nbWireframe);
        }
        // convert the primitive type to OpenGL draw mode
        mode = _getGLMode(mesh.desc.primitiveType);
    }

  public:
    std::vector<GLVertexAttribute> glVertexAttributes;
    std::vector<GLuint> glVBIds;
    GLuint glIBId = 0;
    GLsizei count = 0;
    GLenum mode = GL_TRIANGLES;
    GLuint glIBWireframeId = 0;
    GLsizei wireframeCount = 0;

  private:
    void _glDelete()
    {
        // delete all vertex buffers
        for (auto& vb : glVBIds)
        {
            _gl.glDeleteBuffers(1, &vb);
        }
        glVBIds.clear();
        // delete index buffer
        if (glIBId != 0)
        {
            _gl.glDeleteBuffers(1, &glIBId);
        }
        glIBId = 0;
        // clear all attributes
        glVertexAttributes.clear();
        count = 0;
        // delete wireframe index buffer
        if (glIBWireframeId != 0)
        {
            _gl.glDeleteBuffers(1, &glIBWireframeId);
        }
        wireframeCount = 0;
    }

    GL const& _gl;

    static std::vector<GLMesh::GLVertexAttribute>
    _getGLVertexAttributes(GLuint glVBId, std::vector<GLMesh::GLVertexComponent> const& layout)
    {
        auto vas = std::vector<GLMesh::GLVertexAttribute>();
        vas.reserve(layout.size());
        auto offset = 0;
        for (auto& vc : layout)
        {
            auto va = GLMesh::GLVertexAttribute();
            va.glVBId = glVBId;
            va.size = vc.size;
            va.type = vc.type;
            va.offset = offset;
            switch (va.type)
            {
            case GL_BYTE:
            case GL_UNSIGNED_BYTE: offset += va.size * sizeof(uint8_t); break;
            case GL_SHORT:
            case GL_UNSIGNED_SHORT: offset += va.size * sizeof(uint16_t); break;
            case GL_FLOAT: offset += va.size * sizeof(float); break;
            default: return std::vector<GLMesh::GLVertexAttribute>(); break;
            }
            vas.emplace_back(va);
        }
        for (auto& va : vas)
        {
            va.stride = static_cast<GLsizei>(offset);
        }
        return vas;
    }

    static GLenum _getGLMode(eMeshTopology const topology)
    {
        switch (topology)
        {
        case eMeshTopology::Points: return GL_POINTS; break;
        case eMeshTopology::Lines: return GL_LINES; break;
        case eMeshTopology::Triangles: return GL_TRIANGLES; break;
        default: return GL_NONE;
        }
    }
};

struct GLFence final
{
    Event event;

    GLFence() { event.raise(); }

    bool isReached() const { return event.isRaised(); }

    void wait() { event.wait(); }

    void waitFor(int32_t ms) { event.waitFor(ms); }
};

using GLCmd = std::function<void(GL const&)>;

namespace impl
{
using ProcGLActiveTexture = void(GL_APIENTRY*)(GLenum texture);
using ProcGLAttachShader = void(GL_APIENTRY*)(GLuint program, GLuint shader);
using ProcGLBindAttribLocation = void(GL_APIENTRY*)(GLuint program,
                                                    GLuint index,
                                                    GLchar const* name);

using ProcGLBindBuffer = void(GL_APIENTRY*)(GLenum target, GLuint buffer);
using ProcGLBindFramebuffer = void(GL_APIENTRY*)(GLenum target, GLuint framebuffer);
using ProcGLBindRenderbuffer = void(GL_APIENTRY*)(GLenum target, GLuint renderbuffer);
using ProcGLBindTexture = void(GL_APIENTRY*)(GLenum target, GLuint texture);
using ProcGLBlendColor = void(GL_APIENTRY*)(GLclampf red,
                                            GLclampf green,
                                            GLclampf blue,
                                            GLclampf alpha);
using ProcGLBlendEquation = void(GL_APIENTRY*)(GLenum mode);
using ProcGLBlendEquationSeparate = void(GL_APIENTRY*)(GLenum modeRGB, GLenum modeAlpha);
using ProcGLBlendFunc = void(GL_APIENTRY*)(GLenum sfactor, GLenum dfactor);
using ProcGLBlendFuncSeparate = void(GL_APIENTRY*)(GLenum srcRGB,
                                                   GLenum dstRGB,
                                                   GLenum srcAlpha,
                                                   GLenum dstAlpha);
using ProcGLBufferData = void(GL_APIENTRY*)(GLenum target,
                                            GLsizeiptr size,
                                            GLvoid const* data,
                                            GLenum usage);

using ProcGLCheckFramebufferStatus = GLenum(GL_APIENTRY*)(GLenum target);
using ProcGLClear = void(GL_APIENTRY*)(GLbitfield mask);
using ProcGLClearColor = void(GL_APIENTRY*)(GLclampf red,
                                            GLclampf green,
                                            GLclampf blue,
                                            GLclampf alpha);
using ProcGLClearDepthf = void(GL_APIENTRY*)(GLclampf depth);
using ProcGLClearStencil = void(GL_APIENTRY*)(GLint s);
using ProcGLColorMask = void(GL_APIENTRY*)(GLboolean red,
                                           GLboolean green,
                                           GLboolean blue,
                                           GLboolean alpha);
using ProcGLCompileShader = void(GL_APIENTRY*)(GLuint shader);
using ProcGLCompressedTexImage2D = void(GL_APIENTRY*)(GLenum target,
                                                      GLint level,
                                                      GLint internalformat,
                                                      GLsizei width,
                                                      GLsizei height,
                                                      GLint border,
                                                      GLsizei imageSize,
                                                      GLvoid const* data);
using ProcGLCreateProgram = GLuint(GL_APIENTRY*)();
using ProcGLCreateShader = GLuint(GL_APIENTRY*)(GLenum type);
using ProcGLCullFace = void(GL_APIENTRY*)(GLenum mode);

using ProcGLDeleteBuffers = void(GL_APIENTRY*)(GLsizei n, GLuint const* buffers);
using ProcGLDeleteFramebuffers = void(GL_APIENTRY*)(GLsizei n, GLuint const* framebuffers);
using ProcGLDeleteProgram = void(GL_APIENTRY*)(GLuint program);
using ProcGLDeleteRenderbuffers = void(GL_APIENTRY*)(GLsizei n, GLuint const* renderbuffers);
using ProcGLDeleteShader = void(GL_APIENTRY*)(GLuint shader);
using ProcGLDeleteTextures = void(GL_APIENTRY*)(GLsizei n, GLuint const* textures);
using ProcGLDepthFunc = void(GL_APIENTRY*)(GLenum func);
using ProcGLDepthMask = void(GL_APIENTRY*)(GLboolean flag);
using ProcGLDepthRangef = void(GL_APIENTRY*)(GLclampf zNear, GLclampf zFar);
using ProcGLDetachShader = void(GL_APIENTRY*)(GLuint program, GLuint shader);
using ProcGLDisable = void(GL_APIENTRY*)(GLenum cap);
using ProcGLDisableVertexAttribArray = void(GL_APIENTRY*)(GLuint index);
using ProcGLDrawArrays = void(GL_APIENTRY*)(GLenum mode, GLint first, GLsizei count);
using ProcGLDrawElements = void(GL_APIENTRY*)(GLenum mode,
                                              GLsizei count,
                                              GLenum type,
                                              GLvoid const* indices);

using ProcGLEnable = void(GL_APIENTRY*)(GLenum cap);
using ProcGLEnableVertexAttribArray = void(GL_APIENTRY*)(GLuint index);

using ProcGLFinish = void(GL_APIENTRY*)();
using ProcGLFlush = void(GL_APIENTRY*)();
using ProcGLFramebufferRenderbuffer = void(GL_APIENTRY*)(GLenum target,
                                                         GLenum attachment,
                                                         GLenum renderbuffertarget,
                                                         GLuint renderbuffer);
using ProcGLFramebufferTexture2D = void(GL_APIENTRY*)(
    GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
using ProcGLFrontFace = void(GL_APIENTRY*)(GLenum mode);

using ProcGLGenBuffers = void(GL_APIENTRY*)(GLsizei n, GLuint* buffers);
using ProcGLGenerateMipmap = void(GL_APIENTRY*)(GLenum target);
using ProcGLGenFramebuffers = void(GL_APIENTRY*)(GLsizei n, GLuint* framebuffers);
using ProcGLGenRenderbuffers = void(GL_APIENTRY*)(GLsizei n, GLuint* renderbuffers);
using ProcGLGenTextures = void(GL_APIENTRY*)(GLsizei n, GLuint* textures);
using ProcGLGetActiveAttrib = void(GL_APIENTRY*)(GLuint program,
                                                 GLuint index,
                                                 GLsizei bufsize,
                                                 GLsizei* length,
                                                 GLint* size,
                                                 GLenum* type,
                                                 GLchar* name);
using ProcGLGetActiveUniform = void(GL_APIENTRY*)(GLuint program,
                                                  GLuint index,
                                                  GLsizei bufsize,
                                                  GLsizei* length,
                                                  GLint* size,
                                                  GLenum* type,
                                                  GLchar* name);
using ProcGLGetAttribLocation = GLint(GL_APIENTRY*)(GLuint program, GLchar const* name);
using ProcGLGetError = GLenum(GL_APIENTRY*)();
using ProcGLGetIntegerv = void(GL_APIENTRY*)(GLenum pname, GLint* data);
using ProcGLGetProgramInfoLog = void(GL_APIENTRY*)(GLuint program,
                                                   GLsizei bufSize,
                                                   GLsizei* length,
                                                   GLchar* infoLog);
using ProcGLGetProgramiv = void(GL_APIENTRY*)(GLuint program, GLenum pname, GLint* params);
using ProcGLGetShaderInfoLog = void(GL_APIENTRY*)(GLuint shader,
                                                  GLsizei bufSize,
                                                  GLsizei* length,
                                                  GLchar* infoLog);
using ProcGLGetShaderiv = void(GL_APIENTRY*)(GLuint shader, GLenum pname, GLint* params);
using ProcGLGetString = GLubyte const*(GL_APIENTRY*)(GLenum name);
using ProcGLGetUniformLocation = GLint(GL_APIENTRY*)(GLuint program, GLchar const* name);

using ProcGLHint = void(GL_APIENTRY*)(GLenum target, GLenum mode);

using ProcGLIsEnabled = GLboolean(GL_APIENTRY*)(GLenum cap);

using ProcGLLinkProgram = void(GL_APIENTRY*)(GLuint program);

using ProcGLPixelStorei = void(GL_APIENTRY*)(GLenum pname, GLint param);
using ProcGLPolygonOffset = void(GL_APIENTRY*)(GLfloat factor, GLfloat units);

using ProcGLReadPixels = void(GL_APIENTRY*)(
    GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid* pixels);
using ProcGLRenderbufferStorage = void(GL_APIENTRY*)(GLenum target,
                                                     GLenum internalformat,
                                                     GLsizei width,
                                                     GLsizei height);

using ProcGLSampleCoverage = void(GL_APIENTRY*)(GLclampf value, GLboolean invert);
using ProcGLScissor = void(GL_APIENTRY*)(GLint x, GLint y, GLsizei width, GLsizei height);
using ProcGLShaderSource = void(GL_APIENTRY*)(GLuint shader,
                                              GLsizei count,
                                              GLchar const* const* string,
                                              GLint const* length);
using ProcGLStencilFunc = void(GL_APIENTRY*)(GLenum func, GLint ref, GLuint mask);
using ProcGLStencilOp = void(GL_APIENTRY*)(GLenum fail, GLenum zfail, GLenum zpass);
using ProcGLTexImage2D = void(GL_APIENTRY*)(GLenum target,
                                            GLint level,
                                            GLint internalformat,
                                            GLsizei width,
                                            GLsizei height,
                                            GLint border,
                                            GLenum format,
                                            GLenum type,
                                            void const* pixels);
using ProcGLTexParameteri = void(GL_APIENTRY*)(GLenum target, GLenum pname, GLint param);
using ProcGLUniform1fv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLfloat const* v);
using ProcGLUniform2fv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLfloat const* v);
using ProcGLUniform3fv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLfloat const* v);
using ProcGLUniform4fv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLfloat const* v);
using ProcGLUniform1i = void(GL_APIENTRY*)(GLint location, GLint x);
using ProcGLUniform1iv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLint const* v);
using ProcGLUniform2iv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLint const* v);
using ProcGLUniform3iv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLint const* v);
using ProcGLUniform4iv = void(GL_APIENTRY*)(GLint location, GLsizei count, GLint const* v);
using ProcGLUniformMatrix2fv = void(GL_APIENTRY*)(GLint location,
                                                  GLsizei count,
                                                  GLboolean transpose,
                                                  GLfloat const* value);
using ProcGLUniformMatrix3fv = void(GL_APIENTRY*)(GLint location,
                                                  GLsizei count,
                                                  GLboolean transpose,
                                                  GLfloat const* value);
using ProcGLUniformMatrix4fv = void(GL_APIENTRY*)(GLint location,
                                                  GLsizei count,
                                                  GLboolean transpose,
                                                  GLfloat const* value);
using ProcGLUseProgram = void(GL_APIENTRY*)(GLuint program);
using ProcGLVertexAttribPointer = void(GL_APIENTRY*)(
    GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLvoid const* ptr);
using ProcGLViewport = void(GL_APIENTRY*)(GLint x, GLint y, GLsizei width, GLsizei height);

// extensions
using ProcGLGetTranslatedShaderSourceANGLE = void(GL_APIENTRY*)(GLuint shader,
                                                                GLsizei bufSize,
                                                                GLsizei* length,
                                                                GLchar* source);
using ProcGLGetProgramBinaryOES = void(GL_APIENTRY*)(
    GLuint program, GLsizei bufSize, GLsizei* length, GLenum* binaryFormat, void* binary);

#if defined(WITH_SDL)
GL createGL()
{
    auto hal = GL{};
    hal.glActiveTexture =
        reinterpret_cast<ProcGLActiveTexture>(SDL_GL_GetProcAddress("glActiveTexture"));
    hal.glAttachShader =
        reinterpret_cast<ProcGLAttachShader>(SDL_GL_GetProcAddress("glAttachShader"));

    hal.glBindAttribLocation =
        reinterpret_cast<ProcGLBindAttribLocation>(SDL_GL_GetProcAddress("glBindAttribLocation"));
    hal.glBindBuffer = reinterpret_cast<ProcGLBindBuffer>(SDL_GL_GetProcAddress("glBindBuffer"));
    hal.glBindFramebuffer =
        reinterpret_cast<ProcGLBindFramebuffer>(SDL_GL_GetProcAddress("glBindFramebuffer"));
    hal.glBindRenderbuffer =
        reinterpret_cast<ProcGLBindRenderbuffer>(SDL_GL_GetProcAddress("glBindRenderbuffer"));
    hal.glBindTexture = reinterpret_cast<ProcGLBindTexture>(SDL_GL_GetProcAddress("glBindTexture"));
    hal.glBlendColor = reinterpret_cast<ProcGLBlendColor>(SDL_GL_GetProcAddress("glBlendColor"));
    hal.glBlendEquation =
        reinterpret_cast<ProcGLBlendEquation>(SDL_GL_GetProcAddress("glBlendEquation"));
    hal.glBlendEquationSeparate = reinterpret_cast<ProcGLBlendEquationSeparate>(
        SDL_GL_GetProcAddress("glBlendEquationSeparate"));
    hal.glBlendFunc = reinterpret_cast<ProcGLBlendFunc>(SDL_GL_GetProcAddress("glBlendFunc"));
    hal.glBlendFuncSeparate =
        reinterpret_cast<ProcGLBlendFuncSeparate>(SDL_GL_GetProcAddress("glBlendFuncSeparate"));
    hal.glBufferData = reinterpret_cast<ProcGLBufferData>(SDL_GL_GetProcAddress("glBufferData"));

    hal.glCheckFramebufferStatus = reinterpret_cast<ProcGLCheckFramebufferStatus>(
        SDL_GL_GetProcAddress("glCheckFramebufferStatus"));
    hal.glClear = reinterpret_cast<ProcGLClear>(SDL_GL_GetProcAddress("glClear"));
    hal.glClearColor = reinterpret_cast<ProcGLClearColor>(SDL_GL_GetProcAddress("glClearColor"));
    hal.glClearDepthf = reinterpret_cast<ProcGLClearDepthf>(SDL_GL_GetProcAddress("glClearDepthf"));
    hal.glClearStencil =
        reinterpret_cast<ProcGLClearStencil>(SDL_GL_GetProcAddress("glClearStencil"));
    hal.glColorMask = reinterpret_cast<ProcGLColorMask>(SDL_GL_GetProcAddress("glColorMask"));
    hal.glCompileShader =
        reinterpret_cast<ProcGLCompileShader>(SDL_GL_GetProcAddress("glCompileShader"));
    hal.glCompressedTexImage2D = reinterpret_cast<ProcGLCompressedTexImage2D>(
        SDL_GL_GetProcAddress("glCompressedTexImage2D"));
    hal.glCreateProgram =
        reinterpret_cast<ProcGLCreateProgram>(SDL_GL_GetProcAddress("glCreateProgram"));
    hal.glCreateShader =
        reinterpret_cast<ProcGLCreateShader>(SDL_GL_GetProcAddress("glCreateShader"));
    hal.glCullFace = reinterpret_cast<ProcGLCullFace>(SDL_GL_GetProcAddress("glCullFace"));

    hal.glDeleteBuffers =
        reinterpret_cast<ProcGLDeleteBuffers>(SDL_GL_GetProcAddress("glDeleteBuffers"));
    hal.glDeleteFramebuffers =
        reinterpret_cast<ProcGLDeleteFramebuffers>(SDL_GL_GetProcAddress("glDeleteFramebuffers"));
    hal.glDeleteProgram =
        reinterpret_cast<ProcGLDeleteProgram>(SDL_GL_GetProcAddress("glDeleteProgram"));
    hal.glDeleteRenderbuffers =
        reinterpret_cast<ProcGLDeleteRenderbuffers>(SDL_GL_GetProcAddress("glDeleteRenderbuffers"));
    hal.glDeleteShader =
        reinterpret_cast<ProcGLDeleteShader>(SDL_GL_GetProcAddress("glDeleteShader"));
    hal.glDeleteTextures =
        reinterpret_cast<ProcGLDeleteTextures>(SDL_GL_GetProcAddress("glDeleteTextures"));
    hal.glDepthFunc = reinterpret_cast<ProcGLDepthFunc>(SDL_GL_GetProcAddress("glDepthFunc"));
    hal.glDepthMask = reinterpret_cast<ProcGLDepthMask>(SDL_GL_GetProcAddress("glDepthMask"));
    hal.glDepthRangef = reinterpret_cast<ProcGLDepthRangef>(SDL_GL_GetProcAddress("glDepthRangef"));
    hal.glDetachShader =
        reinterpret_cast<ProcGLDetachShader>(SDL_GL_GetProcAddress("glDetachShader"));
    hal.glDisable = reinterpret_cast<ProcGLDisable>(SDL_GL_GetProcAddress("glDisable"));
    hal.glDisableVertexAttribArray = reinterpret_cast<ProcGLDisableVertexAttribArray>(
        SDL_GL_GetProcAddress("glDisableVertexAttribArray"));
    hal.glDrawArrays = reinterpret_cast<ProcGLDrawArrays>(SDL_GL_GetProcAddress("glDrawArrays"));
    hal.glDrawElements =
        reinterpret_cast<ProcGLDrawElements>(SDL_GL_GetProcAddress("glDrawElements"));

    hal.glEnable = reinterpret_cast<ProcGLEnable>(SDL_GL_GetProcAddress("glEnable"));
    hal.glEnableVertexAttribArray = reinterpret_cast<ProcGLEnableVertexAttribArray>(
        SDL_GL_GetProcAddress("glEnableVertexAttribArray"));

    hal.glFinish = reinterpret_cast<ProcGLFinish>(SDL_GL_GetProcAddress("glFinish"));
    hal.glFlush = reinterpret_cast<ProcGLFlush>(SDL_GL_GetProcAddress("glFlush"));
    hal.glFramebufferRenderbuffer = reinterpret_cast<ProcGLFramebufferRenderbuffer>(
        SDL_GL_GetProcAddress("glFramebufferRenderbuffer"));
    hal.glFramebufferTexture2D = reinterpret_cast<ProcGLFramebufferTexture2D>(
        SDL_GL_GetProcAddress("glFramebufferTexture2D"));
    hal.glFrontFace = reinterpret_cast<ProcGLFrontFace>(SDL_GL_GetProcAddress("glFrontFace"));

    hal.glGenBuffers = reinterpret_cast<ProcGLGenBuffers>(SDL_GL_GetProcAddress("glGenBuffers"));
    hal.glGenerateMipmap =
        reinterpret_cast<ProcGLGenerateMipmap>(SDL_GL_GetProcAddress("glGenerateMipmap"));
    hal.glGenFramebuffers =
        reinterpret_cast<ProcGLGenFramebuffers>(SDL_GL_GetProcAddress("glGenFramebuffers"));
    hal.glGenRenderbuffers =
        reinterpret_cast<ProcGLGenRenderbuffers>(SDL_GL_GetProcAddress("glGenRenderbuffers"));
    hal.glGenTextures = reinterpret_cast<ProcGLGenTextures>(SDL_GL_GetProcAddress("glGenTextures"));
    hal.glGetActiveAttrib =
        reinterpret_cast<ProcGLGetActiveAttrib>(SDL_GL_GetProcAddress("glGetActiveAttrib"));
    hal.glGetActiveUniform =
        reinterpret_cast<ProcGLGetActiveUniform>(SDL_GL_GetProcAddress("glGetActiveUniform"));
    hal.glGetAttribLocation =
        reinterpret_cast<ProcGLGetAttribLocation>(SDL_GL_GetProcAddress("glGetAttribLocation"));
    hal.glGetError = reinterpret_cast<ProcGLGetError>(SDL_GL_GetProcAddress("glGetError"));
    hal.glGetIntegerv = reinterpret_cast<ProcGLGetIntegerv>(SDL_GL_GetProcAddress("glGetIntegerv"));
    hal.glGetProgramInfoLog =
        reinterpret_cast<ProcGLGetProgramInfoLog>(SDL_GL_GetProcAddress("glGetProgramInfoLog"));
    hal.glGetProgramiv =
        reinterpret_cast<ProcGLGetProgramiv>(SDL_GL_GetProcAddress("glGetProgramiv"));
    hal.glGetShaderInfoLog =
        reinterpret_cast<ProcGLGetShaderInfoLog>(SDL_GL_GetProcAddress("glGetShaderInfoLog"));
    hal.glGetShaderiv = reinterpret_cast<ProcGLGetShaderiv>(SDL_GL_GetProcAddress("glGetShaderiv"));
    hal.glGetString = reinterpret_cast<ProcGLGetString>(SDL_GL_GetProcAddress("glGetString"));
    hal.glGetUniformLocation =
        reinterpret_cast<ProcGLGetUniformLocation>(SDL_GL_GetProcAddress("glGetUniformLocation"));

    hal.glHint = reinterpret_cast<ProcGLHint>(SDL_GL_GetProcAddress("glHint"));

    hal.glIsEnabled = reinterpret_cast<ProcGLIsEnabled>(SDL_GL_GetProcAddress("glIsEnabled"));

    hal.glLinkProgram = reinterpret_cast<ProcGLLinkProgram>(SDL_GL_GetProcAddress("glLinkProgram"));

    hal.glPixelStorei = reinterpret_cast<ProcGLPixelStorei>(SDL_GL_GetProcAddress("glPixelStorei"));
    hal.glPolygonOffset =
        reinterpret_cast<ProcGLPolygonOffset>(SDL_GL_GetProcAddress("glPolygonOffset"));

    hal.glReadPixels = reinterpret_cast<ProcGLReadPixels>(SDL_GL_GetProcAddress("glReadPixels"));
    hal.glRenderbufferStorage =
        reinterpret_cast<ProcGLRenderbufferStorage>(SDL_GL_GetProcAddress("glRenderbufferStorage"));

    hal.glSampleCoverage =
        reinterpret_cast<ProcGLSampleCoverage>(SDL_GL_GetProcAddress("glSampleCoverage"));
    hal.glScissor = reinterpret_cast<ProcGLScissor>(SDL_GL_GetProcAddress("glScissor"));
    hal.glShaderSource =
        reinterpret_cast<ProcGLShaderSource>(SDL_GL_GetProcAddress("glShaderSource"));
    hal.glStencilFunc = reinterpret_cast<ProcGLStencilFunc>(SDL_GL_GetProcAddress("glStencilFunc"));
    hal.glStencilOp = reinterpret_cast<ProcGLStencilOp>(SDL_GL_GetProcAddress("glStencilOp"));

    hal.glTexImage2D = reinterpret_cast<ProcGLTexImage2D>(SDL_GL_GetProcAddress("glTexImage2D"));
    hal.glTexParameteri =
        reinterpret_cast<ProcGLTexParameteri>(SDL_GL_GetProcAddress("glTexParameteri"));

    hal.glUniform1fv = reinterpret_cast<ProcGLUniform1fv>(SDL_GL_GetProcAddress("glUniform1fv"));
    hal.glUniform2fv = reinterpret_cast<ProcGLUniform2fv>(SDL_GL_GetProcAddress("glUniform2fv"));
    hal.glUniform3fv = reinterpret_cast<ProcGLUniform3fv>(SDL_GL_GetProcAddress("glUniform3fv"));
    hal.glUniform4fv = reinterpret_cast<ProcGLUniform4fv>(SDL_GL_GetProcAddress("glUniform4fv"));
    hal.glUniform1i = reinterpret_cast<ProcGLUniform1i>(SDL_GL_GetProcAddress("glUniform1i"));
    hal.glUniform1iv = reinterpret_cast<ProcGLUniform1iv>(SDL_GL_GetProcAddress("glUniform1iv"));
    hal.glUniform2iv = reinterpret_cast<ProcGLUniform2iv>(SDL_GL_GetProcAddress("glUniform2iv"));
    hal.glUniform3iv = reinterpret_cast<ProcGLUniform3iv>(SDL_GL_GetProcAddress("glUniform3iv"));
    hal.glUniform4iv = reinterpret_cast<ProcGLUniform4iv>(SDL_GL_GetProcAddress("glUniform4iv"));
    hal.glUniformMatrix2fv =
        reinterpret_cast<ProcGLUniformMatrix2fv>(SDL_GL_GetProcAddress("glUniformMatrix2fv"));
    hal.glUniformMatrix3fv =
        reinterpret_cast<ProcGLUniformMatrix3fv>(SDL_GL_GetProcAddress("glUniformMatrix3fv"));
    hal.glUniformMatrix4fv =
        reinterpret_cast<ProcGLUniformMatrix4fv>(SDL_GL_GetProcAddress("glUniformMatrix4fv"));
    hal.glUseProgram = reinterpret_cast<ProcGLUseProgram>(SDL_GL_GetProcAddress("glUseProgram"));

    hal.glVertexAttribPointer =
        reinterpret_cast<ProcGLVertexAttribPointer>(SDL_GL_GetProcAddress("glVertexAttribPointer"));
    hal.glViewport = reinterpret_cast<ProcGLViewport>(SDL_GL_GetProcAddress("glViewport"));
    return hal;
}
#endif

} // namespace impl

class GLThread : public GPUThread
{
  public:
    GLThread()
    {
        _glDatas.reserve(1024);
        _thread = new std::thread(&GLThread::_funcThread, this);
    }

    ~GLThread() override
    {
        _isThreadInterrupted = true;
        _swapCond.notify_one();
        if (_thread->joinable())
        {
            _thread->join();
        }
        else
        {
            logError("GLThread::~GLThread: thread not joinable");
        }
        delete _thread;
    }

    void push(std::vector<GLCmd> const& cmds, GLFence* fence)
    {
        std::lock_guard<std::mutex> lock{_swapMutex};
        append(_cmdsWait, cmds);
        if (fence != nullptr)
        {
            fence->event.reset();
            _cmdsWait.emplace_back([=](GL const&) { fence->event.raise(); });
        }
        _swapCond.notify_one();
    }

    void push(std::vector<GLCmd> const& cmds) { push(cmds, nullptr); }

    // void push(GLCmd const& cmd, GLFence* fence) { push({cmd}, fence); }
    // void push(GLCmd const& cmd) { push({cmd}, nullptr); }

    void setOnTerminate(std::vector<GLCmd> const& cmds) { _cmdsOnTerminate = cmds; }

    // void pushOnTerminate(GLCmd const& cmd) { pushOnTerminate({cmd}); }

    // update will be done on gl thread
    void updateData(Id id) override
    {
        std::lock_guard<std::mutex> lock(_updateMutex);
        if (_getData(id) != nullptr)
        {
            _tmpUpdateIds.insert(id);
        }
    }

    // destroy will be done on gl thread
    void destroyData(Id id) override
    {
        std::lock_guard<std::mutex> lock(_destroyMutex);
        if (_getData(id) != nullptr)
        {
            _tmpDestroyIds.insert(id);
        }
    }

  private:
    void _funcThread()
    {
        _glInitialize();
        while (true)
        {
            auto cmdsWork = std::vector<GLCmd>();
            // swap data from main thread
            {
                std::unique_lock<std::mutex> lock(_swapMutex);
                while ((_cmdsWait.empty() == true) && (_isThreadInterrupted == false))
                {
                    _swapCond.wait(lock);
                }
                if (_isThreadInterrupted == true)
                {
                    _glTerminate();
                    logDebug("stopping opengl es thread");
                    return;
                }
                cmdsWork.reserve(_cmdsWait.size()); // small optim
                cmdsWork.swap(_cmdsWait);
            }

            // then get update/destroy data for this frame
            {
                std::lock_guard<std::mutex> lock{_updateMutex};
                _updateIds.swap(_tmpUpdateIds);
            }
            {
                std::lock_guard<std::mutex> lock{_destroyMutex};
                _destroyIds.swap(_tmpDestroyIds);
            }
            for (auto id : _updateIds)
            {
                auto data = _getData(id);
                if (data == nullptr)
                {
                    continue;
                }
                auto srcData = GPUData::getGPUData(id);
                if (srcData != nullptr)
                {
                    data->glUpdate(srcData);
                    // logDebug("update %d", id);
                }
                else
                {
                    logDebug("null src data");
                    auto iter = _destroyIds.find(id);
                    if (iter != _destroyIds.cend())
                    {
                        logDebug("won't be deleted!");
                    }
                    else
                    {
                        logDebug("but will be deleted...");
                    }
                }
            }
            _updateIds.clear();

            // process cmds
            for (auto& cmd : cmdsWork)
            {
                cmd(_gl);
            }
        }
    }

    void _glInitialize()
    {
#if defined(WITH_SDL)
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);

        auto window = SDL_CreateWindow("",
                                       SDL_WINDOWPOS_UNDEFINED,
                                       SDL_WINDOWPOS_UNDEFINED,
                                       16,
                                       16,
                                       SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN);
        _glContext = SDL_GL_CreateContext(window);
        _gl = impl::createGL();
        _gl.wglMakeCurrent = [&](SDL_Window* window)
        {
            assert(window != nullptr);
            auto curWindow = SDL_GL_GetCurrentWindow();
            auto curContext = SDL_GL_GetCurrentContext();
            if ((curWindow == window) && (curContext == _glContext))
            {
                return;
            }
            auto r = SDL_GL_MakeCurrent(window, _glContext);
        };
        _gl.wglSwapWindow = [&](SDL_Window* window) { SDL_GL_SwapWindow(window); };
        _gl.glData = [&](Id id)
        {
            auto data = _getData(id);
            if (data != nullptr)
            {
                return data;
            }
            auto& dst = _glDatas[id.index()];
            auto const src = GPUData::getGPUData(id);
            assert(src != nullptr);
            if (src->isTypeOf<GPUMesh>() == true)
            {
                dst = new GLMesh{_gl, src};
            }
            // else if (src->isTypeOf<TextureData>() == true)
            //{
            //     dst = new GLTexture{_gl, src};
            // }
            return dst;
        };

        logInfo("[GL]vendor: %s", _gl.glGetString(GL_VENDOR));
        logInfo("[GL]renderer: %s", _gl.glGetString(GL_RENDERER));
        logInfo("[GL]version: %s", _gl.glGetString(GL_VERSION));
        logInfo("[GL]shading language: %s", _gl.glGetString(GL_SHADING_LANGUAGE_VERSION));
        // compressed format supported
        {
            auto count = GLint{0};
            _gl.glGetIntegerv(GL_NUM_COMPRESSED_TEXTURE_FORMATS, &count);
            auto texFormats = std::vector<GLint>();
            texFormats.resize(count);
            _gl.glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, texFormats.data());
        }
        SDL_DestroyWindow(window);
#endif
    }

    void _glTerminate()
    {
        // call all terminate commands
        for (auto& cmd : _cmdsOnTerminate)
        {
            cmd(_gl);
        }
        // delete all remaining data if any
        for (auto& data : _glDatas)
        {
            if (data != nullptr)
            {
                delete data;
                data = nullptr;
            }
        }
#if defined(WITH_SDL)
        if (_glContext != nullptr)
        {
            _gl = GL{};
            SDL_GL_DeleteContext(_glContext);
            _glContext = nullptr;
        }
#endif
        logInfo("[GL]terminate");

        // if(_hGLRC != nullptr)
        //{
        //    delete _gl;
        //    _gl = nullptr;
        //    delete _wgl;
        //    _wgl = nullptr;
        //    wglDeleteContext(_hGLRC);
        //    _hGLRC = nullptr;
        //}
        // logInfo("shutdown GL");
        ////if(_glOffFramebuffer != 0)
        ////{
        ////	glDeleteFramebuffers(1, &_glOffFramebuffer);
        ////	glDeleteTextures(1, &_glOffColor);
        ////	glDeleteRenderbuffers(1, &_glOffDepth);
        ////}
        //
        //
        ////checkGLError("GLThread::_glShutdown");
        ////_eglShutdown();
    }

    GLData* const _getData(Id id)
    {
        auto index = id.index();
        if (_glDatas.size() <= index)
        {
            _glDatas.resize(index + 1);
        }
        return _glDatas[index];
    }

    std::vector<GLCmd> _cmdsWait;
    std::vector<GLCmd> _cmdsOnTerminate;
    std::thread* _thread = nullptr;
    std::mutex _swapMutex;
    std::condition_variable _swapCond;
    std::atomic<bool> _isThreadInterrupted{false};

    std::set<Id> _tmpUpdateIds;
    std::set<Id> _updateIds;
    std::mutex _updateMutex;
    std::set<Id> _tmpDestroyIds;
    std::set<Id> _destroyIds;
    std::mutex _destroyMutex;

    // gl data
#if defined(WITH_SDL)
    SDL_GLContext _glContext = nullptr;
#endif
    GL _gl;
    std::vector<GLData*> _glDatas;
};

} // namespace gles2
} // namespace nopp
