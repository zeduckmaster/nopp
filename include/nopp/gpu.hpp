#pragma once

#include <deque>
#include <vector>
#include "container.hpp"
#include "fast_rtti.hpp"
#include "id.hpp"
#include "ref_object.hpp"

namespace nopp
{

class GPUData : public RefObject
{
    FAST_RTTI_CLASS_ROOT(GPUData)

  public:
    ~GPUData() override
    {
        if (_id == IdInvalid)
        {
            return;
        }
        auto p = _gpuDataManager.remove(_id);
        assert(p == this);
    }

    GPUData(GPUData const&) = delete;

    GPUData(GPUData&& gpuData) : _id(std::move(gpuData._id)) { gpuData._id = IdInvalid; }

    GPUData& operator=(GPUData const&) = delete;

    GPUData& operator=(GPUData&& gpuData)
    {
        assert(this != &gpuData);
        if (_id != IdInvalid)
        {
            auto p = _gpuDataManager.remove(_id);
            assert(p == this);
        }
        _id = gpuData._id;
        gpuData._id = IdInvalid;
        return *this;
    }

    Id id() const { return _id; }

    operator Id() const { return _id; }

    inline void commit();

    static bool isGPUDataIdValid(Id id) { return _gpuDataManager.isIdValid(id); }

    static GPUData* getGPUData(Id id) { return _gpuDataManager.data(id); }

  protected:
    GPUData() { _id = _gpuDataManager.add(this); }

  private:
    Id _id;

    struct _GPUDataRemover
    {
        inline void operator()(Id id);
    };

    inline static auto _gpuDataManager = SimpleIdDataManager<GPUData*, _GPUDataRemover>(1024);
};

class GPUThread
{
  public:
    virtual ~GPUThread() { swapAndPopBack(_gpuThreads, this); }

    virtual void updateData(Id id) = 0;

    virtual void destroyData(Id id) = 0;

    void updateData(GPUData const* gpuData)
    {
        assert(gpuData != nullptr);
        updateData(gpuData->id());
    }

    void destroyData(GPUData const* gpuData)
    {
        assert(gpuData != nullptr);
        destroyData(gpuData->id());
    }

    static std::vector<GPUThread*> const& currentThreads() { return _gpuThreads; }

  protected:
    GPUThread() { _gpuThreads.emplace_back(this); }

  private:
    inline static std::vector<GPUThread*> _gpuThreads;
};

inline void GPUData::commit()
{
    for (auto& gpuThread : GPUThread::currentThreads())
    {
        gpuThread->updateData(_id);
    }
}

void GPUData::_GPUDataRemover::operator()(Id id)
{
    for (auto& gpuThread : GPUThread::currentThreads())
    {
        gpuThread->destroyData(id);
    }
}

} // namespace nopp

////////////////////////////////////////////////////////////////////////////////////////////////////

#include "mesh.hpp"
#include "texture.hpp"

namespace nopp
{

class GPUMesh final : public GPUData
{
    FAST_RTTI_CLASS(GPUMesh, GPUData)

  public:
    Mesh mesh;
};

class GPUTexture final : public GPUData
{
    FAST_RTTI_CLASS(GPUTexture, GPUData)

  public:
    Texture texture;
};

} // namespace nopp
