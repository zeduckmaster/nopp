#include <catch2/catch.hpp>
#include "nopp/hash_string.hpp"

using namespace nopp;

TEST_CASE("compute and replace by hash", "[hash_string]")
{
    auto result = computeHash("foo::bar");
    REQUIRE(result == replaceByHash("foo::bar"));
    REQUIRE(result != replaceByHash("foo::bat"));
}
