--------------------------------------------------------------------------------
-- rendering

local glPrgObjVColor
local glPrgObjMesh

local glInitialize = function(gl)
  print("glInitialize")
  glPrgObjVColor = gl:Program(dataPath() .. "/prg_obj_vcolor.gles100")
  glPrgObjMesh = gl:Program(dataPath() .. "/prg_obj_mesh.gles100")
end

local glFrame = function(gl)
  --print("glFrame")
  local frame = frameData()
  
  -- clear background
  gl:glClearColor(0, 0, 0, 1)
  gl:glViewport(0, 0, frame.windowSize.x, frame.windowSize.y)
  gl:glEnable(gl.GL_DEPTH_TEST)
  gl:glDepthFunc(gl.GL_LEQUAL)
  gl:glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
  
  local matView = frame:renderDataAsMat4("matView")
  local matProj = frame:renderDataAsMat4("matProj")  
  
  -- draw object
  local meshBin = frame:findRenderBin("mesh")
  glPrgObjMesh:glBegin()
  glPrgObjMesh:glSetUniform("matView", matView)
  glPrgObjMesh:glSetUniform("matProj", matProj)
  for i, v in ipairs(meshBin.objects) do
    glPrgObjMesh:glDrawMeshWireframe(v.mesh)
  end
  glPrgObjMesh:glEnd()
  
  -- draw grid
  local vcolorBin = frame:findRenderBin("vcolor")
  glPrgObjVColor:glBegin()
  glPrgObjVColor:glSetUniform("matView", matView)
  glPrgObjVColor:glSetUniform("matProj", matProj)
  for i, v in ipairs(vcolorBin.objects) do
    glPrgObjVColor:glDrawMesh(v.mesh)
  end
  glPrgObjVColor:glEnd()
end

local glTerminate = function(gl)
  print("glTerminate")
end

setRendering(glInitialize, glFrame, glTerminate)

--------------------------------------------------------------------------------
-- gui

local firstRun = true

local glGuiDraw = function(gl)
  if (firstRun == true) then
    ImGui.SetNextWindowPos(Vec2(0, 0))
    firstRun = false
  end

  --local frame = frameData()
  --ImGui.Begin("Fire Shader", ImGui.ImGuiWindowFlags_AlwaysAutoResize)
  --timeSpeed = ImGui.SliderFloat("timeSpeed", timeSpeed, 0.1, 3.0)
  --normalStrength = ImGui.SliderFloat("normalStrength", normalStrength, 0.0, 50.0)
  --distortionStrength = ImGui.SliderFloat("distortionStrength", distortionStrength, 0.0, 5.0)
  --ImGui.End()
end
setImGuiDraw(glGuiDraw)
