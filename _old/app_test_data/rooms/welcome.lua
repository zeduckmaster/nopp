-------------------------------------------------------------------------------
-- room creation

local tb = Room:new()

local State =
{
   Welcome           = 0,
   ChooseCharacter   = 1,
   ValidateCharacter = 2
}

local cStart

local function setState(room, session, newState)
   local data = room:sessionData(session)
   --if (data.state == newState) then
   --   return
   --end
   if (newState == State.Welcome) then
      local msg = "YAT Aventure !\n"
      msg = msg.."Entrez 'démarrer' pour démarrer l'aventure.\n\n"
      msg = msg.."Entrez 'aide' à tout moment pour avoir la liste des commandes disponibles.\n"
      msg = msg.."Entrez 'aide [commande]' pour obtenir de l'aide sur une commande.\n"
      session:send(msg)
      room.commands.start = cStart
   elseif (newState == State.ChooseCharacter) then
      session:send("Êtes vous plutôt:\n(1) Physique - (2) Intellectuel - (3) équilibré")
      room.commands.start = nil
   end
   data.state = newState
end

cStart = function(room, session, args)
   setState(room, session, State.ChooseCharacter)
end

tb.processCommand = function(room, session, args)
   local data = room:sessionData(session)
   if (data.state == State.ChooseCharacter) then
      local n = math.tointeger(args[1])
      local char = Character:new()
      if (n ~= 1) and (n ~= 2) and (n ~= 2) then
         setState(room, session, State.ChooseCharacter)
         return
      elseif (n == 1) then
         char.phy = 15
         char.int = 5
      elseif (n == 2) then
         char.phy = 5
         char.int = 15
      end
      session.character = char
   end
   Room.processCommand(room, session, args)
end

tb.createSessionData = function(room, session)
   return { state = -1 }
end

tb.enter = function(room, session)
   Room.enter(room, session)
   setState(room, session, State.Welcome)
end





--local function processCommandCharacterValid(room, session, args)
--   local arg1 = string.lower(args[1])
--   if (arg1 == "n") then
--      room.commands.start(room, session)
--      return
--   end
--   
--   
--end

--local function processCommandCharacter(room, session, args)
--   local nn = math.tointeger(args[1])
--   local char = { phy = 10, int = 10, hp = 10 }
--   local schar = "équilibré"
--   if (nn ~= 1) and (nn ~= 2) and (nn ~= 3) then
--      room.commands.start(room, session)
--      return
--   elseif (nn == 1) then
--      char.phy = 15
--      char.int = 5
--      schar = "physique"
--   elseif (nn == 2) then
--      char.phy = 5
--      char.int = 15
--      schar = "intellectuel"
--   end
--   session.character = Character:new(char)
--   local msg = "Vous avez choisi d'être "..schar.."\n(o) oui - (n) non" 
--   session:send(msg)
--   room.processCommand = processCommandCharacterValid
--end

-------------------------------------------------------------------------------

CommandAliases["démarrer"] = "start"
CommandDescriptions["start"] = "Commencer l'histoire."

return tb