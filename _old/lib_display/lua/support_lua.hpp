#pragma once

#include <lib_core/lua/support_lua.hpp>

namespace nopp
{
void loadLuaDisplay(sol::state& luaState);

#if defined(WITH_IMGUI)
void loadLuaDisplayImGui(sol::state& luaState);
#endif

#if defined(WITH_GLES2)
namespace gles2
{
void loadLuaDisplay(sol::state& luaState);
}
#endif
}
