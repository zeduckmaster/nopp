#pragma once

#include <chrono>
#include <condition_variable>
#include <mutex>

namespace nopp
{

// A simple event class to help synchronize thread operations
// When constructed, the event is not raised.
class Event final
{
  public:
    void reset() { _isRaised = false; }

    void wait()
    {
        std::unique_lock<std::mutex> lock(_mutex);
        while (_isRaised == false)
        {
            _cond.wait(lock);
        }
    }

    void waitFor(int32_t ms)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        while (_isRaised == false)
        {
            _cond.wait_for(lock, std::chrono::milliseconds{static_cast<size_t>(ms)});
        }
    }

    void raise()
    {
        std::lock_guard<std::mutex> lock(_mutex);
        _isRaised = true;
        _cond.notify_all();
    }

    constexpr bool isRaised() const { return _isRaised; }

  private:
    std::condition_variable _cond;
    std::mutex _mutex;
    bool _isRaised = false;
};

} // namespace nopp
