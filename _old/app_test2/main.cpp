#include <iostream>
#include <csignal>
#include "io.h"
#include "helpers.h"
#include "mud.h"
#include "log.h"
//#include "clock.h"

using namespace nopp;

auto dataPath = Path{"c:/users/david/documents/nopp/app_test_data/"};
//auto dataPath = Path{"/home/duckmaster/Documents/nopp/app_test_data/"};

#if defined(_WIN32)
#include <windows.h>
void* termInput = nullptr;
void* termOutput = nullptr;
void initTerm()
{
    SetConsoleOutputCP(CP_UTF8);
    SetConsoleCP(CP_UTF8);
    termInput = GetStdHandle(STD_INPUT_HANDLE);
    termOutput = GetStdHandle(STD_OUTPUT_HANDLE);
}

std::string readTerm()
{
    wchar_t wstr[512];
    auto read = DWORD{0};
    ReadConsoleW(termInput, wstr, 512, &read, NULL);
    auto size = WideCharToMultiByte(CP_UTF8, 0, wstr, read, nullptr, 0, nullptr, nullptr);
    auto str = std::string{};
    str.resize(size);
    size = WideCharToMultiByte(CP_UTF8, 0, wstr, read, &str.front(), str.size(), nullptr, nullptr);
    while((str.back() == '\r') || (str.back() == '\n'))
        str.pop_back();
    return str;
}

void writeTerm(std::string const& value)
{
    auto size = MultiByteToWideChar(CP_UTF8, 0, &value.front(), value.size(), nullptr, 0);
    auto wstr = std::wstring{};
    wstr.resize(size);
    size = MultiByteToWideChar(CP_UTF8, 0, &value.front(), value.size(), &wstr.front(), size);
    auto written = DWORD{0};
    WriteConsoleW(termOutput, &wstr.front(), wstr.size(), &written, nullptr);
}
#else

void initTerm()
{

}

std::string readTerm()
{
    auto str = std::string{};
    //std::cin >> str;
    return str;
}

void writeTerm(std::string const& value)
{
    std::cout << value;
}

#endif

void logF(eLogLevel logLevel, char const* fmt, va_list argList)
{
    char buf[1024];
    vsnprintf(buf, 1024 - 1, fmt, argList);
    buf[1024 - 1] = '\0';
    auto slog = std::string{};
    switch(logLevel)
    {
    case eLogLevel::Info:    slog = "[I]"; break;
    case eLogLevel::Debug:   slog = "[D]"; break;
    case eLogLevel::Warning: slog = "[W]"; break;
    case eLogLevel::Error:   slog = "[E]"; break;
    case eLogLevel::Crash:   slog = "[!]"; break;
    default:
        break;
    }
    slog.append(buf);
    slog.push_back('\n');
    writeTerm(slog);
}

auto interrupted = false;
void signalHandler(int value)
{
    if(value == SIGINT)
    {
        writeTerm("CTRL+C");
        interrupted = true;
        exit(1);
    }
}

struct Args
{
    std::string lang = "fr";
};
Args parseArguments(int argc, char* argv[])
{
    if(argc == 1)
        return Args{};
    auto args = Args{};

    return args;
}

#if defined(__cplusplus)
extern "C"
#endif
int main(int argc, char* argv[])
{
    auto args = parseArguments(argc, argv);
    setLogFunction(logF);
    std::signal(SIGINT, signalHandler);
    initTerm();

    // boot world
    World world{dataPath, args.lang};
    auto sessionIO = SessionIO{};
    sessionIO.output = writeTerm;
    sessionIO.quit = [&]() { interrupted = true; };
    auto sessionId = world.addSession(sessionIO);
    // start command
    while(interrupted == false)
    {
        auto cmd = std::string{};
        writeTerm("\n> ");
        cmd = readTerm();
        world.processCommand(sessionId, cmd);
    }
    return 0;
}

