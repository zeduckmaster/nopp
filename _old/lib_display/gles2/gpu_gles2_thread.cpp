#include "../mesh.hpp"
#include "../texture.hpp"
#include "gpu_gles2.hpp"
#include <lib_core/container.hpp>
#include <lib_core/log.hpp>

using namespace nopp;
using namespace nopp::gles2;

namespace
{
using ProcGLActiveTexture = void(GL_APIENTRY*)(GLenum texture);
using ProcGLAttachShader = void(GL_APIENTRY*)(GLuint program, GLuint shader);
using ProcGLBindAttribLocation = void(GL_APIENTRY*)(GLuint program,
                                                    GLuint index,
                                                    GLchar const* name);

using ProcGLBindBuffer = void(GL_APIENTRY*)(GLenum target, GLuint buffer);
using ProcGLBindFramebuffer = void(GL_APIENTRY*)(GLenum target,
                                                 GLuint framebuffer);
using ProcGLBindRenderbuffer = void(GL_APIENTRY*)(GLenum target,
                                                  GLuint renderbuffer);
using ProcGLBindTexture = void(GL_APIENTRY*)(GLenum target, GLuint texture);
using ProcGLBlendColor = void(GL_APIENTRY*)(GLclampf red,
                                            GLclampf green,
                                            GLclampf blue,
                                            GLclampf alpha);
using ProcGLBlendEquation = void(GL_APIENTRY*)(GLenum mode);
using ProcGLBlendEquationSeparate = void(GL_APIENTRY*)(GLenum modeRGB,
                                                       GLenum modeAlpha);
using ProcGLBlendFunc = void(GL_APIENTRY*)(GLenum sfactor, GLenum dfactor);
using ProcGLBlendFuncSeparate = void(GL_APIENTRY*)(GLenum srcRGB,
                                                   GLenum dstRGB,
                                                   GLenum srcAlpha,
                                                   GLenum dstAlpha);
using ProcGLBufferData = void(GL_APIENTRY*)(GLenum target,
                                            GLsizeiptr size,
                                            GLvoid const* data,
                                            GLenum usage);

using ProcGLCheckFramebufferStatus = GLenum(GL_APIENTRY*)(GLenum target);
using ProcGLClear = void(GL_APIENTRY*)(GLbitfield mask);
using ProcGLClearColor = void(GL_APIENTRY*)(GLclampf red,
                                            GLclampf green,
                                            GLclampf blue,
                                            GLclampf alpha);
using ProcGLClearDepthf = void(GL_APIENTRY*)(GLclampf depth);
using ProcGLClearStencil = void(GL_APIENTRY*)(GLint s);
using ProcGLColorMask = void(GL_APIENTRY*)(GLboolean red,
                                           GLboolean green,
                                           GLboolean blue,
                                           GLboolean alpha);
using ProcGLCompileShader = void(GL_APIENTRY*)(GLuint shader);
using ProcGLCompressedTexImage2D = void(GL_APIENTRY*)(GLenum target,
                                                      GLint level,
                                                      GLint internalformat,
                                                      GLsizei width,
                                                      GLsizei height,
                                                      GLint border,
                                                      GLsizei imageSize,
                                                      GLvoid const* data);
using ProcGLCreateProgram = GLuint(GL_APIENTRY*)();
using ProcGLCreateShader = GLuint(GL_APIENTRY*)(GLenum type);
using ProcGLCullFace = void(GL_APIENTRY*)(GLenum mode);

using ProcGLDeleteBuffers = void(GL_APIENTRY*)(GLsizei n,
                                               GLuint const* buffers);
using ProcGLDeleteFramebuffers = void(GL_APIENTRY*)(GLsizei n,
                                                    GLuint const* framebuffers);
using ProcGLDeleteProgram = void(GL_APIENTRY*)(GLuint program);
using ProcGLDeleteRenderbuffers =
    void(GL_APIENTRY*)(GLsizei n, GLuint const* renderbuffers);
using ProcGLDeleteShader = void(GL_APIENTRY*)(GLuint shader);
using ProcGLDeleteTextures = void(GL_APIENTRY*)(GLsizei n,
                                                GLuint const* textures);
using ProcGLDepthFunc = void(GL_APIENTRY*)(GLenum func);
using ProcGLDepthMask = void(GL_APIENTRY*)(GLboolean flag);
using ProcGLDepthRangef = void(GL_APIENTRY*)(GLclampf zNear, GLclampf zFar);
using ProcGLDetachShader = void(GL_APIENTRY*)(GLuint program, GLuint shader);
using ProcGLDisable = void(GL_APIENTRY*)(GLenum cap);
using ProcGLDisableVertexAttribArray = void(GL_APIENTRY*)(GLuint index);
using ProcGLDrawArrays = void(GL_APIENTRY*)(GLenum mode,
                                            GLint first,
                                            GLsizei count);
using ProcGLDrawElements = void(GL_APIENTRY*)(GLenum mode,
                                              GLsizei count,
                                              GLenum type,
                                              GLvoid const* indices);

using ProcGLEnable = void(GL_APIENTRY*)(GLenum cap);
using ProcGLEnableVertexAttribArray = void(GL_APIENTRY*)(GLuint index);

using ProcGLFinish = void(GL_APIENTRY*)();
using ProcGLFlush = void(GL_APIENTRY*)();
using ProcGLFramebufferRenderbuffer =
    void(GL_APIENTRY*)(GLenum target,
                       GLenum attachment,
                       GLenum renderbuffertarget,
                       GLuint renderbuffer);
using ProcGLFramebufferTexture2D = void(GL_APIENTRY*)(GLenum target,
                                                      GLenum attachment,
                                                      GLenum textarget,
                                                      GLuint texture,
                                                      GLint level);
using ProcGLFrontFace = void(GL_APIENTRY*)(GLenum mode);

using ProcGLGenBuffers = void(GL_APIENTRY*)(GLsizei n, GLuint* buffers);
using ProcGLGenerateMipmap = void(GL_APIENTRY*)(GLenum target);
using ProcGLGenFramebuffers = void(GL_APIENTRY*)(GLsizei n,
                                                 GLuint* framebuffers);
using ProcGLGenRenderbuffers = void(GL_APIENTRY*)(GLsizei n,
                                                  GLuint* renderbuffers);
using ProcGLGenTextures = void(GL_APIENTRY*)(GLsizei n, GLuint* textures);
using ProcGLGetActiveAttrib = void(GL_APIENTRY*)(GLuint program,
                                                 GLuint index,
                                                 GLsizei bufsize,
                                                 GLsizei* length,
                                                 GLint* size,
                                                 GLenum* type,
                                                 GLchar* name);
using ProcGLGetActiveUniform = void(GL_APIENTRY*)(GLuint program,
                                                  GLuint index,
                                                  GLsizei bufsize,
                                                  GLsizei* length,
                                                  GLint* size,
                                                  GLenum* type,
                                                  GLchar* name);
using ProcGLGetAttribLocation = GLint(GL_APIENTRY*)(GLuint program,
                                                    GLchar const* name);
using ProcGLGetError = GLenum(GL_APIENTRY*)();
using ProcGLGetIntegerv = void(GL_APIENTRY*)(GLenum pname, GLint* data);
using ProcGLGetProgramInfoLog = void(GL_APIENTRY*)(GLuint program,
                                                   GLsizei bufSize,
                                                   GLsizei* length,
                                                   GLchar* infoLog);
using ProcGLGetProgramiv = void(GL_APIENTRY*)(GLuint program,
                                              GLenum pname,
                                              GLint* params);
using ProcGLGetShaderInfoLog = void(GL_APIENTRY*)(GLuint shader,
                                                  GLsizei bufSize,
                                                  GLsizei* length,
                                                  GLchar* infoLog);
using ProcGLGetShaderiv = void(GL_APIENTRY*)(GLuint shader,
                                             GLenum pname,
                                             GLint* params);
using ProcGLGetString = GLubyte const*(GL_APIENTRY*)(GLenum name);
using ProcGLGetUniformLocation = GLint(GL_APIENTRY*)(GLuint program,
                                                     GLchar const* name);

using ProcGLHint = void(GL_APIENTRY*)(GLenum target, GLenum mode);

using ProcGLIsEnabled = GLboolean(GL_APIENTRY*)(GLenum cap);

using ProcGLLinkProgram = void(GL_APIENTRY*)(GLuint program);

using ProcGLPixelStorei = void(GL_APIENTRY*)(GLenum pname, GLint param);
using ProcGLPolygonOffset = void(GL_APIENTRY*)(GLfloat factor, GLfloat units);

using ProcGLReadPixels = void(GL_APIENTRY*)(GLint x,
                                            GLint y,
                                            GLsizei width,
                                            GLsizei height,
                                            GLenum format,
                                            GLenum type,
                                            GLvoid* pixels);
using ProcGLRenderbufferStorage = void(GL_APIENTRY*)(GLenum target,
                                                     GLenum internalformat,
                                                     GLsizei width,
                                                     GLsizei height);

using ProcGLSampleCoverage = void(GL_APIENTRY*)(GLclampf value,
                                                GLboolean invert);
using ProcGLScissor = void(GL_APIENTRY*)(GLint x,
                                         GLint y,
                                         GLsizei width,
                                         GLsizei height);
using ProcGLShaderSource = void(GL_APIENTRY*)(GLuint shader,
                                              GLsizei count,
                                              GLchar const* const* string,
                                              GLint const* length);
using ProcGLStencilFunc = void(GL_APIENTRY*)(GLenum func,
                                             GLint ref,
                                             GLuint mask);
using ProcGLStencilOp = void(GL_APIENTRY*)(GLenum fail,
                                           GLenum zfail,
                                           GLenum zpass);
using ProcGLTexImage2D = void(GL_APIENTRY*)(GLenum target,
                                            GLint level,
                                            GLint internalformat,
                                            GLsizei width,
                                            GLsizei height,
                                            GLint border,
                                            GLenum format,
                                            GLenum type,
                                            void const* pixels);
using ProcGLTexParameteri = void(GL_APIENTRY*)(GLenum target,
                                               GLenum pname,
                                               GLint param);
using ProcGLUniform1fv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLfloat const* v);
using ProcGLUniform2fv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLfloat const* v);
using ProcGLUniform3fv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLfloat const* v);
using ProcGLUniform4fv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLfloat const* v);
using ProcGLUniform1i = void(GL_APIENTRY*)(GLint location, GLint x);
using ProcGLUniform1iv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLint const* v);
using ProcGLUniform2iv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLint const* v);
using ProcGLUniform3iv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLint const* v);
using ProcGLUniform4iv = void(GL_APIENTRY*)(GLint location,
                                            GLsizei count,
                                            GLint const* v);
using ProcGLUniformMatrix2fv = void(GL_APIENTRY*)(GLint location,
                                                  GLsizei count,
                                                  GLboolean transpose,
                                                  GLfloat const* value);
using ProcGLUniformMatrix3fv = void(GL_APIENTRY*)(GLint location,
                                                  GLsizei count,
                                                  GLboolean transpose,
                                                  GLfloat const* value);
using ProcGLUniformMatrix4fv = void(GL_APIENTRY*)(GLint location,
                                                  GLsizei count,
                                                  GLboolean transpose,
                                                  GLfloat const* value);
using ProcGLUseProgram = void(GL_APIENTRY*)(GLuint program);
using ProcGLVertexAttribPointer = void(GL_APIENTRY*)(GLuint indx,
                                                     GLint size,
                                                     GLenum type,
                                                     GLboolean normalized,
                                                     GLsizei stride,
                                                     GLvoid const* ptr);
using ProcGLViewport = void(GL_APIENTRY*)(GLint x,
                                          GLint y,
                                          GLsizei width,
                                          GLsizei height);

// extensions
using ProcGLGetTranslatedShaderSourceANGLE = void(GL_APIENTRY*)(GLuint shader,
                                                                GLsizei bufSize,
                                                                GLsizei* length,
                                                                GLchar* source);
using ProcGLGetProgramBinaryOES = void(GL_APIENTRY*)(GLuint program,
                                                     GLsizei bufSize,
                                                     GLsizei* length,
                                                     GLenum* binaryFormat,
                                                     void* binary);

GL createGL()
{
    auto hal = GL{};
    hal.glActiveTexture = reinterpret_cast<ProcGLActiveTexture>(
        SDL_GL_GetProcAddress("glActiveTexture"));
    hal.glAttachShader = reinterpret_cast<ProcGLAttachShader>(
        SDL_GL_GetProcAddress("glAttachShader"));

    hal.glBindAttribLocation = reinterpret_cast<ProcGLBindAttribLocation>(
        SDL_GL_GetProcAddress("glBindAttribLocation"));
    hal.glBindBuffer = reinterpret_cast<ProcGLBindBuffer>(
        SDL_GL_GetProcAddress("glBindBuffer"));
    hal.glBindFramebuffer = reinterpret_cast<ProcGLBindFramebuffer>(
        SDL_GL_GetProcAddress("glBindFramebuffer"));
    hal.glBindRenderbuffer = reinterpret_cast<ProcGLBindRenderbuffer>(
        SDL_GL_GetProcAddress("glBindRenderbuffer"));
    hal.glBindTexture = reinterpret_cast<ProcGLBindTexture>(
        SDL_GL_GetProcAddress("glBindTexture"));
    hal.glBlendColor = reinterpret_cast<ProcGLBlendColor>(
        SDL_GL_GetProcAddress("glBlendColor"));
    hal.glBlendEquation = reinterpret_cast<ProcGLBlendEquation>(
        SDL_GL_GetProcAddress("glBlendEquation"));
    hal.glBlendEquationSeparate = reinterpret_cast<ProcGLBlendEquationSeparate>(
        SDL_GL_GetProcAddress("glBlendEquationSeparate"));
    hal.glBlendFunc =
        reinterpret_cast<ProcGLBlendFunc>(SDL_GL_GetProcAddress("glBlendFunc"));
    hal.glBlendFuncSeparate = reinterpret_cast<ProcGLBlendFuncSeparate>(
        SDL_GL_GetProcAddress("glBlendFuncSeparate"));
    hal.glBufferData = reinterpret_cast<ProcGLBufferData>(
        SDL_GL_GetProcAddress("glBufferData"));

    hal.glCheckFramebufferStatus =
        reinterpret_cast<ProcGLCheckFramebufferStatus>(
            SDL_GL_GetProcAddress("glCheckFramebufferStatus"));
    hal.glClear =
        reinterpret_cast<ProcGLClear>(SDL_GL_GetProcAddress("glClear"));
    hal.glClearColor = reinterpret_cast<ProcGLClearColor>(
        SDL_GL_GetProcAddress("glClearColor"));
    hal.glClearDepthf = reinterpret_cast<ProcGLClearDepthf>(
        SDL_GL_GetProcAddress("glClearDepthf"));
    hal.glClearStencil = reinterpret_cast<ProcGLClearStencil>(
        SDL_GL_GetProcAddress("glClearStencil"));
    hal.glColorMask =
        reinterpret_cast<ProcGLColorMask>(SDL_GL_GetProcAddress("glColorMask"));
    hal.glCompileShader = reinterpret_cast<ProcGLCompileShader>(
        SDL_GL_GetProcAddress("glCompileShader"));
    hal.glCompressedTexImage2D = reinterpret_cast<ProcGLCompressedTexImage2D>(
        SDL_GL_GetProcAddress("glCompressedTexImage2D"));
    hal.glCreateProgram = reinterpret_cast<ProcGLCreateProgram>(
        SDL_GL_GetProcAddress("glCreateProgram"));
    hal.glCreateShader = reinterpret_cast<ProcGLCreateShader>(
        SDL_GL_GetProcAddress("glCreateShader"));
    hal.glCullFace =
        reinterpret_cast<ProcGLCullFace>(SDL_GL_GetProcAddress("glCullFace"));

    hal.glDeleteBuffers = reinterpret_cast<ProcGLDeleteBuffers>(
        SDL_GL_GetProcAddress("glDeleteBuffers"));
    hal.glDeleteFramebuffers = reinterpret_cast<ProcGLDeleteFramebuffers>(
        SDL_GL_GetProcAddress("glDeleteFramebuffers"));
    hal.glDeleteProgram = reinterpret_cast<ProcGLDeleteProgram>(
        SDL_GL_GetProcAddress("glDeleteProgram"));
    hal.glDeleteRenderbuffers = reinterpret_cast<ProcGLDeleteRenderbuffers>(
        SDL_GL_GetProcAddress("glDeleteRenderbuffers"));
    hal.glDeleteShader = reinterpret_cast<ProcGLDeleteShader>(
        SDL_GL_GetProcAddress("glDeleteShader"));
    hal.glDeleteTextures = reinterpret_cast<ProcGLDeleteTextures>(
        SDL_GL_GetProcAddress("glDeleteTextures"));
    hal.glDepthFunc =
        reinterpret_cast<ProcGLDepthFunc>(SDL_GL_GetProcAddress("glDepthFunc"));
    hal.glDepthMask =
        reinterpret_cast<ProcGLDepthMask>(SDL_GL_GetProcAddress("glDepthMask"));
    hal.glDepthRangef = reinterpret_cast<ProcGLDepthRangef>(
        SDL_GL_GetProcAddress("glDepthRangef"));
    hal.glDetachShader = reinterpret_cast<ProcGLDetachShader>(
        SDL_GL_GetProcAddress("glDetachShader"));
    hal.glDisable =
        reinterpret_cast<ProcGLDisable>(SDL_GL_GetProcAddress("glDisable"));
    hal.glDisableVertexAttribArray =
        reinterpret_cast<ProcGLDisableVertexAttribArray>(
            SDL_GL_GetProcAddress("glDisableVertexAttribArray"));
    hal.glDrawArrays = reinterpret_cast<ProcGLDrawArrays>(
        SDL_GL_GetProcAddress("glDrawArrays"));
    hal.glDrawElements = reinterpret_cast<ProcGLDrawElements>(
        SDL_GL_GetProcAddress("glDrawElements"));

    hal.glEnable =
        reinterpret_cast<ProcGLEnable>(SDL_GL_GetProcAddress("glEnable"));
    hal.glEnableVertexAttribArray =
        reinterpret_cast<ProcGLEnableVertexAttribArray>(
            SDL_GL_GetProcAddress("glEnableVertexAttribArray"));

    hal.glFinish =
        reinterpret_cast<ProcGLFinish>(SDL_GL_GetProcAddress("glFinish"));
    hal.glFlush =
        reinterpret_cast<ProcGLFlush>(SDL_GL_GetProcAddress("glFlush"));
    hal.glFramebufferRenderbuffer =
        reinterpret_cast<ProcGLFramebufferRenderbuffer>(
            SDL_GL_GetProcAddress("glFramebufferRenderbuffer"));
    hal.glFramebufferTexture2D = reinterpret_cast<ProcGLFramebufferTexture2D>(
        SDL_GL_GetProcAddress("glFramebufferTexture2D"));
    hal.glFrontFace =
        reinterpret_cast<ProcGLFrontFace>(SDL_GL_GetProcAddress("glFrontFace"));

    hal.glGenBuffers = reinterpret_cast<ProcGLGenBuffers>(
        SDL_GL_GetProcAddress("glGenBuffers"));
    hal.glGenerateMipmap = reinterpret_cast<ProcGLGenerateMipmap>(
        SDL_GL_GetProcAddress("glGenerateMipmap"));
    hal.glGenFramebuffers = reinterpret_cast<ProcGLGenFramebuffers>(
        SDL_GL_GetProcAddress("glGenFramebuffers"));
    hal.glGenRenderbuffers = reinterpret_cast<ProcGLGenRenderbuffers>(
        SDL_GL_GetProcAddress("glGenRenderbuffers"));
    hal.glGenTextures = reinterpret_cast<ProcGLGenTextures>(
        SDL_GL_GetProcAddress("glGenTextures"));
    hal.glGetActiveAttrib = reinterpret_cast<ProcGLGetActiveAttrib>(
        SDL_GL_GetProcAddress("glGetActiveAttrib"));
    hal.glGetActiveUniform = reinterpret_cast<ProcGLGetActiveUniform>(
        SDL_GL_GetProcAddress("glGetActiveUniform"));
    hal.glGetAttribLocation = reinterpret_cast<ProcGLGetAttribLocation>(
        SDL_GL_GetProcAddress("glGetAttribLocation"));
    hal.glGetError =
        reinterpret_cast<ProcGLGetError>(SDL_GL_GetProcAddress("glGetError"));
    hal.glGetIntegerv = reinterpret_cast<ProcGLGetIntegerv>(
        SDL_GL_GetProcAddress("glGetIntegerv"));
    hal.glGetProgramInfoLog = reinterpret_cast<ProcGLGetProgramInfoLog>(
        SDL_GL_GetProcAddress("glGetProgramInfoLog"));
    hal.glGetProgramiv = reinterpret_cast<ProcGLGetProgramiv>(
        SDL_GL_GetProcAddress("glGetProgramiv"));
    hal.glGetShaderInfoLog = reinterpret_cast<ProcGLGetShaderInfoLog>(
        SDL_GL_GetProcAddress("glGetShaderInfoLog"));
    hal.glGetShaderiv = reinterpret_cast<ProcGLGetShaderiv>(
        SDL_GL_GetProcAddress("glGetShaderiv"));
    hal.glGetString =
        reinterpret_cast<ProcGLGetString>(SDL_GL_GetProcAddress("glGetString"));
    hal.glGetUniformLocation = reinterpret_cast<ProcGLGetUniformLocation>(
        SDL_GL_GetProcAddress("glGetUniformLocation"));

    hal.glHint = reinterpret_cast<ProcGLHint>(SDL_GL_GetProcAddress("glHint"));

    hal.glIsEnabled =
        reinterpret_cast<ProcGLIsEnabled>(SDL_GL_GetProcAddress("glIsEnabled"));

    hal.glLinkProgram = reinterpret_cast<ProcGLLinkProgram>(
        SDL_GL_GetProcAddress("glLinkProgram"));

    hal.glPixelStorei = reinterpret_cast<ProcGLPixelStorei>(
        SDL_GL_GetProcAddress("glPixelStorei"));
    hal.glPolygonOffset = reinterpret_cast<ProcGLPolygonOffset>(
        SDL_GL_GetProcAddress("glPolygonOffset"));

    hal.glReadPixels = reinterpret_cast<ProcGLReadPixels>(
        SDL_GL_GetProcAddress("glReadPixels"));
    hal.glRenderbufferStorage = reinterpret_cast<ProcGLRenderbufferStorage>(
        SDL_GL_GetProcAddress("glRenderbufferStorage"));

    hal.glSampleCoverage = reinterpret_cast<ProcGLSampleCoverage>(
        SDL_GL_GetProcAddress("glSampleCoverage"));
    hal.glScissor =
        reinterpret_cast<ProcGLScissor>(SDL_GL_GetProcAddress("glScissor"));
    hal.glShaderSource = reinterpret_cast<ProcGLShaderSource>(
        SDL_GL_GetProcAddress("glShaderSource"));
    hal.glStencilFunc = reinterpret_cast<ProcGLStencilFunc>(
        SDL_GL_GetProcAddress("glStencilFunc"));
    hal.glStencilOp =
        reinterpret_cast<ProcGLStencilOp>(SDL_GL_GetProcAddress("glStencilOp"));

    hal.glTexImage2D = reinterpret_cast<ProcGLTexImage2D>(
        SDL_GL_GetProcAddress("glTexImage2D"));
    hal.glTexParameteri = reinterpret_cast<ProcGLTexParameteri>(
        SDL_GL_GetProcAddress("glTexParameteri"));

    hal.glUniform1fv = reinterpret_cast<ProcGLUniform1fv>(
        SDL_GL_GetProcAddress("glUniform1fv"));
    hal.glUniform2fv = reinterpret_cast<ProcGLUniform2fv>(
        SDL_GL_GetProcAddress("glUniform2fv"));
    hal.glUniform3fv = reinterpret_cast<ProcGLUniform3fv>(
        SDL_GL_GetProcAddress("glUniform3fv"));
    hal.glUniform4fv = reinterpret_cast<ProcGLUniform4fv>(
        SDL_GL_GetProcAddress("glUniform4fv"));
    hal.glUniform1i =
        reinterpret_cast<ProcGLUniform1i>(SDL_GL_GetProcAddress("glUniform1i"));
    hal.glUniform1iv = reinterpret_cast<ProcGLUniform1iv>(
        SDL_GL_GetProcAddress("glUniform1iv"));
    hal.glUniform2iv = reinterpret_cast<ProcGLUniform2iv>(
        SDL_GL_GetProcAddress("glUniform2iv"));
    hal.glUniform3iv = reinterpret_cast<ProcGLUniform3iv>(
        SDL_GL_GetProcAddress("glUniform3iv"));
    hal.glUniform4iv = reinterpret_cast<ProcGLUniform4iv>(
        SDL_GL_GetProcAddress("glUniform4iv"));
    hal.glUniformMatrix2fv = reinterpret_cast<ProcGLUniformMatrix2fv>(
        SDL_GL_GetProcAddress("glUniformMatrix2fv"));
    hal.glUniformMatrix3fv = reinterpret_cast<ProcGLUniformMatrix3fv>(
        SDL_GL_GetProcAddress("glUniformMatrix3fv"));
    hal.glUniformMatrix4fv = reinterpret_cast<ProcGLUniformMatrix4fv>(
        SDL_GL_GetProcAddress("glUniformMatrix4fv"));
    hal.glUseProgram = reinterpret_cast<ProcGLUseProgram>(
        SDL_GL_GetProcAddress("glUseProgram"));

    hal.glVertexAttribPointer = reinterpret_cast<ProcGLVertexAttribPointer>(
        SDL_GL_GetProcAddress("glVertexAttribPointer"));
    hal.glViewport =
        reinterpret_cast<ProcGLViewport>(SDL_GL_GetProcAddress("glViewport"));
    return hal;
}
} // namespace

#if !defined(NDEBUG)
void nopp::gles2::glCheckError(GL const& gl, char const* func)
{
    auto glerr = gl.glGetError();
    if (glerr == GL_NO_ERROR)
        return;
    logError("[GL]%s: \"%#06x\"", func, glerr);
}
#endif

////////////////////////////////////////////////////////////////////////////////

GLThread::GLThread()
{
    _glDatas.reserve(1024);
    _thread = new std::thread(&GLThread::_funcThread, this);
}

GLThread::~GLThread()
{
    _isThreadInterrupted = true;
    _swapCond.notify_one();
    if (_thread->joinable())
        _thread->join();
    else
        logError("GLThread::~GLThread: thread not joinable");
    delete _thread;
}

void GLThread::push(std::vector<GLCmd> const& cmds, GLFence* fence)
{
    std::lock_guard<std::mutex> lock{_swapMutex};
    append(_cmdsWait, cmds);
    if (fence != nullptr)
    {
        fence->event.reset();
        _cmdsWait.emplace_back([=](GL const&) { fence->event.raise(); });
    }
    _swapCond.notify_one();
}

void GLThread::setOnTerminate(std::vector<GLCmd> const& cmds)
{
    _cmdsOnTerminate = cmds;
}

void GLThread::updateData(Id id)
{
    std::lock_guard<std::mutex> lock{_updateMutex};
    auto data = _getData(id);
    if (data != nullptr)
        _tmpUpdateIds.insert(id);
}

void GLThread::destroyData(Id id)
{
    std::lock_guard<std::mutex> lock{_destroyMutex};
    auto data = _getData(id);
    if (data != nullptr)
        _tmpDestroyIds.insert(id);
}

void GLThread::_funcThread()
{
    _glInitialize();
    while (true)
    {
        auto cmdsWork = std::vector<GLCmd>{};
        // swap data from main thread
        {
            std::unique_lock<std::mutex> lock{_swapMutex};
            while ((_cmdsWait.empty() == true) &&
                   (_isThreadInterrupted == false))
                _swapCond.wait(lock);
            if (_isThreadInterrupted == true)
            {
                _glTerminate();
                logDebug("stopping opengl es thread");
                return;
            }
            cmdsWork.reserve(_cmdsWait.size()); // small optim
            cmdsWork.swap(_cmdsWait);
        }

        // then get update/destroy data for this frame
        {
            std::lock_guard<std::mutex> lock{_updateMutex};
            _updateIds.swap(_tmpUpdateIds);
        }
        {
            std::lock_guard<std::mutex> lock{_destroyMutex};
            _destroyIds.swap(_tmpDestroyIds);
        }
        for (auto id : _updateIds)
        {
            auto data = _getData(id);
            if (data == nullptr)
                continue;
            auto srcData = getGPUData(id);
            if (srcData != nullptr)
            {
                data->glUpdate(srcData);
                // logDebug("update %d", id);
            }
            else
            {
                logDebug("null src data");
                auto iter = _destroyIds.find(id);
                if (iter != _destroyIds.cend())
                    logDebug("won't be deleted!");
                else
                    logDebug("but will be deleted...");
            }
        }
        _updateIds.clear();

        // process cmds
        for (auto& cmd : cmdsWork)
            cmd(_gl);
    }
}

void GLThread::_glInitialize()
{
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);

    auto window = SDL_CreateWindow("",
                                   SDL_WINDOWPOS_UNDEFINED,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   16,
                                   16,
                                   SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN);
    _glContext = SDL_GL_CreateContext(window);
    _gl = createGL();
    _gl.wglMakeCurrent = [&](SDL_Window* window) {
        assert(window != nullptr);
        auto curWindow = SDL_GL_GetCurrentWindow();
        auto curContext = SDL_GL_GetCurrentContext();
        if ((curWindow == window) && (curContext == _glContext))
            return;
        auto r = SDL_GL_MakeCurrent(window, _glContext);
    };
    _gl.wglSwapWindow = [&](SDL_Window* window) { SDL_GL_SwapWindow(window); };
    _gl.glData = [&](Id id) {
        auto data = _getData(id);
        if (data != nullptr)
            return data;
        auto& dst = _glDatas[id.index()];
        auto src = getGPUData(id);
        assert(src != nullptr);
        if (src->isTypeOf<MeshData>() == true)
            dst = new GLMesh{_gl, src};
        else if (src->isTypeOf<TextureData>() == true)
            dst = new GLTexture{_gl, src};
        return dst;
    };

    logInfo("[GL]vendor: %s", _gl.glGetString(GL_VENDOR));
    logInfo("[GL]renderer: %s", _gl.glGetString(GL_RENDERER));
    logInfo("[GL]version: %s", _gl.glGetString(GL_VERSION));
    logInfo("[GL]shading language: %s",
            _gl.glGetString(GL_SHADING_LANGUAGE_VERSION));
    // compressed format supported
    {
        auto count = GLint{0};
        _gl.glGetIntegerv(GL_NUM_COMPRESSED_TEXTURE_FORMATS, &count);
        auto texFormats = std::vector<GLint>{};
        texFormats.resize(count);
        _gl.glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, texFormats.data());
    }
    SDL_DestroyWindow(window);
}

void GLThread::_glTerminate()
{
    // call all terminate commands
    for (auto& cmd : _cmdsOnTerminate)
        cmd(_gl);

    // delete all remaining data if any
    for (auto& data : _glDatas)
    {
        if (data != nullptr)
        {
            delete data;
            data = nullptr;
        }
    }
    if (_glContext != nullptr)
    {
        _gl = GL{};
        SDL_GL_DeleteContext(_glContext);
        _glContext = nullptr;
    }
    logInfo("[GL]terminate");

    // if(_hGLRC != nullptr)
    //{
    //    delete _gl;
    //    _gl = nullptr;
    //    delete _wgl;
    //    _wgl = nullptr;
    //    wglDeleteContext(_hGLRC);
    //    _hGLRC = nullptr;
    //}
    // logInfo("shutdown GL");
    ////if(_glOffFramebuffer != 0)
    ////{
    ////	glDeleteFramebuffers(1, &_glOffFramebuffer);
    ////	glDeleteTextures(1, &_glOffColor);
    ////	glDeleteRenderbuffers(1, &_glOffDepth);
    ////}
    //
    //
    ////checkGLError("GLThread::_glShutdown");
    ////_eglShutdown();
}

GLData* GLThread::_getData(Id id)
{
    auto index = id.index();
    if (_glDatas.size() <= index)
        _glDatas.resize(index + 1);
    return _glDatas[index];
}
