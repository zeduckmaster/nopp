#pragma once

#include <chrono>
#include <cstdint>
#include <cstdlib>

#if defined(_WIN32)
#include <windows.h>
#else // _WIN32
#include <sys/time.h>
#endif // _WIN32

namespace nopp
{

namespace impl
{
#if defined(_WIN32)
inline auto ticksPerMicroSec = double(0LL);
#endif // _WIN32
} // namespace impl

class PrecisionTimer final
{
  public:
    PrecisionTimer()
    {
#if defined(_WIN32)
        if (impl::ticksPerMicroSec == 0)
        {
            auto freq = LARGE_INTEGER();
            QueryPerformanceFrequency(&freq);
            impl::ticksPerMicroSec = 1000000.0 / static_cast<double>(freq.QuadPart);
        }
#endif // _WIN32
    }

    void start()
    {
#if defined(_WIN32)
        auto li = LARGE_INTEGER{};
        QueryPerformanceCounter(&li);
        _start = li.QuadPart;
#else
        auto tv = timeval{};
        gettimeofday(&tv, nullptr);
        _start = static_cast<long long>(tv.tv_sec * 1000000 + tv.tv_usec);
#endif
        _isStopped = false;
    }

    void stop()
    {
        _isStopped = true;
#if defined(_WIN32)
        auto li = LARGE_INTEGER();
        QueryPerformanceCounter(&li);
        _stop = li.QuadPart;
#else
        auto tv = timeval();
        gettimeofday(&tv, nullptr);
        _stop = static_cast<long long>(tv.tv_sec * 1000000 + tv.tv_usec);
#endif
    }

    double timeMicroSec()
    {
        auto start = 0.0;
        auto stop = 0.0;
#if defined(_WIN32)
        if (_isStopped == false)
        {
            auto li = LARGE_INTEGER();
            QueryPerformanceCounter(&li);
            _stop = li.QuadPart;
        }
        start = static_cast<double>(_start) * impl::ticksPerMicroSec;
        stop = static_cast<double>(_stop) * impl::ticksPerMicroSec;
#else  // _WIN32
        if (_isStopped == false)
        {
            auto tv = timeval();
            gettimeofday(&tv, nullptr);
            _stop = static_cast<long long>(tv.tv_sec * 1000000 + tv.tv_usec);
        }
        start = static_cast<double>(_start);
        stop = static_cast<double>(_stop);
#endif // _WIN32
        return stop - start;
    }

    double time() { return timeMicroSec() * 0.000001; }

    double timeMilliSec() { return timeMicroSec() * 0.001; }

  private:
    uint64_t _start = 0;
    uint64_t _stop = 0;
    bool _isStopped = true;
};

inline constexpr bool isLeapYear(uint16_t year)
{
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

inline constexpr uint8_t monthDaysCount(uint8_t month, uint16_t year)
{
    if (month == 2)
    {
        return (isLeapYear(year) == true) ? 29 : 28;
    }
    if ((month == 4) || (month == 6) || (month == 9) || (month == 11))
    {
        return 30;
    }
    return 31;
}

using TimeValue = std::chrono::seconds;

struct TimeStamp final
{
    uint16_t year = 1;
    uint16_t day = 1;
    uint16_t hours = 0;
    uint8_t minutes = 0;
    uint8_t seconds = 0;

    inline constexpr bool isLeapYear() const { return nopp::isLeapYear(year); }
};

inline TimeValue systemNow()
{
    auto now = std::chrono::system_clock::now();
    return std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
}

inline TimeStamp toTimeStamp(TimeValue const& value)
{
    // 1y = 365d, 1d = 24h, 1h = 60m, 1m = 60s
    auto ts = TimeStamp();
    auto s = value.count();
    auto r = std::div(s, static_cast<decltype(s)>(31536000));
    ts.year = r.quot + 1970;
    r = std::div(r.rem, static_cast<decltype(s)>(86400));
    ts.day = r.quot + 1;
    r = std::div(r.rem, static_cast<decltype(s)>(3600));
    ts.hours = r.quot;
    r = std::div(r.rem, static_cast<decltype(s)>(60));
    ts.minutes = r.quot;
    ts.seconds = r.rem;
    return ts;
}

inline TimeValue toTimeValue(TimeStamp const& value)
{
    return TimeValue(((value.year - 1) * 31536000) + ((value.day - 1) * 86400) +
                     (value.hours * 3600) + (value.minutes * 60) + value.seconds);
}

class Clock final
{
  public:
    void start()
    {
        _start = systemNow();
        _isStopped = false;
    }

    void stop()
    {
        _isStopped = true;
        _stop = systemNow();
    }

    TimeValue time()
    {
        auto stop = (_isStopped == true) ? _stop : systemNow();
        return stop - _start;
    }

  private:
    TimeValue _start = TimeValue::zero();
    TimeValue _stop = TimeValue::zero();
    bool _isStopped = true;
};

} // namespace nopp
