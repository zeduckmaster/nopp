#pragma once

#include <cstdint>

namespace nopp
{

template <typename T>
struct Size2 final
{
    T width = 0;
    T height = 0;

    constexpr Size2() = default;

    constexpr Size2(T const& width, T const& height) : width(width), height(height) {}

    constexpr bool operator==(Size2 const& other) const
    {
        return (width == other.width) && (height == other.height);
    }

    constexpr bool operator!=(Size2 const& size) const { return !(*this == size); }

    constexpr bool isValid() const { return (width > 0) && (height > 0); }

    constexpr T area() const { return static_cast<T>(width * height); }
};
using Size2i = Size2<int32_t>;
using Size2f = Size2<float>;
using Size2d = Size2<double>;

template <typename T>
struct Size3 final
{
    T width = 0;
    T height = 0;
    T depth = 0;

    constexpr Size3() = default;

    constexpr Size3(T const& width, T const& height)
        : width(width), height(height), depth(static_cast<T>(1))
    {
    }

    constexpr Size3(T const& width, T const& height, T const& depth)
        : width(width), height(height), depth(depth)
    {
    }

    constexpr bool operator==(Size3 const& other) const
    {
        return (width == other.width) && (height == other.height) && (depth == other.depth);
    }

    constexpr bool operator!=(Size3 const& size) const { return !(*this == size); }

    constexpr bool isValid() const { return (width > 0) && (height > 0) && (depth > 0); }

    constexpr T volume() const { return static_cast<T>(width * height * depth); }
};
using Size3i = Size3<int32_t>;
using Size3f = Size3<float>;
using Size3d = Size3<double>;

constexpr Size3d operator/(Size3i const& lhs, Size3i const& rhs)
{
    return Size3d(static_cast<double>(lhs.width) / static_cast<double>(rhs.width),
                  static_cast<double>(lhs.height) / static_cast<double>(rhs.height),
                  static_cast<double>(lhs.depth) / static_cast<double>(rhs.depth));
}

} // namespace nopp
