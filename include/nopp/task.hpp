#pragma once

#include <atomic>
#include <cassert>
#include <deque>
#include <thread>
#include <vector>
#include "event.hpp"
#include "log.hpp"
#include "unused.hpp"

namespace nopp
{

// a task should implement operator() where the work is done
class ITask
{
  public:
    virtual ~ITask() = default;

    virtual void _doAsyncWork() = 0;
};

class ITaskThreadPool
{
  public:
    virtual ~ITaskThreadPool() = default;

    virtual void addTask(ITask* task) = 0;

    virtual int32_t threadCount() const = 0;

    // to check if current calling thread is a task thread.
    // can be useful to prevent task call locks/bugs if there are not enough
    // task threads.
    virtual bool isCurrentThreadTaskThread() const = 0;
};

inline ITaskThreadPool* defaultTaskThreadPool = nullptr;

struct TaskFinisher
{
    void operator()(ITask* t) { unused(t); }
};

struct DynamicTaskFinisher
{
    void operator()(ITask* t) { delete t; }
};

// AsyncTask should remain valid until task is finished
template <typename T, typename Finisher = TaskFinisher>
class AsyncTask : public ITask
{
  public:
    AsyncTask() = default;

    AsyncTask(AsyncTask const& asyncTask) = delete;

    AsyncTask(T const& task) : _task(task) {}

    template <typename T1>
    AsyncTask(T1 arg1) : _task(arg1)
    {
    }

    template <typename T1, typename T2>
    AsyncTask(T1 arg1, T2 arg2) : _task(arg1, arg2)
    {
    }

    template <typename T1, typename T2, typename T3>
    AsyncTask(T1 arg1, T2 arg2, T3 arg3) : _task(arg1, arg2, arg3)
    {
    }

    template <typename T1, typename T2, typename T3, typename T4>
    AsyncTask(T1 arg1, T2 arg2, T3 arg3, T4 arg4) : _task(arg1, arg2, arg3, arg4)
    {
    }

    void start(bool async, ITaskThreadPool* taskThreadPool)
    {
        assert(taskThreadPool != nullptr);
        _assertNotWorking();
        --_finished;
        if (async == true)
        {
            _owner = taskThreadPool;
            _done.reset();
            _owner->addTask(this);
        }
        else
        {
            _task();
            ++_finished;
            Finisher()(this);
        }
    }

    void start(bool async)
    {
        assert(defaultTaskThreadPool != nullptr);
        start(async, defaultTaskThreadPool);
    }

    constexpr bool isFinished() const { return _finished == 1; }

    void waitUntilDone(size_t ms)
    {
        if (_owner == nullptr)
        {
            return;
        }
        _done.waitFor(ms);
    }

    void waitUntilDone()
    {
        if (_owner == nullptr)
        {
            return;
        }
        _done.wait();
    }

    T& task()
    {
        _assertNotWorking();
        return _task;
    }

    T const& task() const
    {
        _assertNotWorking();
        return _task;
    }

  private:
    void _doAsyncWork() override
    {
        _task();
        _owner = nullptr;
        ++_finished;
        _done.raise();
        Finisher()(this);
    }

    void _assertNotWorking() const
    {
        assert(_finished == 1);
        assert(_owner == nullptr);
    }

    T _task;
    ITaskThreadPool* _owner = nullptr;
    Event _done;                       // when task is done async
    std::atomic<int32_t> _finished{1}; // when work is done sync+async
};

// schedule an async task that will automatically delete itself when completed
// - task will run on default task thread pool
// - should be created on heap i.e.: auto newTask = new DynamicAsyncTask<Foo>();
template <typename T>
using DynamicAsyncTask = AsyncTask<T, DynamicTaskFinisher>;

template <typename T, typename... Args>
void startDynamicAsyncTask(Args&&... args)
{
    auto task = new DynamicAsyncTask<T>(args...);
    task->start(true);
}

////////////////////////////////////////////////////////////////////////////////
// simple task thread pool

class SimpleTaskThreadPool final : public ITaskThreadPool
{
  public:
    SimpleTaskThreadPool(int32_t threadCount)
    {
        for (auto n = 0; n < threadCount; ++n)
        {
            _threads.emplace_back(std::make_unique<_SimpleTaskThread>(this));
        }
        _waitingThreads.reserve(threadCount);
        for (auto& thread : _threads)
        {
            _waitingThreads.emplace_back(thread.get());
        }
    }

    void addTask(ITask* task) override
    {
        assert(_threads.size() > 0);
        std::lock_guard<std::mutex> lock(_taskMutex);
        if (_waitingThreads.size() > 0)
        {
            auto thread = _waitingThreads.back();
            _waitingThreads.pop_back();
            thread->doTask(task);
        }
        else
        {
            _tasks.emplace_back(task);
        }
    }

    int32_t threadCount() const override { return static_cast<int32_t>(_threads.size()); }

    bool isCurrentThreadTaskThread() const override
    {
        auto tid = std::this_thread::get_id();
        for (auto& thread : _threads)
        {
            if (tid == thread->id())
            {
                return true;
            }
        }
        return false;
    }

  private:
    class _SimpleTaskThread final
    {
      public:
        _SimpleTaskThread(SimpleTaskThreadPool* owner) : _owner(owner)
        {
            assert(_owner != nullptr);
            _thread = new std::thread(&_SimpleTaskThread::_threadFunc, this);
        }

        ~_SimpleTaskThread()
        {
            _isInterrupted = true;
            _cond.notify_one();
            if (_thread->joinable())
            {
                _thread->join();
            }
            else
            {
                logError("_SimpleTaskThread::~_SimpleTaskThread: thread not joinable");
            }
            delete _thread;
        }

        void doTask(ITask* const task)
        {
            {
                std::lock_guard<std::mutex> lock(_mutex);
                _task = task;
            }
            _cond.notify_one();
        }

        std::thread::id id() const { return _thread->get_id(); }

      private:
        void _threadFunc()
        {
            logDebug("simple task thread started");
            while (true)
            {
                std::unique_lock<std::mutex> lock(_mutex);
                while ((_task == nullptr) && (_isInterrupted == false))
                {
                    _cond.wait(lock);
                }
                if (_isInterrupted == true)
                {
                    logDebug("simple task thread interrupted");
                    return;
                }
                _task->_doAsyncWork();
                _task = _owner->getNextTaskOrWait(this);
            }
        }

        std::thread* _thread = nullptr;
        std::mutex _mutex;
        std::condition_variable _cond;
        std::atomic<bool> _isInterrupted{false};
        ITask* _task = nullptr;
        SimpleTaskThreadPool* _owner = nullptr;
    };

  public:
    ITask* getNextTaskOrWait(_SimpleTaskThread* thread)
    {
        std::lock_guard<std::mutex> lock(_taskMutex);
        if (_tasks.size() > 0)
        {
            auto task = _tasks.front();
            _tasks.pop_front();
            return task;
        }
        _waitingThreads.emplace_back(thread);
        return nullptr;
    }

  private:
    std::vector<std::unique_ptr<_SimpleTaskThread>> _threads;
    std::vector<_SimpleTaskThread*> _waitingThreads;
    std::mutex _taskMutex;
    std::deque<ITask*> _tasks;
};

} // namespace nopp
