Room = { sessionDatas = {}, commands = {} }

function Room:new(o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function Room:createSessionData(session)
   print("Not implemented")
   return nil
end

function Room:sessionData(session)
   return self.sessionDatas[session.id]
end

function Room:enter(session)
   self.sessionDatas[session.id] = self:createSessionData(session)
end

function Room:exit(session)
   self.sessionDatas[session.id] = nil
end

function Room:processCommand(session, args)
   local token = args[1]
   local cmd = CommandAliases[token]
   local f = self.commands[cmd]
   if (f ~= nil) then
      f(self, session, args)
   else
      session:send(Errors.InvalidCommand.."'"..token.."'")
   end
end

--function Room:roll(session, args)
--   local arg2 = args[2]
--   local msg = ""
--   if (arg2 ~= nil) then
--      msg = arg2.." = "..roll(session.id, arg2)
--   else
--      msg = Errors.InvalidArgument
--   end
--   session:send(msg)
--end
--
--function Room:look(session, args)
--   local argCount = #args
--   if (argCount == 1) then
--      session:send(self.description)
--   end
--end

-------------------------------------------------------------------------------
-- default commands

function Room:cHelp(session, args)
   local msg = ""
   local arg2 = args[2]
   if (arg2 ~= nil) then
      local cmd = CommandAliases[arg2]
      local desc = nil
      if (cmd ~= nil) and (self.commands[cmd] ~= nil) then
            desc = CommandDescriptions[cmd]
      end
      if (desc ~= nil) then
         msg = desc
      else
         msg = Errors.InvalidArgument.."'"..arg2.."'"
      end
   else
      for k,v in pairs(self.commands) do
         for kk,vv in pairs(CommandAliases) do
            if (vv == k) then
               msg = msg.."'"..kk.."' "
            end
         end
      end
      msg = msg..""
   end
   session:send(msg)
end

function Room:cQuit(session)
   session:quit()
end

--Room.commands.roll = Room.roll
--Room.commands.look = Room.look
Room.commands.help = Room.cHelp
Room.commands.quit = Room.cQuit