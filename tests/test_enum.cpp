#include <catch2/catch.hpp>
#include "nopp/enum.hpp"

using namespace nopp;

enum class eFoo : uint32_t
{
    Val1 = 0x1,
    Val2 = 0x2,
    Val3 = 0x4000,
    Val4 = 0x8,
};

TEST_CASE("type conversion", "[enum]")
{
    REQUIRE(toType<eFoo>(eFoo::Val3) == 0x4000);
    REQUIRE(toEnum<eFoo>(8) == eFoo::Val4);
}

TEST_CASE("enum flag", "[enum]")
{
    using FooFlag = std::underlying_type<eFoo>::type;
    FooFlag fooFlag = 0;
    setEnumFlag(fooFlag, eFoo::Val4);
    REQUIRE(hasEnumFlag(fooFlag, eFoo::Val4) == true);
    setEnumFlag(fooFlag, eFoo::Val2);
    REQUIRE(hasEnumFlag(fooFlag, eFoo::Val2) == true);
    unsetEnumFlag(fooFlag, eFoo::Val4);
    REQUIRE(hasEnumFlag(fooFlag, eFoo::Val4) == false);
}
