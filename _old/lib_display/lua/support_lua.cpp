#include "support_lua.hpp"
#include "../camera.hpp"

using namespace nopp;

namespace
{

}

namespace sol
{
template <> struct is_automagical<Mat4> : std::false_type
{
};
template <> struct is_container<Mat4> : std::false_type
{
};
} // namespace sol

namespace nopp
{
void loadLuaDisplayMesh(sol::state& luaState);
void loadLuaDisplayTexture(sol::state& luaState);
} // namespace nopp

void nopp::loadLuaDisplay(sol::state& luaState)
{
    // camera editor
    {
        auto t = luaState.new_usertype<CameraEditor>(
            "CameraEditor",
            sol::call_constructor,
            sol::constructors<CameraEditor()>());
        t["fitView"] = &CameraEditor::fitView;
        t["setProjType"] = &CameraEditor::setProjType;
        t["setPerspFOVV"] = &CameraEditor::setPerspFOVV;
        t["setOrtho"] = &CameraEditor::setOrtho;
        t["setAspectRatio"] = &CameraEditor::setAspectRatio;
        t["setViewport"] = &CameraEditor::setViewport;
        t["lookAt"] = &CameraEditor::lookAt;
        t["zoom"] = &CameraEditor::zoom;
        t["pan"] = &CameraEditor::pan;
        t["rotate"] = &CameraEditor::rotate;
        t["pos"] = &CameraEditor::pos;
        t["at"] = &CameraEditor::at;
        t["up"] = &CameraEditor::up;
        t["lateral"] = &CameraEditor::lateral;
        t["longitudinal"] = &CameraEditor::longitudinal;
        t["projType"] = &CameraEditor::projType;
        t["transform"] = &CameraEditor::transform;
        t["frustum"] = &CameraEditor::frustum;
        t["matView"] = &CameraEditor::matView;
        t["matProj"] = &CameraEditor::matProj;
        t["viewport"] = &CameraEditor::viewport;
    }

    loadLuaDisplayMesh(luaState);
    loadLuaDisplayTexture(luaState);
}

// void nopp::loadLuaDisplayAllLibs(sol::state& luaState)
//{
//    loadLuaMathLib(luaState);
//}
