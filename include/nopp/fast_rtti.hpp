#pragma once

#include "hash_string.hpp"

// To use fast rtti it is recommended to use full scope with namespaces as
// "_classname" or
// "_typename" to avoid name collision.
// ex.: FAST_RTTI_REGISTER_TYPE(std::string);
// ex.: FAST_RTTI_CLASS(nopp::Mesh, nopp::GPUResource);

namespace nopp
{

using TypeId = HashValue;
constexpr auto TypeIdInvalid = TypeId(HashInvalid);

template <typename T>
struct TypeTraits
{
    constexpr static TypeId id() { return T::_Type::id(); }
    constexpr static char const* name() { return T::_Type::name(); }
};

} // namespace nopp

#define FAST_RTTI_REGISTER_TYPE(_typename)                                 \
    namespace nopp                                                         \
    {                                                                      \
    template <>                                                            \
    struct TypeTraits<_typename>                                           \
    {                                                                      \
        constexpr static TypeId id() { return replaceByHash(#_typename); } \
        constexpr static char const* name() { return #_typename; }         \
    };                                                                     \
    }

#define FAST_RTTI_TRAITS(_classname)                                                        \
    friend ::nopp::TypeTraits<_classname>;                                                  \
                                                                                            \
  private:                                                                                  \
    struct _Type                                                                            \
    {                                                                                       \
        constexpr static ::nopp::TypeId id() { return ::nopp::replaceByHash(#_classname); } \
        constexpr static char const* name() { return #_classname; }                         \
    };

#define FAST_RTTI_CLASS_ROOT(_classname)                                                    \
    FAST_RTTI_TRAITS(_classname)                                                            \
                                                                                            \
  public:                                                                                   \
    virtual ::nopp::TypeId typeId() const { return ::nopp::TypeTraits<_classname>::id(); }  \
    virtual char const* typeName() const { return ::nopp::TypeTraits<_classname>::name(); } \
    virtual bool isTypeOf(::nopp::TypeId const tid) const                                   \
    {                                                                                       \
        return ::nopp::TypeTraits<_classname>::id() == tid;                                 \
    }                                                                                       \
    template <typename T>                                                                   \
    bool isTypeOf() const                                                                   \
    {                                                                                       \
        return isTypeOf(::nopp::TypeTraits<T>::id());                                       \
    }

#define FAST_RTTI_CLASS(_classname, _baseclassname)                                           \
    FAST_RTTI_TRAITS(_classname)                                                              \
                                                                                              \
  public:                                                                                     \
    ::nopp::TypeId typeId() const override { return ::nopp::TypeTraits<_classname>::id(); }   \
    char const* typeName() const override { return ::nopp::TypeTraits<_classname>::name(); }  \
    bool isTypeOf(::nopp::TypeId const tid) const override                                    \
    {                                                                                         \
        return (::nopp::TypeTraits<_classname>::id() == tid) ? true                           \
                                                             : _baseclassname::isTypeOf(tid); \
    }                                                                                         \
    template <typename T>                                                                     \
    bool isTypeOf() const                                                                     \
    {                                                                                         \
        return isTypeOf(::nopp::TypeTraits<T>::id());                                         \
    }

// register some basic types
FAST_RTTI_REGISTER_TYPE(bool)
FAST_RTTI_REGISTER_TYPE(int8_t)
FAST_RTTI_REGISTER_TYPE(uint8_t)
FAST_RTTI_REGISTER_TYPE(int16_t)
FAST_RTTI_REGISTER_TYPE(uint16_t)
FAST_RTTI_REGISTER_TYPE(int32_t)
FAST_RTTI_REGISTER_TYPE(uint32_t)
FAST_RTTI_REGISTER_TYPE(int64_t)
FAST_RTTI_REGISTER_TYPE(uint64_t)
FAST_RTTI_REGISTER_TYPE(float)
FAST_RTTI_REGISTER_TYPE(double)
FAST_RTTI_REGISTER_TYPE(std::string)
