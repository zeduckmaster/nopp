#include "texture.hpp"
#include <lib_core/log.hpp>

using namespace nopp;

namespace
{
}

int32_t nopp::imageByteSize(Size3i const& size, ePixelFormat format)
{
    switch (format)
    {
    case ePixelFormat::RGBx8_UNorm:
    case ePixelFormat::RGBA8_UNorm:
        return size.width * size.height * size.depth * 4;
    case ePixelFormat::R8_UNorm: return size.width * size.height * size.depth;
    case ePixelFormat::RGBA16_Float:
        return size.width * size.height * size.depth * 8;
    case ePixelFormat::RGBA32_Float:
        return size.width * size.height * size.depth * 16;

    case ePixelFormat::RGB_ETC1:
        // case ePixelFormat::RGB_S3TC_DXT1:
        {
            auto f = [](int32_t dim) {
                return std::max<int32_t>(1, ((dim + 3) / 4));
            };
            return f(size.width) * f(size.height) * 8;
        }
    default: return -1;
    }
}

int32_t nopp::pixelByteSize(ePixelFormat format)
{
    switch (format)
    {
    case ePixelFormat::RGBx8_UNorm:
    case ePixelFormat::RGBA8_UNorm: return 4;
    case ePixelFormat::R8_UNorm: return 1;
    case ePixelFormat::RGBA16_Float: return 8;
    case ePixelFormat::RGBA32_Float: return 16;

    case ePixelFormat::RGB_ETC1: return 8;
    default: return -1;
    }
}

bool nopp::isPixelFormatCompressed(ePixelFormat format)
{
    return format == ePixelFormat::RGB_ETC1;
}

////////////////////////////////////////////////////////////////////////////////

namespace
{
struct BestPixelFormat
{
    ePixelFormat dstFormat = ePixelFormat::Unknown;
    uint32_t dstSDLFormat = 0;
};
BestPixelFormat findBestPixelFormat(uint32_t sdlFormat)
{
    switch (sdlFormat)
    {
    case SDL_PIXELFORMAT_RGBA32:
        return BestPixelFormat{ePixelFormat::RGBA8_UNorm,
                               SDL_PIXELFORMAT_RGBA32};
    case SDL_PIXELFORMAT_RGB24:
        return BestPixelFormat{ePixelFormat::RGBx8_UNorm,
                               SDL_PIXELFORMAT_RGBA32};
    default: return BestPixelFormat{};
    }
}

uint32_t toSDLPixelFormat(ePixelFormat format)
{
    switch (format)
    {
    case ePixelFormat::RGBA8_UNorm:
    case ePixelFormat::RGBx8_UNorm: return SDL_PIXELFORMAT_RGBA32; break;
    default: return SDL_PIXELFORMAT_UNKNOWN;
    }
}

int32_t getMipLevelCount(int32_t maxDimension)
{
    auto r = 1;
    while (maxDimension >>= 1)
        ++r;
    return r;
}

} // namespace

////////////////////////////////////////////////////////////////////////////////

TextureDesc::TextureDesc(int32_t width, int32_t height, ePixelFormat format)
    : size{width, height, 1}, format{format}
{
}

bool TextureDesc::isValid() const
{
    return (size.isZero() == false) && (format != ePixelFormat::Unknown) &&
           (arraySize > 0);
}

////////////////////////////////////////////////////////////////////////////////

TextureData::TextureData(TextureDesc const& textureDesc)
{
    create(textureDesc);
}

TextureData::TextureData(SDL_Surface* surface) { create(surface); }

TextureData::TextureData(Path const& filepath) { create(filepath); }

TextureData::TextureData(TextureData const& textureData) { _copy(textureData); }

void TextureData::create(TextureDesc const& textureDesc)
{
    desc = textureDesc;
    assert(desc.isValid() == true);

    // get correct value for mip level count
    auto mipLevelCount = desc.mipLevelCount;
    if (mipLevelCount == -1)
        mipLevelCount = 1;
    assert(mipLevelCount > 0);
    if (mipLevelCount != 1)
    {
        auto maxdim = std::max(std::max(desc.size.width, desc.size.height),
                               desc.size.depth);
        assert(mipLevelCount == getMipLevelCount(maxdim));
    }
    levels.resize(mipLevelCount);
    auto imgSize = desc.size;
    for (auto& level : levels)
    {
        level.images.resize(desc.arraySize);
        level.size = imgSize;
        auto imgByteSize = imageByteSize(level.size, desc.format);
        for (auto& image : level.images)
            image.resize(imgByteSize);
        imgSize.width = std::max<int32_t>(1, imgSize.width >> 1);
        imgSize.height = std::max<int32_t>(1, imgSize.height >> 1);
        imgSize.depth = std::max<int32_t>(1, imgSize.depth >> 1);
    }
}

void TextureData::create(SDL_Surface* surface)
{
    assert(surface != nullptr);
    if (SDL_LockSurface(surface) != 0)
        return;
    auto srcSDLFormat = surface->format->format;
    auto bpf = findBestPixelFormat(srcSDLFormat);
    auto newDesc = TextureDesc{};
    newDesc.size = Size3i{surface->w, surface->h, 1};
    newDesc.format = bpf.dstFormat;
    create(newDesc);
    auto& img0 = data(0, 0);
    auto dstPitch = desc.size.width * pixelByteSize(desc.format);
    auto r = SDL_ConvertPixels(surface->w,
                               surface->h,
                               srcSDLFormat,
                               surface->pixels,
                               surface->pitch,
                               bpf.dstSDLFormat,
                               img0.data(),
                               dstPitch);
    if (r != 0)
        logError("TextureData::create: %s", SDL_GetError());
    SDL_UnlockSurface(surface);
}

void TextureData::create(Path const& filepath)
{
    auto ext = filepath.extension();
    if (ext.string() == "ktx2")
        _loadKTX2(filepath);
    else if (ext.string() == "dds")
        _loadDDS(filepath);
    else
    {
        auto img = IMG_Load(filepath.c_str());
        if (img == nullptr)
        {
            logError("TextureData::create: %s", SDL_GetError());
            return;
        }
        create(img);
        SDL_FreeSurface(img);
    }
}

void TextureData::setData(int32_t level, int32_t pos, SDL_Surface* surface)
{
    assert(surface != nullptr);
    if (SDL_LockSurface(surface) != 0)
        return;
    auto srcSDLFormat = surface->format->format;
    auto bpf = findBestPixelFormat(srcSDLFormat);
    if (desc.format != bpf.dstFormat)
    {
        logError("TextureData::setData: wrong format");
        return;
    }
    auto levelSize = levels[level].size;
    if (levelSize != Size3i{surface->w, surface->h, 1})
    {
        logError("TextureData::setData: wrong size");
        return;
    }
    auto& img = data(level, pos);
    auto dstPitch = levelSize.width * pixelByteSize(desc.format);
    auto r = SDL_ConvertPixels(surface->w,
                               surface->h,
                               srcSDLFormat,
                               surface->pixels,
                               surface->pitch,
                               bpf.dstSDLFormat,
                               img.data(),
                               dstPitch);
    if (r != 0)
        logError("TextureData::create: %s", SDL_GetError());
    SDL_UnlockSurface(surface);
}

void TextureData::setData(int32_t level, int32_t pos, Path const& filepath)
{
    auto img = IMG_Load(filepath.c_str());
    if (img == nullptr)
        return;
    setData(level, pos, img);
    SDL_FreeSurface(img);
}

namespace
{
SDL_Surface* createSurface(ImageData const& imgData,
                           Size3i const& imgSize,
                           TextureDesc const& desc)
{
    auto sdlFormat = toSDLPixelFormat(desc.format);
    if (sdlFormat == SDL_PIXELFORMAT_UNKNOWN)
        return nullptr;
    auto pixSize = pixelByteSize(desc.format);
    auto depth = pixSize * 8;
    auto pitch = pixSize * imgSize.width;
    return SDL_CreateRGBSurfaceWithFormatFrom(
        const_cast<uint8_t*>(imgData.data()),
        imgSize.width,
        imgSize.height,
        depth,
        pitch,
        sdlFormat);
}
} // namespace

eIOResult
TextureData::saveData(int32_t level, int32_t pos, Path const& filepath) const
{
    auto ext = filepath.extension();
    if (ext.string() == "ktx2")
        return _saveKTX2(level, pos, filepath);
    else if (ext.string() == "dds")
        return _saveDDS(level, pos, filepath);
    else
    {
        // then use SDL
        if (level < 0)
            level = 0;
        if (pos < 0)
            pos = 0;
        auto surface =
            createSurface(data(level, pos), levels[level].size, desc);
        if (surface == nullptr)
        {
            logError("TextureData::saveData: couldn't create surface");
            return eIOResult::InvalidFileFormat;
        }

        if (ext.string() == "png")
        {
            IMG_SavePNG(surface, filepath.c_str());
        }
        else if (ext.string() == "jpg")
        {
            IMG_SaveJPG(surface, filepath.c_str(), 90);
        }
    }

    return eIOResult::OK;
}

void TextureData::_copy(TextureData const& textureData)
{
    desc = textureData.desc;
    levels = textureData.levels;
}
