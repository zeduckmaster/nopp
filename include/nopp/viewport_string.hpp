#pragma once

#include "math_string.hpp"
#include "viewport.hpp"

namespace nopp
{

inline std::string toString(Viewport const& value)
{
    auto str = std::string();
    str.append(toString(value.x));
    str.push_back(' ');
    str.append(toString(value.y));
    str.push_back(' ');
    str.append(toString(value.width));
    str.push_back(' ');
    str.append(toString(value.height));
    str.push_back(' ');
    str.append(toString(value.minDepth));
    str.push_back(' ');
    str.append(toString(value.maxDepth));
    return str;
}

inline Viewport toViewport(std::string const& value)
{
    auto const vals = splitString(value, impl::VectorDelims);
    assert(vals.size() == 6);
    auto v = Viewport();
    v.x = toFloat(vals[0]);
    v.y = toFloat(vals[1]);
    v.width = toFloat(vals[2]);
    v.height = toFloat(vals[3]);
    v.minDepth = toFloat(vals[4]);
    v.maxDepth = toFloat(vals[5]);
    return v;
}

} // namespace nopp
